﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MixedPlace.API.Data;
using MixedPlace.API.Models.Common;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using MixedPlace.MessageSender.Implementations;
using MixedPlace.MessageSender.Abstractions;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.ResponseModels;
using MixedPlace.API.Models.User.RequestModels;
using AutoMapper;
using System.Security.Cryptography.X509Certificates;
using MixedPlace.API.Providers;
using MixedPlace.API.Models.MPModel;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Models.Package;
using Microsoft.EntityFrameworkCore;

namespace MixedPlace.API.Services
{
    public class AuthService : IAuthService
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly ApplicationDbContext _dbContext;
        private readonly IMessageSender _messageSender;
        private readonly ILogger<AuthService> _logger;
        private readonly IMapper _autoMapper;
        private readonly ITransactionService _transactionService;

        public AuthService(
            ApplicationDbContext dbContext,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IConfiguration configuration,
            IPasswordHasher<User> passwordHasher,
            IMapper autoMapper,
            ILogger<AuthService> logger,
            ITransactionService transactionService
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _passwordHasher = passwordHasher;
            _dbContext = dbContext;
            _autoMapper = autoMapper;
            _messageSender = MessageSenderFactory.CreateMessageSender(_configuration["Messages:Provider"]);
            _logger = logger;
            _transactionService = transactionService;
        }


        #region Public Methods

        public async Task<IResponseModel<AuthResponse>> Login(LoginRequest loginRequest)
        {
            var response = new ResponseModel<AuthResponse>();
            
            try
            {
                var user = await _userManager.FindByNameAsync(loginRequest.Username);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                    return response;
                }


                if (IsAdminUser(user,loginRequest.Username, loginRequest.Password))
                {
                    var lastLoginDate= DateTime.Now;
                    user.LastLoginDate = lastLoginDate;
                    var jwtToken = GenerateAdminJwtToken();
                    var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
                    DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    var expirationDate = DateTime.UtcNow.AddDays(Convert.ToDouble(_configuration["Auth:TemporaryPassword:ExpirationDays"]));
                    var expirUnix = (long)(expirationDate - sTime).TotalSeconds;
                    response.Data = new AuthResponse { RefreshToken = refreshToken, Token = jwtToken, ExpirationDate = expirUnix,LastLoginDate=lastLoginDate};
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    _dbContext.SaveChanges();
                    return response;
                }


                var verifyResult = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, loginRequest.Password);
                switch (verifyResult)
                {
                    case PasswordVerificationResult.Failed:
                        {
                            response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                            return response;

                        }
                    case PasswordVerificationResult.SuccessRehashNeeded:
                    case PasswordVerificationResult.Success:
                        {
                            if (user.IsTemporaryPassword)
                            {
                                if (user.PasswordExpirationDate > DateTime.UtcNow)
                                {
                                    response.StatusCode = (int)InnerErrorCode.ChangePassowrdRequired;
                                    return response;
                                }
                                else
                                {
                                    if (GenerateUserTemporaryPassword(user) == false)
                                    {
                                        response.StatusCode = (int)InnerErrorCode.UnknownError;
                                    }
                                    else
                                    {
                                        var result = await _userManager.UpdateAsync(user);
                                        if (result.Succeeded)
                                            response.StatusCode = (int)InnerErrorCode.PasswordExipired;
                                        else
                                            response.StatusCode = (int)InnerErrorCode.UnknownError;
                                    }

                                    return response;
                                }
                            }
                            else
                            {
                                var userTokens = _dbContext.UserTokens.Where(x => x.UserId == user.Id);
                                if (userTokens != null && userTokens.Count() > 0)
                                {
                                    _dbContext.RemoveRange(userTokens);
                                    _dbContext.SaveChanges();
                                }


                                var token = GenerateJwtToken(user.Id);
                                var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
                                await _signInManager.SignInAsync(user, false);
                                await _userManager.SetAuthenticationTokenAsync(user, _configuration["Auth:Jwt:Provider"], refreshToken, token);
                                response.StatusCode = (int)InnerErrorCode.Ok;
                                response.Data = new AuthResponse { Token = token, RefreshToken = refreshToken };
                                return response;
                            }

                        }
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
            }
            return response;
        }

        /// <summary>
        /// Method responsible for validate user access token aginst passed provider.
        /// In case if user authenticated user data existence checked in db.
        /// If user not exist new one created.
        /// JWT token and refresh token are passed to client.
        /// </summary>
        /// <param name="externalLoginRequest"></param>
        /// <returns></returns>
        public async Task<IResponseModel<AuthResponse>> ExternalLogin(ExternalLoginRequest externalLoginRequest)
        {

            var response = new ResponseModel<AuthResponse>();
            try
            {
                var loginProvider = LoginProviderFactory.CreateLoginProvider(externalLoginRequest.ProviderName, _configuration);
                if (loginProvider == null)
                {
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                    return response;
                }

                var authResult = await loginProvider.AuthenticateToken(externalLoginRequest.AccessToken);
                if (authResult)
                {
                    var userProviderInfo = await loginProvider.GetUserDetials(externalLoginRequest.AccessToken);
                    User existingUser = new User();
                    if (userProviderInfo != null)
                    {
                        var userLogin = await _dbContext.UserLogins.FirstOrDefaultAsync(x => x.LoginProvider == externalLoginRequest.ProviderName &&
                                                                                             x.ProviderKey == userProviderInfo.Id);

                        if (userLogin == null )
                        {
                            existingUser = string.IsNullOrWhiteSpace(userProviderInfo.Email) ? null : await _userManager.FindByNameAsync(userProviderInfo.Email);
                            if (existingUser == null)
                            {
                                existingUser = new User
                                {
                                    Email = userProviderInfo.Email,
                                    UserStatusId = (int)UserStatus.Registered,
                                    UserName = userProviderInfo.Email,
                                    Image = userProviderInfo.ImageUrl,
                                    Gender = userProviderInfo.Gender,
                                    FirstName = userProviderInfo.FirstName,
                                    LastName = userProviderInfo.LastName,
                                    SecurityStamp = Guid.NewGuid().ToString(),
                                };
                                AddDefaultPackageAndModels(existingUser);
                                await _dbContext.Users.AddAsync(existingUser);
                            }

                            userLogin = new IdentityUserLogin<string>
                            {
                                LoginProvider = externalLoginRequest.ProviderName,
                                ProviderKey = userProviderInfo.Id,
                                ProviderDisplayName = userProviderInfo.FullName,
                                UserId = existingUser.Id
                            };
                            await _dbContext.UserLogins.AddAsync(userLogin);
                        }
                        else
                        {
                            if (string.IsNullOrWhiteSpace(userLogin.UserId))
                            {
                                existingUser = new User
                                {
                                    Email = userProviderInfo.Email,
                                    UserStatusId = (int)UserStatus.Registered,
                                    UserName = userProviderInfo.Email,
                                    Image = userProviderInfo.ImageUrl,
                                    Gender = userProviderInfo.Gender,
                                    FirstName = userProviderInfo.FirstName,
                                    LastName = userProviderInfo.LastName,
                                    SecurityStamp = Guid.NewGuid().ToString(),
                                };
                                AddDefaultPackageAndModels(existingUser);
                                await _dbContext.Users.AddAsync(existingUser);
                                userLogin.UserId = existingUser.Id;
                                _dbContext.Update(userLogin);
                            }
                        }

                       
                        var userTokens = _dbContext.UserTokens.Where(x => x.UserId == userLogin.UserId);
                        if (userTokens != null && userTokens.Count() > 0)
                        {
                            _dbContext.RemoveRange(userTokens);
                        }
                        if (existingUser != null)
                        {
                            if (existingUser.UserStatusId == (int)UserStatus.Suspended)
                            {
                                response.StatusCode = (int)InnerErrorCode.UserAccountSuspended;
                                return response;
                            }

                            if (existingUser.UserStatusId == (int)UserStatus.Disabled)
                            {
                                response.StatusCode = (int)InnerErrorCode.UserAccountDisabled;
                                return response;
                            }
                        }
                       
                        var token = GenerateJwtToken(userLogin.UserId);
                        var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
                        await _dbContext.UserTokens.AddAsync(new IdentityUserToken<string>
                        {
                            LoginProvider = _configuration["Auth:Jwt:Provider"],
                            Name = refreshToken,
                            UserId = userLogin.UserId,
                            Value = token
                        });

                        _dbContext.SaveChanges();

                        response.StatusCode = (int)InnerErrorCode.Ok;
                        response.Data = new AuthResponse { Token = token, RefreshToken = refreshToken };
                        return response;
                    }
                   response.StatusCode = (int)InnerErrorCode.UnknownError;
                   return response;
                }
                response.StatusCode = (int)InnerErrorCode.UnauthorizedRequest;
                return response;

            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        private async Task<User> CreateUser(DataTransferObjects.ProviderUserInfoDto userProviderInfo)
        {
            User existingUser = new User
            {
                Email = userProviderInfo.Email,
                UserName = userProviderInfo.Email,
                Image = userProviderInfo.ImageUrl,
                Gender = userProviderInfo.Gender,
                FirstName = userProviderInfo.FirstName,
                LastName = userProviderInfo.LastName,
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            await _userManager.CreateAsync(existingUser);
            return existingUser;
        }

        private void AddDefaultPackageAndModels(User existingUser)
        {
            var defaultPackage = _dbContext.Packages.Include(x => x.PackageItems).Where(x => x.PackageCategoryId == (int)PackageCategoryType.Default && x.PackageStatusId == (int)PackageStatus.Active)
                .ToList();

            if (defaultPackage != null && defaultPackage.Count() > 0)
            {
                foreach (PackageItemModel item in defaultPackage[0].PackageItems)
                {
                    if (item.PackageItemTypeId == (int)PackageItemType.Model)
                    {
                        _dbContext.MPModelsPerUsers.Add(
                         new MPModelPerUser()
                         {
                             CreationDate = DateTime.Today,
                             MPModel = item.Model,
                             MPModelId = (int)item.ModelId,
                             TotalModelsCount = (int)item.NumOfUnits,
                             RemainingModelsCount = (int)item.NumOfUnits,
                             User = existingUser,
                             UserId = existingUser.Id
                         });
                    }
                    else
                    {
                      //  existingUser.CoinBalance = Convert.ToDecimal(item.NumOfUnits);
                        _transactionService.AddTransaction(TransactionCategoryEnum.DefaultPackageReward, Convert.ToDecimal(item.NumOfUnits), 0, "", existingUser.Id);
                    }

                }
                _dbContext.PurchasedPackages.Add(new PackagePurchaseModel()
                {
                    AmountPaid = 0,
                    CreatedDate = DateTime.Today,
                    PackageId = defaultPackage[0].Id,
                    PaymentDate = DateTime.Today,
                    PurchaseStatusId = (int)PurchaseStatus.PaidUp,
                    UserId = existingUser.Id,
                    InvoiceUrl = defaultPackage[0].ImageUrl
                });
                _dbContext.SaveChanges();
            }
        }

        public async Task<IResponseModel<EmptyBodyResponse>> Logout(string userId)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                    return response;
                }

                var userTokens = _dbContext.UserTokens.Where(x => x.UserId == userId);
                if (userTokens != null && userTokens.Count() > 0)
                {
                    _dbContext.RemoveRange(userTokens);
                    _dbContext.SaveChanges();
                }
                await _signInManager.SignOutAsync();
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        /// <summary>
        /// Method recieve refresh token and old jwt token, in
        /// case if refresh token valid new jwt token generated and
        /// saved to db
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IResponseModel<AuthResponse>> RefreshToken(RefeshTokenRequest request)
        {
            var response = new ResponseModel<AuthResponse>();
            try
            {
                var jwtToken = new JwtSecurityToken(request.Token);
                if (string.IsNullOrWhiteSpace(jwtToken.Subject))
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }
                var user = _dbContext.Users.FirstOrDefault(x=> x.Id == jwtToken.Subject);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var token = await _dbContext.UserTokens.FirstOrDefaultAsync(x=> x.Value == request.Token && 
                                                                    x.LoginProvider == _configuration["Auth:Jwt:Provider"] &&
                                                                    x.Name == request.RefreshToken );

                if (token != null)
                {
                    _dbContext.UserTokens.Remove(token);
                    

                    var tokenStr = GenerateJwtToken(jwtToken.Subject);
                    var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
                    await _dbContext.UserTokens.AddAsync(new IdentityUserToken<string>
                    {
                        LoginProvider = _configuration["Auth:Jwt:Provider"],
                        Name = refreshToken,
                        UserId = jwtToken.Subject,
                        Value = tokenStr
                    });

                    await _dbContext.SaveChangesAsync();
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    response.Data = new AuthResponse { Token = tokenStr, RefreshToken = refreshToken };
                    return response;
                }
                response.StatusCode = (int)InnerErrorCode.UnauthorizedRequest;
                return response;
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public async Task<IResponseModel<EmptyBodyResponse>> ChangePassword(ChangePasswordRequest changePasswordRequest)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {

                var user = await _userManager.FindByEmailAsync(changePasswordRequest.Email);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                    return response;
                }

                var verifyResult = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, changePasswordRequest.OldPassword);
                switch (verifyResult)
                {
                    case PasswordVerificationResult.Failed:
                        response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                        return response;
                    case PasswordVerificationResult.SuccessRehashNeeded:
                    case PasswordVerificationResult.Success:
                        {
                            var hashedPassword = _passwordHasher.HashPassword(user, changePasswordRequest.NewPassword);
                            user.PasswordHash = hashedPassword;
                            user.IsTemporaryPassword = false;
                            var updateResult = await _userManager.UpdateAsync(user);
                            if (updateResult.Succeeded)
                                response.StatusCode = (int)InnerErrorCode.Ok;
                            else
                                response.StatusCode = (int)InnerErrorCode.UnknownError;
                            break;
                        }
                    default:
                        response.StatusCode = (int)InnerErrorCode.UnknownError;
                        break;
                }
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
            }

            return response;
        }

        public async Task<IResponseModel<EmptyBodyResponse>> ForgotPassword(ForgotPasswordRequest forgotPasswordRequest)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                if (forgotPasswordRequest == null || string.IsNullOrWhiteSpace(forgotPasswordRequest.Email))
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var user = await _userManager.FindByEmailAsync(forgotPasswordRequest.Email);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                    return response;
                }

                if (GenerateUserTemporaryPassword(user))
                {
                    var updateResult = await _userManager.UpdateAsync(user);
                    if (updateResult.Succeeded)
                        response.StatusCode = (int)InnerErrorCode.Ok;
                    else
                        response.StatusCode = (int)InnerErrorCode.UnknownError;
                }
                else
                {
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                }

            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
            }
            return response;
        }

        public async Task<IResponseModel<AuthResponse>> ObtainToken(string provider, string externalToken)
        {
            var response = new ResponseModel<AuthResponse>();
            try
            {
                if (string.IsNullOrWhiteSpace(provider) || string.IsNullOrWhiteSpace(externalToken))
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var verifiedAccessToken = await VerifyExternalToken(provider.ToLower(), externalToken);
                if (verifiedAccessToken == null)
                {
                    response.StatusCode = (int)InnerErrorCode.UnauthorizedRequest;
                    return response;
                }

                var user = await _userManager.FindByLoginAsync(provider, verifiedAccessToken.user_id);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var userTokens = _dbContext.UserTokens.Where(x => x.UserId == user.Id);
                if (userTokens != null && userTokens.Count() > 0)
                {
                    _dbContext.RemoveRange(userTokens);
                    _dbContext.SaveChanges();
                }


                var token = GenerateJwtToken(user.Id);
                var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
                await _userManager.SetAuthenticationTokenAsync(user, _configuration["Auth:Jwt:Provider"], refreshToken, token);
                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = new AuthResponse { Token = token, RefreshToken = refreshToken };
                return response;
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public async Task<IResponseModel<EmptyBodyResponse>> Register(RegisterRequest registerRequest)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                if (registerRequest == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var existingUser = await _userManager.FindByNameAsync(registerRequest.Username);
                if (existingUser != null)
                {
                    response.StatusCode = (int)InnerErrorCode.UsernameInUse;
                    return response;
                }

                var user = _autoMapper.Map<User>(registerRequest);

                if (GenerateUserTemporaryPassword(user, hashPassword: false))
                {
                    var result = await _userManager.CreateAsync(user, user.PasswordHash);

                    if (result.Succeeded)
                    {
                        var newuser = await _userManager.FindByNameAsync(user.UserName);
                        AddDefaultPackageAndModels(newuser);
                        _dbContext.SaveChanges();
                        response.StatusCode = (int)InnerErrorCode.Ok;
                    }
                       
                    else
                        response.StatusCode = (int)InnerErrorCode.UnknownError;
                }
                else
                    response.StatusCode = (int)InnerErrorCode.UnknownError;

            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
            }

            return response;
        }

        public async Task<IResponseModel<AuthResponse>> ExternalCallback(HttpContext httpContext)
        {
            var response = new ResponseModel<AuthResponse>();
            try
            {
                var authResult = await httpContext.AuthenticateAsync("Temporary");
                if (authResult.Succeeded)
                {
                    var userEmail = authResult.Principal.FindFirst(ClaimTypes.Email)?.Value;
                    var provider = authResult.Principal.Identity.AuthenticationType;
                    var nameIdentifier = authResult.Principal.FindFirstValue(ClaimTypes.NameIdentifier);
                    var name = authResult.Principal.FindFirstValue(ClaimTypes.Name);
                    var userInfo = new UserLoginInfo(provider, nameIdentifier, name);

                    var existingUser = await _userManager.FindByNameAsync(userEmail);
                    if (existingUser == null)
                    {
                        existingUser = new User
                        {
                            Email = userEmail,
                            UserName = userEmail,
                            SecurityStamp = Guid.NewGuid().ToString(),
                        };
                        await _userManager.CreateAsync(existingUser);
                    }


                    var loginUser = await _userManager.FindByLoginAsync(provider, nameIdentifier);
                    if (loginUser == null)
                        await _userManager.AddLoginAsync(existingUser, userInfo);

                    await httpContext.SignInAsync(authResult.Principal);
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    response.Data = new AuthResponse { Token = GenerateJwtToken(existingUser.Id) };
                    return response;
                }
                else
                {
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                    return response;
                }
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }

        }


        #endregion Public Methods

        #region Private Methods

        private bool IsAdminUser(User user,string username, string password)
        {
            var confUsername = _configuration["Auth:App:AdminUsername"];
            var confPass = _configuration["Auth:App:AdminPassword"];

            if (string.IsNullOrWhiteSpace(confUsername) || string.IsNullOrWhiteSpace(confPass))
                return false;


            if (confUsername.Equals(username) == false || confPass.Equals(password) == false)
                return false; 

           var verifyResult =  _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, password);
           if (verifyResult == PasswordVerificationResult.Success) return true;
            return false;
        }

        private string GenerateAdminJwtToken()
        {
            var claims = new List<Claim>
            {
                new Claim("Admin","Admin"),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["Auth:App:JwtExpireDays"]));

            X509Certificate2 cert = new X509Certificate2(_configuration["Auth:Jwt:CertName"], _configuration["Auth:Jwt:CertPass"]);
            SecurityKey signKey = new X509SecurityKey(cert);
            SigningCredentials credentials = new SigningCredentials(signKey, SecurityAlgorithms.RsaSha256);

            var token = new JwtSecurityToken(
                _configuration["Auth:Jwt:JwtIssuer"],
                _configuration["Auth:Jwt:JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// Method responsible for generate and set user
        /// temporary password with expiration date.
        /// Send email if required
        /// </summary>
        /// <param name="user">User object to be updated with new pass</param>
        /// <param name="hashPassword">Indicate if need to hash the password</param>
        /// <param name="sendEmail">Indicate if need to send message with newly pass</param>
        /// <returns></returns>
        private bool GenerateUserTemporaryPassword(User user, bool hashPassword = true, bool sendEmail = true)
        {
            var expireDays = Convert.ToDouble(_configuration["Auth:TemporaryPassword:ExpirationDays"]);
            var tempPassLength = Convert.ToInt32(_configuration["Auth:TemporaryPassword:Lenght"]);
            var tempPassword = Convert.ToBoolean(_configuration["Env:IsDev"]) == true ? _configuration["Env:DevPassword"]
                                                                                      : GenerateTemporaryPassword(tempPassLength);
            if (hashPassword)
            {
                var hashedPassword = _passwordHasher.HashPassword(user, tempPassword);
                user.PasswordHash = hashedPassword;
            }
            else
            {
                user.PasswordHash = tempPassword;
            }

            user.IsTemporaryPassword = true;
            user.PasswordExpirationDate = DateTime.UtcNow.AddDays(expireDays);

            if (sendEmail && Convert.ToBoolean(_configuration["Env:IsDev"]) == false)
            {


                var sendResult = _messageSender.SendSingleMessageToRecipient(_configuration["Messages:OutgoingEmailAddress"],
                                 user.Email,
                                 tempPassword,
                                 string.Empty);
                if (sendResult == false)
                    return false;

            }
            return true;
        }

        /// <summary>
        /// Method generate JWT token with userid inserted to claims
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GenerateJwtToken(string userId)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, userId),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["Auth:Jwt:JwtExpireDays"]));

            X509Certificate2 cert = new X509Certificate2(_configuration["Auth:Jwt:CertName"], _configuration["Auth:Jwt:CertPass"]);
            SecurityKey signKey = new X509SecurityKey(cert);
            SigningCredentials credentials = new SigningCredentials(signKey, SecurityAlgorithms.RsaSha256);

            var token = new JwtSecurityToken(
                _configuration["Auth:Jwt:JwtIssuer"],
                _configuration["Auth:Jwt:JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// Helper method that generate tempory password with
        /// lenght equal to passed by caller
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private string GenerateTemporaryPassword(int length)
        {
            const string valid = "abcdefghijklmnozABCDEFGHIJKLMNOZ1234567890!@#$%^&*()-=";
            StringBuilder strB = new StringBuilder(100);
            Random random = new Random();
            while (0 < length--)
            {
                strB.Append(valid[random.Next(valid.Length)]);
            }
            return strB.ToString();
        }

        private async Task<ParsedExternalAccessToken> VerifyExternalToken(string provider, string accessToken)
        {
            ParsedExternalAccessToken parsedToken = null;

            var verifyTokenEndPoint = "";

            if (provider == "facebook")
            {
                var appToken = _configuration["Auth:Facebook:AppToken"];
                verifyTokenEndPoint = string.Format("https://graph.facebook.com/debug_token?input_token={0}&access_token={1}", accessToken, appToken);
            }
            else if (provider == "google")
            {
                verifyTokenEndPoint = string.Format("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={0}", accessToken);
            }
            else
            {
                return null;
            }

            var client = new HttpClient();
            var uri = new Uri(verifyTokenEndPoint);
            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();

                dynamic jObj = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(content);

                parsedToken = new ParsedExternalAccessToken();

                if (provider == "facebook")
                {
                    parsedToken.user_id = jObj["data"]["user_id"];
                    parsedToken.app_id = jObj["data"]["app_id"];

                    if (!string.Equals(_configuration["Auth:Facebook:Appid"], parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }
                }
                else if (provider == "google")
                {
                    parsedToken.user_id = jObj["user_id"];
                    parsedToken.app_id = jObj["audience"];

                    if (!string.Equals(_configuration["Auth:Google:Clientid"], parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }

                }

            }

            return parsedToken;
        }

        #endregion Private Methods
    }
}
