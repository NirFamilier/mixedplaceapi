﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map.RequestModel;
using MixedPlace.API.Models.MPDesign.ResponseModel;
using MixedPlace.API.Models.UserManagement;
using MixedPlace.API.Models.UserManagement.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public interface IUserManagementService
    {
        IResponseModel<EmptyBodyResponse> SetMPComment(CommentRequestModel comment, string userId);
        IResponseModel<CommentResponseModel> GetComments(long x, long y, long z, int pageId, int numPerPage);
        IResponseModel<EmptyBodyResponse> SetMPVisits(MixedPlaceRequestModel umrModel, string userId);
        IResponseModel<EmptyBodyResponse> SetMPLike(MixedPlaceRequestModel umrModel, string userId);
        IResponseModel<EmptyBodyResponse> SetMPShare(MPCoordinateDto coordinates,string source, string userId);
        IResponseModel<EmptyBodyResponse> SetMPReport(ReportRequestModel report, string userId);
        Task<IResponseModel<EmptyBodyResponse>> SetMPVisits(SetMPVisitRequest request, string userId);
        IResponseModel<EmptyBodyResponse> EditComment(CommentRequestModel request, string userId);
        IResponseModel<EmptyBodyResponse> RemoveComment(CommentRequestModel request, string userId);
        void RefuseReport(string token);
        void ApproveReport(string token);
    }
}
