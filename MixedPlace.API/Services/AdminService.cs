﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Data;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Helpers;
using MixedPlace.API.Models.Base.ResponseModel;
using MixedPlace.API.Models.Coins;
using MixedPlace.API.Models.Coins.RequestModel;
using MixedPlace.API.Models.Coins.ResponseModel;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Common.MixedPlaceExtensions;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.MPChallenge.ResponseModels;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.MPModel.RequestModels;
using MixedPlace.API.Models.MPModel.ResponseModels;
using MixedPlace.API.Models.Package;
using MixedPlace.API.Models.Package.RequestModels;
using MixedPlace.API.Models.User;
using MixedPlace.ImageUploader.Abstractions;
using MixedPlace.ImageUploader.Implementations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public class AdminService:IAdminService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _autoMapper;
        private readonly IImageUploader _imageUploader;
        private readonly IStoreService _storeService;
        private readonly ITransactionService _transactionService;
        private readonly AdminTablesMapper _adminTableMapper;
        private readonly ILogger<AdminService> _logger;

        public AdminService(ApplicationDbContext context,
                          IMapper autoMapper,
                          IStoreService storeService,
                          ITransactionService transactionService, 
                          ILogger<AdminService> logger,
                          AdminTablesMapper adminTablesMapper)
        {
            _context = context;
            _autoMapper = autoMapper;
            _imageUploader = ImageUploaderFactory.CreateImageUploader();
            _storeService = storeService;
            _transactionService = transactionService;
            _logger = logger;
            _adminTableMapper = adminTablesMapper;
        }


        public IResponseModel<bool> AddModelCategory(ModelCategory category)
        {
            var response = new ResponseModel<bool>();
            try
            {
                var modelCategories = _context.ModelCategories.Where(x => x.Name == category.Name).FirstOrDefault();
                if (modelCategories != null)
                {
                    response.Data = false;
                    response.StatusCode = (int)InnerErrorCode.ModelCategoryExists;
                    return response;
                }
                _context.ModelCategories.Add(category);
                _context.SaveChanges();
                response.Data = true;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<bool> AddMPCategory(MPCategory category)
        {
            var response = new ResponseModel<bool>();
            try
            {
                // Check if exists
                var mpCategories = _context.MPCategories.Where(x => x.CategoryName == category.CategoryName).FirstOrDefault();
                if (mpCategories != null)
                {
                    response.Data = false;
                    response.StatusCode = (int)InnerErrorCode.MPCategoryExists;
                    return response;
                }
                _context.MPCategories.Add(category);
                _context.SaveChanges();
                response.Data = true;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }
        public IResponseModel<bool> AddMPSubCategory(MPSubCategory subCategory)
        {
            var response = new ResponseModel<bool>();
            try
            {
                // To check if categorie name  exists
                var mpCategory = _context.MPCategories.Where(x => x.Id == subCategory.CategoryId).FirstOrDefault();
                if (mpCategory == null)
                {
                    response.Data = false;
                    response.StatusCode = (int)InnerErrorCode.MixedPlaceCategoryNotExists;
                    return response;
                }
                //To check if sub categorie name  exists
                var mpSubCategory = _context.MPSubCategories.Where(x => x.Name == subCategory.Name).FirstOrDefault();
                if (mpSubCategory != null)
                {
                    response.Data = false;
                    response.StatusCode = (int)InnerErrorCode.MixedPlaceSubCategoryExists;
                    return response;
                }
                _context.MPSubCategories.Add(subCategory);
                _context.SaveChanges();
                response.Data = true;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<bool> AddModelSubCategory(ModelSubCategory subCategory)
        {
            var response = new ResponseModel<bool>();
            try
            {
                // To check if categorie name  exists
                var modelCategory = _context.ModelCategories.Where(x => x.Id == subCategory.ModelCategoryId).FirstOrDefault();
                if (modelCategory == null)
                {
                    response.Data = false;
                    response.StatusCode = (int)InnerErrorCode.ModelCategoryNotExists;
                    return response;
                }
                //To check if sub categorie name  exists
                var modelSubCategory = _context.ModelSubCategories.Where(x => x.Name == subCategory.Name).FirstOrDefault();
                if (modelSubCategory!= null)
                {
                    response.Data = false;
                    response.StatusCode = (int)InnerErrorCode.ModelSubCategoryExists;
                    return response;
                }
                _context.ModelSubCategories.Add(subCategory);
                _context.SaveChanges();
                response.Data = true;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<bool> AddBundle(BundleRequest request)
        {
            var response = new ResponseModel<bool>();
            try
            {
                var bundle = new BundleModel
                {
                    Name = request.Name,
                    Description = request.Description,
                    Url = request.URL,
                    NumberOfModels = request.NumberOfItems,
                    CreationDate = DateTime.Now,
                    UpdateTime = DateTime.Now,
                    Size = request.Size
                };
                _context.BundleModel.Add(bundle);
                var listOfCategories = new List<BundleModelCatSubCat>();

                foreach (var cat in request.BundelCategories)
                {
                    var category = cat.Category;
                    var subcategory = cat.SubCategory;
                    _context.BundleModelCatSubCat.Add(new BundleModelCatSubCat()
                    {
                        ModelCategoryId = _autoMapper.Map<ModelCategoryDto, ModelCategory>(category).Id,
                        ModelSubCategoryId = _autoMapper.Map<ModelSubCategoryDto, ModelSubCategory>(subcategory).Id,
                        BundleId = bundle.Id
                    });
                }
                //   _context.BundleModelCatSubCat.AddRange(listOfCategories);
                _context.SaveChanges();
                response.Data = true;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<string> UploadImage(UploadImageRequest request)
        {
            var response = new ResponseModel<string>();
            try
            {
                var formFile = request.FormFile;
                if (formFile == null || formFile.Length == 0)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                using (Stream stream = formFile.OpenReadStream())
                {
                    using (var binaryReader = new BinaryReader(stream))
                    {
                        var fileContent = binaryReader.ReadBytes((int)formFile.Length);
                        string subDirctory = "AssetsBundels";
                        var fileUrl = _imageUploader.UploadImage(fileContent, formFile.FileName, formFile.ContentType, subDirctory);
                        if (!string.IsNullOrWhiteSpace(fileUrl))
                        {
                            response.Data = fileUrl;
                            response.StatusCode = (int)InnerErrorCode.Ok;
                        }
                        else
                            response.StatusCode = (int)InnerErrorCode.UnknownError;

                        return response;
                    }
                }
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<BundleResponseModel> GetBundleById(int id)
        {
            var response = new ResponseModel<BundleResponseModel>();
            try
            {
                var bundle = _context.BundleModel
                                     .Include(x => x.BundleModelCatSubCat).ThenInclude(x => x.ModelCategory)
                                     .Include(x => x.BundleModelCatSubCat).ThenInclude(x => x.ModelSubCategory)
                                     .FirstOrDefault(x => x.Id == id);

                var item = _autoMapper.Map<BundleModel, BundleModelDto>(bundle);
                if (bundle != null)
                {
                    item.Categories = _autoMapper.Map<IEnumerable<BundleModelCatSubCat>,
                                                     IEnumerable<ModelCatSubCatDto>>(bundle.BundleModelCatSubCat);
                    response.Data.List = new List<BundleModelDto> { item };
                    response.StatusCode = (int)InnerErrorCode.Ok;
                }
                else
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<BundleResponseModel> GetBundles()
        {
            var response = new ResponseModel<BundleResponseModel>();
            try
            {
                var bundles = _context.BundleModel
                                      .Include(x => x.BundleModelCatSubCat).ThenInclude(x=>x.ModelCategory)
                                      .Include(x => x.BundleModelCatSubCat).ThenInclude(x => x.ModelSubCategory)
                                      .ToList();

                response.Data = new BundleResponseModel();
                var bundlesList = new List< BundleModelDto>();
                foreach (var bundle in bundles)
                {
                    bundlesList.Add(new BundleModelDto
                    {
                         Id = bundle.Id,
                         Name = bundle.Name,
                         Description = bundle.Description,
                         Url = bundle.Url,
                         Categories = _autoMapper.Map<IEnumerable<BundleModelCatSubCat>,
                                                     IEnumerable<ModelCatSubCatDto>>(bundle.BundleModelCatSubCat)
                    });
                }
                response.Data.List = bundlesList;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<GetGlobalMetadataResponse> GetGlobalMetadata()
        {
            var response = new ResponseModel<GetGlobalMetadataResponse>();
            try
            {
                var conditions = _context.Conditions.Select(x => new ConditionDto { Id = x.Id, Name = x.Name }).ToList();
                response.Data = new GetGlobalMetadataResponse { Filters = new GetGlobalMetadataFilters { Conditions = conditions } };
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<bool> AddModel(AddModelRequest request)
        {
            var response = new ResponseModel<bool>();
            try
            {
                var bundle = _context.BundleModel.FirstOrDefault(x => x.Id == request.Bundle.Id);
                if (bundle == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BundleNotExist;
                    return response;
                }

                PackageModel packageModel = CreatePackageWithDefaultModel(request, bundle);
                _storeService.CreatePackage(packageModel);

                if (request.ModelCategories != null)
                    RemoveAndAddCategoriesSubCategoriesToModel(request,packageModel.PackageItems.FirstOrDefault().Model.Id);
                _context.SaveChanges();

                response.Data = true;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        private static PackageModel CreatePackageWithDefaultModel(AddModelRequest request, BundleModel bundle)
        {
            PackageModel packageModel = new PackageModel();
            packageModel.Name = request.Name + "_Package";
            packageModel.CreateDate = DateTime.Now;
            packageModel.Price = request.Price;
            packageModel.PriceTypeId = request.PriceType.Id;
            packageModel.PackageCategoryId = (int)PackageCategoryType.SingleModel;
            packageModel.AvailableFrom = DateTime.Now;
            packageModel.PackageStatusId = (int)PackageStatus.Active;
            packageModel.PackageItems = new List<PackageItemModel>();
            var packageItem = new PackageItemModel();
            packageModel.PackageItems.Add(new PackageItemModel
            {
                PackageItemTypeId = (int)PackageItemType.Model,
                NumOfUnits = 1,
                Type = (int)PackageItemType.Model,
                Model = new MPModel
                {
                    Name = request.Name,
                    BundleId = bundle.Id,
                    ModelStatusId = (int)ModelStatus.Active,
                    CreationDate = DateTime.Now,
                    BundleImageName = bundle.Name,
                    Description = request.Description,
                    Price = request.Price,
                    PriceTypeId = request.PriceType.Id,
                    UpdateDate = DateTime.Now,
                    UniqueId = request.UniqueId
                }
            });
            return packageModel;
        }

        public IResponseModel<bool> UpdateModel(UpdateModelRequest updatedModel)
        {
            var response = new ResponseModel<bool>();
            try
            {
                var model = _context.Models.Include(x => x.PackageItems).ThenInclude(x => x.Package)
                                           .FirstOrDefault(x => x.Id == updatedModel.Id && x.ModelStatusId == (int)ModelStatus.Active);
                if (updatedModel.ModelCategories != null)
                   RemoveAndAddCategoriesSubCategoriesToModel(updatedModel,updatedModel.Id);

                if (model == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                //Add validation - check if model not exists in not active packages that not single
                var modelExists = model.PackageItems.Any(x => x.Package.PackageCategoryId != (int)PackageCategoryType.SingleModel &&
                                                         x.Package.PackageStatusId == (int)PackageStatus.Active);
                if (modelExists)
                {
                    response.StatusCode = (int)InnerErrorCode.ModelExistsInNotSinglePackage;
                    return response;
                }

                model.Price = updatedModel.Price;
                model.PriceTypeId = updatedModel.PriceType.Id;
                model.Name = updatedModel.Name;


                //Add validation -  package type is Single
                var packageItems = model.PackageItems.Where(x => x.Package.PackageCategoryId == (int)PackageCategoryType.SingleModel && x.Package.PackageStatusId == (int)PackageStatus.Active);

                //var package = model.PackageItems.Select(x =>
                var package = packageItems.Select(x =>
                {
                    x.Package.PriceTypeId = (int)model.PriceTypeId;
                    x.Package.Price = model.Price;
                    return x.Package;
                });
                foreach (var item in package)
                {
                    _context.Packages.Update(item);
                }

                _context.Models.Update(model);
                _context.SaveChanges();
                response.Data = true;
                response.StatusCode = (int)InnerErrorCode.Ok;
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
            return response;
        }

        private void RemoveAndAddCategoriesSubCategoriesToModel(ModelRequestBase updatedModel,int modelid)
        {
            if (updatedModel.ModelCategories == null || updatedModel.ModelCategories.Count == 0)
                return;

            var currenModeCategories = _context.MPModelCatSubCat.Where(x => x.ModelId == modelid).ToList();
            if (currenModeCategories.Any())
            {
                _context.MPModelCatSubCat.RemoveRange(currenModeCategories);
            }
            var listOfCategories = new List<MPModelCatSubCat>();

            foreach (var cat in updatedModel.ModelCategories)
            {
                var category = cat.Category;
                var subcategory = cat.SubCategory;
                listOfCategories.Add(new MPModelCatSubCat()
                {
                    ModelCategoryId = category.Id,
                    ModelSubCategoryId = subcategory.Id,
                    ModelId = modelid
                });
            }
            _context.MPModelCatSubCat.AddRange(listOfCategories);
        }

        public IResponseModel<bool> SuspendModel(SuspendModelRequest suspendedModel)
        {

            var response = new ResponseModel<bool>();
            try
            {
                var model = _context.Models.Include(x => x.PackageItems).ThenInclude(x => x.Package)
                                           .FirstOrDefault(x => x.Id == suspendedModel.Id);
                if (model == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                //Add validation - check if model not exists in not active packages that not single
                var modelExists = model.PackageItems.Any(x => x.Package.PackageCategoryId != (int)PackageCategoryType.SingleModel &&
                                                         x.Package.PackageStatusId == (int)PackageStatus.Active);
                if (modelExists)
                {
                    response.StatusCode = (int)InnerErrorCode.ModelExistsInNotSinglePackage;
                    return response;
                }

                model.ModelStatusId = (int)ModelStatus.Suspended;
                //Add validation -  package type is Single
                var packageItems = model.PackageItems.Where(x => x.Package.PackageCategoryId == (int)PackageCategoryType.SingleModel);

                //var package = model.PackageItems.Select(x =>
                var package = packageItems.Select(x =>
                {
                    x.Package.PackageStatusId = (int)PackageStatus.Suspended;
                    return x.Package;
                });
                foreach (var item in package)
                {
                    _context.Packages.Update(item);
                }

                _context.Models.Update(model);
                _context.SaveChanges();
                response.Data = true;
                response.StatusCode = (int)InnerErrorCode.Ok;
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
            return response;
        }

        public IResponseModel<GetCoinRulesMetadataResponse> GetCoinRulesMetadata()
        {
            var response = new ResponseModel<GetCoinRulesMetadataResponse>();
            try
            {
                var coinRuleTypes = _context.CoinsRuleTypes.Select(x => new CoinRuleTypeDto { Id = x.Id, Name = x.Name }).ToList();
                var logicalOperators = _context.LogicalOperators.Select(x => new LogicalOperatorDto { Id = x.Id, Name = x.Name }).ToList();

                response.Data = new GetCoinRulesMetadataResponse
                {
                    CoinRuleTypes = coinRuleTypes,
                    Filters = _adminTableMapper.CoinsRulesTableFiltersConditionsMap,
                    UserFilter = _adminTableMapper.UsersTableFiltesConditionsMap,
                    LogicalOperators = logicalOperators
            };
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }


        public IResponseModel<GetCoinRulesResponse> GetCoinRules(FilterRequest filterRequest = null)
        {
            var response = new ResponseModel<GetCoinRulesResponse>();
            try
            {
                if (filterRequest == null)
                {
                    var coinRules = _context.CoinsRuleModels.Include(u => u.LogicalOperator)
                                                .Include(x => x.CoinsRuleType).ToList();
                    response.Data = new GetCoinRulesResponse {CoinRulesList=_autoMapper.Map<List<CoinsRuleModel>,List<CoinRuleDto>>(coinRules)};
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                else
                {
                    var dbCoinRules = _context.CoinsRuleModels.Include(u => u.LogicalOperator)
                                                  .Include(x => x.CoinsRuleType).ToList();
                    var coinRules = _autoMapper.Map<List<CoinsRuleModel>, List<CoinRuleDto>>(dbCoinRules);

                    IEnumerable<CoinRuleDto> result = new List<CoinRuleDto>(coinRules);

                    if (!string.IsNullOrWhiteSpace(filterRequest.Search))
                        result = SearchCoinsRules(result, filterRequest.Search);

                    if (filterRequest.Filters != null && filterRequest.Filters.Count() > 0)
                        result = FilterHelper.Filter(result, filterRequest.Filters, _adminTableMapper.CoinsTableMap);

                    response.Data = new GetCoinRulesResponse
                    {
                        CoinRulesList = result?.Skip((filterRequest.Page - 1) * filterRequest.AmountPerPage)?.Take(filterRequest.AmountPerPage),
                        Summary = filterRequest
                    };
                    response.Data.Summary.TotalAmount = result?.Count() ?? 0;
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }





            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }

        }

        public IResponseModel<bool> AddRewardPerBulkOfUsers(RewardRequest request)
        {
            var response = new ResponseModel<bool>();
            try
            {
                var filters = request.RuleConditions.Select(x => new FilterItemRequest
                {
                    ColumnId = x.FieldName.Id,
                    Condition = (ConditionsType)x.Operator.Id,
                    Value = x.Value
                });

                request.Amount = request.CoinRuleType.Id == (int)CoinRuleType.Fine ? request.Amount * (-1) : request.Amount;
                var usersDb =_context.Users.ToList();
                var users = _autoMapper.Map<IEnumerable<User>, IEnumerable<UserDetailsDto>>(usersDb);
                users = FilterHelper.Filter(users, filters, _adminTableMapper.UsersTableMap,(LogicalOperatorType)request.LogicalOperator.Id);
                var affectedUsersCount = users.Count();
                foreach (var user in users)
                {
                    //CReate Transactions and update user balance
                    var oldCoinsStatus = _transactionService.GetCoinsStatus(user.UserId);
                    if ((oldCoinsStatus + request.Amount) < 0)
                    {
                        response.Data = false;
                        response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                        return response;
                    }
                     _transactionService.AddTransaction((TransactionCategoryEnum)request.CoinRuleType.Id, request.Amount, 0, String.Empty, user.UserId);
                     var newCoinsStatus = _transactionService.GetCoinsStatus(user.UserId);
                   
                    var userData = usersDb.FirstOrDefault(x => x.Id == user.UserId);
                    if (userData != null)
                    {
                        userData.CoinBalance = newCoinsStatus;
                        _context.Users.Update(userData);
                    }
                }

                //Insert Rules to DB
                var ruleContidions = _autoMapper.Map<IEnumerable<CoinsRuleConditionDto>, IEnumerable<CoinsRuleConditionModel>>(request.RuleConditions);

                _context.CoinsRuleModels.Add(new CoinsRuleModel
                {
                    Amount = request.Amount,
                    TypeId = request.CoinRuleType.Id,
                    CreatedDate = DateTime.Now,
                    Description = request.Description,
                    CoinRuleConditions = new List<CoinsRuleConditionModel>(ruleContidions),
                    NumberOfAffectedUsers= affectedUsersCount,
                    Name=request.Name,
                    LogicalOperatorId=request.LogicalOperator.Id
                });

                _context.SaveChanges();
                response.Data = true;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception ex)
            {
                response.Data = false;
                response.StatusCode = (int)InnerErrorCode.UserNotFound;
                return response;
            }
        }

       
        public IResponseModel<GetClientChallengesResponse> GetClientChallengesPerMonth(string userId)
        {
            var response = new ResponseModel<GetClientChallengesResponse>();
            //get all challenges of user from table
            var challenges = _context.AchievedChallenges.Where(x => x.UserId == userId).ToList();
            //create dictionary with key=date(MM/YYYY), value=Count of User's Challenges
            return response;
        }

        #region Private Methods
        private IEnumerable<CoinRuleDto> SearchCoinsRules(IEnumerable<CoinRuleDto> list, string searchTerm)
        {
            return list.Where(x => (x.Name != null && x.Name.Contains(searchTerm)) ||
                                  (x.Description != null && x.Description.Contains(searchTerm)) ||
                                   (x.Condition?.Name != null && x.Condition.Name.Contains(searchTerm)) ||
                                    x.CreatedDate.ToString().Contains(searchTerm) ||
                                   x.Amount.ToString().Contains(searchTerm));
        }

        #endregion


    }
}
