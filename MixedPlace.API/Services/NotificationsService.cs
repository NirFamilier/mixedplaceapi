﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Data;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Helpers;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.MPChallenge;
using MixedPlace.API.Models.Notifications.Requests;
using MixedPlace.API.Models.User;
using MixedPlace.MessageSender.Abstractions;
using MixedPlace.MessageSender.Implementations;
using MixedPlace.NotificationService.Abstractions;
using MixedPlace.NotificationService.Implementations;

#region Commented Code With Sample For Last Notification Send Data
// if (user.NotificationSendDate == null || user.NotificationSendDate <= DateTime.Now.AddHours(-6))
//                        {
//                            SendNewNotification(user, request.Message, NotificationOriginator.MixedPlaceAdminManagement);
//SendQueuedNotifications(queuedNotifications.FirstOrDefault(x => x.UserId == user.Id)?.Notifications);
//                        }
//                        else
//                        {
//                            QueueNotification(user, request.Message, NotificationOriginator.MixedPlaceAdminManagement);
//                        }
#endregion 

namespace MixedPlace.API.Services
{
    /// <summary>
    /// Service contain business logic and functionality 
    /// for users notification . All types of notifications - Email,Push,Mobile
    /// </summary>
    public class NotificationsService : INotificationsService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _autoMapper;
        private readonly ILogger<NotificationsService> _logger;
        private readonly AdminTablesMapper _adminTableMapper;
        private readonly IConfiguration _configuration;
        private readonly JWTTokenHelper _jWTTokenHelper;
        private readonly INotificationService _notificationSerivce;
        private readonly IMessageSender _messageSender;
        public static readonly object lockObject = new object();

        public NotificationsService(ApplicationDbContext context,
                                    IMapper autoMapper,
                                    ILogger<NotificationsService> logger,
                                    AdminTablesMapper adminTableMapper,
                                    IConfiguration configuration,
                                    JWTTokenHelper jWTTokenHelper)
        {
            _context = context;
            _autoMapper = autoMapper;
            _logger = logger;
            _adminTableMapper = adminTableMapper;
            _configuration = configuration;
            _jWTTokenHelper = jWTTokenHelper;
            _notificationSerivce = NotificationServiceFactory.CreateNotificationService(_configuration["Notifications:Provider"]);
            _messageSender = MessageSenderFactory.CreateMessageSender(_configuration["Messages:Provider"]);
        }

        #region Public Methods

        #region APIs Methods
        public IResponseModel<EmptyBodyResponse> NotifyAllChallenges(AdminNotifyAllRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                lock (lockObject)
                {
                    var challenges = GetFilteredChallenges(request.Filter);
                    var allUsers = GetFilteredUsers().ToList();

                    foreach (var challenge in challenges)
                    {
                        var completedChallangeUsers = allUsers.Join(inner: challenge.AchievedChallenges,
                                                               outerKeySelector: usr => usr.Id,
                                                               innerKeySelector: mp => mp.UserId,
                                                               resultSelector: (usr, mp) => usr).ToList();

                        if (request.IsAchieved)
                        {
                            foreach (var user in completedChallangeUsers)
                            {
                                QueueNotification(user, request.Message,request.Title, NotificationOriginator.MixedPlaceAdminManagement);
                            }
                        }
                        else
                        {
                            var users = new List<User>(allUsers);
                            users.RemoveAll(x => completedChallangeUsers.Contains(x));
                            foreach (var user in users)
                            {
                              QueueNotification(user, request.Message,request.Title, NotificationOriginator.MixedPlaceAdminManagement);
                            }

                        }
                    }
                    _context.SaveChanges();
                }
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> NotifyAllMixedPlaceOwners(AdminNotifyAllRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                lock (lockObject)
                {
                    var mixedPlaces = GetFilteredMixedPlaces(request.Filter).Where(x => !string.IsNullOrWhiteSpace(x.OwnerName));
                    var allUsers = GetFilteredUsers();

                    var users = allUsers.Join(inner: mixedPlaces,
                                              outerKeySelector: usr => usr.Id,
                                              innerKeySelector: mp => mp.OwnerId,
                                              resultSelector: (usr, mp) => usr);

                    foreach (var user in users)
                    {
                      QueueNotification(user, request.Message,request.Title, NotificationOriginator.MixedPlaceAdminManagement);
                    }
                    _context.SaveChanges();
                    return response;
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> NotifyAllUsers(AdminNotifyAllRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.Ok };
            try
            {
                lock (lockObject)
                {
                    var users = GetFilteredUsers(request.Filter);
                    if (users == null || users.Count() == 0) return response;

                    foreach (var user in users)
                    {
                       QueueNotification(user, request.Message,request.Title, NotificationOriginator.UserAdminManagement);
                    }

                    _context.SaveChanges();
                    return response;
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> NotifyChallenge(AdminNotifyChallengeRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                lock (lockObject)
                {
                    var challenge = _context.Challenges.Include(x => x.AchievedChallenges)
                                                       .ThenInclude(y => y.User)
                                                       .FirstOrDefault(x => x.Id == request.Id);

                    if (request.IsAchieved) 
                    {
                        foreach (var challangeAchievment in challenge.AchievedChallenges)
                        {
                          QueueNotification(challangeAchievment.User, request.Message,request.Title, NotificationOriginator.ChallengesAdminManagement);
                        }
                    }
                    else
                    {
                        var users = _context.Users.ToList();
                        users.RemoveAll(x => challenge.AchievedChallenges.Any(ach => ach.UserId == x.Id));
                        foreach (var user in users)
                        {
                          QueueNotification(user, request.Message,request.Title, NotificationOriginator.ChallengesAdminManagement);
                        }
                    }
                    _context.SaveChanges();
                }
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> NotifyMixedPlaceOwner(AdminNotifyMixedPlaceRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                lock (lockObject)
                {
                    var mixedPlace = _context.MixedPlaces.Include(x => x.User).First(x => x.Id == request.Id);
                    if (mixedPlace.User == null)
                    {
                        response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                        return response;
                    }

                    var user = mixedPlace.User;
                    QueueNotification(user, request.Message,request.Title, NotificationOriginator.MixedPlaceAdminManagement);
                    _context.SaveChanges();
                }
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> NotifyUser(AdminNotifyUserRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                lock (lockObject)
                {
                    var user = _context.Users.Include(x => x.Notifications).First(x => x.Id == request.Id);
                    QueueNotification(user, request.Message,request.Title, NotificationOriginator.UserAdminManagement);
                    _context.SaveChanges();
                }

                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> SendQueuedNotifications()
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var queuedNotifications = _context.Notifications.Include(x => x.User)
                                                  .Where(x => x.NotificationStatusId == (int)NotificationStatus.Queued).ToList();

                if (queuedNotifications.Count() == 0)
                {
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                SendQueuedNotifications(queuedNotifications);
                _context.SaveChanges();
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                return response;
            }
        }

        #endregion APIs Methods

        #region Helper Methods

        public bool NotifyUserAboutReport(string userId, string reportText)
        {
            try
            {
                lock (lockObject)
                {
                    var user = _context.Users.Include(x => x.Notifications).First(x => x.Id == userId);
                    QueueNotification(user,reportText, "Bad Content Notification", NotificationOriginator.ContentReport);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                return false ;
            }
        }

        public bool SendReportToAdmin(int id,string reporterName,string ownerName, string reportText)
        {
            try
            {
                var ntfTemplateHtml = _context.NotificationTemplates.FirstOrDefault(x => !string.IsNullOrWhiteSpace(x.UniqueName) ?
                                                                                     x.UniqueName.Trim().Equals(UniqueTemplatesName.SC_Email_ReportToAdminHtml) : false);
                var ntfTemplateText = _context.NotificationTemplates.FirstOrDefault(x => !string.IsNullOrWhiteSpace(x.UniqueName) ?
                                                                                     x.UniqueName.Trim().Equals(UniqueTemplatesName.SC_Email_ReportToAdminText) : false);

                if (string.IsNullOrWhiteSpace(ntfTemplateHtml?.Message) || string.IsNullOrWhiteSpace(ntfTemplateText?.Message))
                {
                    return false;
                }
                

                var authToken = _jWTTokenHelper.GenerateAdminEmailJWTToken(sub: Convert.ToString(id));
                var messageText = ntfTemplateText.Message;
                var messageHtml = ntfTemplateHtml.Message;

                messageText = messageText.Replace("{reportText}", reportText);
                messageText = messageText.Replace("{reportDate}", DateTime.Now.ToShortDateString());
                messageHtml = messageHtml.Replace("{reportText}", messageText);
                messageHtml = messageHtml.Replace("{btnApproveLink}", $"{_configuration["ProdEnv:BaseUrl"]}/api/usermanagement/approvereport?token={authToken}");
                messageHtml = messageHtml.Replace("{btnRefuseLink}", $"{_configuration["ProdEnv:BaseUrl"]}/api/usermanagement/refusereport?token={authToken}");
                var sendingResult = _messageSender.SendSingleMessageToRecipient(from: _configuration["Messages:OutgoingEmailAddress"],
                                                                                to: _configuration["Messages:IncomingEmailAdress"],
                                                                                message: null,
                                                                                htmlContent:messageHtml
                                                                                );
                var notification = new NotificationModel
                {
                    CreatedDate = DateTime.Now,
                    DateSent = sendingResult ? DateTime.Now : (DateTime?)null,
                    UpdateDate = DateTime.Now,
                    Message = messageText,
                    NotificationMethodId = (int)NotificationMethod.Email,
                    NotificationOriginationId = (int)NotificationOriginator.ContentReport,
                    NotificationStatusId = sendingResult ? (int)NotificationStatus.Send : (int)NotificationStatus.Failed,
                    NotificationTemplateId = ntfTemplateText.Id,
                    Recipient = _configuration["Messages:IncomingEmailAdress"],
                };

                _context.Update(notification);

                return sendingResult;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                return false;
            }
        }

        public bool SendUpdateUserStatusEmail(string userId, UserStatus userStatus)
        {
            try
            {
                string userTempateName;
                switch (userStatus)
                {
                    case UserStatus.Registered:
                        userTempateName = "";
                        break;
                    case UserStatus.Active:
                        userTempateName = UniqueTemplatesName.SC_Email_UserActivated;
                        break;
                    case UserStatus.Suspended:
                        userTempateName = UniqueTemplatesName.SC_Email_UserSuspended;
                        break;
                    case UserStatus.Disabled:
                        userTempateName = UniqueTemplatesName.SC_Email_UserDisabled;
                        break;
                    default:
                        return false;
                }


                var user = _context.Users.First(x => x.Id == userId);
                var ntfTemplate = _context.NotificationTemplates.FirstOrDefault(x => x.UniqueName == userTempateName);
                if (ntfTemplate?.Message != null)
                {
                    var message = ntfTemplate.Message;
                    message = message.Replace("{User name}", $"{user.FirstName} {user.LastName}");
                    var sendingResult = _messageSender.SendSingleMessageToRecipient(from: _configuration["Messages:OutgoingEmailAddress"],
                                                                                    to: user.Email,
                                                                                    message: message,
                                                                                    htmlContent: string.Empty);
                    var notification = new NotificationModel
                    {
                        CreatedDate = DateTime.Now,
                        DateSent = sendingResult ? DateTime.Now : (DateTime?)null,
                        UpdateDate = DateTime.Now,
                        Message = message,
                        NotificationMethodId = (int)NotificationMethod.Email,
                        NotificationOriginationId = (int)NotificationOriginator.UserStatusChange,
                        NotificationStatusId = sendingResult ? (int)NotificationStatus.Send : (int)NotificationStatus.Failed,
                        NotificationTemplateId = ntfTemplate.Id,
                        Recipient = user.Email
                    };

                    _context.Update(notification);

                    return sendingResult;

                }
                return false;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                return false;
            }
        }

        #endregion Helper Methods


        #endregion Public Methods

        #region Private Methods

        private IEnumerable<User> GetFilteredUsers(FilterRequest filter = null)
        {
            var dbUsers = _context.Users.ToList();
            var users = _autoMapper.Map<IEnumerable<User>, IEnumerable<UserDetailsDto>>(dbUsers);

            if (filter != null && filter.Filters.Count() > 0)
                users = FilterHelper.Filter(users, filter.Filters, _adminTableMapper.UsersTableMap);

            return users.Join(dbUsers, usr => usr.UserId, dbUsr => dbUsr.Id, (usr, dbUsr) => dbUsr).ToList();

        }

        private IEnumerable<MPDetailsDto> GetFilteredMixedPlaces(FilterRequest filter)
        {
            var dbMps = _context.MixedPlaces.Include(x => x.User)
                                          .Include(x => x.DesignStatus)
                                          .Include(x => x.MPStatus)
                                          .Include(x => x.Price)
                                          .Include(x => x.MPComments)
                                          .Include(x => x.MPVisits)
                                          .Include(x => x.MPLike)
                                          .Include(x => x.MPShares)
                                          .ToList();

            var mps = _autoMapper.Map<IEnumerable<MP>, IEnumerable<MPDetailsDto>>(dbMps);


            if (filter.Filters != null && filter.Filters.Count() > 0)
                mps = FilterHelper.Filter(mps, filter.Filters, _adminTableMapper.MixedPlaceTableMap);

            return mps;
        }

        private IEnumerable<ChallengeDto> GetFilteredChallenges(FilterRequest filter)
        {
            var dbChallenges = _context.Challenges.Include(x=> x.AchievedChallenges).ThenInclude(x=> x.User)
                                                  .Include(x => x.ChallenegeStatus)
                                                  .Include(x => x.ChallengeType)
                                                  .ToList();

            var challenges = _autoMapper.Map<IEnumerable<Challenge>, IEnumerable<ChallengeDto>>(dbChallenges);

     
            if (filter.Filters != null && filter.Filters.Count() > 0)
                challenges = FilterHelper.Filter(challenges, filter.Filters, _adminTableMapper.ChallengesTableMap);

            return challenges;
        }

        private IEnumerable<UserNotificationGroupItem> GetQueuedNotifications()
        {
            var queuedNotifications = _context.Notifications.Where(x => x.NotificationStatusId == (int)NotificationStatus.Queued)
                                                            .GroupBy(x => x.UserId)
                                                            .Select(x => new UserNotificationGroupItem
                                                            {
                                                                UserId = x.Key,
                                                                Notifications = x.Select(q => q)
                                                            })
                                                            .ToList();
            return queuedNotifications;
        }

        private void SendNewNotification(User user,string message,string title,NotificationOriginator originator)
        {
            var notification = new NotificationModel
            {
                CreatedDate = DateTime.Now,
                Message = message,
                NotificationOriginationId = (int)originator,
                NotificationMethodId = (int)NotificationMethod.PushNotification,
                UserId = user.Id,
                Recipient = user.DeviceID
            };

            if (_notificationSerivce.Notify(user.DeviceID, message,title))
            {
                notification.NotificationStatusId = (int)NotificationStatus.Send;
                notification.DateSent = DateTime.Now;
                user.NotificationSendDate = DateTime.Now;
                _context.Update(user);
            }
            else
                notification.NotificationStatusId = (int)NotificationStatus.Failed;

            _context.Add(notification);
        }

        private void SendQueuedNotifications(IEnumerable<NotificationModel> notifications)
        {
            if (notifications == null || notifications.Count() == 0) return;

            foreach (var notification in notifications)
            {
                if (_notificationSerivce.Notify(notification.Recipient, notification.Message,notification.Title))
                {
                    notification.NotificationStatusId = (int)NotificationStatus.Send;
                    notification.DateSent = DateTime.Now;
                }
                else
                {
                    notification.NotificationStatusId = (int)NotificationStatus.Failed;
                }
                    
                notification.UpdateDate = DateTime.Now;
                _context.Update(notification);
            }
        }

        private void QueueNotification(User user, string message,string title, NotificationOriginator originator)
        {
            var notification = new NotificationModel
            {
                CreatedDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Message = message,
                Title = title,
                NotificationOriginationId = (int)originator,
                NotificationMethodId = (int)NotificationMethod.PushNotification,
                NotificationStatusId = (int)NotificationStatus.Queued,
                UserId = user.Id,
                Recipient = user.DeviceID
            };
            _context.Update(notification);
        }

        #endregion Private Methods
    }

    public class UserNotificationGroupItem
    {
        public string UserId { get; set; }
        public IEnumerable<NotificationModel> Notifications { get; set; }
    }
}
