﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Data;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Helpers;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Common.MixedPlaceExtensions;
using MixedPlace.API.Models.MPChallenge;
using MixedPlace.API.Models.MPChallenge.RequestModels;
using MixedPlace.API.Models.MPChallenge.ResponseModels;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.ResponseModels;
using MixedPlace.ImageUploader.Abstractions;
using MixedPlace.ImageUploader.Implementations;
using MixedPlace.NotificationService.Abstractions;
using MixedPlace.NotificationService.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public class ChallengeService : IChallengeService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _autoMapper;
        private readonly ILogger<ChallengeService> _logger;
        private readonly AdminTablesMapper _adminTableMapper;
        private readonly IImageUploader _imageUploader;
        private readonly INotificationService _notificationSerivce;
        private readonly IConfiguration _configuration;
        private readonly ITransactionService _transactionService;
        

        public ChallengeService(ApplicationDbContext context,
                                IMapper autoMapper,
                                ILogger<ChallengeService> logger,
                                AdminTablesMapper adminTableMapper,
                                IConfiguration configuration,
                                ITransactionService transactionService
                                )
        {
            _context = context;
            _autoMapper = autoMapper;
            _logger = logger;
            _adminTableMapper = adminTableMapper;
            _imageUploader = ImageUploaderFactory.CreateImageUploader();
            _configuration = configuration;
            _transactionService = transactionService;
            _notificationSerivce = NotificationServiceFactory.CreateNotificationService(_configuration["Notifications:Provider"]);
        }

        #region Public Methods

        #region Challenge Controller Methods

        public IResponseModel<EmptyBodyResponse> ClientChallengeAchieved(string userId, int challengeId)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var challenge = _context.Challenges.FirstOrDefault(x => x.Id == challengeId);
                var notificationTemplate = _context.NotificationTemplates.FirstOrDefault(x => x.UniqueName == "Client Challenge Achievement");
                var user = _context.Users.First(x => x.Id == userId);

                if (challenge == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }
                if (challenge.ChallengeStatusId != (int)ChallengeStatus.Complete && challenge.ChallengeTypeId == (int)ChallengeType.Client &&
                    (challenge.UnlimitedDuration || (challenge.StartDate < DateTime.Today && DateTime.Today < challenge.EndDate)))
                {
                    GrantReward(challenge, user);

                    _context.AchievedChallenges.Add(new AchievedChallengeModel
                    {
                         ChallengeId = challenge.Id,
                          Date = DateTime.Now,
                           UserId = userId                            
                    });

                    bool notifyResult = false;
                    if (notificationTemplate != null)
                        notifyResult = NotifyUser(user, notificationTemplate);

                    _context.Notifications.Add(new NotificationModel
                    {
                        CreatedDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        Message = notificationTemplate != null ? notificationTemplate.Message : "Message not send",
                        NotifyResult = notifyResult.ToString(),
                        Recipient = user.DeviceID,
                        UserId = user.Id
                    });

                    _context.SaveChanges();
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }

        }
        public IResponseModel<GetClientChallengesResponse> GetClientChallenges(string userId)
        {
            var response = new ResponseModel<GetClientChallengesResponse>();
            try
            {
                //TODO: Check about challenge duration
                var clientChallenges = _context.Challenges.Where(x => 
                                                x.ChallengeTypeId == (int)ChallengeType.Client &&
                                                (x.UnlimitedDuration == true ||  x.EndDate > DateTime.Now))
                                     ?.Select(x => new ClientChallengeDto { Id = x.Id, ImageUrl = x.ImageUrl })
                                     ?.ToList();
                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = new GetClientChallengesResponse
                {
                    ClientChallenges = clientChallenges
                };

                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }
        public IResponseModel<GetChallengesAchievedResponse> GetAchievedChallenges(string userid)
        {
            var response = new ResponseModel<GetChallengesAchievedResponse>();
            try
            {
                response.Data = new GetChallengesAchievedResponse
                {
                    Challenges = _autoMapper.Map<List<Challenge>, List<ChallengeDto>>
                                   (
                                   _context.AchievedChallenges
                                   .Include(x => x.Challenge)
                                   .Where(x => x.UserId == userid)
                                   .OrderByDescending(x => x.Date)
                                   .Select(x => x.Challenge)
                                   .ToList()
                                   )
                };
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }
        public IResponseModel<GetChallengesAchievedResponse> GetLastAchievedChallenge(string userid)
        {
            var response = new ResponseModel<GetChallengesAchievedResponse>();
            try
            {
                response.Data = new GetChallengesAchievedResponse
                {
                    Challenges = _autoMapper.Map<List<Challenge>, List<ChallengeDto>>
                                   (
                                   _context.AchievedChallenges
                                   .Include(x => x.Challenge)
                                   ?.Where(x => x.UserId == userid)
                                   ?.OrderByDescending(x => x.Date)
                                   ?.Select(x => x.Challenge)
                                   ?.Take(1)
                                   ?.ToList()
                                   )
                };
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        #endregion Challenge Controller Methods

        #region Admin Methods

        public IResponseModel<GetChallengeMetadataResponse> GetChallengeMetadata()
        {
            var response = new ResponseModel<GetChallengeMetadataResponse>();
            try
            {
                var actionTypes = _context.ChallengeActionTypes.Select(x => new ChallengeActionTypeDto { Id = x.Id, Name = x.Name })
                                                               .ToList();

                var challengeTypes = _context.ChallengeTypes.Select(x => new ChallengeTypeDto { Id = x.Id, Name = x.Name })
                                    .ToList();

                var statuses = _context.ChallengeStatuses.Select(x => new ChallengeStatusDto { Id = x.Id, Name = x.Name })
                                                      .ToList();
                var challengesCategories = _context.ChallengeCategories.Select(x => new ChallengeCategoryDto { Id = x.Id, Name = x.Name }).ToList();


                response.Data = new GetChallengeMetadataResponse
                {
                    ActionTypes = actionTypes,
                    ChallengeStatuses = statuses,
                    ChallengeTypes = challengeTypes,
                    Filters = _adminTableMapper.ChallengesTableFiltersConditionsMap,
                    ChallengeCategories = challengesCategories
                };
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<ExploreChallengeCriteriaResponse> ExploreChallengeCriteria(int challengeId)
        {
            var response = new ResponseModel<ExploreChallengeCriteriaResponse>();
            try
            {
                var rules = _context.ChallengeRules.Include(x => x.Condition)
                                                   .Include(x => x.ActionType)
                                                   .Where(x => x.ChallengeId == challengeId)
                                                   .ToList();

                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = new ExploreChallengeCriteriaResponse
                {
                    ChallengeId = challengeId,
                    Criterias = _autoMapper.Map<IEnumerable<ChallengeRule>, IEnumerable<ChallengeRuleDto>>(rules)
                };
                return response;
            }
             catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<ExploreCompletedUsersResponse> ExploreCompletedUsers(int challengeId)
        {
            var response = new ResponseModel<ExploreCompletedUsersResponse>();
            try
            {
                var completedUsers = _context.AchievedChallenges
                                             .Include(x=> x.User)
                                             .Where(x => x.ChallengeId == challengeId && !string.IsNullOrWhiteSpace(x.UserId) )
                                             .ToList();
                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = new ExploreCompletedUsersResponse
                {
                     ChallengeId = challengeId,
                     Users = completedUsers?.Select(x=> new ChallengeComletedUserDto
                     {
                          Id = x.UserId,
                          UserName = x.User != null ? x.User.UserName : string.Empty
                     })
                };
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<GetChallengesResponse> GetChallenges(FilterRequest filterRequest = null)
        {
            var response = new ResponseModel<GetChallengesResponse>();
            try
            {
                if (filterRequest == null)
                {

                    var challenges = _context.Challenges.Include(x => x.Rules).ThenInclude(y => y.Condition)
                                                        .Include(x => x.Rules).ThenInclude(y => y.ActionType)
                                                        .Include(x => x.ChallenegeStatus)
                                                        .Include(x => x.ChallengeType)
                                                        .Include(x=>x.ChallengeCategory).ToList();
                    response.Data = new GetChallengesResponse
                    {
                        Challenges = _autoMapper.Map<List<Challenge>,
                                                    List<ChallengeDto>>(challenges)
                    };
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                else
                {
                    var dbChallenges = _context.Challenges.Include(x => x.Rules).ThenInclude(y => y.Condition)
                                                          .Include(x => x.Rules).ThenInclude(y => y.ActionType)
                                                          .Include(x => x.ChallenegeStatus)
                                                          .Include(x => x.ChallengeType)
                                                          .Include(x => x.ChallengeCategory).ToList();

                    var challenges = _autoMapper.Map<List<Challenge>, List<ChallengeDto>>(dbChallenges);

                    IEnumerable<ChallengeDto> result = new List<ChallengeDto>(challenges);

                    if (!string.IsNullOrWhiteSpace(filterRequest.Search))
                        result = SearchChallenges(result, filterRequest.Search);

                    if (filterRequest.Filters != null && filterRequest.Filters.Count() > 0)
                        result = FilterHelper.Filter(result, filterRequest.Filters, _adminTableMapper.ChallengesTableMap);

                    response.Data = new GetChallengesResponse
                    {
                        Challenges = result?.Skip((filterRequest.Page - 1) * filterRequest.AmountPerPage)?.Take(filterRequest.AmountPerPage),
                        Summary = filterRequest,
                    };
                    response.Data.Summary.TotalAmount = result?.Count() ?? 0;
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
               

            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int) InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> CreateChallenge(CreateChallengeReqeust request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var challengeModel = _autoMapper.Map<CreateChallengeReqeust, Challenge>(request);
                challengeModel.CreationDate = DateTime.Now;
                challengeModel.UpdateDate = DateTime.Now;
                var rules = _autoMapper.Map<IEnumerable<ChallengeRuleDto>, IEnumerable<ChallengeRule>>(request.Rules)
                                       .Select(x=> { x.CreateDate = DateTime.Now; return x; })
                                       .ToList();


                if (!string.IsNullOrWhiteSpace(request.ImageBase64))
                {
                    var image = request.ImageBase64.ParseFromBase64();
                    var bytes = Convert.FromBase64String(image.data);
                    var imageUrl = _imageUploader.UploadImage(bytes, $"{request.Name}", image.mimeType, "ChallengeImgages");
                    if (!string.IsNullOrWhiteSpace(imageUrl))
                        challengeModel.ImageUrl = imageUrl;
                }
                challengeModel.ChallengeCategoryId=request.UnlimitedDuration?1:2;

                challengeModel.ChallengeStatusId = (int)ChallengeStatus.Scheduled;
                challengeModel.Rules = rules;
                _context.Challenges.Add(challengeModel);
                _context.SaveChanges();

                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> UpdateChallenge(UpdateChallengeRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var challenge = _context.Challenges.Include(x => x.Rules)
                                                   .ThenInclude(y=> y.Condition)
                                                   .First(x=> x.Id == request.Id);
                if (challenge.ChallengeStatusId == (int)ChallengeStatus.Ongoing)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                _context.ChallengeRules.RemoveRange(challenge.Rules);
                _context.SaveChanges();

                _autoMapper.Map(request, challenge, typeof(UpdateChallengeRequest), typeof(Challenge));

                if (!string.IsNullOrWhiteSpace(request.ImageBase64))
                {
                    var image = request.ImageBase64.ParseFromBase64();
                    var bytes = Convert.FromBase64String(image.data);
                    var imageUrl = _imageUploader.UploadImage(bytes, $"{request.Name}", image.mimeType, "ChallengeImgages");
                    if (!string.IsNullOrWhiteSpace(imageUrl))
                        challenge.ImageUrl = imageUrl;
                }
                challenge.ChallengeCategoryId = request.UnlimitedDuration ? 1 : 2;

                challenge.UpdateDate = DateTime.Now;
                challenge.Rules = _autoMapper.Map<List<ChallengeRuleDto>,List<ChallengeRule>>(request.Rules)
                                             .Select(x=> { x.CreateDate = DateTime.Now; return x; })
                                             .ToList();

                _context.Challenges.Update(challenge);
                _context.SaveChanges();

                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }

        }

        /// <summary>
        /// Method recieve userId and social event details,
        /// base on active challenges check for challenge achivment
        /// by user or mixed palces applied.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="social"></param>
        public void CheckChallengeAchievment(string userId,SocialBaseDto social)
        {
            var challenges = _context.Challenges.Where(y => (y.ChallengeTypeId == (int)ChallengeType.ServerUser ||
                                                             y.ChallengeTypeId == (int)ChallengeType.ServerMixedPlace) &&
                                                             y.ChallengeStatusId != (int)ChallengeStatus.Complete &&
                                                             (y.UnlimitedDuration || (y.StartDate < DateTime.Today && y.EndDate > DateTime.Today)))
                                                .Include(x => x.Rules)
                                                .GroupBy(keySelector: x => x.ChallengeTypeId,
                                                         elementSelector: e => e,
                                                         resultSelector: (k, g) => new { Type = k, Group = g })
                                                .ToList();
            if (challenges?.Count() == 0) return;

            var activeChallenges = challenges.FirstOrDefault(x => x.Type == (int)ChallengeType.ServerUser)?.Group;

            if (activeChallenges != null)
                CheckActiveChallenge(userId,social, activeChallenges);

            var passiveChallenges = challenges.FirstOrDefault(x => x.Type == (int)ChallengeType.ServerMixedPlace)?.Group;

            if (passiveChallenges != null)
                CheckPassiveChallenge(userId,social, passiveChallenges);

        }

        #endregion  Admin Methods

        #endregion Public Methods

        #region Private Methods


        private IEnumerable<ChallengeDto> SearchChallenges(IEnumerable<ChallengeDto> challenges, string searchTerm)
        {
            return challenges.Where(x => (x.Name != null && x.Name.Contains(searchTerm)) ||
                                         (x.ChallengeType?.Name != null && x.ChallengeType.Name.Contains(searchTerm)) ||
                                         (x.ChallengeStatus?.Name != null && x.ChallengeStatus.Name.Contains(searchTerm)) ||
                                         x.StartDate.ToString().Contains(searchTerm)||
                                         x.EndDate.ToString().Contains(searchTerm)||
                                         x.UnlimitedDuration.ToString().Contains(searchTerm)||
                                         x.Reward.ToString().Contains(searchTerm) ||
                                         x.CompletedUsersCount.ToString().Contains(searchTerm));
        }

        private void CheckPassiveChallenge(string userId,SocialBaseDto social, IEnumerable<Challenge> challengesCollection)
        {
            var mp = _context.MixedPlaces.Include(x => x.AchievedChallenges)
                                         .Include(x=> x.User)
                                         .FirstOrDefault(x => x.X == social.MPCoordinate.X &&
                                                              x.Y == social.MPCoordinate.Y &&
                                                              x.Z == social.MPCoordinate.Z &&
                                                              x.OwnerId != null);
            if (mp == null) return;
            var challenges = challengesCollection.Where(x => mp.AchievedChallenges
                                                              .FirstOrDefault(m => m.ChallengeId == x.Id) == null);

            var notificationTemplate = _context.NotificationTemplates.FirstOrDefault(x=> x.UniqueName == "Passive Challenge Achievement");

            if (challenges == null || challenges.Count() == 0) return;

            var mpLikes = _context.MPLikes.Where(x => x.MPId == mp.Id)?.Sum(x => x.Id) ?? 0;
            var mpShares = _context.MPComments.Where(x => x.MPId == mp.Id)?.Sum(x => x.Id) ?? 0;
            var mpComments = _context.MPComments.Where(x => x.MPId == mp.Id)?.Sum(x => x.Id) ?? 0;
            var mpVisits = _context.MPVisits.Where(x => x.MPId == mp.Id)?.Sum(x => x.PublicCounter) ?? 0;

            foreach (var challenge in challenges)
            {
                var challengeComplete = CheckRules(challenge.Rules,challenge.LogicalOperator ?? 0 , mpLikes, mpShares, mpComments, mpVisits);
                if (challengeComplete)
                {
                    GrantReward(challenge, mp.User);

                    _context.AchievedChallenges.Add(new AchievedChallengeModel
                    {
                        ChallengeId = challenge.Id,
                        UserId = mp.OwnerId,
                        Date = DateTime.Now
                    });

                    bool notifyResult = false;
                    if (notificationTemplate != null)
                        notifyResult = NotifyUser(mp.User, notificationTemplate);

                    _context.Notifications.Add(new NotificationModel
                    {
                        CreatedDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        Message = notificationTemplate != null ? notificationTemplate.Message : "Message not send",
                        NotifyResult = notifyResult.ToString(),
                        Recipient = mp.User.DeviceID,
                        UserId = mp.User.Id
                    });
                }
            }
            _context.SaveChanges();
        }

        private void CheckActiveChallenge(string userId,SocialBaseDto social, IEnumerable<Challenge> challengesCollection)
        {

            var user = _context.Users.Include(x => x.AchievedChallenges)
                                     .First(x => x.Id == userId);
            var notificationTemplate = _context.NotificationTemplates.FirstOrDefault(x => x.UniqueName == "Active Challenge Achievement");

            var challenges = challengesCollection.Where(x => user.AchievedChallenges
                                                             .FirstOrDefault(u => u.ChallengeId == x.Id) == null);

            if (challenges == null || challenges.Count() == 0) return;

            var userLikes = _context.MPLikes.Where(x => x.UserId == userId)?.Sum(x => x.Id) ?? 0;
            var userShares = _context.MPComments.Where(x => x.UserId == userId)?.Sum(x => x.Id) ?? 0;
            var userComments = _context.MPComments.Where(x => x.UserId == userId)?.Sum(x => x.Id) ?? 0;
            var userVisits = _context.MPVisits.Where(x => x.UserId == userId)?.Sum(x => x.PublicCounter) ?? 0;
            foreach (var challenge in challenges)
            {
                var challengeComplete = CheckRules(challenge.Rules,challenge.LogicalOperator ?? 0, userLikes, userShares, userComments, userVisits);
                if (challengeComplete)
                {
                    GrantReward(challenge, user);

                    _context.AchievedChallenges.Add(new AchievedChallengeModel
                    {
                        ChallengeId = challenge.Id,
                        UserId = user.Id,
                        Date = DateTime.Now
                    });

                    bool notifyResult = false;
                    if(notificationTemplate != null)
                        notifyResult = NotifyUser(user,notificationTemplate);

                    _context.Notifications.Add(new NotificationModel
                    {
                         CreatedDate = DateTime.Now,
                         UpdateDate = DateTime.Now,
                         Message = notificationTemplate != null ? notificationTemplate.Message : "Message not send",
                         NotifyResult = notifyResult.ToString(),
                         Recipient = user.DeviceID,
                         UserId = user.Id
                    });
                }
            }
            _context.SaveChanges();
        }

        private bool CheckRules(IEnumerable<ChallengeRule> rules,int logicalOperator, int likes, int shares, int comments, int visits)
        {
            foreach (var rule in rules)
            {
                var checkConditionResult = false;
                switch (rule.ActionTypeId)
                {
                    case (int)ActionType.Visit:
                        checkConditionResult = CheckCondition(rule.ConditionId ?? 0, visits, rule.Amount);
                        break;
                    case (int)ActionType.Like:
                        checkConditionResult = CheckCondition(rule.ConditionId ?? 0, likes, rule.Amount);
                        break;
                    case (int)ActionType.Comment:
                        checkConditionResult = CheckCondition(rule.ConditionId ?? 0, comments, rule.Amount);
                        break;
                    case (int)ActionType.Share:
                        checkConditionResult = CheckCondition(rule.ConditionId ?? 0, shares, rule.Amount);
                        break;
                    default:
                        return false;
                }

                if (checkConditionResult)
                {
                    if (logicalOperator == (int)LogicalOperatorType.OR)
                        return true;
                    else
                        continue;
                }
                else
                {
                    if (logicalOperator == (int)LogicalOperatorType.AND)
                        return false;
                    else
                        continue;
                }
            }

            if (logicalOperator == (int)LogicalOperatorType.AND)
                return true;
            else
                return false;
        }

        private bool CheckCondition(int condition, int currentAmount, int targetAmount)
        {
            switch (condition)
            {
                case (int)ConditionsType.EqualTo:
                    return (currentAmount == targetAmount);
                case (int)ConditionsType.GreaterThan:
                    return (currentAmount > targetAmount);
                case (int)ConditionsType.GreaterThanOrEqualTo:
                    return (currentAmount >= targetAmount);
                case (int)ConditionsType.LessThan:
                    return (currentAmount < targetAmount);
                case (int)ConditionsType.LessThanOrEqualTo:
                    return (currentAmount <= targetAmount);
                default:
                    return false;
            }
        }

        //Add creattion of transactions
        private void GrantReward(Challenge challenge, User user)
        {
            user.CoinBalance += challenge.Reward;
            _context.Update(user);
            _transactionService.AddTransaction(TransactionCategoryEnum.ChallengeReward, challenge.Reward, 0, "", user.Id);
            _context.SaveChanges();
        }

        private bool NotifyUser(User user, NotificationTemplateModel notificationTemlate)
        {
           return  _notificationSerivce.Notify(user.DeviceID, notificationTemlate.Message,"", user.DeviceOSId ?? 0);
        }
      

        #endregion Private Methods


    }
}
