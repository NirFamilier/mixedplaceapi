﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public interface ITransactionService
    {
        IEnumerable<TransactionCategory> GetAllTransactionTypes();
        void AddTransaction(TransactionCategoryEnum category, decimal valueInCoins, decimal valueInMoney, string currencyCode, string userId);
        decimal GetCoinsStatus(string userId);
        IEnumerable<MoneyTotal> GetMoneySpent(string userId);
        IEnumerable<TransactionDto> GetTransactions(string userid);
        IEnumerable<Transaction> GetTransactions(FilterRequest filter = null);
        GetUserCoinsBalance GetUserCoinsBalance(string userId);
    }
}
