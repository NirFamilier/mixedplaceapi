﻿using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Notifications.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public interface INotificationsService
    {
        IResponseModel<EmptyBodyResponse> NotifyAllUsers(AdminNotifyAllRequest request);
        IResponseModel<EmptyBodyResponse> NotifyAllChallenges(AdminNotifyAllRequest request);
        IResponseModel<EmptyBodyResponse> NotifyAllMixedPlaceOwners(AdminNotifyAllRequest request);
        IResponseModel<EmptyBodyResponse> NotifyUser(AdminNotifyUserRequest request);
        IResponseModel<EmptyBodyResponse> NotifyMixedPlaceOwner(AdminNotifyMixedPlaceRequest request);
        IResponseModel<EmptyBodyResponse> NotifyChallenge(AdminNotifyChallengeRequest request);
        IResponseModel<EmptyBodyResponse> SendQueuedNotifications();
        bool SendUpdateUserStatusEmail(string userId, UserStatus userStatus);
        bool SendReportToAdmin(int reportId,string reporterName,string ownerName, string reportText);
        bool NotifyUserAboutReport(string userId, string reportText);
    }
}
