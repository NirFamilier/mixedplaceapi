﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Data;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Helpers;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _autoMapper;
        private readonly ILogger<TransactionService> _logger;
        private readonly AdminTablesMapper _adminTableMapper;

        public TransactionService(ApplicationDbContext context,
                         IMapper autoMapper,
                          ILogger<TransactionService> logger,
                         AdminTablesMapper adminTableMapper)
        {
            _context = context;
            _autoMapper = autoMapper;
            _logger = logger;
            _adminTableMapper = adminTableMapper;
        }

        public IEnumerable<TransactionCategory> GetAllTransactionTypes()
        {
            return _context.TransactionCategories;
        }


        public void AddTransaction(TransactionCategoryEnum category, decimal valueInCoins, decimal valueInMoney,string currencyCode, string userId)
        {
            var trans = new Transaction()
            {
                CreationDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Coins = valueInCoins,
                Money = valueInMoney,
                CurrencyCode = currencyCode,
                OwnerId = userId,
                TransactionCategoryId = (int)category
            };
            _context.Transactions.Add(trans);
            _context.SaveChanges();
            var user = _context.Users.Where(x => x.Id == userId).FirstOrDefault();
            user.CoinBalance = GetCoinsStatus(userId);
            _context.Update(user);
        }

        public decimal GetCoinsStatus(string userId)
        {
            return _context.Transactions.Where(x => x.OwnerId == userId ).Sum(i => i.Coins);
        }

        public IEnumerable<MoneyTotal> GetMoneySpent(string userId)
        {
            var listOfTotals = _context.Transactions.Where(x => x.OwnerId == userId && x.Money > 0)
                                         .GroupBy(k => k.CurrencyCode,
                                                  g => g,
                                                  (k, g) => new MoneyTotal
                                                  {
                                                      CurrencyCode = k,
                                                      Money = g.Sum(x => x.Money)
                                                  });
            return listOfTotals;
        }

        public GetUserCoinsBalance GetUserCoinsBalance(string userId)
        {
            var obj = new GetUserCoinsBalance();

            var listOfTotals = _context.Transactions.Where(x => x.OwnerId == userId)
                                         .GroupBy(k => k.TransactionCategoryId);
            foreach(var totals in listOfTotals)
            {

            }
            return obj;
        }

        //TransactionDto
        public IEnumerable<TransactionDto> GetTransactions(string userid)
        {
            return _autoMapper.Map<IEnumerable<Transaction> ,IEnumerable<TransactionDto>>(_context.Transactions.Include(x => x.TransactionCategory).Where(x => x.OwnerId == userid));
        }


        //TransactionDto
        public IEnumerable<Transaction> GetTransactions(FilterRequest filter = null)
        {
            if (filter == null)
            {
                return _context.Transactions;
            }
            else
            {
                var transactions = _context.Transactions;
                if (filter.Filters != null && filter.Filters.Count() > 0)
                    return  FilterHelper.Filter(transactions, filter.Filters, _adminTableMapper.TransactionsTableMap);
                return _context.Transactions;
            }
        }



    }
}
