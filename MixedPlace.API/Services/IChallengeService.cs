﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.MPChallenge.RequestModels;
using MixedPlace.API.Models.MPChallenge.ResponseModels;
using MixedPlace.API.Models.User.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public interface IChallengeService
    {
        IResponseModel<EmptyBodyResponse> UpdateChallenge(UpdateChallengeRequest request);
        void CheckChallengeAchievment(string userId,SocialBaseDto social);
        IResponseModel<EmptyBodyResponse> ClientChallengeAchieved(string userId, int challengeId);
        IResponseModel<GetChallengesAchievedResponse> GetAchievedChallenges(string userid);
        IResponseModel<GetChallengesAchievedResponse> GetLastAchievedChallenge(string userid);
        IResponseModel<GetClientChallengesResponse> GetClientChallenges(string userId);
        IResponseModel<GetChallengeMetadataResponse> GetChallengeMetadata();
        IResponseModel<GetChallengesResponse> GetChallenges(FilterRequest filterRequest = null);
        IResponseModel<EmptyBodyResponse> CreateChallenge(CreateChallengeReqeust reqeust);
        IResponseModel<ExploreChallengeCriteriaResponse> ExploreChallengeCriteria(int challengeId);
        IResponseModel<ExploreCompletedUsersResponse> ExploreCompletedUsers(int challengeId);

    }
}
