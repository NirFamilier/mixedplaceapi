﻿using MixedPlace.API.Models.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public interface IHistoryManagmentService
    {
        bool MoveMixedPlaceDataToHistoryTable(MP mp);
    }
}
