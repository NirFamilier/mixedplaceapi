﻿using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map.ResponseModel;
using MixedPlace.API.Models.MPDesign.ResponseModel;
using MixedPlace.API.Models.MPModel.ResponseModels;
using MixedPlace.API.Models.User.ResponseModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using MixedPlace.API.Models.Map.RequestModel;
using MixedPlace.API.Models.Map;
using MixedPlace.API.DataTransferObjects;
using Microsoft.AspNetCore.Http;
using MixedPlace.API.Models.MPModel.RequestModels;
using MixedPlace.API.Models.MPDesign.RequestModel;
using System.IO;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Controllers;

namespace MixedPlace.API.Services
{
    public interface IMapService
    {
        IResponseModel<EmptyBodyResponse> UpdateMPStatus(UpdateMPStatusRequest request);
        IResponseModel<GetMPMetadataResponse> GetMPMetadata();
        Task<IResponseModel<UserDataResponseModel>> GetMPUserData(GetMPUserDataRequest request);
        IResponseModel<MPModelResponseModel> GetAllModels();
        IResponseModel<MPUserModelsResponse> GetAllModelsPerUser(string id);
        IResponseModel<GetAllMPCategoriesResponse> GetAllMPCategories();
        List<MP> CreateMixedPlacesWithPrice(List<MixedPlaceRequestModel> mixedPlaceList);
        IResponseModel<EmptyBodyResponse> CreateNewModel(CreateNewModelRequest formFile);
        IResponseModel<EmptyBodyResponse> UserRecieveNewModel(string userId, List<UserRecieveNewModelItem> request);
        Task<IResponseModel<EmptyBodyResponse>> BuyMp(string userId, MPDto mpDto);
        Task<IResponseModel<GetMPItemsResponse>> GetMPItems(GetMPItemRequest request);
        IResponseModel<EmptyBodyResponse> SetMPItems(string userId, SetMPItemsRequest request);
        Task<IResponseModel<GetMixedPlaceInfoResponse>> GetMixedPlaceInfo(GetMixedPlaceInfoRequest request);
        IResponseModel<GetMixedPlacesResponse> GetMixedPlaces(FilterRequest request = null);
        IResponseModel<EmptyBodyResponse> UpdateMixedPlace(UpdateMixedPlaceRequest request);
        IResponseModel<EmptyBodyResponse> UpdateMixedPlaceOnwer(UpdateMixedPlaceOwnerRequest request);
        FileStream GetModelData(string fileId);
        IResponseModel<MPModelResponseModel> GetAllModelItems();
        IResponseModel<EmptyBodyResponse> AddNewModel(AddNewModelRequest modelRequest);
        IResponseModel<EmptyBodyResponse> UpdateModelStatus(int id, int status);
        IResponseModel<MPModelResponseModel> GetModels(FilterRequest request);
        IResponseModel<ModelCategoryResponse> GetModelCategories();
        IResponseModel<GetMPSubCategoryResponse> GetMPSubCategories();
        IResponseModel<ModelSubCategoryResponse> GetModelSubCategories(GetModelSubCategoryRequest modelCategoryIds);
        IResponseModel<GetModelMetadataResponse> GetModelMetadata();
        IResponseModel<bool> UpdateMPUserData(string userId, MPUserDataDto request);
        IResponseModel<GetModelResponse> GetModel(int id);
        IResponseModel<bool> IsModelUniqueidExists(IsUniqueIdExistsRequest request);
        IResponseModel<EmptyBodyResponse> EnableModel(EnableModelRequest request);
        IResponseModel<GetModelsSelectItemsResponse> GetModelsSelectItems();
        Task<MPAdressDto> GetMPAdress(MPCoordinateDto mpCoordinate);
    }
}
