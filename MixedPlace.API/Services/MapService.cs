﻿using MixedPlace.API.Data;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.Map.ResponseModel;
using MixedPlace.API.Models.MPDesign;
using MixedPlace.API.Models.MPDesign.ResponseModel;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.MPModel.ResponseModels;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.ResponseModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map.RequestModel;
using MixedPlace.API.DataTransferObjects;
using AutoMapper;
using System.IO;
using MixedPlace.ImageUploader.Abstractions;
using MixedPlace.ImageUploader.Implementations;
using MixedPlace.API.Models.MPModel.RequestModels;
using MixedPlace.API.Models.MPDesign.RequestModel;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Helpers;
using MixedPlace.API.Models.Common.MixedPlaceExtensions;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Text;

namespace MixedPlace.API.Services
{
    public class MapService : IMapService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _autoMapper;
        private readonly IImageUploader _imageUploader;
        private readonly ILogger<MapService> _logger;
        private readonly AdminTablesMapper _adminTableMapper;
        private readonly ITransactionService _transactionService;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configurations;

        public MapService(ApplicationDbContext context,
                          IMapper autoMapper,
                           ILogger<MapService> logger,
                           AdminTablesMapper adminTableMapper,
                           ITransactionService transactionService,
                           IMapper mapper, IConfiguration configurations)
        {
            _context = context;
            _autoMapper = autoMapper;
            _imageUploader = ImageUploaderFactory.CreateImageUploader();
            _logger = logger;
            _adminTableMapper = adminTableMapper;
            _transactionService = transactionService;
            _mapper = mapper;
            _configurations = configurations;
        }

        public async Task<IResponseModel<UserDataResponseModel>> GetMPUserData(GetMPUserDataRequest request)
        {
            var response = new ResponseModel<UserDataResponseModel>();
            try
            {
                var userData = new UserDataResponseModel();
                var mpPrice = _context.Prices.First();
                foreach (var item in request.MPCoordinates)
                {
                    MP mp = _context.MixedPlaces.Include(x=> x.User).Where(b => b.X == item.X &&
                                                            b.Y == item.Y &&
                                                            b.Z == item.Z).FirstOrDefault();
                    if (mp != null && mp.User != null)
                    {
                        userData.List.Add(new MPUserData
                        {
                            MPCoordinates = new MPCoordinateDto { X = mp.X, Y = mp.Y, Z = mp.Z },
                            MPName = mp.Name,
                            Owner = _autoMapper.Map<User,UserBaseDto>(mp.User)
                        });
                    }
                    else if (mp == null)
                    {
                        var mpAddress = await GetMPAdress(new MPDto { X = item.X, Y = item.Y, Z = item.Z });
                        if (mpAddress == null)
                        {
                            response.StatusCode = (int)InnerErrorCode.MPAddressMotFound;
                            return response;
                        }

                        mp = new MP
                        {
                            CreationDate = DateTime.Now,
                            UpdateDate = DateTime.Now,
                            X = item.X,
                            Y = item.Y,
                            Z = item.Z,
                            PriceId = mpPrice.Id,
                            Country = mpAddress.Country,
                            City = mpAddress.City
                        };

                        _context.Add(mp);
                        _context.SaveChanges();

                        userData.List.Add(new MPUserData
                        {
                            MPCoordinates = new MPCoordinateDto { X = mp.X, Y = mp.Y, Z = mp.Z }
                        });
                    }
                    else
                    {
                        userData.List.Add(new MPUserData
                        {
                            MPCoordinates = new MPCoordinateDto { X = mp.X, Y = mp.Y, Z = mp.Z },
                            MPName = mp.Name
                        });
                    }
                }
                response.Data = userData;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                response.Description = ex.Message;
                return response;
            }
        }

        public IResponseModel<MPModelResponseModel> GetAllModels()
        {
            var response = new ResponseModel<MPModelResponseModel>();
            try
            {
                List<MPModel> mpModelList = _context.Models
                                                    .Include(x => x.Bundle)
                                                    .Include(x=>x.MPModelCatSubCats).ThenInclude(y=>y.ModelCategory)
                                                    .Include(x => x.MPModelCatSubCats).ThenInclude(y => y.ModelSubCategory)
                                                    .Where(x => x.ModelStatusId == (int)ModelStatus.Active).ToList<MPModel>();
                List<MPModelDto> outlist = new List<MPModelDto>();
                foreach (var item in mpModelList)
                {
                    var inItem = _autoMapper.Map<MPModel, MPModelDto>(item);
                    inItem.ModelCategories = new List<ModelCatSubCatDto>();
                    foreach (var i in item.MPModelCatSubCats)
                    {
                        inItem.ModelCategories.Add(new ModelCatSubCatDto()
                        {
                            Category = new ModelCategoryDto()
                            {
                                Id = i.ModelCategory.Id,
                                Name = i.ModelCategory.Name
                            },
                            SubCategory = new ModelSubCategoryDto()
                            {
                                Id = i.ModelSubCategory.Id,
                                Name = i.ModelSubCategory.Name
                            }
                        });
                    }
                    outlist.Add(inItem);
                }
                var responseData = new MPModelResponseModel
                {
                    List = outlist//_autoMapper.Map<List<MPModel>, List<MPModelDto>>(mpModelList)
                };
                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = responseData;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                response.Description = ex.Message;
            }
            return response;
        }

        public IResponseModel<EmptyBodyResponse> EnableModel(EnableModelRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var model = _context.Models.FirstOrDefault(x => x.Id == request.Id);
                if (model == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                model.ModelStatusId = (int)ModelStatus.Active;
                _context.Update(model);
                _context.SaveChanges();
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        /// <summary>
        /// Method retrieve all models for specific user.
        /// This data stored in many-to-many relationship table
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IResponseModel<MPUserModelsResponse> GetAllModelsPerUser(string id)
        {
            IResponseModel<MPUserModelsResponse> response = new ResponseModel<MPUserModelsResponse>();
            try
            {
                var mpModels = _context.MPModelsPerUsers.Where(x => x.UserId == id)
                                                         .Include(x => x.MPModel).ThenInclude(x => x.Bundle)
                                                         .ToList();
                var modelsList = new List<UserModelDto>();
                foreach (var mpuModel in mpModels)
                {
                    var mpModelDto = new UserModelDto
                    {
                        BundleImageName = mpuModel.MPModel.Bundle?.Name,
                        Url = mpuModel.MPModel.Bundle?.Url,
                        Name = mpuModel.MPModel.Name,
                        TotalModelsCount= mpuModel.TotalModelsCount,
                        RemainingModelsCount = mpuModel.RemainingModelsCount,
                        UniqueId = mpuModel.MPModel.UniqueId,
                        UpdateDate = mpuModel.MPModel.UpdateDate ?? DateTime.MinValue
                    };
                    modelsList.Add(mpModelDto);
                }

                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = new MPUserModelsResponse { List = modelsList };
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public async Task<MPAdressDto> GetMPAdress(MPCoordinateDto mpCoordinate)
        {
            try
            {
                double longitude = (double)mpCoordinate.X / 1000000;
                double latitude = (double)mpCoordinate.Y / 1000000;
                var mpAddress = new MPAdressDto();
                using (var httpClient = new HttpClient())
                {
                    var getResponse = await httpClient.GetAsync($"https://maps.googleapis.com/maps/api/geocode/json?latlng={latitude},{longitude}&key={_configurations["Auth:GoogleMap:APIKey"]}");
                    if (getResponse.IsSuccessStatusCode)
                    {
                        var contentString = await getResponse.Content.ReadAsStringAsync();
                        if(!string.IsNullOrWhiteSpace(contentString))
                        {
                            var addressResults = JsonConvert.DeserializeObject<RootObjectDto>(contentString);
                            if (addressResults.Status == "OK")
                            {
                                var addressCountry = addressResults.Results[0].Address_components.Where(item => item.Types.Contains("country")).FirstOrDefault();
                                var addressCity = addressResults.Results[0].Address_components.Where(item => item.Types.Contains("locality")).FirstOrDefault();
                                mpAddress.Country = addressCountry != null ? addressCountry.Long_name : null;
                                mpAddress.City = addressCity != null ? addressCity.Long_name : null;
                                return mpAddress;
                                
                            }
                        }
                    }
                    return null;
                }
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public async Task<IResponseModel<GetMixedPlaceInfoResponse>> GetMixedPlaceInfo(GetMixedPlaceInfoRequest request)
        {
            var response = new ResponseModel<GetMixedPlaceInfoResponse>();
            try
            {
                GetMixedPlaceInfoResponse output = new GetMixedPlaceInfoResponse();
                output.List = new List<GetMixedPlaceInfoItem>();
                var mpPrice = _context.Prices.First();

                foreach (MPCoordinateDto mpDto in request.MPCoordinates)
                {
                    MP mp = _context.MixedPlaces.Include(x => x.MPShares)
                                                .Include(x => x.MPComments)
                                                .Include(x => x.MPLike)
                                                .Include(x => x.MPVisits)
                                                .Include(x => x.MPCategories).ThenInclude(x => x.MPCategory)
                                                .Include(x => x.MPCategories).ThenInclude(x => x.MPSubCategory)
                                                .Where(x => x.X == mpDto.X && x.Y == mpDto.Y && x.Z == mpDto.Z).FirstOrDefault();
                    if (mp != null)
                    {
                       var categories = new List<MPCatSubCatDto>();
                       foreach (var cat in mp.MPCategories)
                       {
                            categories.Add(new MPCatSubCatDto
                            {
                                 Category = new MPCategoryDto
                                 {
                                      Id = cat.MPCategory.Id,
                                      Name = cat.MPCategory.CategoryName
                                 },
                                 SubCategory = new MPSubCategoryDto
                                 {
                                     Id = cat.MPSubCategory?.Id ?? 0,
                                     Name = cat.MPSubCategory?.Name,
                                     MPCategoryId = cat.MPCategoryId ?? 0
                                 }
                            });
                       }


                        GetMixedPlaceInfoItem mpResponse = new GetMixedPlaceInfoItem();

                        if (mp.OwnerId != null)
                        {
                            mpResponse.IsFree = false;
                            mpResponse.OwnerID = new Guid(mp.OwnerId);
                        }
                        mpResponse.MPCoordinate = mpDto;
                        mpResponse.UpdateDate = mp.UpdateDate ?? DateTime.MinValue;
                        mpResponse.Categories = categories;
                        mpResponse.Name = mp.Name;
                        mpResponse.Description = mp.Description;
                        mpResponse.LikesCount = mp.MPLike == null ? 0 : mp.MPLike.Count(); // GetLikesNumber(mp.Id);
                        mpResponse.CommentsCount = mp.MPComments == null ? 0 : mp.MPComments.Count();//GetCommentsNumber(mp.Id);
                        mpResponse.ShareCount = mp.MPShares == null ? 0 : mp.MPShares.Count(); ; //GetShareNumber(mp.Id);
                        mpResponse.VisitsCount = mp.MPVisits == null ? 0 : mp.MPVisits.Sum(x => x.PublicCounter); ;//GetVisitsNumber(mp.Id);
                        mpResponse.Price = _context.Prices.Where(p => p.Id == mp.PriceId)?.FirstOrDefault()?.Price ?? 0;
                        output.List.Add(mpResponse);
                    }
                    else
                    {
                        var defaultMP = new MP
                        {
                            PriceId = mpPrice.Id,
                            X = mpDto.X,
                            Y = mpDto.Y,
                            Z = mpDto.Z
                        };

                        var mpAddress = await GetMPAdress(mpDto);
                        if (mpAddress == null)
                        {
                            response.StatusCode = (int)InnerErrorCode.MPAddressMotFound;
                            return response;
                        }

                         defaultMP.City = mpAddress.City;
                         defaultMP.Country = mpAddress.Country;
                        
                        _context.MixedPlaces.Add(defaultMP);

                        var mpResponseItem = new GetMixedPlaceInfoItem
                        {
                            MPCoordinate = mpDto,
                            Price = mpPrice.Price,
                            IsFree = true
                        };
                        output.List.Add(mpResponseItem);
                    }
                    _context.SaveChanges();
                }
                response.Data = output;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                response.Description = ex.Message;
            }
            return response;
        }

        public List<MP> CreateMixedPlacesWithPrice(List<MixedPlaceRequestModel> mixedPlaceList)
        {
            List<MP> newMixedPlaceList = new List<MP>();
            var _mixedPlaces = _context.MixedPlaces.ToList<MP>();
            foreach (var mixedPlace in mixedPlaceList)
            {
                MP mp = _context.MixedPlaces.Where(b => b.X == mixedPlace.X && b.Y == mixedPlace.Y && b.Z == mixedPlace.Z).FirstOrDefault();
                if (mp == null)
                {
                    newMixedPlaceList.Add(new MP
                    {
                        X = mixedPlace.X,
                        Y = mixedPlace.Y,
                        Z = mixedPlace.Z,
                    });
                }
                else if (mp != null && mp.OwnerId != null)
                {
                    VisitsWithOwner(mp);
                }
                else
                    VisitsWithOwner(mp);
            }
            foreach (MP item in newMixedPlaceList)
            {
                item.PriceId = _context.Prices.FirstOrDefault<MPPrice>().Id;
                _context.MixedPlaces.Add(item);
                _context.SaveChanges();
                VisitsIgnoreOwner(item);
                _context.SaveChanges();
            }
            return newMixedPlaceList;
        }

        public async Task<IResponseModel<EmptyBodyResponse>> BuyMp(string userId, MPDto mpDto)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var mpPrice = _context.Prices.First();

                if (!ValidateUserBalance(userId, mpPrice.Price))
                {
                    response.StatusCode = (int)InnerErrorCode.InsufficientBalance;
                    return response;
                }

                var mp = _context.MixedPlaces.FirstOrDefault(x => x.X == mpDto.X &&
                                                   x.Y == mpDto.Y &&
                                                   x.Z == mpDto.Z);
                if (mp == null)
                {
                    var mpAddress = await GetMPAdress(mpDto);
                    if (mpAddress == null)
                    {
                        response.StatusCode = (int)InnerErrorCode.MPAddressMotFound;
                        return response;
                    }
                    mp = new MP
                    {
                        OwnerId = userId,
                        CreationDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        X = mpDto.X,
                        Y = mpDto.Y,
                        Z = mpDto.Z,
                        PriceId = mpPrice.Id,
                        PurchaseDate = DateTime.Now,
                        MPStatusId = (int)MixedPlaceStatus.Purchased,
                        DesignStatusId = (int)DesignStatus.Empty,
                        Country = mpAddress.Country,
                        City = mpAddress.City
                    };

                    _context.Add(mp);
                    _context.SaveChanges();



                    CreateMPPurchageTransaction(userId, mp.Id, mpPrice.Price);
                    response.StatusCode = (int)InnerErrorCode.Ok;
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(mp.OwnerId))
                    {
                        response.StatusCode = (int)InnerErrorCode.MixedPlaceAlreadyOwned;
                        return response;
                    }

                    mp.DesignStatusId = (int)DesignStatus.Empty;
                    mp.PurchaseDate = DateTime.Now;
                    mp.MPStatusId = (int)MixedPlaceStatus.Purchased;
                    mp.UpdateDate = DateTime.Now;
                    mp.OwnerId = userId;
                    _context.Update(mp);
                    _context.SaveChanges();

                    response.StatusCode = (int)InnerErrorCode.Ok;
                }
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> CreateNewModel(CreateNewModelRequest modelRequest)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var formFile = modelRequest.FormFile;
                if (formFile == null || formFile.Length == 0)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;

                }

                using (Stream stream = formFile.OpenReadStream())
                {
                    using (var binaryReader = new BinaryReader(stream))
                    {
                        var fileContent = binaryReader.ReadBytes((int)formFile.Length);
                        var fileUrl = _imageUploader.UploadImage(fileContent, formFile.FileName, formFile.ContentType);
                        if (!string.IsNullOrWhiteSpace(fileUrl))
                        {
                            var mpModel = new MPModel
                            {
                                Name = modelRequest.Name,
                                Description = modelRequest.Description,
                                Price = modelRequest.Price,
                                CreationDate = DateTime.Now,
                                UpdateDate = DateTime.Now
                            };
                            _context.Models.Add(mpModel);
                            _context.SaveChanges();
                        }
                        else
                            response.StatusCode = (int)InnerErrorCode.UnknownError;

                        return response;
                    }
                }
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> UserRecieveNewModel(string userId, List<UserRecieveNewModelItem> request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {


                foreach (var item in request)
                {
                    var mpModel = _context.Models.FirstOrDefault(x => x.Id == item.ModelId);
                    if (mpModel == null)
                    {

                        response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                        return response;

                    }
                    _context.MPModelsPerUsers.Add(new MPModelPerUser
                    {
                        UserId = userId,
                        MPModelId = item.ModelId,
                        CreationDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        TotalModelsCount = item.RecievedModelsCount,
                        RemainingModelsCount = item.RecievedModelsCount,
                    });
                }

                _context.SaveChanges();
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;

                #region Commented Code From Previous Version
                //var mpModelPerUser = _context.MPModelsPerUsers.FirstOrDefault(x => x.UserId == userId &&
                //                                              x.MPModelId == modelId);

                //var mpModel = _context.Models.FirstOrDefault(x => x.Id == modelId);

                //if (mpModelPerUser != null || mpModel == null)
                //{
                //    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                //    return response;
                //}

                //var userModel = new MPModelPerUser
                //{
                //    UserId = userId,
                //    MPModelId = modelId,
                //    CreationDate = DateTime.Now,
                //    UpdateDate = DateTime.Now
                //};
                //_context.MPModelsPerUsers.Add(userModel);
                //_context.SaveChanges();

                //response.StatusCode = (int)InnerErrorCode.Ok;
                //return response;
                #endregion Commented Code From Previouse Version
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> SetMPItems(string userId, SetMPItemsRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                #region Validations
                var mp = _context.MixedPlaces.FirstOrDefault(x => x.X == request.MPCoordinate.X &&
                                               x.Y == request.MPCoordinate.Y &&
                                               x.Z == request.MPCoordinate.Z);
                if (mp == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                if (string.IsNullOrWhiteSpace(mp.OwnerId))
                {
                    response.StatusCode = (int)InnerErrorCode.MixedPlaceNotOwned;
                    return response;
                }

                if (!mp.OwnerId.Equals(userId))
                {
                    response.StatusCode = (int)InnerErrorCode.MixedPlaceOwnedByOtherUser;
                    return response;
                }

                var userModles = _context.MPModelsPerUsers.Where(x => x.UserId == userId)
                                                        .Include(x => x.MPModel)
                                                        .ToList();
                foreach (var appliedModel in request.AppliedModelsCollection)
                {
                    var userModel = userModles.FirstOrDefault(x => x.MPModelId == appliedModel.ModelId);
                    if (appliedModel != null && userModel != null)
                    {
                        var delta = userModel.RemainingModelsCount - appliedModel.Amount;
                        if (0 <= delta && delta <= userModel.TotalModelsCount)
                            continue;
                    }

                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }


                #endregion Validations

                #region Set Mixed Place Data V2

                var mpDesings = _context.MPItems.Where(x => x.MPId == mp.Id).ToList();
                foreach (var mpItem in request.Items)
                {
                    var dbItem = mpDesings.FirstOrDefault(x => x.ItemType == mpItem.ItemsType);
                    if (dbItem != null)
                    {
                        dbItem.Json = mpItem.MPItemsJson;
                        dbItem.UpdateDate = DateTime.Now;
                        _context.MPItems.Update(dbItem);
                    }
                    else
                    {
                        var mpItemModel = new MPItemModel
                        {
                            MPId = mp.Id,
                            Json = mpItem.MPItemsJson,
                            UpdateDate = DateTime.Now,
                            CreationDate = DateTime.Now,
                            ItemType = mpItem.ItemsType
                        };
                        _context.MPItems.Add(mpItemModel);
                    }
                }

                foreach (var appliedModel in request.AppliedModelsCollection)
                {
                    var userModel = userModles.First(x => x.MPModelId == appliedModel.ModelId);
                    userModel.RemainingModelsCount -= appliedModel.Amount;
                    _context.Update(userModel);
                }

                mp.UpdateDate = DateTime.Now;
                _context.Update(mp);
                _context.SaveChanges();

                #endregion Set Mixed Place Data V2
                
                //get MP Items , if its empty - update Design Status to Empty
                var mpItems = _context.MPItems.Where(x => x.MPId == mp.Id).ToList();
                if(mpItems.Count == 0 || mpItems.All(x=> string.IsNullOrWhiteSpace(x.Json)))
                {
                    var updatedMP=_context.MixedPlaces.Where(x => x.Id == mp.Id).FirstOrDefault();
                    updatedMP.DesignStatusId = (int)DesignStatus.Empty;
                    _context.Update(mp);
                    _context.SaveChanges();

                }

                response.InnerCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public async Task<IResponseModel<GetMPItemsResponse>> GetMPItems(GetMPItemRequest request)
        {
            var response = new ResponseModel<GetMPItemsResponse>();
            try
            {
                var mpPrice = _context.Prices.First();
                var responseData = new GetMPItemsResponse();

                foreach (var item in request.GetMPItemsCollection)
                {
                    var mp = _context.MixedPlaces.FirstOrDefault(x => x.X == item.MPCoordinate.X &&
                                                                      x.Y == item.MPCoordinate.Y &&
                                                                      x.Z == item.MPCoordinate.Z);

                    if (mp != null)
                    {
                        var sqlQuery = _context.MPItems.Where(x => x.MPId == mp.Id && x.Json != null);
                        sqlQuery = item.ItemsType == (int)MixedPlaceItemType.All ? sqlQuery :
                                                                           sqlQuery.Where(x => x.ItemType == item.ItemsType);

                        var mpItems = sqlQuery.ToList();

                        if (mpItems.Count() > 0)
                        {
                            if (item.ItemsType == (int)MixedPlaceItemType.All)
                            {
                                var stringBuilder = new StringBuilder($"{mpItems[0].Json.Trim()}");
                                for (int i = 1; i < mpItems.Count; i++)
                                {
                                    stringBuilder.Append($",{mpItems[i].Json.Trim().Trim()}");
                                }

                                var joinedJson = stringBuilder.ToString();
                                responseData.MPItems.Add(new MPItemDto
                                {
                                    Json = joinedJson,
                                    MPCoordinate = item.MPCoordinate
                                });
                            }
                            else
                            {
                                responseData.MPItems.AddRange(_autoMapper.Map<List<MPItemModel>, List<MPItemDto>>(mpItems));
                            }
                        }
                        else
                        {
                            responseData.MPItems.Add(new MPItemDto
                            {
                                MPCoordinate = item.MPCoordinate
                            });
                        }
                    }
                    else
                    {
                        var mpAddress = await GetMPAdress(new MPDto { X = item.MPCoordinate.X, Y = item.MPCoordinate.Y, Z = item.MPCoordinate.Z });
                        if (mpAddress == null)
                        {
                            response.StatusCode = (int)InnerErrorCode.MPAddressMotFound;
                            return response;
                        }
                        var newMP = new MP
                        {
                            X = item.MPCoordinate.X,
                            Y = item.MPCoordinate.Y,
                            Z = item.MPCoordinate.Z,
                            UpdateDate = DateTime.Now,
                            CreationDate = DateTime.Now,
                            Country = mpAddress.Country,
                            City = mpAddress.City
                        };
                        _context.MixedPlaces.Add(newMP);
                        _context.SaveChanges();
                        responseData.MPItems.Add(new MPItemDto
                        {
                            MPCoordinate = item.MPCoordinate
                        });
                    }
                }

                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = responseData;
                return response;
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public FileStream GetModelData(string fileId)
        {
            throw new NotImplementedException();
        }

        public IResponseModel<MPModelResponseModel> GetAllModelItems()
        {
            ResponseModel<MPModelResponseModel> response = new ResponseModel<MPModelResponseModel>();
            try
            {
                response = new ResponseModel<MPModelResponseModel>();
                var responseData = new MPModelResponseModel();
                
                responseData.List = GetModelsFromDB(); 
                response.Data = responseData;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        private List<MPModelDto> GetModelsFromDB()
        {
            var list = new List<MPModelDto>();
            var models = _context.Models.Include(x => x.ModelStatus)
                                        .Include(x => x.PriceType)
                                        .Include(x=> x.Bundle)
                                        .Include(x => x.MPModelCatSubCats).ThenInclude(x => x.ModelCategory)
                                        .Include(x => x.MPModelCatSubCats).ThenInclude(x => x.ModelSubCategory)
                                        .ToList();

            foreach (MPModel item in models)
            {
                var categories = new List<ModelCatSubCatDto>();
                foreach (var cat in item.MPModelCatSubCats)
                {
                    categories.Add(new ModelCatSubCatDto()
                    {
                        Category = _autoMapper.Map<ModelCategory, ModelCategoryDto>(cat.ModelCategory),
                        SubCategory = _autoMapper.Map<ModelSubCategory, ModelSubCategoryDto>(cat.ModelSubCategory)
                    });
                }
                list.Add(new MPModelDto
                {
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description,
                    ModelCategories = categories,
                    Price = item.Price,
                    CreationDate = item.CreationDate,//Date entered
                    PriceType = _autoMapper.Map<PriceTypeModel, PriceTypeDto>(item.PriceType),
                    PriceTypeId = item.PriceTypeId,
                    ModelStatusId = item.ModelStatusId,
                    BundleImageName = item.Bundle?.Name ?? "",
                    Url = item.Bundle?.Url ?? "",
                    Bundle = item.Bundle != null ? new DropdownDto { Id = item.Bundle.Id, Name = item.Bundle.Name } : new DropdownDto(),
                    ModelStatus = _autoMapper.Map<ModelStatusModel, ModelStatusDto>(item.ModelStatus),
                    UniqueId = item.UniqueId,
                    PurchasedUnits = CalculatePurchasedModels(item.Id)
                });
            }

            return list;
        }

        public IResponseModel<EmptyBodyResponse> AddNewModel(AddNewModelRequest modelRequest)
        {
            ResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                MPModel newModel = new MPModel();
                if (modelRequest != null)
                {
                    newModel.Name = modelRequest.Name;
                    newModel.Description = modelRequest.Description;
                    newModel.PriceTypeId = modelRequest.PriceType;
                    newModel.Price = modelRequest.PricePerUnit;
                    newModel.CreationDate = DateTime.Now;
                    _context.Models.Add(newModel);
                    _context.SaveChanges();
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                else
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

            }
            catch (Exception ex)
            {

                response.StatusCode = (int)InnerErrorCode.UnknownError;
                response.Description = ex.InnerException.Message;
                return response;
            }
        }

     
        public IResponseModel<EmptyBodyResponse> UpdateModelStatus(int id, int status)
        {
            ResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                MPModel model = _context.Models.Where(a => a.Id == id).First();
                if (model != null)
                {
                    model.ModelStatusId = status;
                    _context.Models.Update(model);
                    _context.SaveChanges();
                    response.StatusCode = (int)InnerErrorCode.Ok;
                }
                else
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                response.Description = ex.InnerException.Message;
                return response;
            }
        }

        public IResponseModel<bool> IsModelUniqueidExists(IsUniqueIdExistsRequest request)
        {
            var response = new ResponseModel<bool>();
            try
            {
                response.Data = _context.Models.Any(x=> x.UniqueId == request.UniqueId);
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<MPModelResponseModel> GetModels(FilterRequest request=null)
        {
            ResponseModel<MPModelResponseModel> response = new ResponseModel<MPModelResponseModel>();
            try
            {
                if (request == null)
                {
                    return GetAllModelItems();
                }
                else
                {
                    IEnumerable<MPModelDto> result = GetModelsFromDB();

                    if (!string.IsNullOrWhiteSpace(request.Search))
                        result = SearchModels(result, request.Search);

                    if (request.Filters != null && request.Filters.Count() > 0)
                        result = FilterHelper.Filter(result, request.Filters, _adminTableMapper.ModelsTableMap);
                    response.Data = new MPModelResponseModel
                    {
                        List = result?.Skip((request.Page - 1) * request.AmountPerPage)?.Take(request.AmountPerPage)?.ToList(),
                        Summary = request
                    };
                    response.Data.Summary.TotalAmount = result?.Count() ?? 0;
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;

                }
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                response.Description = ex.InnerException.Message;
                return response;
            }
            return response;
        }

        public IResponseModel<GetModelMetadataResponse> GetModelMetadata()
        {
            var response = new ResponseModel<GetModelMetadataResponse>();
            try
            {
                var modelStatuses = _context.ModelStatuses.Select(x => new ModelStatusDto { Id = x.Id, Name = x.Name }).ToList();
                var modelCategories = _context.ModelCategories.Select(x => new ModelCategoryDto { Id = x.Id, Name = x.Name }).ToList();
                var priceTypes = _context.PriceTypes.Select(x => new PriceTypeDto { Id = x.Id, Name = x.Name }).ToList();
                var bundles = _context.BundleModel.Select(x => new DropdownDto { Id = x.Id, Name = x.Name }).ToList();
                var modelSubCategories = _context.ModelSubCategories
                                            .Select(x => new ModelSubCategoryDto { Id = x.Id, Name = x.Name, ModelCategoryId = x.ModelCategoryId }).ToList();
                response.Data = new GetModelMetadataResponse
                {
                    ModelStatuses = modelStatuses,
                    ModelCategories = modelCategories,
                    ModelSubCategories = modelSubCategories,
                    PriceTypes = priceTypes,
                    Bundles = bundles,
                    Filters=_adminTableMapper.ModelsTableFiltersConditionsMap
                };
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        #region Private Methods


        //TODO- need to implement this flow
        private void VisitsIgnoreOwner(MP mp)
        {
        }
        //TODO - need to implement this flow
        private void VisitsWithOwner(MP mp)
        {
        }
        private bool ValidateUserBalance(string userId, decimal price)
        {
            return true;
        }
        //TODO: Need to implement this flow
        private void CreateMPPurchageTransaction(string userId, int mpId, decimal price)
        {
            _transactionService.AddTransaction(TransactionCategoryEnum.BuyMixedPlace, price, 0, "", userId);
            _context.SaveChanges();
        }

        #endregion Private Methods

        public IResponseModel<GetMPMetadataResponse> GetMPMetadata()
        {
            var response = new ResponseModel<GetMPMetadataResponse>();
            try
            {
                var mpStatuses = _context.MPStatuses.Select(x => new MPStatusDto { Id = x.Id, Name = x.Name })
                                                  .ToList();

                var designStatuses = _context.DesignStatuses.Select(x => new DesignStatusDto { Id = x.Id, Name = x.Name })
                                                  .ToList();

                var users = _context.Users.Select(x => new OwnerDto { Id = x.Id, UserName = x.UserName });

                response.Data = new GetMPMetadataResponse
                {
                    Users = users,
                    DesignStatuses = designStatuses,
                    MPStatuses = mpStatuses,
                    Filters = _adminTableMapper.MixedPlacesTableFiltersConditionsMap
                };
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;

            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<GetMixedPlacesResponse> GetMixedPlaces(FilterRequest request = null)
        {
            var response = new ResponseModel<GetMixedPlacesResponse>();
            try
            {
                if (request == null)
                {
                    var mps = _context.MixedPlaces.Include(x => x.User)
                                                  .Include(x => x.DesignStatus)
                                                  .Include(x => x.MPStatus)
                                                  .Include(x => x.Price)
                                                  .Include(x => x.MPComments)
                                                  .Include(x => x.MPVisits)
                                                  .Include(x => x.MPLike)
                                                  .Include(x => x.MPShares)
                                                .Include(x => x.MPCategories).ThenInclude(x => x.MPCategory)
                                                .Include(x => x.MPCategories).ThenInclude(x => x.MPSubCategory)

                                                  .ToList();
                    var listOfDtos = _autoMapper.Map<List<MP>, List<MPDetailsDto>>(mps);
                    foreach(var dto in listOfDtos)
                    {
                        int i = 0;
                        dto.Categories = new List<ModelCatSubCatDto>();
                        foreach (var cat in mps[i++].MPCategories)
                        {
                            dto.Categories.Add(new ModelCatSubCatDto()
                            {
                                Category = new ModelCategoryDto() { Id = cat.MPCategory.Id,Name = cat.MPCategory.CategoryName},
                                SubCategory = new ModelSubCategoryDto() { Id = cat.MPSubCategory.Id, Name = cat.MPSubCategory.Name,ModelCategoryId = cat.MPCategory.Id },
                            });
                        }

                    }

                    response.Data = new GetMixedPlacesResponse { MixedPlaces = listOfDtos };
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                else
                {
                    var dbmps = _context.MixedPlaces.Include(x => x.User)
                                                    .Include(x => x.DesignStatus)
                                                    .Include(x => x.MPStatus)
                                                    .Include(x => x.Price)
                                                    .Include(x => x.MPComments)
                                                    .Include(x => x.MPVisits)
                                                    .Include(x => x.MPLike)
                                                    .Include(x => x.MPShares)
                                                    .Include(x => x.MPCategories).ThenInclude(x => x.MPCategory)
                                                    .Include(x => x.MPCategories).ThenInclude(x => x.MPSubCategory)

                                                    .ToList();

                    var mpDtos = _autoMapper.Map<List<MP>, List<MPDetailsDto>>(dbmps);

                    IEnumerable<MPDetailsDto> result = new List<MPDetailsDto>(mpDtos);
                    foreach (var dto in result)
                    {
                        int i = 0;
                        dto.Categories = new List<ModelCatSubCatDto>();
                        foreach (var cat in dbmps[i++].MPCategories)
                        {
                            dto.Categories.Add(new ModelCatSubCatDto()
                            {
                                Category = new ModelCategoryDto() { Id = cat.MPCategory.Id, Name = cat.MPCategory.CategoryName },
                                SubCategory = new ModelSubCategoryDto() { Id = cat.MPSubCategory.Id, Name = cat.MPSubCategory.Name, ModelCategoryId = cat.MPCategory.Id },
                            });
                        }

                    }
                    if (!string.IsNullOrWhiteSpace(request.Search))
                        result = SearchMixedPlaces(result, request.Search);

                    if (request.Filters != null && request.Filters.Count() > 0)
                        result = FilterHelper.Filter(result, request.Filters, _adminTableMapper.MixedPlaceTableMap);

                    response.Data = new GetMixedPlacesResponse
                    {
                        MixedPlaces = result.Skip((request.Page - 1) * request.AmountPerPage)?.Take(request.AmountPerPage),
                        Summary = request

                    };
                    response.Data.Summary.TotalAmount = result?.Count() ?? 0;
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> UpdateMixedPlace(UpdateMixedPlaceRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var mp = _context.MixedPlaces.FirstOrDefault(x => x.Id == request.Id);
                if (mp == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var price = _context.Prices.FirstOrDefault(x => x.Price == request.Price);
                if (price != null)
                    mp.PriceId = price.Id;
                else
                    mp.Price = new MPPrice { Price = request.Price };

                if (request.MPStatus.Id != 0)
                    mp.MPStatusId = request.MPStatus.Id;
                if (request.DesignStatus.Id != 0)
                    mp.DesignStatusId = request.DesignStatus.Id;
                mp.Name = request.Name;
                mp.OwnerId = request.OwnerId;
                mp.UpdateDate = DateTime.Now;

                UpdateMPCategoriesSubCategories(request);

                _context.Update(mp);
                _context.SaveChanges();

                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        private void UpdateMPCategoriesSubCategories(UpdateMixedPlaceRequest request)
        {
            if (request.Categories != null && request.Categories.Count > 0)
            {
                var mpCatSubCat = _context.MPCatSubCat.Where(x => x.Id == request.Id).ToList();
                foreach (var mod in mpCatSubCat)
                {
                    _context.MPCatSubCat.Remove(mod);
                }
                foreach (var mod in request.Categories)
                {
                    _context.MPCatSubCat.Add(new MPCatSubCat()
                    {
                        MPId = request.Id,
                        MPCategoryId = mod.Category.Id,
                        MPSubCategoryId = mod.SubCategory.Id
                    });
                }
            }
        }

        public IResponseModel<EmptyBodyResponse> UpdateMixedPlaceOnwer(UpdateMixedPlaceOwnerRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var mp = _context.MixedPlaces.FirstOrDefault(x => x.Id == request.Id);
                var user = string.IsNullOrWhiteSpace(request.OwnerId) ? null : _context.Users.FirstOrDefault(x => x.Id == request.OwnerId);
                if (mp == null || mp.DesignStatusId != (int)DesignStatus.Empty || (!string.IsNullOrWhiteSpace(request.OwnerId) && user == null))
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                
                mp.OwnerId = user == null ? null : user.Id;
                _context.SaveChanges();

                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;

            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;

            }
        }

        private IEnumerable<MPDetailsDto> SearchMixedPlaces(IEnumerable<MPDetailsDto> mps, string searchTerm)
        {
            return mps.Where(x => (x.Name != null && x.Name.Contains(searchTerm)) ||
                                   x.MPStatus?.Name != null && x.MPStatus.Name.Contains(searchTerm) ||
                                   x.DesignStatus?.Name != null && x.DesignStatus.Name.Contains(searchTerm) ||
                                   x.OwnerName != null && x.OwnerName.Contains(searchTerm) ||
                                   x.PurchaseDate.ToString().Contains(searchTerm) ||
                                   x.UpdateDate.ToString().Contains(searchTerm) ||
                                   x.Price.ToString().Contains(searchTerm) ||
                                   x.X.ToString().Contains(searchTerm) ||
                                   x.Y.ToString().Contains(searchTerm) ||
                                   x.Z.ToString().Contains(searchTerm) ||
                                   x.VisitsCount.ToString().Contains(searchTerm) ||
                                   x.LikesCount.ToString().Contains(searchTerm) ||
                                   x.CommentsCount.ToString().Contains(searchTerm) ||
                                   x.SharesCount.ToString().Contains(searchTerm) ||
                                   x.Demand.ToString().Contains(searchTerm) ||
                                   x.Country != null && x.Country.Contains(searchTerm) ||
                                   x.City != null && x.City.Contains(searchTerm));
        }


        public IResponseModel<GetAllMPCategoriesResponse> GetAllMPCategories()
        {
            IResponseModel<GetAllMPCategoriesResponse> response = new ResponseModel<GetAllMPCategoriesResponse>();
            try
            {
                var mpCategories = new GetAllMPCategoriesResponse();
                mpCategories.List = _context.MPCategories.ToList();
                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = mpCategories;
                return response;
            }
            catch (Exception)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<ModelCategoryResponse> GetModelCategories()
        {
            IResponseModel<ModelCategoryResponse> response = new ResponseModel<ModelCategoryResponse>();
            try
            {
                response.Data = new ModelCategoryResponse();
                // response.Data.list = new List<ModelCategoryDto>();

                response.Data.list = _mapper.Map<List<ModelCategory>, List<ModelCategoryDto>>(_context.ModelCategories.ToList()); ;
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<GetMPSubCategoryResponse> GetMPSubCategories()
        {
            var response = new ResponseModel<GetMPSubCategoryResponse>();
            try
            {
                response.Data = new GetMPSubCategoryResponse();
                response.Data.list = new List<MPSubCategory>();
                response.Data.list = _context.MPSubCategories.ToList();
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<ModelSubCategoryResponse> GetModelSubCategories(GetModelSubCategoryRequest modelCategoryIds)
        {
            var response = new ResponseModel<ModelSubCategoryResponse>();
            try
            {
                response.Data = new ModelSubCategoryResponse();
                response.Data.list = new List<ModelCatSubCatDto>();
                var modelSubCategories = new List<ModelSubCategory>();
                //return all model subCategories from data
                if (modelCategoryIds == null || modelCategoryIds.ModelCategoryIds==null ||modelCategoryIds.ModelCategoryIds.Count==0)
                {
                    modelSubCategories = _context.ModelSubCategories.ToList();
                    foreach (var modelSubCategory in modelSubCategories)
                    {
                        var modelCategory = _context.ModelCategories.Where(x => x.Id == modelSubCategory.ModelCategoryId).FirstOrDefault();
                        if (modelCategory != null)
                        {
                            var modelCatSubCat = new ModelCatSubCatDto();
                            modelCatSubCat.Category = new ModelCategoryDto();
                            modelCatSubCat.SubCategory = new ModelSubCategoryDto();
                            modelCatSubCat.Category.Id = modelCategory.Id;
                            modelCatSubCat.Category.Name = modelCategory.Name;
                            modelCatSubCat.SubCategory.Id = modelSubCategory.Id;
                            modelCatSubCat.SubCategory.Name = modelSubCategory.Name;
                            modelCatSubCat.SubCategory.ModelCategoryId = modelCategory.Id;
                            response.Data.list.Add(modelCatSubCat);
                        }
                    }
                }
                else  //return subCategories per model Categories ids from data
                {
                    foreach (var modelCategoryId in modelCategoryIds.ModelCategoryIds)
                    {
                        var modelCategory = new ModelCategoryDto
                        {
                            Id = modelCategoryId,
                            Name = _context.ModelCategories.Where(x => x.Id == modelCategoryId).FirstOrDefault().Name
                        };
                        modelSubCategories = _context.ModelSubCategories.Where(x=>x.ModelCategoryId== modelCategoryId).ToList();
                        foreach (var modelSubCategory in modelSubCategories)
                        {
                            var subCategoryName = _context.ModelSubCategories.Where(x => x.Id == modelSubCategory.Id).FirstOrDefault().Name;
                            response.Data.list.Add(new ModelCatSubCatDto
                            {
                                Category = modelCategory,
                                SubCategory = new ModelSubCategoryDto { Id = modelSubCategory.Id, Name = subCategoryName, ModelCategoryId= modelCategoryId }
                            });
                        }
                    }
                }
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> UpdateMPStatus(UpdateMPStatusRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var mp = _context.MixedPlaces.Include(x => x.MPDesigns)
                                             .FirstOrDefault(x => x.Id == request.MPId);
                if (mp == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                mp.MPStatusId = request.MPStatus.Id;
                _context.Update(mp);

                _context.SaveChanges();
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        private void RemoveAndAddCategoriesSubCategoriesToModel(MPUserDataDto mpdto,MP mp)
        {
            if (mpdto.Categories == null || mpdto.Categories.Count == 0)
                return;

            _context.MPCatSubCat.RemoveRange(_context.MPCatSubCat.Where(x => x.MPId == mp.Id));

            var listOfCategories = new List<MPCatSubCat>();

            foreach (var cat in mpdto.Categories)
            {
                var category = cat.Category;
                var subcategory = cat.SubCategory;
                listOfCategories.Add(new MPCatSubCat()
                {
                    MPId = mp.Id,
                    MPCategoryId = category.Id,
                    MPSubCategoryId = subcategory.Id
                    
                });
            }
            _context.MPCatSubCat.AddRange(listOfCategories);
        }

        public IResponseModel<bool> UpdateMPUserData(string userId,MPUserDataDto request)
        {
            var response = new ResponseModel<bool>();
            try
            {
                var mp = _context.MixedPlaces.Where(x => x.OwnerId == userId && x.X == request.Coordinates.X
                                                                             && x.Y == request.Coordinates.Y 
                                                                             && x.Z == request.Coordinates.Z).FirstOrDefault();
                if (mp!=null)
                {
                    mp.Description = request.MPDescription;
                    mp.Name = request.Name;


                    RemoveAndAddCategoriesSubCategoriesToModel(request,mp);

                    _context.SaveChanges();
                    response.Data = true;
                    response.StatusCode = (int)InnerErrorCode.Ok;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<GetModelResponse> GetModel(int id)
        {
            var response = new ResponseModel<GetModelResponse>();
            try
            {
                var dbModel = _context.Models.LoadWithAllRelationships(x => x.Id == id)?.FirstOrDefault();
                if (dbModel == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var responseModel = _autoMapper.Map<MPModel, GetModelResponse>(dbModel);
                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = responseModel;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
                
            }
        }

        public IResponseModel<GetModelsSelectItemsResponse> GetModelsSelectItems()
        {
            var response = new ResponseModel<GetModelsSelectItemsResponse>();
            try
            {
                var models = _context.Models.Select(x => new DropdownDto { Id = x.Id, Name = x.Name }).ToList();
                response.Data = new GetModelsSelectItemsResponse { Models = models };
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        #region Private Methods

        private IEnumerable<MPModelDto> SearchModels(IEnumerable<MPModelDto> list, string searchTerm)
        {

            return list.Where(x => (x.Description != null && x.Description.Contains(searchTerm)) ||
                                  (x.Name != null && x.Name.Contains(searchTerm)) ||
                                   x.ModelStatus != null && x.ModelStatus.Name.Contains(searchTerm) ||
                                   x.CreationDate.ToString().Contains(searchTerm) ||
                                   x.UpdateDate.ToString().Contains(searchTerm) ||
                                   (x.ModelCategories != null && x.ModelCategories.Count > 0 && x.ModelCategories.Where(c=>c.Category.Name.Contains(searchTerm)).Count() > 0) ||
                                   (x.ModelCategories != null && x.ModelCategories.Count > 0 && x.ModelCategories.Where(c => c.SubCategory.Name.Contains(searchTerm)).Count() > 0) ||
                                   x.ModelSize.ToString().Contains(searchTerm) ||
                                   x.Url.ToString().Contains(searchTerm) ||
                                   x.Price.ToString().Contains(searchTerm) ||
                                   x.PriceType.Name != null && x.PriceType.Name.ToString().Contains(searchTerm) ||
                                   x.PurchasedUnits != null && x.PurchasedUnits.ToString().Contains(searchTerm));
        }

        private int CalculatePurchasedModels(int modelId)
        {
            var pckItems = _context.PackageItems.Where(x => x.ModelId == modelId).Include(x => x.Package).ToList();
            if (!pckItems.Any()) return 0;
            return pckItems.Where(x => x.Package != null)
                           .Sum(x => x.Package.PurchasedUnits  * (x.NumOfUnits ?? 0));
        }
        #endregion Private Methods
    }
}
