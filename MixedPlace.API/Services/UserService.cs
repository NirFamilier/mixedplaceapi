﻿using AutoMapper;

using Microsoft.AspNetCore.Identity;
using MixedPlace.API.Data;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.ResponseModels;
using MixedPlace.MessageSender.Implementations;
using MixedPlace.MessageSender.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MixedPlace.ImageUploader.Abstractions;
using MixedPlace.ImageUploader.Implementations;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.DataTransferObjects;
using Microsoft.Extensions.Logging;
using System.Linq.Expressions;
using System.Reflection;
using MixedPlace.API.Helpers;
using System.Collections;
using Microsoft.EntityFrameworkCore;
using MixedPlace.API.Models.Map;

namespace MixedPlace.API.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly ApplicationDbContext _dbContext;
        private readonly IMessageSender _messageSender;
        private readonly ILogger<UserService> _logger;
        private readonly AdminTablesMapper _adminTableMapper;
        private readonly IHistoryManagmentService _historyManagmentService;
        private readonly INotificationsService _notificationsService;
        private readonly IImageUploader _imageUploader;
        private readonly IMapper _autoMapper;

        public UserService(
            ApplicationDbContext dbContext,
            UserManager<User> userManager,
            IConfiguration configuration,
            IPasswordHasher<User> passwordHasher,
            IMapper autoMapper,
            ILogger<UserService> logger,
            AdminTablesMapper adminTableMapper,
            IHistoryManagmentService historyManagmentService,
            INotificationsService notificationsService
            )
        {
            _userManager = userManager;
            _configuration = configuration;
            _passwordHasher = passwordHasher;
            _dbContext = dbContext;
            _autoMapper = autoMapper;
            _imageUploader = ImageUploaderFactory.CreateImageUploader();
            _messageSender = MessageSenderFactory.CreateMessageSender(_configuration["Messages:Provider"]);
            _logger = logger;
            _adminTableMapper = adminTableMapper;
            _historyManagmentService = historyManagmentService;
            _notificationsService = notificationsService;
        }

        public async Task<IResponseModel<UserBaseDto>> GetUserDetails(string userId)
        {
            var response = new ResponseModel<UserBaseDto>();
            try
            {
                if (string.IsNullOrWhiteSpace(userId))
                {
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                    return response;
                }

                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                    return response;
                }
                var userDto = _autoMapper.Map<UserBaseDto>(user);

                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = userDto;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
            }
            return response;
        }

        public async Task<IResponseModel<EmptyBodyResponse>> UpdateUser(string userId, UserBaseDto userDto)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                if (string.IsNullOrWhiteSpace(userId))
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                    return response;
                }
                //_autoMapper.Map(userDto, user, typeof(UserBaseDto), typeof(User));
                //if (!string.IsNullOrWhiteSpace(userDto.ImageUrl))
                //{
                //    var bytes = Convert.FromBase64String(userDto.ImageUrl);
                //    var imageUrl = _imageUploader.UploadImage(bytes, $"{userDto.FirstName}{userDto.LastName}ProfileImage", "image/jpeg", "ProifleImages");
                //    if (!string.IsNullOrWhiteSpace(imageUrl))
                //        user.Image = imageUrl;
                //}

                var updateResult = await _userManager.UpdateAsync(user);

                if (updateResult.Succeeded)
                    response.StatusCode = (int)InnerErrorCode.Ok;
                else
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
            return response;
        }

        public async Task<IResponseModel<EmptyBodyResponse>> UpdateUserDetails(UpdateUserDetailsRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var user = await _userManager.FindByIdAsync(request.UserId);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                    return response;
                }
                _autoMapper.Map(request, user, typeof(UpdateUserDetailsRequest), typeof(User));
                user.SecurityStamp = Guid.NewGuid().ToString();
                if (!string.IsNullOrWhiteSpace(request.ImageBase64))
                {
                    var bytes = Convert.FromBase64String(request.ImageBase64);
                    var imageUrl = _imageUploader.UploadImage(bytes, $"{user.FirstName}{user.LastName}ProfileImage", request.ImageMimeType, "ProifleImages");
                    if (!string.IsNullOrWhiteSpace(imageUrl))
                        user.Image = imageUrl;
                }

                if (user.UserStatusId != request.UserStatus.Id)
                {
                    _notificationsService.SendUpdateUserStatusEmail(user.Id, (UserStatus)request.UserStatus.Id);
                }

                user.UpdateDate = DateTime.Now;
                var updateResult = await _userManager.UpdateAsync(user);

                if (updateResult.Succeeded)
                    response.StatusCode = (int)InnerErrorCode.Ok;
                else
                    response.StatusCode = (int)InnerErrorCode.UnknownError;

                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
           
        }

        public IResponseModel<EmptyBodyResponse> UpdateUserStatus(UpdateUserStatusRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var user = _dbContext.Users.Include(x=> x.ModelsPerUser).FirstOrDefault(x => x.Id == request.UserId);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                switch (request.UserStatus.Id)
                {
                    case (int)UserStatus.Registered:
                    case (int)UserStatus.Active:
                        {
                            _notificationsService.SendUpdateUserStatusEmail(user.Id, UserStatus.Active);
                            break;
                        }
                    case (int)UserStatus.Suspended:
                        {
                            var mps = _dbContext.MixedPlaces.Where(x => x.OwnerId == request.UserId).ToList();
                            mps.ForEach(x => x.DesignStatusId = (int)DesignStatus.Suspended);
                            _notificationsService.SendUpdateUserStatusEmail(user.Id, UserStatus.Suspended);
                            _dbContext.UpdateRange(mps);

                            break;
                        }
                    case (int)UserStatus.Disabled:
                        {
                            var result = false;
                            var mps = _dbContext.MixedPlaces.Include(x => x.AchievedChallenges)
                                                .Include(x => x.MPComments)
                                                .Include(x => x.MPLike)
                                                .Include(x => x.MPShares)
                                                .Include(x => x.MPVisits)
                                                .Include(x => x.MPDesigns)
                                                .Where(x => x.OwnerId == request.UserId).ToList();
                            
                            foreach (var mp in mps)
                            {
                                if (!_historyManagmentService.MoveMixedPlaceDataToHistoryTable(mp))
                                {
                                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                                    return response;
                                }
                            }
                            _notificationsService.SendUpdateUserStatusEmail(user.Id, UserStatus.Disabled);
                            _dbContext.MPModelsPerUsers.RemoveRange(user.ModelsPerUser);
                            break;
                        }
                    default:
                        {
                            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                            return response;
                        }
                }

                user.UserStatusId = request.UserStatus.Id;
                _dbContext.Update(user);
                _dbContext.SaveChanges();
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public async Task<IResponseModel<EmptyBodyResponse>> CreateUser(CreateUserRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var user = _dbContext.Users.FirstOrDefault(x => x.UserName.Equals(request.UserName));
                if (user != null)
                {
                    response.StatusCode = (int)InnerErrorCode.UsernameInUse;
                    return response;
                }
                user = _autoMapper.Map<CreateUserRequest, User>(request);
                if (!string.IsNullOrWhiteSpace(request.ImageBase64))
                {
                    var bytes = Convert.FromBase64String(request.ImageBase64);
                    var imageUrl = _imageUploader.UploadImage(bytes, $"{request.FirstName}_{request.LastName}", request.ImageMimeType, "ProifleImages");
                    if (!string.IsNullOrWhiteSpace(imageUrl))
                        user.Image = imageUrl;
                }
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                    response.StatusCode = (int)InnerErrorCode.Ok;
                else
                    response.StatusCode = (int)InnerErrorCode.UnknownError;

                return response;
            }
            catch (Exception exp)
            {

                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<GetUsersResponse> GetUsers(FilterRequest filterRequest = null)
        {
            var response = new ResponseModel<GetUsersResponse>();
            try
            {
                if (filterRequest == null)
                {
                    var users = _dbContext.Users.Include(u => u.UserStatus)
                                                .Include(x=> x.MixedPlaces).ThenInclude(x=> x.MPReports)
                                                .ToList();
                    response.Data = new GetUsersResponse { Users = _autoMapper.Map<List<User>, List<UserDetailsDto>>(users) };
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                else
                {
                    var dbUsers = _dbContext.Users.Include(u=> u.UserStatus)
                                                  .Include(x => x.MixedPlaces).ThenInclude(x => x.MPReports)
                                                  .ToList();
                    var users  = _autoMapper.Map<List<User>, List<UserDetailsDto>>(dbUsers);

                    IEnumerable<UserDetailsDto> result = new List<UserDetailsDto>(users);

                    if (!string.IsNullOrWhiteSpace(filterRequest.Search))
                        result = SearchUsers(result, filterRequest.Search);

                    if(filterRequest.Filters != null && filterRequest.Filters.Count() > 0)
                        result = FilterHelper.Filter(result, filterRequest.Filters, _adminTableMapper.UsersTableMap);
                     
                    response.Data = new GetUsersResponse
                    {
                        Users = result?.Skip((filterRequest.Page - 1) * filterRequest.AmountPerPage)?.Take(filterRequest.AmountPerPage),
                        Summary = filterRequest
                    };
                    response.Data.Summary.TotalAmount = result?.Count() ?? 0;
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<GetUserMetadataResponse> GetUserMetadata()
        {
            var response = new ResponseModel<GetUserMetadataResponse>();
            try
            {
                var statuses = _dbContext.UserStatuses.Select(x => new UserStatusDto { Id = x.Id, Name = x.Name })
                                                      .ToList();

                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = new GetUserMetadataResponse
                {
                    UserStatuses = statuses,
                    Filters = _adminTableMapper.UsersTableFiltesConditionsMap
                };
           
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> UpdateDeviceDetails(string userId, UpdateDeviceDetailsRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var user = _dbContext.Users.FirstOrDefault(x=> x.Id == userId);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                user.DeviceID = request.DeviceID;
                user.DeviceOSId = _dbContext.DeviceOperationSystems
                                            .FirstOrDefault(x => x.Name != null && 
                                                                 x.Name.ToLower().Contains(request.DeviceOS.ToLower()))?
                                            .Id;
                _dbContext.Update(user);
                _dbContext.SaveChanges();

                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> NotifyUser(NotificationRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var user = _dbContext.Users.FirstOrDefault(x => x.Id == request.UserId);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                switch (request.NotificationType)
                {
                    case NotificationMethod.SMS:
                        break;
                    case NotificationMethod.Email:
                        var sendResult = _messageSender.SendSingleMessageToRecipient(_configuration["Messages:OutgoingEmailAddress"],
                                         user.Email,
                                         request.Message,
                                         htmlContent: string.Empty);

                        if (sendResult == false)
                        {
                            response.StatusCode = (int)InnerErrorCode.UnknownError;
                            return response;
                        }
                        break;
                    case NotificationMethod.PushNotification:
                        break;
                    default:
                        break;
                }

                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        #region Private Methods


        private IEnumerable<UserDetailsDto> SearchUsers(IEnumerable<UserDetailsDto> list, string searchTerm)
        {
            return list.Where(x=> (x.Email != null &&  x.Email.Contains(searchTerm))||
                                  (x.UserName != null && x.UserName.Contains(searchTerm)) ||
                                  (x.FirstName != null && x.FirstName.Contains(searchTerm)) ||
                                  (x.LastName != null && x.LastName.Contains(searchTerm)) ||
                                   x.Age.ToString().Contains(searchTerm) ||
                                  (x.PhoneNumber != null && x.PhoneNumber.Contains(searchTerm)) ||
                                   (x.UserStatus?.Name != null && x.UserStatus.Name.Contains(searchTerm) )||
                                   x.RegistrationDate.ToString().Contains(searchTerm) ||
                                   x.CoinBalance.ToString().Contains(searchTerm) ||
                                   x.MPLevel.ToString().Contains(searchTerm) ||
                                   x.MPCount.ToString().Contains(searchTerm) ||
                                   x.DesignLevel.ToString().Contains(searchTerm) ||
                                   x.ChallengeLevel.ToString().Contains(searchTerm) ||
                                   x.SocialLevel.ToString().Contains(searchTerm) ||
                                   x.ReportsCount.ToString().Contains(searchTerm));
        }

        #endregion Private Methods
    }
}
