﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Base.ResponseModel;
using MixedPlace.API.Models.Coins.RequestModel;
using MixedPlace.API.Models.Coins.ResponseModel;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.MPChallenge.ResponseModels;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.MPModel.RequestModels;
using MixedPlace.API.Models.MPModel.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public interface IAdminService
    {
        IResponseModel<bool> AddModelCategory(ModelCategory category);
        IResponseModel<bool> AddMPSubCategory(MPSubCategory subCategory);
        IResponseModel<bool> AddModelSubCategory(ModelSubCategory subCategory);
        IResponseModel<bool> AddMPCategory(MPCategory category);
        IResponseModel<GetGlobalMetadataResponse> GetGlobalMetadata();
        IResponseModel<bool> AddBundle(BundleRequest request);
        IResponseModel<string> UploadImage(UploadImageRequest request);
        IResponseModel<BundleResponseModel> GetBundleById(int id);
        IResponseModel<BundleResponseModel> GetBundles();
        IResponseModel<bool> AddModel(AddModelRequest request);
        IResponseModel<bool> UpdateModel(UpdateModelRequest request);
        IResponseModel<bool> SuspendModel(SuspendModelRequest suspendedModel);
        IResponseModel<bool> AddRewardPerBulkOfUsers(RewardRequest request);
        IResponseModel<GetClientChallengesResponse> GetClientChallengesPerMonth(string userId);
        IResponseModel<GetCoinRulesResponse> GetCoinRules(FilterRequest filterRequestfilterRequest = null);
        IResponseModel<GetCoinRulesMetadataResponse> GetCoinRulesMetadata();
    }
}
