﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.User.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public interface IUserService
    {
        IResponseModel<EmptyBodyResponse> NotifyUser(NotificationRequest request);
        Task<IResponseModel<UserBaseDto>> GetUserDetails(string userId);
        Task<IResponseModel<EmptyBodyResponse>> UpdateUser(string userId, UserBaseDto userDto);
        IResponseModel<GetUsersResponse> GetUsers(FilterRequest filterRequest = null);
        Task<IResponseModel<EmptyBodyResponse>> UpdateUserDetails(UpdateUserDetailsRequest request);
        IResponseModel<EmptyBodyResponse> UpdateUserStatus(UpdateUserStatusRequest request);
        Task<IResponseModel<EmptyBodyResponse>> CreateUser(CreateUserRequest request);
        IResponseModel<GetUserMetadataResponse> GetUserMetadata();
        IResponseModel<EmptyBodyResponse> UpdateDeviceDetails(string userId,UpdateDeviceDetailsRequest request);
    }
}
