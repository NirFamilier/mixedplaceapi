﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Package;
using MixedPlace.API.Models.Package.RequestModels;
using MixedPlace.API.Models.Package.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public interface IStoreService
    {
        IResponseModel<GetStoreMetadataResponse> GetStoreMetadata();
        IResponseModel<GetPackagesResponse> GetPackages(FilterRequest filterRequest = null);
        IResponseModel<EmptyBodyResponse> UpdatePackage(UpdatePackageRequest request); 
        IResponseModel<EmptyBodyResponse> CreatePackage(CreatePackageRequest request);
        IResponseModel<EmptyBodyResponse> UpdatePackageStatus(UpdatePackageStatusRequest request);
        IResponseModel<EmptyBodyResponse> BuyPackages(string userId, BuyPackageRequest request);
        IResponseModel<GetClientStorePackagesResponse> GetClientStorePackages(PackageCategoryType ptype,bool retrieveallpackages,int numofpages,int pagenum);
        IResponseModel<EmptyBodyResponse> CreatePackage(PackageModel package);
        IResponseModel<GetPackageResponse> AddPackageToShoppingCart(string userId, AddPackageToUserShoppingCartRequest request);
        IResponseModel<EmptyBodyResponse> RemovePackageFromUserShoppingCart(string userId, PackageRequest request);
        IResponseModel<GetPackagesResponse> GetSubmittedPackagesFromShopingcart(string userId);
    }
}
