﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Data;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Helpers;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.Map.RequestModel;
using MixedPlace.API.Models.MPDesign;
using MixedPlace.API.Models.MPDesign.ResponseModel;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.UserManagement;
using MixedPlace.API.Models.UserManagement.RequestModels;
using MixedPlace.ImageUploader.Abstractions;
using MixedPlace.ImageUploader.Implementations;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public class UserManagementService:IUserManagementService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _autoMapper;
        private readonly IImageUploader _imageUploader;
        private readonly ILogger<UserManagementService> _logger;
        private readonly IChallengeService _challengeService;
        private readonly ITransactionService _transactionService;
        private readonly INotificationsService _notificationsService;
        private readonly JWTTokenHelper _jWTTokenHelper;
        private readonly IMapService _mapService;

        public UserManagementService(ApplicationDbContext context,
                         IMapper autoMapper,
                         ILogger<UserManagementService> logger,
                         IChallengeService challengeService,
                         ITransactionService transactionService,
                         INotificationsService notificationsService,
                         JWTTokenHelper jWTTokenHelper,
                         IMapService mapService)
        {
            _context = context;
            _autoMapper = autoMapper;
            _imageUploader = ImageUploaderFactory.CreateImageUploader();
            _logger = logger;
            _challengeService = challengeService;
            _transactionService = transactionService;
            _mapService = mapService;
            _notificationsService = notificationsService;
            _jWTTokenHelper = jWTTokenHelper;
        }


        public IResponseModel<CommentResponseModel> GetComments(long x, long y, long z, int pageId, int numPerPage)
        {

            var response = new ResponseModel<CommentResponseModel>();

            MP mp = _context.MixedPlaces.FirstOrDefault(a => a.X == x && a.Y == y && a.Z == z);
            if (mp != null && mp.OwnerId != null)
            {
                var comments = _context.MPComments.Where(a => a.MPId == mp.Id).Include(u => u.User).Skip((pageId - 1)* numPerPage).Take(numPerPage).ToList();
                var retComments = _autoMapper.Map<List<MPComment>,
                               List<CommentDto>>(comments);

               
                response.Data = new CommentResponseModel() { List = new List<CommentDto>() };
                foreach (var item in retComments)
                {
                    response.Data.List.Add(item);
                }
                response.StatusCode = (int)InnerErrorCode.Ok;
            }
            else 
             response.StatusCode = (int)InnerErrorCode.MixedPlaceNotOwned;

            return response;

        }

        public IResponseModel<EmptyBodyResponse> SetMPComment(CommentRequestModel comment, string userId)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                //find MP by coordinats
                MP mp = _context.MixedPlaces.Where(a => a.X == comment.MPCoordinate.X && a.Y == comment.MPCoordinate.Y && a.Z == comment.MPCoordinate.Z).FirstOrDefault();
                if (mp != null && mp.OwnerId!=null)
                {
                    MPComment mpComment = new MPComment
                    {
                        CommentText = comment.Comment,
                        MPId = mp.Id,
                        UserId = userId,
                        CreatedDate = DateTime.Now
                    };

                    _context.MPComments.Add(mpComment);
                    _context.SaveChanges();

                    var commentDto = new CommentDto
                    {
                        Date = mpComment.CreatedDate,
                        MPCoordinate =  comment.MPCoordinate
                    };
                    _challengeService.CheckChallengeAchievment(userId, commentDto);

                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                response.StatusCode = (int)InnerErrorCode.MixedPlaceNotExistsOrNotQwned;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                response.Description = ex.Message;
            }
            return response;
        }

        public IResponseModel<EmptyBodyResponse> EditComment(CommentRequestModel request,string userId)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var comment = _context.MPComments.FirstOrDefault(x => x.Id == request.Id && x.UserId == userId);
                if (comment == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                comment.CommentText = request.Comment;
                _context.Update(comment);
                _context.SaveChanges();
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> RemoveComment(CommentRequestModel request,string userId)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var comment = _context.MPComments.FirstOrDefault(x => x.Id == request.Id && x.UserId == userId);
                if (comment == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                _context.Remove(comment);
                _context.SaveChanges();
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> SetMPReport(ReportRequestModel report, string userId)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {   
                MP mp = _context.MixedPlaces.Include(x => x.User).Where(a => a.X == report.mpCoordinats.X && a.Y == report.mpCoordinats.Y && a.Z == report.mpCoordinats.Z).FirstOrDefault();
                User reporter = _context.Users.First(x => x.Id == userId);
                if (mp != null && mp.OwnerId != null)
                {
                    MPReport mpReport = new MPReport
                    {
                        ReportText = report.ReportText,
                        MPId = mp.Id,
                        UserId = userId,
                        CreatedDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                        ReportStatusId = (int)ReportStatus.SubmitedForApprove
                    };
                    _context.Add(mpReport);
                    _context.SaveChanges();

                    _notificationsService.NotifyUserAboutReport(mp.OwnerId, report.ReportText);
                    var reportSend = _notificationsService.SendReportToAdmin(mpReport.Id,reporter.UserName,mp.User.UserName, report.ReportText);
                    _context.SaveChanges();

                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                response.StatusCode = (int)InnerErrorCode.MixedPlaceNotExistsOrNotQwned;
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                response.Description = ex.Message;
            }
            return response;
        }

        public IResponseModel<EmptyBodyResponse> SetMPLike(MixedPlaceRequestModel umrModel, string userId)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                MP mp = _context.MixedPlaces.Where(a => a.X == umrModel.X && a.Y == umrModel.Y && a.Z == umrModel.Z).FirstOrDefault();
               

                if (mp != null && mp.OwnerId!=null)
                {
                    var lastUserLike = _context.MPLikes.Where(x => x.MPId == mp.Id && x.UserId == userId).OrderByDescending(x=> x.Date).FirstOrDefault();
                    if (lastUserLike != null && lastUserLike.Date.Date.AddDays(1) <= DateTime.Now.Date || lastUserLike == null)
                    {
                        MPLike mpLike = new MPLike
                        {
                            MPId = mp.Id,
                            UserId = userId,
                            Date = DateTime.Now
                        };

                    _context.MPLikes.Add(mpLike);
                    UpdateMPOwnerPrice(mp.OwnerId, ActionType.Like);
                    _context.SaveChanges();

                        var likeDto = new LikeDto
                        {
                            Date = mpLike.Date,
                            MPCoordinate = umrModel
                        };
                        _challengeService.CheckChallengeAchievment(userId, likeDto);
                        response.StatusCode = (int)InnerErrorCode.Ok;
                        return response;
                    }

                    response.StatusCode = (int)InnerErrorCode.OnlyOneLikeADayApproved;
                    return response;
                }
                response.StatusCode = (int)InnerErrorCode.MixedPlaceNotExistsOrNotQwned;
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                response.Description = ex.Message;
            }
            return response;
        }

        public async Task<IResponseModel<EmptyBodyResponse>> SetMPVisits(SetMPVisitRequest request, string userId)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var defaultPrice = _context.Prices.First();
                var mps = _context.MixedPlaces.Include(x=> x.MPVisits)
                                              .ThenInclude(x=> x.User)                          
                                              .ToList();

                var groupedCollection = request.MPVisitsCollection.GroupBy(x => new { x.MPCoordinate.X, x.MPCoordinate.Y, x.MPCoordinate.Z },
                                                                           elementSelector: e => e.Date,
                                                                           resultSelector: (k, g) => new { Key = k, Group = g });
                foreach (var groupItem in groupedCollection)
                {
                    var mp = mps.FirstOrDefault(x => x.X == groupItem.Key.X &&
                                                    x.Y == groupItem.Key.Y &&
                                                    x.Z == groupItem.Key.Z);
                 
                    if (mp != null)
                    {
                        #region Logic When Mixed Place Is Exists
                        mp.HiddenCounter = mp.HiddenCounter + groupItem.Group.Count();
                        mp.UpdateDate = DateTime.Now;

                        if (!string.IsNullOrWhiteSpace(mp.OwnerId))
                        {
                            var visitByUser = mp.MPVisits.FirstOrDefault(x => x.UserId == userId);
                            var lastVisitInRequest = groupItem.Group.OrderByDescending(x => x).First();

                            if (visitByUser != null)
                            {
                                if (visitByUser.Date.Date.AddDays(1) <= lastVisitInRequest)
                                {
                                    visitByUser.PublicCounter = visitByUser.PublicCounter + 1;
                                    visitByUser.Date = lastVisitInRequest;
                                    _context.MPVisits.Update(visitByUser);
                                }
                            }
                            else
                            {
                                visitByUser = new MPVisit
                                {
                                    Date = DateTime.Now,
                                    MPId = mp.Id,
                                    UserId = userId,
                                    PublicCounter = 1
                                };
                                _context.MPVisits.Add(visitByUser);
                                UpdateMPOwnerPrice(mp.OwnerId, ActionType.Visit);
                            }
                        }

                        _context.MixedPlaces.Update(mp);
                        #endregion Logic When Mixed Place Is Exists
                    }
                    else
                    {
                        #region Logic When Mixed Place Is Not Exists
                        var mpAddress = await _mapService.GetMPAdress(new MPDto { X = groupItem.Key.X, Y = groupItem.Key.Y, Z = groupItem.Key.Z });
                        if (mpAddress == null)
                        {
                            response.StatusCode = (int)InnerErrorCode.MPAddressMotFound;
                            return response;
                        }

                        mp = new MP
                        {
                            CreationDate = DateTime.Now,
                            UpdateDate = DateTime.Now,
                            HiddenCounter = groupItem.Group.Count(),
                            X = groupItem.Key.X,
                            Y = groupItem.Key.Y,
                            Z = groupItem.Key.Z,
                            PriceId = defaultPrice.Id,
                            Country = mpAddress.Country,
                            City = mpAddress.City
                        };
                        _context.MixedPlaces.Add(mp);
                        #endregion Logic When Mixed Place Is Not Exists
                    }
                }

                _context.SaveChanges();

                foreach (var groupItem in groupedCollection)
                {
                    var visitDto = new VisitDto
                    {
                        MPCoordinate = new MPCoordinateDto { X = groupItem.Key.X, Y = groupItem.Key.Y, Z = groupItem.Key.Z }
                    };

                    _challengeService.CheckChallengeAchievment(userId, visitDto);
                }

                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> SetMPVisits(MixedPlaceRequestModel umrModel,string userId)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                //find MP by coordinats
                MP mp = _context.MixedPlaces.Where(a => a.X == umrModel.X && a.Y == umrModel.Y && a.Z == umrModel.Z).FirstOrDefault();

                //if MP not exists, add to MixedPlace Table (without ownerId)
                if (mp == null)
                {
                    mp = new MP();
                    mp.X = umrModel.X;
                    mp.Y = umrModel.Y;
                    mp.Z = umrModel.Z;
                    mp.HiddenCounter =1;
                    mp.CreationDate = DateTime.Now;
                    mp.UpdateDate = DateTime.Now;
                    _context.MixedPlaces.Add(mp);
                    _context.SaveChanges();
                }
                else
                {
                    mp.HiddenCounter++;
                    mp.UpdateDate = DateTime.Now;
                    _context.MixedPlaces.Update(mp);
                    _context.SaveChanges();
                }
                //check if MP has OwnerId
                if (mp != null && !String.IsNullOrEmpty(mp.OwnerId))
                {
                    //check if User already has visited this MP today
                    MPVisit mPVisit = _context.MPVisits.Where(x => x.MPId == mp.Id && x.UserId==userId).FirstOrDefault();
                   
                   /* DateTime mpVisitDate= DateTime.ParseExact(mPVisit.Date.ToString("s"), "yyyy-mm-dd", CultureInfo.InvariantCulture);
                    DateTime today = DateTime.ParseExact(DateTime.Today.Date.ToString("s"), "yyyy-mm-dd", CultureInfo.InvariantCulture);*/
                    if (mPVisit != null && mPVisit.Date.Date.AddDays(1) <= DateTime.Now.Date)
                    {
                        response.StatusCode = (int)InnerErrorCode.MixedPlaceAlreadyVisited;
                        return response;
                    }
                    if (mPVisit == null)
                    {
                        mPVisit = new MPVisit();
                        mPVisit.Date = DateTime.Now;
                        mPVisit.UserId = userId;
                        mPVisit.MPId = mp.Id;
                        mPVisit.PublicCounter = 1;
                        _context.MPVisits.Add(mPVisit);
                        _context.SaveChanges();
                        response.StatusCode = (int)InnerErrorCode.Ok;
                        return response;
                    }
                    mPVisit.PublicCounter++;
                    mPVisit.Date= DateTime.Now;
                    _context.MPVisits.Update(mPVisit);
                    UpdateMPOwnerPrice(mp.OwnerId, ActionType.Visit);
                    _context.SaveChanges();
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                else
                {
                    response.StatusCode = (int)InnerErrorCode.MixedPlaceNotOwned;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                response.Description = ex.Message;
            }
            return response;
        }

        public IResponseModel<EmptyBodyResponse> SetMPShare(MPCoordinateDto coordinates, string source, string userId)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                //find MP by coordinats
                MP mp = _context.MixedPlaces.Where(a => a.X == coordinates.X && a.Y == coordinates.Y && a.Z == coordinates.Z).FirstOrDefault();

                //if MP not exists, add to MixedPlace Table (without ownerId)
                if (mp != null && mp.OwnerId != null)
                {
                    MPShare mpShare = new MPShare
                    {
                        MPId = mp.Id,
                        UserId = userId,
                        Date = DateTime.Now,
                        Source = source
                    };

                    _context.MPShares.Add(mpShare);

                    UpdateMPOwnerPrice(mp.OwnerId, ActionType.Share);
                    _context.SaveChanges();

                    var shareDto = new ShareDto
                    {
                        Date = mpShare.Date,
                        MPCoordinate = coordinates
                    };
                    _challengeService.CheckChallengeAchievment(userId, shareDto);
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                response.StatusCode = (int)InnerErrorCode.MixedPlaceNotExistsOrNotQwned;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                response.Description = ex.Message;
            }
            return response;
        }

        public void ApproveReport(string token)
        {
            try
            {
                if (_jWTTokenHelper.ValidateAdminEmailJWTToken(token, out ClaimsPrincipal claimsPrincipal))
                {
                    var reportId = _jWTTokenHelper.GetSubFromJWTToken(claimsPrincipal);
                    if (!string.IsNullOrWhiteSpace(reportId))
                    {
                        var report = _context.MPReports.FirstOrDefault(x => x.Id == Convert.ToInt32(reportId));
                        report.ReportStatusId = (int)ReportStatus.ApprovedByAdmin;
                        report.UpdateDate = DateTime.Now;
                        _context.Update(report);
                        _context.SaveChanges();
                    }
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
            }
        }

        public void RefuseReport(string token)
        {
            try
            {
                if (_jWTTokenHelper.ValidateAdminEmailJWTToken(token, out ClaimsPrincipal claimsPrincipal))
                {
                    var reportId = _jWTTokenHelper.GetSubFromJWTToken(claimsPrincipal);
                    if (!string.IsNullOrWhiteSpace(reportId))
                    {
                        var report = _context.MPReports.FirstOrDefault(x => x.Id == Convert.ToInt32(reportId));
                        report.ReportStatusId = (int)ReportStatus.RefusedByAdmin;
                        report.UpdateDate = DateTime.Now;
                        _context.Update(report);
                        _context.SaveChanges();
                    }
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
            }
        }

        public void UpdateMPOwnerPrice(string userid, ActionType type)
        {
            var actiontype = _context.ChallengeActionTypes.Where(x => x.Name == type.ToString()).FirstOrDefault();
            if (actiontype != null)
            {
                var price = _context.MPOperationeCoinsPrice.Where(x => x.Id == actiontype.Id).FirstOrDefault();
                if (price != null && price.PriceInCoins > 0)
                {
                    _transactionService.AddTransaction(TransactionCategoryEnum.MPOwnerPriceReceived,Convert.ToDecimal(price.PriceInCoins), 0, "", userid);
                }
            }
        }
    }
}
