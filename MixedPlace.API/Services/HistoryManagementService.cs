﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Data;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.HistoryManagement;
using MixedPlace.API.Models.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public class HistoryManagementService : IHistoryManagmentService
    {
        private ApplicationDbContext _dbContext;
        private ILogger<HistoryManagementService> _logger;
        private IMapper _mapper;

        public HistoryManagementService(ApplicationDbContext dbContext,
                                        IMapper mapper,
                                        ILogger<HistoryManagementService> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
            _mapper = mapper;
        }

      

        public bool MoveMixedPlaceDataToHistoryTable(MP mp)
        {
            try
            {
                if (mp == null) return false;

                var mpHistory = _mapper.Map<MP, MPHistoryModel>(mp);
                var categories = _dbContext.MPCatSubCat.Where(x => x.MPId == mp.Id).ToList();

                var historyCartegories = _mapper.Map<List<MPCatSubCat>, List<MPHistoryModelCatSubCat>>(categories);
                
                foreach(var item in historyCartegories)
                {
                    item.MP = mpHistory;
                };
                _dbContext.MPHistoryCatSubCat.AddRange(historyCartegories);
                _dbContext.MPCatSubCat.RemoveRange(categories);
                _dbContext.Add(mpHistory);

                mp.OwnerId = null;
                mp.UpdateDate = DateTime.Now;
                mp.MPStatusId = (int)MixedPlaceStatus.Available;
                mp.DesignStatus = null;
                mp.Description = null;
                mp.PurchaseDate = null;
                _dbContext.Update(mp);
                

                _dbContext.AchievedChallenges.RemoveRange(mp.AchievedChallenges);
                _dbContext.MPItems.RemoveRange(mp.MPDesigns);
                _dbContext.MPComments.RemoveRange(mp.MPComments);
                _dbContext.MPShares.RemoveRange(mp.MPShares);
                _dbContext.MPVisits.RemoveRange(mp.MPVisits);
                _dbContext.MPLikes.RemoveRange(mp.MPLike);

                _dbContext.SaveChanges();

                return true;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                return false;
            }
        }
    }
}
