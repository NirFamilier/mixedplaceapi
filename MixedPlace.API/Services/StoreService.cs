﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Data;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.DataTransferObjects.StoreDtos;
using MixedPlace.API.Helpers;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Common.MixedPlaceExtensions;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.Package;
using MixedPlace.API.Models.Package.RequestModels;
using MixedPlace.API.Models.Package.ResponseModels;
using MixedPlace.ImageUploader.Abstractions;
using MixedPlace.ImageUploader.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public class StoreService: IStoreService
    {
        private readonly ILogger<StoreService> _logger;
        private readonly ApplicationDbContext _dbContext;
        private readonly IMapper _autoMapper;
        private readonly AdminTablesMapper _adminTableMapper;
        private readonly ITransactionService _transactionService;
        private readonly IImageUploader _imageUploader;

        public StoreService(ILogger<StoreService> logger,
            ApplicationDbContext dbContext,
            AdminTablesMapper adminTableMapper,
            IMapper autoMapper,
            ITransactionService transactionService)
        {
            _logger = logger;
            _dbContext = dbContext;
            _autoMapper = autoMapper;
            _adminTableMapper = adminTableMapper;
            _transactionService = transactionService;
            _imageUploader = ImageUploaderFactory.CreateImageUploader();
        }

        #region Public Methods

        #region Store Controller Methods

        public IResponseModel<EmptyBodyResponse> BuyPackages(string userId, BuyPackageRequest request)
        {
            
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
               
                var user = _dbContext.Users.Include(x=> x.PurchasedPackages).ThenInclude(x => x.Package)
                                           .Where(p => p.PurchasedPackages.Any(pi=> pi.PurchaseStatusId == (int)PurchaseStatus.Submitted))
                                           .Include(x=> x.ModelsPerUser)
                                           .FirstOrDefault(x=> x.Id == userId);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.NoPackagesInSoppingCart;
                    return response;
                }
                if (user.CoinBalance < user.PurchasedPackages.Where(x=>x.Package.PriceTypeId == (int)PriceType.Coins) .Sum(x=> x.Package.Price))
                {
                    var declienedPackages = user.PurchasedPackages.Select(x => { x.PurchaseStatusId = (int)PurchaseStatus.Decliend; return x; }).ToList();
                    _dbContext.PurchasedPackages.UpdateRange(declienedPackages);
                    _dbContext.SaveChanges();

                    response.StatusCode = (int)InnerErrorCode.InsufficientBalance;
                    return response;
                }

                if (user == null || user.PurchasedPackages?.Count() == 0)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                string incoiceUrl = string.Empty; ;
                if (!string.IsNullOrWhiteSpace(request.InvoiceBase64))
                {
                    var bytes = Convert.FromBase64String(request.InvoiceBase64);
                    incoiceUrl = _imageUploader.UploadImage(bytes, $"Invoice", request.InvoiceMimeType, "Invoices");
                }

                foreach (var packagePurchase in user.PurchasedPackages)
                {
                    var package = packagePurchase.Package;

                    if (package.PriceTypeId == (int)PriceType.Coins)
                    {
                       
                        HandlePackageItems(package);
                        user.CoinBalance -= package.Price;
                        _transactionService.AddTransaction(TransactionCategoryEnum.BuyPackage, package.Price, 0, "", userId);
                        _dbContext.Update(user);
                    }
                    else
                    {
                        HandlePackageItems(package);
                        _transactionService.AddTransaction(TransactionCategoryEnum.BuyPackage, 0, package.Price, "", userId);
                    }

                    package.PurchasedUnits += 1;
                    packagePurchase.AmountPaid = package.Price;
                    packagePurchase.PurchaseStatusId = (int)PurchaseStatus.PaidUp;
                    packagePurchase.PaymentDate = DateTime.Now;
                    packagePurchase.UpdatedDate = DateTime.Now;

                    packagePurchase.InvoiceUrl = package.PriceTypeId == (int)PriceType.Currency ? incoiceUrl : string.Empty;

                    _dbContext.Update(packagePurchase);
                    _dbContext.Update(package);
                }

             
              
                _dbContext.SaveChanges();

                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;

                #region Inner Methods

                void HandlePackageItems(PackageModel package)
                {
                    foreach (var packageItem in package.PackageItems)
                    {
                        if (packageItem.PackageItemTypeId == (int)PackageItemType.Coin)
                        {
                            user.CoinBalance += (packageItem.NumOfUnits ?? 0);
                            _transactionService.AddTransaction(TransactionCategoryEnum.BuyPackageReward, packageItem.NumOfUnits ?? 0, 0, "USD", userId);
                        }
                        else
                        {
                            if (user.ModelsPerUser.Any(x => x.MPModelId == packageItem.ModelId))
                            {
                                var userModel = user.ModelsPerUser.First(x => x.MPModelId == packageItem.ModelId);
                                userModel.TotalModelsCount += (packageItem.NumOfUnits ?? 0);
                                userModel.RemainingModelsCount += (packageItem.NumOfUnits ?? 0);
                                userModel.UpdateDate = DateTime.Now;
                                _dbContext.Update(userModel);
                            }
                            else
                            {
                                var userModel = new MPModelPerUser
                                {
                                    RemainingModelsCount = (packageItem.NumOfUnits ?? 0),
                                    TotalModelsCount = (packageItem.NumOfUnits ?? 0),
                                    CreationDate = DateTime.Now,
                                    UpdateDate = DateTime.Now,
                                    MPModelId = packageItem.ModelId ?? 0,
                                    UserId = userId
                                };
                                _dbContext.Add(userModel);
                            }

                        }
                    }
                    _dbContext.SaveChanges();
                }

                #endregion Inner Methods
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<GetPackageResponse> AddPackageToShoppingCart(string userId, AddPackageToUserShoppingCartRequest request)
        {
            var response = new ResponseModel<GetPackageResponse>();
            try
            {
                var package = _dbContext.Packages.LoadWithAllRelationships(x => x.PackageStatusId == (int)PackageStatus.Active &&
                                                                                x.Id == request.PackageId).FirstOrDefault();
                var user = _dbContext.Users.FirstOrDefault(x => x.Id == userId);

                if (package == null || user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                if (package.PriceTypeId == (int)PriceType.Coins && user.CoinBalance < package.Price)
                {
                    response.StatusCode = (int)InnerErrorCode.InsufficientBalance;
                    return response;
                }

                var packagePurchase = new PackagePurchaseModel
                {
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    PackageId = request.PackageId,
                    PurchaseStatusId = (int)PurchaseStatus.Submitted,
                    UserId = userId
                };
                _dbContext.Add(packagePurchase);
                _dbContext.SaveChanges();
                response.Data = new GetPackageResponse { PackageId=request.PackageId};
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;

            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> RemovePackageFromUserShoppingCart(string userId, PackageRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                //remove user's package with PurchaseStatus is Submitted from Purchased Table 
                var package = _dbContext.PurchasedPackages.OrderByDescending(x=> x.UpdatedDate)
                                                          .FirstOrDefault(x => x.UserId == userId && 
                                                                      x.PackageId==request.PackageId &&
                                                                      x.PurchaseStatusId == (int)PurchaseStatus.Submitted);
                if (package != null && package.PurchaseStatusId==(int)PurchaseStatus.Submitted)
                {
                    package.PurchaseStatusId = (int)PurchaseStatus.Canceled;
                    _dbContext.PurchasedPackages.Update(package);
                    _dbContext.SaveChanges();
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                response.StatusCode = (int)InnerErrorCode.PackageInSubmittedStatusNotExist;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<GetPackagesResponse> GetSubmittedPackagesFromShopingcart(string userId)
        {
            var response = new ResponseModel<GetPackagesResponse>();
            try
            {
                var packages = _dbContext.PurchasedPackages.Where(x => x.UserId == userId && x.PurchaseStatusId==(int)PurchaseStatus.Submitted).ToList();
                if (packages != null && packages.Count>0)
                {
                    var packagesDto = _autoMapper.Map<IEnumerable<PackagePurchaseModel>, IEnumerable<PackageDto>>(packages);
                    response.Data = new GetPackagesResponse { Packages = packagesDto };
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
                response.StatusCode = (int)InnerErrorCode.PackageInSubmittedStatusNotExist;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<GetClientStorePackagesResponse> GetClientStorePackages(PackageCategoryType ptype, bool retrieveallpackages, int numofpages, int pagenum)
        {
            var response = new ResponseModel<GetClientStorePackagesResponse>();
            try
            {

                var currentDate = DateTime.Now;
                IEnumerable<PackageModel> dbPackages = null;
                if (retrieveallpackages)
                {
                    dbPackages = _dbContext.Packages.LoadWithAllRelationships(x => currentDate > x.AvailableFrom.Value &&
                                                                               (x.ExpiresOn != null && x.ExpiresOn.Value > currentDate) &&
                                                                               x.PackageStatusId == (int)PackageStatus.Active &&
                                                                               x.PackageCategoryId != (int)PackageCategoryType.Default
                                                                               ).Skip((pagenum - 1) * numofpages).Take(numofpages).ToList();
                }
                else
                dbPackages = _dbContext.Packages.LoadWithAllRelationships(x => currentDate > x.AvailableFrom.Value &&
                                                                                (x.ExpiresOn != null && x.ExpiresOn.Value > currentDate) &&
                                                                                x.PackageStatusId == (int)PackageStatus.Active &&
                                                                                x.PackageCategoryId == (int)ptype
                                                                                ).Skip((pagenum - 1) * numofpages).Take(numofpages).ToList();

                var packages = dbPackages.Select(x => new ClientStorePackageDto
                {
                    Id = x.Id,
                    ImageUrl = x.ImageUrl,
                    ExpiresOn = x.ExpiresOn ?? DateTime.MinValue,
                    Description = x.Description,
                    Name = x.Name,
                    PackageType = x.PackageCategoryId == null ? default(PackageCategoryType) : (PackageCategoryType)x.PackageCategoryId,
                    Price = x.Price,
                    PriceType = (PriceType)x.PriceTypeId,
                    UpdateDate = x.UpdateDate,
                    PackageItems = x.PackageItems.Select(pi => new PackageItemDto
                    {
                        Id = pi.Id,
                        Sum = pi.Sum ?? 0,
                        NumberOfUnits = pi.NumOfUnits ?? 0,
                        TotalSize = (int)pi.TotalSize ,
                        PackageItemType = new PackageItemTypeDto { Id = pi.PackageItemTypeId ?? 0 },
                        Model = new PackageModelItemDto
                        {
                            Id = pi.ModelId ?? 0,
                            ImageUrl = pi.Model?.Bundle?.Url,
                            Name = pi.Model?.Name
                        }
                    })
                });
                response.Data = new GetClientStorePackagesResponse { Packages = packages };
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        #endregion Store Controller Methods

        #region Admin Methods

        public IResponseModel<GetStoreMetadataResponse> GetStoreMetadata()
        {
            var response = new ResponseModel<GetStoreMetadataResponse>();
            try
            {
                var packageStatuses = _dbContext.PackageStatuses.Select(x => new PackageStatusDto
                                                                             {
                                                                                Id = x.Id,
                                                                                Name = x.Name
                                                                             }).ToList();

                var packageCategories = _dbContext.PackageCategories.Select(x=> new PackageCategoryDto
                                                                                {
                                                                                    Id = x.Id,
                                                                                    Name = x.Name
                                                                                }).ToList();

                var packageItemsTypes = _dbContext.PackageItemTypes.Select(x=> new PackageItemTypeDto
                                                                                {
                                                                                    Id = x.Id,
                                                                                    Name = x.Name
                                                                                }).ToList();

                var priceTypes = _dbContext.PriceTypes.Select(x=> new PriceTypeDto { Id = x.Id, Name = x.Name }).ToList();

                response.Data = new GetStoreMetadataResponse
                {
                     PackageCategories = packageCategories,
                     PackageItemsTypes = packageItemsTypes,
                     PackageStatuses = packageStatuses,
                     PriceTypes = priceTypes,
                     Filters = _adminTableMapper.PackagesTableFiltersConditionsMap
                };
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<GetPackagesResponse> GetPackages(FilterRequest filterRequest = null)
        {

            var response = new ResponseModel<GetPackagesResponse>();
            try
            {
                if (filterRequest == null) 
                {
                    var packages = _dbContext.Packages.LoadWithAllRelationships();
                    
                    response.Data = new GetPackagesResponse
                    {
                        Packages = _autoMapper.Map<IEnumerable<PackageModel>, IEnumerable<PackageDto>>(packages),
                        Summary = filterRequest
                    };
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;

                }
                else  
                {
                    var packages = _dbContext.Packages.LoadWithAllRelationships();

                    IEnumerable<PackageDto> result = _autoMapper.Map<IEnumerable<PackageModel>, IEnumerable<PackageDto>>(packages);
                    if (!string.IsNullOrWhiteSpace(filterRequest.Search))
                        result = SearchPackages(result, filterRequest.Search);

                    if (filterRequest.Filters != null && filterRequest.Filters.Count() > 0)
                        result = FilterHelper.Filter(result, filterRequest.Filters, _adminTableMapper.PackagesTableMap);

                    response.Data = new GetPackagesResponse
                    {
                        Packages = result?.Skip((filterRequest.Page - 1) * filterRequest.AmountPerPage)?.Take(filterRequest.AmountPerPage)?.ToList(),
                        Summary = filterRequest
                    };
                    response.Data.Summary.TotalAmount = result?.Count() ?? 0;
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    return response;
                }
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> UpdatePackage(UpdatePackageRequest request)
        {

            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {

                var package = _dbContext.Packages.LoadWithAllRelationships(x => x.PackageStatusId != (int)PackageStatus.Active &&
                                                                                x.Id == request.Id).FirstOrDefault();
                if (package == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                _dbContext.RemoveRange(package.PackageItems);

                 _autoMapper.Map(request, package, typeof(UpdatePackageRequest), typeof(PackageModel));
                var packageItems = request.PackageItems.Select(x => new PackageItemModel
                {
                    NumOfUnits = x.NumberOfUnits,
                    PackageItemTypeId = x.PackageItemType?.Id,
                    ModelId = x.Model?.Id,
                    PackageId = package.Id
                }).ToList();


                if (!string.IsNullOrWhiteSpace(request.ImageBase64))
                {
                    var image = request.ImageBase64.ParseFromBase64();
                    var bytes = Convert.FromBase64String(image.data);
                    var imageUrl = _imageUploader.UploadImage(bytes, $"{request.Name}", image.mimeType, "Packages");
                    if (!string.IsNullOrWhiteSpace(imageUrl))
                        package.ImageUrl = imageUrl;
                }

                _dbContext.AddRange(packageItems);
                _dbContext.Update(package);
                _dbContext.SaveChanges();

                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;

            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public IResponseModel<EmptyBodyResponse> CreatePackage(CreatePackageRequest request)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var packageModel = _autoMapper.Map<CreatePackageRequest, PackageModel>(request);
                packageModel.PackageStatusId = (int)PackageStatus.Active;
                packageModel.UpdateDate = DateTime.Now;
                packageModel.PackageItems = request.PackageItems.Select(x=> new PackageItemModel
                                                   { NumOfUnits = x.NumberOfUnits,
                                                     PackageItemTypeId = x.PackageItemType?.Id,
                                                     ModelId = x.Model?.Id}).ToList();
                if (!string.IsNullOrWhiteSpace(request.ImageBase64))
                {
                    var image = request.ImageBase64.ParseFromBase64();
                    var bytes = Convert.FromBase64String(image.data);
                    var imageUrl = _imageUploader.UploadImage(bytes, $"{request.Name}", image.mimeType, "Packages");
                    if (!string.IsNullOrWhiteSpace(imageUrl))
                        packageModel.ImageUrl = imageUrl;
                }
                packageModel.CreateDate = DateTime.Now;
                _dbContext.Add(packageModel);
                _dbContext.SaveChanges();
                
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;

            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }

        }

        public IResponseModel<EmptyBodyResponse> CreatePackage(PackageModel package)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                //package.CreateDate =DateTime.Now;
                _dbContext.Packages.Add(package);
                _dbContext.SaveChanges();

                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
            
        }


        public IResponseModel<EmptyBodyResponse> UpdatePackageStatus(UpdatePackageStatusRequest request)
        {

            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var package = _dbContext.Packages.FirstOrDefault(x => x.Id == request.Id);
                var packageStatus = _dbContext.PackageStatuses.FirstOrDefault(x => x.Id == request.PackageStatus.Id);
                if (package == null || packageStatus == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }
                package.PackageStatusId = packageStatus.Id;
                _dbContext.Update(package);
                _dbContext.SaveChanges();

                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;

            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }

        }

      

        #endregion  Admin Methods

        #endregion Public Methods

        #region Private Methods

        private IEnumerable<PackageDto> SearchPackages(IEnumerable<PackageDto> packages, string search)
        {
            return packages.Where(x => x.CreateDate.ToString().Contains(search) ||
                                       x.AvailableFrom.ToString().Contains(search) ||
                                       x.ExpiresOn.ToString().Contains(search) ||
                                       (!string.IsNullOrWhiteSpace(x.Description) && x.Description.Contains(search)) ||
                                       x.Id.ToString().Contains(search) ||
                                       x.PurchasedUnits.ToString().Contains(search)||
                                       x.Price.ToString().Contains(search) ||
                                       (!string.IsNullOrWhiteSpace(x.Name) && x.Name.Contains(search) )||
                                       (x.PackageCategory != null && x.PackageCategory.Name.Contains(search)) ||
                                       (x.PackageStatus != null && x.PackageStatus.Name.Contains(search) ||
                                       (x.PriceType != null && x.PriceType.Name.Contains(search))));
        }

        #endregion Private Methods

    }
}
