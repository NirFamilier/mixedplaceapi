﻿using Microsoft.Extensions.Configuration;
using MixedPlace.API.Providers.Abstractions;
using MixedPlace.API.Providers.Implementatios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Providers
{
    public class LoginProviderFactory
    {
        public static ILoginProvider CreateLoginProvider(string providerName, IConfiguration configuration)
        {
            switch (providerName.ToLower())
            {
                case "googleios": 
                case "googleandroid":
                    {
                        return new GoogleLoginProvider(configuration);
                    }
                case "facebook":
                    {
                        return new FacebookLoginProvider(configuration);
                    }
                default:
                    return null;
            }               
        }
    }
}
