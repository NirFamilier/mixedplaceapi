﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Providers.Abstractions
{
    public interface ILoginProvider
    {
        Task<bool> AuthenticateToken(string accessToken);
        Task <ProviderUserInfoDto> GetUserDetials(string accessToken);
    }
}
