﻿using Microsoft.Extensions.Configuration;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Providers.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Providers.Implementatios
{
    public class GoogleLoginProvider : ILoginProvider
    {
        private string googleToken;
        public IConfiguration _configurations;
        public GoogleLoginProvider(IConfiguration configuration)
        {
            _configurations = configuration;
        }
        public async Task<bool> AuthenticateToken(string accessToken)
        {
            try
            {
                using (var client = new HttpClient())
                {

                    var formData = new Dictionary<string, string>();
                    formData.Add("client_id", _configurations["Auth:Google:Clientid"]);
                    formData.Add("redirect_uri", _configurations["Auth:Google:RedirectUrl"]);
                    formData.Add("client_secret", _configurations["Auth:Google:Clientsecret"]);
                    formData.Add("grant_type", _configurations["Auth:Google:GrantType"]);
                    formData.Add("code", accessToken);

                    var request = new HttpRequestMessage(HttpMethod.Post, _configurations["Auth:Google:AuthUrl"])
                    {
                        Content = new FormUrlEncodedContent(formData)
                    };
                    var responseMessage = await client.SendAsync(request);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var contentString = await responseMessage.Content.ReadAsStringAsync();
                        var authResponse = JsonConvert.DeserializeObject<GoogleTokenResponse>(contentString);
                        if (DateTime.Now.AddSeconds(authResponse.ExpiresIn) > DateTime.Now)
                        {
                            googleToken = authResponse.AccessToken;
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;


                }
            }
            catch (Exception exp)
            {
                return false;
            }
          
        }

        public async Task<ProviderUserInfoDto> GetUserDetials(string accessToken = null)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var getResponse = await httpClient.GetAsync($"{_configurations["Auth:Google:UserDetailsUrl"]}access_token={googleToken}");
                    if (getResponse.IsSuccessStatusCode)
                    {
                        var contentString = await getResponse.Content.ReadAsStringAsync();
                        if (!string.IsNullOrWhiteSpace(contentString))
                        {
                            var userInfo = JsonConvert.DeserializeObject<GoogleUsrInfo>(contentString);
                            if (userInfo != null)
                            {
                                var userDto = new ProviderUserInfoDto
                                {
                                    Email = userInfo.Email,
                                    FirstName = userInfo.FirstName,
                                    FullName = userInfo.Name,
                                    Id = userInfo.Id,
                                    LastName = userInfo.LastName,
                                    Gender = userInfo.Gender == "male" ? true: false ,
                                    ImageUrl = userInfo.PictureUrl
                                };

                                return userDto;
                            }
                        }
                    }
                    return null;
                }
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }

    class GoogleTokenResponse
    {
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }
        [JsonProperty(PropertyName = "token_type")]
        public string TokenType { get; set; }
        [JsonProperty(PropertyName = "expires_in")]
        public int ExpiresIn { get; set; }
        [JsonProperty(PropertyName = "id_token")]
        public string IdToken { get; set; }



    }

    class GooglTokenInfoResponse
    {
        [JsonProperty(PropertyName = "azp")]
        public string AZP { get; set; }

        [JsonProperty(PropertyName = "aud")]
        public string Aud { get; set; }

        [JsonProperty(PropertyName = "sub")]
        public string Sub { get; set; }

        [JsonProperty(PropertyName = "scope")]
        public string Scope { get; set; }

        [JsonProperty(PropertyName = "exp")]
        public int ExpireAt { get; set; }

        [JsonProperty(PropertyName = "expires_in")]
        public int ExpireIn { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "email_verified")]
        public bool EmailVerified { get; set; }

        [JsonProperty(PropertyName = "access_type")]
        public string AccessType { get; set; }

    }

    class GoogleUsrInfo
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "verified_email")]
        public bool EmailVerified { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "given_name")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "family_name")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "link")]
        public string Link { get; set; }

        [JsonProperty(PropertyName = "picture")]
        public string PictureUrl { get; set; }

        [JsonProperty(PropertyName = "gender")]
        public string Gender { get; set; }

        [JsonProperty(PropertyName = "locale")]
        public string Locale { get; set; }

    }
}
