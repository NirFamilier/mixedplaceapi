﻿using Microsoft.Extensions.Configuration;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Providers.Abstractions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Providers.Implementatios
{
    public class FacebookLoginProvider : ILoginProvider
    {
        private readonly IConfiguration _configurations;
        public FacebookLoginProvider(IConfiguration configuration)
        {
            _configurations = configuration;
        }
        public async Task<bool> AuthenticateToken(string accessToken)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var getResponse = await httpClient.GetAsync($"{_configurations["Auth:Facebook:UserDetailsUrl"]}access_token={accessToken}");//($"{ _configurations["Auth:Facebook:AuthUrl"]}input_token={accessToken}&access_token={accessToken}");
                    if (getResponse.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception exp)
            {
                return false;
            }
        }


        public async Task<ProviderUserInfoDto> GetUserDetials(string accessToken)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var getResponse = await httpClient.GetAsync($"{_configurations["Auth:Facebook:UserDetailsUrl"]}access_token={accessToken}&fields=name,email,location,first_name,last_name,gender,picture.width(1200)");
                    if (getResponse.IsSuccessStatusCode)
                    {
                        var contentString = await getResponse.Content.ReadAsStringAsync();
                        if (!string.IsNullOrWhiteSpace(contentString))
                        {
                            var userInfo = JsonConvert.DeserializeObject<FacebookUserInfo>(contentString);
                            if (userInfo != null)
                            {
                                var userDto = new ProviderUserInfoDto
                                {
                                    Email = userInfo.Email,
                                    FirstName = userInfo.FirstName,
                                    FullName = userInfo.Name,
                                    Id = userInfo.Id,
                                    LastName = userInfo.LastName,
                                    Gender = userInfo.Gender == "male" ? true: false
                                };

                                return userDto;
                            }
                        }
                    }
                    return null;
                }
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }

    class FacebookUserInfo
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "first_name")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "last_name")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "gender")]
        public string Gender { get; set; }
        

    }

    class FacebookAuthenticationResponse
    {
        [JsonProperty(PropertyName = "data")]
        public FacebookAuthencticatioData Data { get; set; }
    }

    class FacebookAuthencticatioData
    {
        [JsonProperty(PropertyName = "app_id")]
        public string AppId { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "application")]
        public string Application { get; set; }

        [JsonProperty(PropertyName = "expires_at")]
        public int ExpiresAt { get; set; }

        [JsonProperty(PropertyName = "is_valid")]
        public bool IsValid { get; set; }

        [JsonProperty(PropertyName = "issued_at")]
        public int IssuedAt { get; set; }

        [JsonProperty(PropertyName = "scopes")]
        public IEnumerable<string> Scopes { get; set; }

        [JsonProperty(PropertyName = "user_id")]
        public string UserId { get; set; }

    }
}
