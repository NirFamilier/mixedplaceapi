﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using MixedPlace.API.Data;
using MixedPlace.API.Helpers;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.User;
using MixedPlace.API.Services;
using Serilog;
using Serilog.Events;
using Swashbuckle.AspNetCore.Swagger;



namespace MixedPlace.API
{
    public class Startup
    {


        public Startup(IConfiguration configuration, IHostingEnvironment currentEnvironment)
        {
            Configuration = configuration;
            CurrentEnvironment = currentEnvironment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment CurrentEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });


            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddSerilog(dispose: true).AddConfiguration(Configuration);
            });

            // ===== Add our DbContext ========
            //"Env:IsDev"
         
            if (CurrentEnvironment.IsEnvironment("Testing"))
            {
                services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseInMemoryDatabase("TestingDB"));
            }
            else
            {
                services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(Configuration["Default:ConnectionString"]));
            }
            //  services.AddDbContext<ApplicationDbContext>();
            //services.AddDbContext<ApplicationDbContext>(options =>
            //   options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // ===== Add Identity ========
            services.AddIdentity<User, IdentityRole>(options=> 
                {
                    options.Password.RequiredUniqueChars = 0;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 4;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // ===== Add JWT Authentication ========
            ConfigureJWTAuthentication(services);

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1",
                    new Info
                    {
                        Title = "Mixed Place",
                        Version = "v1",
                        Description = "API Documentation",
                        TermsOfService = "WTFPL",
                        Contact = new Contact
                        {
                            Email = "",
                            Name = "Mixed Place",
                            Url = ""
                        }
                    }
                );

                var xmlDocFile = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, @"MixedPlace.API.xml");
                options.IncludeXmlComments(xmlDocFile);
                options.DescribeAllEnumsAsStrings();
            });

            //====== Add AutoMapper =
            services.AddAutoMapper(x=> x.AddProfile(new MappingProfile()));

            //====== Add Dependencies
            ConfigureDependencies(services);

            // ===== Add MVC ========
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,IHostingEnvironment env,ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                
            }
            
            loggerFactory.AddSerilog().AddFile(Configuration.GetSection("Logging"));
            app.UseCors("CorsPolicy");
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });



            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            //// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MixedPlace  V1");
            });


            app.UseAuthentication();
            app.UseMvc();

            // ===== Create tables ======
            //dbContext.Database.EnsureCreated();
        }

        #region Private Methods
        private void ConfigureDependencies(IServiceCollection services)
        {
            services.AddScoped<IHistoryManagmentService, HistoryManagementService>();
            services.AddSingleton<AdminTablesMapper>();
            services.AddSingleton<JWTTokenHelper>();
            services.AddScoped<IPasswordHasher<User>, PasswordHasher<User>>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IMapService, MapService>();
            services.AddScoped<IChallengeService, ChallengeService>();
            services.AddScoped<IUserManagementService, UserManagementService>();
            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<IStoreService, StoreService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<INotificationsService, NotificationsService>();
        }

        private void ConfigureJWTAuthentication(IServiceCollection services)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            X509Certificate2 cert = new X509Certificate2(Configuration["Auth:Jwt:CertName"], Configuration["Auth:Jwt:CertPass"]);
            SecurityKey signKey = new X509SecurityKey(cert);
            services
                .AddAuthorization(
                options =>
                {
                    options.AddPolicy("Admin", policy => policy.RequireClaim("Admin"));
                })
                .AddAuthentication(
               options =>
               {
                   options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                   options.DefaultSignInScheme = "Temporary";
                   options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
               }
                ).AddGoogle("google", options =>
                {
                    options.ClientId = Configuration["Auth:Google:Clientid"];
                    options.ClientSecret = Configuration["Auth:Google:Clientsecret"];
                }).AddFacebook("facebook", options =>
                {
                    options.AppId = Configuration["Auth:Facebook:Appid"];
                    options.AppSecret = Configuration["Auth:Facebook:Appsecret"];
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["Auth:Jwt:JwtIssuer"],
                        ValidAudience = Configuration["Auth:Jwt:JwtIssuer"],
                        IssuerSigningKey = signKey,
                        RequireExpirationTime = true,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };
                })
                .AddCookie("Temporary");
        }

        #endregion Private Methods
    }

}
