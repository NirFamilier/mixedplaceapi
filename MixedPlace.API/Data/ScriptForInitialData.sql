USE [MixedPlaceAdminIntegrationThree]
GO
SET IDENTITY_INSERT [dbo].[ChallengeActionTypes] ON 
GO
INSERT [dbo].[ChallengeActionTypes] ([Id], [Name]) VALUES (1, N'Visit')
GO
INSERT [dbo].[ChallengeActionTypes] ([Id], [Name]) VALUES (2, N'Like')
GO
INSERT [dbo].[ChallengeActionTypes] ([Id], [Name]) VALUES (3, N'Comment')
GO
INSERT [dbo].[ChallengeActionTypes] ([Id], [Name]) VALUES (4, N'Share')
GO
SET IDENTITY_INSERT [dbo].[MPOperationeCoinsPrice] ON 
GO
INSERT [dbo].[MPOperationeCoinsPrice] ([CATMId],[PriceInCoins]) VALUES (1, 0)
GO
INSERT [dbo].[MPOperationeCoinsPrice] ([CATMId],[PriceInCoins]) VALUES (2, 0)
GO
INSERT [dbo].[MPOperationeCoinsPrice] ([CATMId],[PriceInCoins]) VALUES (3, 0)
GO
INSERT [dbo].[MPOperationeCoinsPrice] ([CATMId],[PriceInCoins]) VALUES (4, 0)
GO
SET IDENTITY_INSERT [dbo].[ChallengeActionTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[ChallengeStatuses] ON 
GO
INSERT [dbo].[ChallengeStatuses] ([Id], [Name]) VALUES (1, N'Scheduled')
GO
INSERT [dbo].[ChallengeStatuses] ([Id], [Name]) VALUES (2, N'Ongoing')
GO
INSERT [dbo].[ChallengeStatuses] ([Id], [Name]) VALUES (3, N'Complete')
GO
SET IDENTITY_INSERT [dbo].[ChallengeStatuses] OFF
GO
SET IDENTITY_INSERT [dbo].[ChallengeTypes] ON 
GO
INSERT [dbo].[ChallengeTypes] ([Id], [Name]) VALUES (1, N'Client')
GO
INSERT [dbo].[ChallengeTypes] ([Id], [Name]) VALUES (2, N'Active')
GO
INSERT [dbo].[ChallengeTypes] ([Id], [Name]) VALUES (3, N'Passive')
GO
SET IDENTITY_INSERT [dbo].[ChallengeTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[Conditions] ON 
GO
INSERT [dbo].[Conditions] ([Id], [Name]) VALUES (1, N'=')
GO
INSERT [dbo].[Conditions] ([Id], [Name]) VALUES (2, N'Contains')
GO
INSERT [dbo].[Conditions] ([Id], [Name]) VALUES (3, N'StartWith')
GO
INSERT [dbo].[Conditions] ([Id], [Name]) VALUES (4, N'EndWith')
GO
INSERT [dbo].[Conditions] ([Id], [Name]) VALUES (5, N'!=')
GO
INSERT [dbo].[Conditions] ([Id], [Name]) VALUES (6, N'>')
GO
INSERT [dbo].[Conditions] ([Id], [Name]) VALUES (7, N'>=')
GO
INSERT [dbo].[Conditions] ([Id], [Name]) VALUES (8, N'<')
GO
INSERT [dbo].[Conditions] ([Id], [Name]) VALUES (9, N'<=')
GO
SET IDENTITY_INSERT [dbo].[Conditions] OFF
GO
SET IDENTITY_INSERT [dbo].[DeviceOperationSystems] ON 
GO
INSERT [dbo].[DeviceOperationSystems] ([Id], [Name]) VALUES (1, N'IOS')
GO
INSERT [dbo].[DeviceOperationSystems] ([Id], [Name]) VALUES (2, N'Android')
GO
INSERT [dbo].[DeviceOperationSystems] ([Id], [Name]) VALUES (3, N'Windows')
GO
SET IDENTITY_INSERT [dbo].[DeviceOperationSystems] OFF
GO
SET IDENTITY_INSERT [dbo].[ModelStatuses] ON 
GO
INSERT [dbo].[ModelStatuses] ([Id], [Name]) VALUES (1, N'Defualt')
GO
INSERT [dbo].[ModelStatuses] ([Id], [Name]) VALUES (2, N'Active')
GO
INSERT [dbo].[ModelStatuses] ([Id], [Name]) VALUES (3, N'Suspended')
GO
SET IDENTITY_INSERT [dbo].[ModelStatuses] OFF
GO
SET IDENTITY_INSERT [dbo].[PackageItemTypes] ON 
GO
INSERT [dbo].[PackageItemTypes] ([Id], [Name]) VALUES (1, N'Coin')
GO
INSERT [dbo].[PackageItemTypes] ([Id], [Name]) VALUES (2, N'Model')
GO
SET IDENTITY_INSERT [dbo].[PackageItemTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[PackageStatuses] ON 
GO
INSERT [dbo].[PackageStatuses] ([Id], [Name]) VALUES (1, N'Active')
GO
INSERT [dbo].[PackageStatuses] ([Id], [Name]) VALUES (2, N'Suspended')
GO
INSERT [dbo].[PackageStatuses] ([Id], [Name]) VALUES (3, N'Disabled')
GO
SET IDENTITY_INSERT [dbo].[PackageStatuses] OFF
GO
SET IDENTITY_INSERT [dbo].[Prices] ON 
GO
INSERT [dbo].[Prices] ([Id], [Price]) VALUES (1, CAST(100.00 AS Decimal(18, 2)))
GO
INSERT [dbo].[Prices] ([Id], [Price]) VALUES (2, CAST(3424.00 AS Decimal(18, 2)))
GO
SET IDENTITY_INSERT [dbo].[Prices] OFF
GO
SET IDENTITY_INSERT [dbo].[PriceTypes] ON 
GO
INSERT [dbo].[PriceTypes] ([Id], [Name]) VALUES (1, N'Coins')
GO
INSERT [dbo].[PriceTypes] ([Id], [Name]) VALUES (2, N'Currency')
GO
SET IDENTITY_INSERT [dbo].[PriceTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[PurchaseStatuses] ON 
GO
INSERT [dbo].[PurchaseStatuses] ([Id], [Name]) VALUES (1, N'Submitted')
GO
INSERT [dbo].[PurchaseStatuses] ([Id], [Name]) VALUES (2, N'PaidUp')
GO
INSERT [dbo].[PurchaseStatuses] ([Id], [Name]) VALUES (3, N'Decliend')
GO
INSERT [dbo].[PurchaseStatuses] ([Id], [Name]) VALUES (4, N'Canceled')
GO
SET IDENTITY_INSERT [dbo].[PurchaseStatuses] OFF
GO
SET IDENTITY_INSERT [dbo].[TransactionCategories] ON 
GO
INSERT [dbo].[TransactionCategories] ([Id], [Name]) VALUES (1, N'ChallengeReward')
GO
INSERT [dbo].[TransactionCategories] ([Id], [Name]) VALUES (2, N'BuyPackage')
GO
INSERT [dbo].[TransactionCategories] ([Id], [Name]) VALUES (3, N'BuyPackageReward')
GO
INSERT [dbo].[TransactionCategories] ([Id], [Name]) VALUES (4, N'BuyMixedPlace')
GO
INSERT [dbo].[TransactionCategories] ([Id], [Name]) VALUES (5, N'AdminReward')
GO
INSERT [dbo].[TransactionCategories] ([Id], [Name]) VALUES (6, N'AdminFine')
GO
INSERT [dbo].[TransactionCategories] ([Id], [Name]) VALUES (7, N'DefaultPackageReward')
GO
SET IDENTITY_INSERT [dbo].[TransactionCategories] OFF
GO
SET IDENTITY_INSERT [dbo].[UserStatuses] ON 
GO
INSERT [dbo].[UserStatuses] ([Id], [Name]) VALUES (1, N'Registered')
GO
INSERT [dbo].[UserStatuses] ([Id], [Name]) VALUES (2, N'Active')
GO
INSERT [dbo].[UserStatuses] ([Id], [Name]) VALUES (3, N'Suspended')
GO
INSERT [dbo].[UserStatuses] ([Id], [Name]) VALUES (4, N'Disabled')
GO
SET IDENTITY_INSERT [dbo].[UserStatuses] OFF
GO
INSERT INTO [dbo].[NotificationMethodModel]
           ([Name])
     VALUES
           ('SMS')
GO

INSERT INTO [dbo].[NotificationMethodModel]
           ([Name])
     VALUES
           ('Email')
GO

INSERT INTO [dbo].[NotificationMethodModel]
           ([Name])
     VALUES
           ('Push Notification')
GO
INSERT INTO [dbo].[NotificationMethodModel]
           ([Name])
     VALUES
           ('Priority Push Notification')
GO


INSERT INTO [dbo].[NotificationOriginationModel]
           ([Name])
     VALUES
           ('UserAdminManagement')
GO
INSERT INTO [dbo].[NotificationOriginationModel]
           ([Name])
     VALUES
           ('MixedPlaceAdminManagement')
GO
INSERT INTO [dbo].[NotificationOriginationModel]
           ([Name])
     VALUES
           ('ChallengesAdminManagement')
GO
INSERT INTO [dbo].[NotificationOriginationModel]
           ([Name])
     VALUES
           ('ContentReport')
GO
INSERT INTO [dbo].[NotificationOriginationModel]
           ([Name])
     VALUES
           ('UserStatusChange')
GO
INSERT INTO [dbo].[NotificationStatusModel]
           ([Name])
     VALUES
           ('Send')
GO

INSERT INTO [dbo].[NotificationStatusModel]
           ([Name])
     VALUES
           ('NotSend')
GO

INSERT INTO [dbo].[NotificationStatusModel]
           ([Name])
     VALUES
           ('Failed')
GO

INSERT INTO [dbo].[NotificationStatusModel]
           ([Name])
     VALUES
           ('Decliended')
GO

INSERT INTO [dbo].[NotificationStatusModel]
           ([Name])
     VALUES
           ('Queued')
GO

INSERT INTO [dbo].[NotificationTemplates]
           ([Message]
           ,[UniqueName]
           )
     VALUES
           ('<!DOCTYPE html>
             <html>
             <body>
              <div class="container">
			  <div>
			  {reportText} 
			  </div>
                 <div class="action-wrapper">
                   <style>
                     button{
                       border: none;
                       color: white;
                       padding: 15px 32px;
                       text-align: center;
                       text-decoration: none;
                       display: inline-block;
                       font-size: 16px;
                     }
                   </style>
                   <button style="background:#007bff; color: #fff"><a href="{btnApproveLink}">Approve</a></button>
                   <button style="background:#dc3545; color: #fff"><a href="{btnRefuseLink}">Decline</a></button>
                 </div>
               </div>
             </body>
             </html>'
           ,'Email_ReportToAdminAboutBadContentHTML'
		   )
GO

INSERT INTO [dbo].[NotificationTemplates]
           ([Message]
           ,[UniqueName]
           )
     VALUES
           ('{User name}
		     Your account was disabled.
			 For more information please contact support.'
           ,'Email_UserDisabled'
		   )
GO

INSERT INTO [dbo].[NotificationTemplates]
           ([Message]
           ,[UniqueName]
           )
     VALUES
           ('{User name}
		     Your account was suspended.
			 For more information please contact support.'
           ,'Email_UserSuspended'
		   )
GO

INSERT INTO [dbo].[NotificationTemplates]
           ([Message]
           ,[UniqueName]
           )
     VALUES
           ('{User name}
		     Your account was activated.
			 Now you may login to application'
           ,'Email_UserActivated'
		   )
GO

INSERT INTO [dbo].[NotificationTemplates]
           ([Message]
           ,[UniqueName]
           )
     VALUES
           ('Bad Mixed Place content was reported : {reportDate}
		     
			 {reportText}'
           ,'Email_ReportToAdminAboutBadContentText'
		   )
GO

INSERT INTO [dbo].[LogicalOperators]
           ([Name])
     VALUES
           ('OR')
GO

INSERT INTO [dbo].[LogicalOperators]
           ([Name])
     VALUES
           ('AND')
GO



INSERT INTO [dbo].[MPReportStatuses]
           ([Name])
     VALUES
           ('SubmitedForApprove')
GO

INSERT INTO [dbo].[MPReportStatuses]
           ([Name])
     VALUES
           ('ApprovedByAdmin')
GO

INSERT INTO [dbo].[MPReportStatuses]
           ([Name])
     VALUES
           ('RefusedByAdmin')
GO
