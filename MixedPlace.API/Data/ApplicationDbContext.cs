﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MixedPlace.API.Models.MPDesign;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Models.UserManagement;
using MixedPlace.API.Models.MPChallenge;
using MixedPlace.API.Models.Package;
using MixedPlace.API.Models.HistoryManagement;
using Microsoft.EntityFrameworkCore.Metadata;
using MixedPlace.API.Models.Base;
using MixedPlace.API.Models.Transactions;
using MixedPlace.API.Models.Coins;

namespace MixedPlace.API.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        IConfiguration _configuration;

        #region Commons
        public DbSet<MPPrice> Prices { get; set; }
        public DbSet<ConditionModel> Conditions { get; set; }
        public DbSet<NotificationModel> Notifications { get; set; }
        public DbSet<NotificationTemplateModel> NotificationTemplates { get; set; }
        public DbSet<CurrencyModel> Currencies { get; set; }
        #endregion Commons

        #region Models
        public DbSet<MPModel> Models { get; set; }
        public DbSet<ModelCategory> ModelCategories { get; set; }
        public DbSet<MPModelPerUser> MPModelsPerUsers { get; set; }

        public DbSet<MPModelCatSubCat> MPModelCatSubCat { get; set; }
        public DbSet<ModelSubCategory> ModelSubCategories { get; set; }
        public DbSet<BundleModel> BundleModel{ get; set; }
        public DbSet<BundleModelCatSubCat> BundleModelCatSubCat { get; set; }
        #endregion 

        #region Coins
        public DbSet<CoinsRuleType> CoinsRuleTypes { get; set; }
        public DbSet<CoinsRuleConditionModel> CoinsRuleConditionModels { get; set; }
        public DbSet<CoinsRuleModel> CoinsRuleModels { get; set; }
        public DbSet<LogicalOperatorModel> LogicalOperators { get; set; }
        #endregion

        #region Socials
        public DbSet<MPComment> MPComments { get; set; }
        public DbSet<MPLike> MPLikes { get; set; }
        public DbSet<MPShare> MPShares { get; set; }
        public DbSet<MPVisit> MPVisits { get; set; }
        public DbSet<MPOperationeCoinsPrice> MPOperationeCoinsPrice { get; set; }
        
        #endregion Socials

        #region Mixed Places
        public DbSet<MP> MixedPlaces { get; set; }
        public DbSet<MPCategory> MPCategories { get; set; }
        public DbSet<MPItemModel> MPItems { get; set; }
        public DbSet<MPStatusModel> MPStatuses { get; set; }
        public DbSet<DesignStatusModel> DesignStatuses { get; set; }
        public DbSet<MPSubCategory> MPSubCategories { get; set; }
        public DbSet<MPCatSubCat> MPCatSubCat { get; set; }
        public DbSet<MPHistoryModelCatSubCat> MPHistoryCatSubCat { get; set; }
        #endregion Mixed Places

        #region Challenges
        public DbSet<Challenge> Challenges { get; set; }
        public DbSet<ChallengeRule> ChallengeRules { get; set; }
        public DbSet<MPReport> MPReports { get; set; }
        public DbSet <MPReportStatusModel> MPReportStatuses { get; set; }
        public DbSet<PackageModel> Packages { get; set; }
        public DbSet<ChallengeCategoryModel> ChallengeCategories { get; set; }
        public DbSet<AchievedChallengeModel> AchievedChallenges { get; set; }
        public DbSet<ChallengeTypeModel> ChallengeTypes { get; set; }
        public DbSet<ChallengeActionTypeModel> ChallengeActionTypes { get; set; }
        public DbSet<ChallengeStatusModel> ChallengeStatuses { get; set; }
        public DbSet<ModelStatusModel> ModelStatuses { get; set; }
        public DbSet<PriceTypeModel> PriceTypes { get; set; }
        #endregion

        #region Store

        public DbSet<PackageCategoryModel> PackageCategories { get; set; }
        public DbSet<PackageItemTypeModel> PackageItemTypes { get; set; }
        public DbSet<PackageStatusModel> PackageStatuses { get; set; }
        public DbSet<PackageItemModel> PackageItems { get; set; }
        public DbSet<PackagePurchaseModel> PurchasedPackages { get; set; }
        public DbSet<PurchaseStatusModel> PurchaseStatuses { get; set; }


        #endregion Store

        #region History

        public DbSet<MPHistoryModel> MPHistory { get; set; }
        public DbSet<MPItemHistoryModel> MPItemsHistory { get; set; }
        public DbSet<AchivedChallengeHistoryModel> AchivedChallengesHistory { get; set; }
        public DbSet<MPCommentHistoryModel> MPCommentsHistory { get; set; }
        public DbSet<MPLikeHistoryModel> MPLikesHistory { get; set; }
        public DbSet<MPVisitHistoryModel> MPVisitsHistory { get; set; }
        public DbSet<MPShareHistoryModel> MPSharesHistory { get; set; }

        #endregion History



        #region Users
        public DbSet<UserStatusModel> UserStatuses { get; set; }
        public DbSet<DeviceOSModel> DeviceOperationSystems { get; set; }
        #endregion Users

        #region Transactions
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionCategory> TransactionCategories { get; set; }
        #endregion Transactions
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IConfiguration configuration, ILoggerFactory loggerFactory)
            : base(options)
        {

            _configuration= configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           // optionsBuilder.UseInMemoryDatabase("MixedPlace");
            //optionsBuilder.UseSqlServer(_configuration["Default:ConnectionString"]);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<MPModelPerUser>().HasKey(x => new { x.UserId, x.MPModelId });
            builder.Entity<MP>().HasIndex(e => new { e.X, e.Y, e.Z }).IsUnique();
            //builder.Entity<MPModel>().HasIndex(e => new { e.BundleId, e.BundleImageName}).IsUnique();
            builder.Entity<MPModel>().HasIndex(e => new {e.UniqueId }).IsUnique();
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
