﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class CoinRuleDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Amount { get; set; }
        public ConditionDto Condition { get; set; }
        public CoinRuleTypeDto CoinRuleType { get; set; }
        public int NumberOfUsersAffected { get; set; }
    }
}
