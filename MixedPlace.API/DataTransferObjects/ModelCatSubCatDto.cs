﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class ModelCatSubCatDto
    {
        public ModelCategoryDto Category{ get; set; }
        public ModelSubCategoryDto SubCategory{ get; set; }
    }
}
