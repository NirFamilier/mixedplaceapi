﻿using MixedPlace.API.DataTransferObjects.StoreDtos;
using MixedPlace.API.Models.Common;
using System.Collections.Generic;

namespace MixedPlace.API.DataTransferObjects
{
    public class PackageItemDto
    {
        public int Id { get; set; }
        public PackageItemTypeDto PackageItemType{ get; set; }
        public int Sum { get; set; }
        public PackageModelItemDto Model { get; set; }
        public int NumberOfUnits { get; set; }
        public int TotalSize { get; set; }

    }
}
