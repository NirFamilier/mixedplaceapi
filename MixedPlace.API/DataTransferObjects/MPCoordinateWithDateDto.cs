﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    /// <summary>
    /// MP Coordiantes
    /// </summary>
    public class MPCoordinateWithDateDto
    {
        /// <summary>
        /// X
        /// </summary>
        public long X { get; set; }
        /// <summary>
        /// Y
        /// </summary>
        public long Y { get; set; }
        /// <summary>
        /// Z
        /// </summary>
        public long Z { get; set; }

        public DateTime Date { get; set; }
    }
}
