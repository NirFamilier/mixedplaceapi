﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class MPItemDto
    {
        public MPCoordinateDto MPCoordinate { get; set; }
        public string Json { get; set; }
    }
}
