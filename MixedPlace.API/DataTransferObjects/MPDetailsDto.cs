﻿using MixedPlace.API.Models.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{

    public class MPDetailsDto : MPCoordinateDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public MPStatusDto MPStatus { get; set; }
        public DesignStatusDto DesignStatus { get; set; }
        public string OwnerId { get; set; }
        public string OwnerName { get; set; }
        public DateTime PurchaseDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public decimal Price { get; set; }
        public ICollection<ModelCatSubCatDto> Categories { get; set; }
        public int VisitsCount { get; set; }
        public int LikesCount { get; set; }
        public int SharesCount { get; set; }
        public int CommentsCount { get; set; }
        public int Demand { get; set; }
        public int ReportsCount { get; set; }
        public bool IsReported { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
