﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class MPSubCategoryDto: DropdownDto
    {
        public int MPCategoryId { get; set; }
    }
}
