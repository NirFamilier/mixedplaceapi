﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class SocialBaseDto
    {
        public MPCoordinateDto MPCoordinate { get; set; }
        public DateTime Date { get; set; }
    }
}
