﻿using MixedPlace.API.Models.MPModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class MPModelDto
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public List<ModelCatSubCatDto> ModelCategories { get; set; }
        public string BundleImageName { get; set; }
        public int? ModelStatusId { get; set; }
        public ModelStatusDto ModelStatus { get; set; }
        public int? PriceTypeId { get; set; }
        public PriceTypeDto PriceType { get; set; }
        public int? PurchasedUnits { get; set; }
        public long ModelSize { get; set; }
        public int TotalModelsCount { get; set; }
        public int RemainingModelsCount { get; set; }
        public string UniqueId { get; set; }
        public DropdownDto Bundle { get; set; }


    }
}
