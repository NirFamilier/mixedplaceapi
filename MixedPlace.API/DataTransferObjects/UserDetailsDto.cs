﻿using MixedPlace.API.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class UserDetailsDto : UserBaseDto
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public DateTime RegistrationDate { get; set; }
        public decimal CoinBalance { get; set; }

        public UserStatusDto UserStatus { get; set; }


        public int MPLevel { get; set; }
        public int MPCount { get; set; }
        public int DesignLevel { get; set; }
        public int SocialLevel { get; set; }
        public int ChallengeLevel { get; set; }
        public int ReportsCount { get; set; }
        public bool IsReported => ReportsCount > 0;
        public string City { get; set; }
        public DateTime LastUpdate { get; set; }
        public int UserLevel { get; set; }
    }
}
