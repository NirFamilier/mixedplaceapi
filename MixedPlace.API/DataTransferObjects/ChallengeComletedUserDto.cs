﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class ChallengeComletedUserDto
    {
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}
