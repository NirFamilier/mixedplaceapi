﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class CoinRuleFieldNameDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
