﻿using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class ChallengeRuleDto
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public int Amount { get; set; }

        public ChallengeActionTypeDto ActionType { get; set; }

        public ConditionDto Condition { get; set; }
    }
}
