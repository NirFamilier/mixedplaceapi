﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class GetUserCoinsBalance
    {
        public double CurrentBalance { get; set; }
        public double TotalCoinsSpent { get; set; }
        public double TotalCoinsAwarded { get; set; }

        public List<CoinsBalanceDto> CoinsRewared { get; set; }

        public List<CoinsBalanceDto> CoinsSpent { get; set; }
    }
}
