﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class MPUserDataDto
    {
        [Required]
        public MPCoordinateDto Coordinates { get; set; }
        public string Name { get; set; }
        public string MPDescription { get; set; }

        public List<ModelCatSubCatDto> Categories { get; set; }

    }
}
