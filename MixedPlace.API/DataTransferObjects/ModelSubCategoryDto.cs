﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class ModelSubCategoryDto : DropdownDto
    {
        public int ModelCategoryId { get; set; }
    }
}
