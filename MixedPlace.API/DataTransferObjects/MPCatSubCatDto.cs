﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class MPCatSubCatDto:DropdownDto
    {
        public MPCategoryDto Category { get; set; }
        public MPSubCategoryDto SubCategory { get; set; }
    }
}
