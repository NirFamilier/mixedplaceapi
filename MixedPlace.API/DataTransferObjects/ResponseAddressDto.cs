﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class ResponseAddressDto
    {
        public List<AddressResultDto> Results { get; set; }
    }
}
