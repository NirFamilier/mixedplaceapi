﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package
{
    public class PackageDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? AvailableFrom { get; set; }
        public DateTime? ExpiresOn { get; set; }
        public string UserId { get; set; }

        public decimal Price { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }

        public PriceTypeDto PriceType { get; set; }
        public PackageStatusDto PackageStatus { get; set; }
        public PackageCategoryDto PackageCategory { get; set; }
        public int PurchasedUnits { get; set; }
        public long PackageSize { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public List<PackageItemDto> PackageItems { get; set; }
    }
}
