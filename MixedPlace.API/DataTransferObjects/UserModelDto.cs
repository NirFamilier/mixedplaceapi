﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class UserModelDto
    {
        public string BundleImageName { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public int TotalModelsCount { get; set; }
        public int RemainingModelsCount { get; set; }
        public string UniqueId { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
