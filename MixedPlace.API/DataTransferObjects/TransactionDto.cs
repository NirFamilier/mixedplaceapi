﻿using MixedPlace.API.Models.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class TransactionDto
    {
        public int ID { get; set; }
        public double Coins { get; set; }
        public double Money { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string TransactionType { get; set; }
    }
}
