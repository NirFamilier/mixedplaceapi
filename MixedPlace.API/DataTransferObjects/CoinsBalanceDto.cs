﻿using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class CoinsBalanceDto
    {
        public TransactionCategoryEnum Type { get;  set; }
        public double Value { get; set; }
        public DateTime Date { get; set; }
    }
}
