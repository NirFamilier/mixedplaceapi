﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class ClientChallengeDto
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
    }
}
