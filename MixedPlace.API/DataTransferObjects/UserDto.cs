﻿using MixedPlace.API.DataTransferObjects.NotificationsDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class UserDto : UserDetailsDto
    {
        public IEnumerable<NotificationDto> Notifications { get; set; }
    }
}
