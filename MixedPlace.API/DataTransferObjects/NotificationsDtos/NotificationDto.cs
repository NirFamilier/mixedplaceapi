﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects.NotificationsDtos
{
    public class NotificationDto
    {
        public int Id { get; set; }
        public string Recipient { get; set; }
        public string Message { get; set; }
        public string NotifyResult { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime? DateSent { get; set; }

        public int? NotificationOriginationId { get; set; }
        public int? NotificationStatusId { get; set; }
        public int? NotificationTemplateId { get; set; }
        public string UserId { get; set; }
    }
}
