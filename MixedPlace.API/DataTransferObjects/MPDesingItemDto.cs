﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    /// <summary>
    /// Design items Details
    /// </summary>
    public class MPDesignItemDto
    {
        /// <summary>
        /// Currently used 1,2
        /// </summary>
        public int ItemsType { get; set; }

        /// <summary>
        /// Json that contain all designs for mixed place for passed type
        /// </summary>
        public string MPItemsJson { get; set; }
    }
}
