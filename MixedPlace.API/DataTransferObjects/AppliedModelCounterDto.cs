﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class AppliedModelCounterDto
    {
        public int ModelId { get; set; }
        public int Amount { get; set; }
    }
}
