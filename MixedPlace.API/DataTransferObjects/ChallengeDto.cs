﻿using MixedPlace.API.DataTransferObjects.ChallengesDtos;
using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class ChallengeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal Reward { get; set; }
        public bool UnlimitedDuration { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }

        public ChallengeStatusDto ChallengeStatus { get; set; }    

        public ChallengeTypeDto ChallengeType { get; set; }
        
        public int CompletedUsersCount { get; set; }
        public ChallengeCategoryDto ChallengeCategory { get; set; }

        public LogicalOperatorType LogicalOperator { get; set; }
        [Required]
        public List<ChallengeRuleDto> Rules { get; set; }

        public IEnumerable<AchievedChallengeDto> AchievedChallenges { get; set; }
    }
}
