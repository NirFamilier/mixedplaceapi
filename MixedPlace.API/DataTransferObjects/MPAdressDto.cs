﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class MPAdressDto
    {
        public string City { get; set; }
        public string Country { get; set; }
    }
}
