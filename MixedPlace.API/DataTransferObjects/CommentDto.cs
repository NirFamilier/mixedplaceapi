﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class CommentDto : SocialBaseDto
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public string Username { get; set; }
    }
}
