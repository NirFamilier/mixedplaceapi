﻿using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    /// <summary>
    /// Package for client store
    /// </summary>
    public class ClientStorePackageDto
    {
        public int Id { get; set; }

        public decimal Price { get; set; }
        /// <summary>
        /// Price type 1 - Coins; 2 - Currency
        /// </summary>
        public PriceType PriceType { get; set; }
        /// <summary>
        /// Datetime when package would not be available on the store
        /// </summary>
        public DateTime ExpiresOn { get; set; }
        public string Description{ get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Url from where get image for the package
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// Collection of items in package
        /// </summary>
        public IEnumerable<PackageItemDto> PackageItems { get; set; }
        /// <summary>
        /// Date when package was updated. For client cache management
        /// </summary>
        public DateTime UpdateDate { get; set; }

        public PackageCategoryType PackageType { get; set; }
    }
}
