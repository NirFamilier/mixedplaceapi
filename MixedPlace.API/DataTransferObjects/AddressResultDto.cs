﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class AddressResultDto
    {
        public List<AddressComponentDto> Address_components { get;set;}
        public string Formatted_address { get; set; }
        public GeometryDto Geometry { get; set; }
        public string Place_id { get; set; }
        public List<string> Postcode_localities { get; set; }
        public List<string> Types { get; set; }
    }

   /* public class ResultDto
    {
        public List<AddressComponentDto> Address_components { get; set; }
        public string Formatted_address { get; set; }
        public GeometryDto Geometry { get; set; }
        public string Place_id { get; set; }
        public List<string> Postcode_localities { get; set; }
        public List<string> Types { get; set; }
    }*/

    public class RootObjectDto
    {
        public List<AddressResultDto> Results { get; set; }
        public string Status { get; set; }
    }

    public class AddressComponentDto
    {
        public string Long_name { get; set; }
        public string Short_name { get; set; }
        public List<string> Types { get; set; }
    }

    public class NortheastDto
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }

    public class SouthwestDto
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }

    public class BoundsDto
    {
        public NortheastDto Northeast { get; set; }
        public SouthwestDto Southwest { get; set; }
    }

    public class LocationDto
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }

    public class Northeast2Dto
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }

    public class Southwest2Dto
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }

    public class ViewportDto
    {
        public Northeast2Dto Northeast { get; set; }
        public Southwest2Dto Southwest { get; set; }
    }

    public class GeometryDto
    {
        public BoundsDto Bounds { get; set; }
        public LocationDto Location { get; set; }
        public string Location_type { get; set; }
        public ViewportDto Viewport { get; set; }
    }
}
