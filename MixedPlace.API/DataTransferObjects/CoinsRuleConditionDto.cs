﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.DataTransferObjects
{
    public class CoinsRuleConditionDto
    {
        public CoinRuleFieldNameDto FieldName { get; set; }
        public DropdownDto Operator { get; set; }
        public object  Value { get; set; }
    }
}
