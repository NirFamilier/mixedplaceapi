﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using Newtonsoft.Json;

namespace MixedPlace.API.Helpers.FiltersCreation
{
    public class DropDownFilterCreation : IFilterCreation
    {
        public Expression CreateFilterExpression(MemberExpression member, FilterItemRequest filter)
        {
            var constant = Expression.Constant(true);
            var filterValue = JsonConvert.DeserializeObject<DropdownDto>(filter.Value.ToString());
            if (filterValue == null)
                return Expression.Equal(member, constant);

            constant = Expression.Constant(filterValue.Id);
            switch (filter.Condition)
            {
                case ConditionsType.EqualTo:
                    return Expression.Equal(member, constant);
                case ConditionsType.NotEqualTo:
                    return Expression.NotEqual(member, constant);
                case ConditionsType.GreaterThan:
                    return Expression.GreaterThan(member, constant);
                case ConditionsType.GreaterThanOrEqualTo:
                    return Expression.GreaterThanOrEqual(member, constant);
                case ConditionsType.LessThan:
                    return Expression.LessThan(member, constant);
                case ConditionsType.LessThanOrEqualTo:
                    return Expression.LessThanOrEqual(member, constant);
                default:
                    return Expression.Equal(member, Expression.Constant(true));
            }
        }
    }
}
