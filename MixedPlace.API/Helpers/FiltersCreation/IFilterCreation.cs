﻿using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MixedPlace.API.Helpers
{
    public interface IFilterCreation
    {
        Expression CreateFilterExpression(MemberExpression member, FilterItemRequest filter);
    }
}
