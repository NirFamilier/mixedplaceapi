﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MixedPlace.API.Models.Common;

namespace MixedPlace.API.Helpers.FiltersCreation
{
    public class BooleanFilterCreation : IFilterCreation
    {
        public Expression CreateFilterExpression(MemberExpression member, FilterItemRequest filter)
        {
            var constant = Expression.Constant(Convert.ToBoolean(filter.Value));
            switch (filter.Condition)
            {
                case ConditionsType.EqualTo:
                    return Expression.Equal(member, constant);
                case ConditionsType.NotEqualTo:
                    return Expression.NotEqual(member, constant);
                default:
                    return Expression.Constant(true);
            }
        }
    }
}
