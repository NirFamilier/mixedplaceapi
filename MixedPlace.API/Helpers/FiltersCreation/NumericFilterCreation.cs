﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using MixedPlace.API.Models.Common;

namespace MixedPlace.API.Helpers.FiltersCreation
{
    public class NumericFilterCreation : IFilterCreation
    {
        public Expression CreateFilterExpression(MemberExpression member, FilterItemRequest filter)
        {
            ConstantExpression constant;
            object value;
            var memberType = ((PropertyInfo)member.Member).PropertyType;

            if (memberType == typeof(Int16) || memberType == typeof(Int16?))
                value = Convert.ToInt16(filter.Value);
            else if (memberType == typeof(Int32) || memberType == typeof(Int32?))
                value = Convert.ToInt32(filter.Value);
            else if (memberType == typeof(Int64) || memberType == typeof(Int64?))
                value = Convert.ToInt64(filter.Value);
            else if (memberType == typeof(decimal) || memberType == typeof(decimal?))
                value = Convert.ToDecimal(filter.Value);
            else if (memberType == typeof(double))
                value = Convert.ToDouble(filter.Value);
            else if (memberType == typeof(float))
                value = float.Parse(filter.Value.ToString());
            else
                return null;

            constant = Expression.Constant(value, memberType);

            switch (filter.Condition)
            {
                case ConditionsType.EqualTo:
                    return Expression.Equal(member, constant);
                case ConditionsType.NotEqualTo:
                    return Expression.NotEqual(member, constant);
                case ConditionsType.GreaterThan:
                    return Expression.GreaterThan(member, constant);
                case ConditionsType.GreaterThanOrEqualTo:
                    return Expression.GreaterThanOrEqual(member, constant);
                case ConditionsType.LessThan:
                    return Expression.LessThan(member, constant);
                case ConditionsType.LessThanOrEqualTo:
                    return Expression.LessThanOrEqual(member, constant);
                default:
                    return Expression.Equal(member, Expression.Constant(true));
            }
        }
    }
}
