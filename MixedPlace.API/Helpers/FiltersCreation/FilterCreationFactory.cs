﻿using MixedPlace.API.Helpers.FiltersCreation;
using MixedPlace.API.Models.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MixedPlace.API.Helpers.FiltersCreation
{
    /// <summary>
    /// 
    /// </summary>
    public class FilterCreationFactory
    {
        public static IFilterCreation CreateFilter(FilterItemRequest type)
        {
            if (IsNumeric(type.Value))
                return new NumericFilterCreation();
            if (type.Value is DateTime)
                return new DateTimeFilterCreation();
            if (type.Value is bool)
                return new BooleanFilterCreation();
            if (type.Value is string)
                return new StringFilterCreation();
            if (type.Value.GetType() == typeof(JObject))
                return new DropDownFilterCreation();
            else
                return new NoTypeFilterCreation();
         
        }

        private static bool IsNumeric(object filterValue)
        {
            return (filterValue is Int16 ||
                filterValue is Int32 ||
                filterValue is Int64 ||
                filterValue is decimal ||
                filterValue is double ||
                filterValue is float);
        }
    }

  
}
