﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using MixedPlace.API.Models.Common;

namespace MixedPlace.API.Helpers.FiltersCreation
{
    public class DateTimeFilterCreation : IFilterCreation
    {
        public Expression CreateFilterExpression(MemberExpression member, FilterItemRequest filter)
        {
            ConstantExpression constant;
            var valueDate = Convert.ToDateTime(filter.Value);
            var memberType = ((PropertyInfo)member.Member).PropertyType;
            constant = Expression.Constant(valueDate, memberType);
            
            switch (filter.Condition)
            {
                case ConditionsType.EqualTo:
                    return Expression.Equal(member, constant);
                case ConditionsType.NotEqualTo:
                    return Expression.NotEqual(member, constant);
                case ConditionsType.GreaterThan:
                    return Expression.GreaterThan(member, constant);
                case ConditionsType.GreaterThanOrEqualTo:
                    return Expression.GreaterThanOrEqual(member, constant);
                case ConditionsType.LessThan:
                    return Expression.LessThan(member, constant);
                case ConditionsType.LessThanOrEqualTo:
                    return Expression.LessThanOrEqual(member, constant);
                default:
                    return Expression.Constant(true);
            }
        }
    }
}
