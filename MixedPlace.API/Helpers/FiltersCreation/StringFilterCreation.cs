﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using MixedPlace.API.Models.Common;

namespace MixedPlace.API.Helpers.FiltersCreation
{
    public class StringFilterCreation : IFilterCreation
    {
        static MethodInfo containsMethod = typeof(string).GetMethod("Contains");
        static MethodInfo startsWithMethod = typeof(string).GetMethod("StartsWith", new[] { typeof(string) });
        static MethodInfo endsWithMethod = typeof(string).GetMethod("EndsWith", new[] { typeof(string) });
        static MethodInfo toLowerMethod = typeof(string).GetMethod("ToLower", new Type[0]);

        

        public Expression CreateFilterExpression(MemberExpression member, FilterItemRequest filter)
        {
            
            var constant = Expression.Constant(Convert.ToString(filter.Value).ToLower());
            var notNullExpression = Expression.NotEqual(member, Expression.Constant(null,typeof(string)));
            switch (filter.Condition)
            {
                case ConditionsType.Contains:
                    {
                        var expressionCall = Expression.Call(member, toLowerMethod);
                        return Expression.AndAlso(notNullExpression, Expression.Call(expressionCall, containsMethod, constant));
                    }
                    
                case ConditionsType.EndsWith:
                    {
                        var expressionCall = Expression.Call(member, toLowerMethod);
                        return Expression.AndAlso(notNullExpression, Expression.Call(expressionCall, endsWithMethod, constant));
                    }
                   
                case ConditionsType.StartsWith:
                    {
                        var expressionCall = Expression.Call(member, toLowerMethod);
                        return Expression.AndAlso(notNullExpression, Expression.Call(expressionCall, startsWithMethod, constant));
                    }
                default:
                    return Expression.Constant(true);

            }
        }
    }
}
