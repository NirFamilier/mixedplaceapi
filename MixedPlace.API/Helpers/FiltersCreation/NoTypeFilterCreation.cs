﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MixedPlace.API.Models.Common;

namespace MixedPlace.API.Helpers.FiltersCreation
{
    public class NoTypeFilterCreation : IFilterCreation
    {
        public Expression CreateFilterExpression(MemberExpression member, FilterItemRequest filter)
        {
            return null;
        }
    }
}
