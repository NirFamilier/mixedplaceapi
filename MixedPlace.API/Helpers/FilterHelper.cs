﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Helpers.FiltersCreation;
using MixedPlace.API.Models.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Helpers
{
    public class FilterHelper
    {

        

        public static IEnumerable<T> Filter<T>(IEnumerable<T> list, IEnumerable<FilterItemRequest> filters,Dictionary<string,string> dictionary,LogicalOperatorType logicalOperatorType = LogicalOperatorType.AND)
        {
            Expression finalExpression;
            if (LogicalOperatorType.AND == logicalOperatorType)
                finalExpression = Expression.Constant(true);
            else
                finalExpression = Expression.Constant(false);

            var parameter = Expression.Parameter(typeof(T), "x");
            foreach (var filter in filters)
            {
                var member = GetMemberExpression(parameter, dictionary[filter.ColumnId]);
                var memberType = ((PropertyInfo)member.Member).PropertyType;
                Expression expression = null;

                expression = FilterCreationFactory.CreateFilter(filter).CreateFilterExpression(member, filter);

                if (LogicalOperatorType.AND == logicalOperatorType)
                    finalExpression = Expression.AndAlso(finalExpression, expression);
                else
                    finalExpression = Expression.Or(finalExpression, expression);
                
            }

            var lambda = Expression.Lambda<Func<T, bool>>(finalExpression, parameter);
            return list.Where(lambda.Compile()).ToList();
        }

        private static MemberExpression GetMemberExpression(Expression param, string propertyName)
        {
            if (propertyName.Contains("."))
            {
                int index = propertyName.IndexOf(".");
                var subParam = Expression.Property(param, propertyName.Substring(0, index));
                return GetMemberExpression(subParam, propertyName.Substring(index + 1));
            }

            return Expression.Property(param, propertyName);
        }

 
    }
}
