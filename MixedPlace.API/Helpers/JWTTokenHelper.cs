﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Helpers
{
    public class JWTTokenHelper
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<JWTTokenHelper> _logger;

        public JWTTokenHelper(IConfiguration configuration,
                              ILogger<JWTTokenHelper> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        #region Generate Methods
        public string GenerateJWTToken(string sub = "")
        {

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["Auth:App:JwtExpireDays"]));

            X509Certificate2 cert = new X509Certificate2(_configuration["Auth:Jwt:CertName"], _configuration["Auth:Jwt:CertPass"]);
            SecurityKey signKey = new X509SecurityKey(cert);
            SigningCredentials credentials = new SigningCredentials(signKey, SecurityAlgorithms.RsaSha256);

            var token = new JwtSecurityToken(
                _configuration["Auth:Jwt:JwtIssuer"],
                _configuration["Auth:Jwt:JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public string GenerateAdminEmailJWTToken(string sub = "")
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, sub),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["Auth:EmailJwt:JwtExpireDays"]));

            X509Certificate2 cert = new X509Certificate2(_configuration["Auth:EmailJwt:CertName"], _configuration["Auth:EmailJwt:CertPass"]);
            SecurityKey signKey = new X509SecurityKey(cert);
            SigningCredentials credentials = new SigningCredentials(signKey, SecurityAlgorithms.RsaSha256);

            var token = new JwtSecurityToken(
                _configuration["Auth:EmailJwt:JwtIssuer"],
                _configuration["Auth:EmailJwt:JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public string GenerateJWTAdminToken()
        {
            return string.Empty;
        }

        #endregion Generate Methods

        #region Validation Methods

        public bool ValidateAdminEmailJWTToken(string token,out ClaimsPrincipal claimsPrincipal)
        {
            try
            {
                X509Certificate2 cert = new X509Certificate2(_configuration["Auth:EmailJwt:CertName"], _configuration["Auth:EmailJwt:CertPass"]);
                SecurityKey signKey = new X509SecurityKey(cert);
                var tokenValParms = new TokenValidationParameters
                {
                    ValidAudience = _configuration["Auth:EmailJwt:JwtIssuer"],
                    ValidIssuer = _configuration["Auth:EmailJwt:JwtIssuer"],
                    IssuerSigningKey = signKey,
                };

                claimsPrincipal = new JwtSecurityTokenHandler().ValidateToken(token, tokenValParms, out SecurityToken securityToken);
                return true;
            }
            catch (Exception exp)
            {
                _logger.LogError(exp, exp.Message);
                claimsPrincipal = null;
                return false;
            }
        }

        public string GetSubFromJWTToken(ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims
                       .FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Sub)?
                       .Value;
        }

        #endregion Validation Methods

    }
}
