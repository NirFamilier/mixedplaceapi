﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Data;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.MPChallenge.RequestModels;
using MixedPlace.API.Models.MPChallenge.ResponseModels;
using MixedPlace.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Controllers
{
    public class ChallengeContoller : BaseController
    {
        private readonly ApplicationDbContext _context;
        private readonly IChallengeService _challengeService;
        private readonly ITransactionService _transactionService;
        ILogger<ChallengeContoller> _logger;
        public ChallengeContoller(ApplicationDbContext context,
                                  IChallengeService challengeService,
                                  ILogger<ChallengeContoller> logger,
                                  ITransactionService transactionService)
        {
            _context = context;
            _challengeService = challengeService;
            _logger = logger;
            _transactionService = transactionService;
        }

        /// <summary>
        /// Return all Client Challeges types
        /// </summary>
        /// <returns>GetClientChallengesResponse</returns>
        /// 
        [ProducesResponseType(typeof(GetClientChallengesResponse), 200)]
        [Authorize]
        [HttpGet("api/challenge/getclientschallenges")]
        public IActionResult GetClientsChallenges()
        {
            var response = _challengeService.GetClientChallenges(GetUserIdFromCliams());
            return CreateHttpResponse(response);
        }

        /// <summary>
        /// Update the client challenges achieves
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(typeof(EmptyBodyResponse), 200)]
        [Authorize]
        [HttpPost("api/challenge/clientchallengeachieved")]
        public IActionResult ClientChallengeAchieved([FromBody]ClientChallengeAchievedRequest request)
        {
            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (ModelState.IsValid && request != null)
            {
                response = _challengeService.ClientChallengeAchieved(GetUserIdFromCliams(), request.ChallengeId);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        /// <summary>
        /// Get the last challenge that the user achieved
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(typeof(GetChallengesAchievedResponse), 200)]
        [Authorize]
        [HttpGet("api/challenge/lastAchievedchallenge")]
        public IActionResult LastAchievedchallenge()
        {
            IResponseModel<GetChallengesAchievedResponse> response = new ResponseModel<GetChallengesAchievedResponse>();
            if (ModelState.IsValid)
            {

                response = _challengeService.GetLastAchievedChallenge(GetUserIdFromCliams());
                response.StatusCode = (int)InnerErrorCode.Ok;
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        /// <summary>
        /// Get all the challenges that the user achieved
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(typeof(GetChallengesAchievedResponse), 200)]
        [Authorize]
        [HttpGet("api/challenge/achievedchallenges")]
        public IActionResult GetAchievedchallenges()
        {
            IResponseModel<GetChallengesAchievedResponse> response = new ResponseModel<GetChallengesAchievedResponse>();
            if (ModelState.IsValid)
            {
                response = _challengeService.GetAchievedChallenges(GetUserIdFromCliams());
                response.StatusCode = (int)InnerErrorCode.Ok;
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }



    }
}
