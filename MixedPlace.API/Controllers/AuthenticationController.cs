﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.User.ResponseModels;
using MixedPlace.API.Services;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace MixedPlace.API.Controllers
{
    public class AuthenticationController : BaseController
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IAuthService _authService;
        private readonly ILogger<AuthenticationController> _logger;

        public AuthenticationController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IConfiguration configuration,
            IPasswordHasher<User> passwordHasher,
            IAuthService authService,
            ILogger<AuthenticationController> logger,
            ILoggerFactory loggerFactory
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _passwordHasher = passwordHasher;
            _authService = authService;
            _logger = logger;

        }

        #region Public Methods

        /// <summary>
        /// Regular Registration process
        /// </summary>
        /// <param name="registerRequest"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        [HttpPost("api/authentication/register")]
        public async Task<IActionResult> Register([FromBody]RegisterRequest registerRequest)
        {
            IResponseModel<EmptyBodyResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.Register(registerRequest);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Regular User Login
        /// </summary>
        /// <param name="loginRequest"></param>
        /// <returns>AuthResponse</returns>
        /// 
       [ProducesResponseType(typeof(AuthResponse), 200)]
        [HttpPost("api/authentication/login")]
        public async Task<IActionResult> Login([FromBody]LoginRequest loginRequest)
        {
           
            IResponseModel<AuthResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.Login(loginRequest);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<AuthResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Allow login for external providers
        /// </summary>
        /// <param name="externalLoginRequest"></param>
        /// <returns></returns>
        ///
        [ProducesResponseType(typeof(AuthResponse), 200)]
        [HttpPost("api/authentication/externallogin")]
        public async Task<IActionResult> ExternalLogin([FromBody]ExternalLoginRequest externalLoginRequest)
        {

            IResponseModel<AuthResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.ExternalLogin(externalLoginRequest);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<AuthResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }
        
        /// <summary>
        /// Logout 
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(typeof(EmptyBodyResponse), 200)]
        [Authorize]
        [HttpPost("api/authentication/logout")]
        public async Task<IActionResult> Logout()
        {
            var result = await _authService.Logout(GetUserIdFromCliams());
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// In case the Token is expired , Client should ask for new token
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(AuthResponse), 200)]
        [HttpPost("api/authentication/refreshtoken")]
        public async Task<IActionResult> RefreshToken([FromBody]RefeshTokenRequest request)
        {
            IResponseModel<AuthResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.RefreshToken(request);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<AuthResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Forgot password
        /// </summary>
        /// <param name="forgotPasswordRequest"></param>
        /// <returns></returns>
       [ProducesResponseType(typeof(EmptyBodyResponse), 200)]
        [HttpPost("api/authentication/forgotpassword")]
        public async Task<IActionResult> ForgotPassword([FromBody]ForgotPasswordRequest forgotPasswordRequest)
        {
            IResponseModel<EmptyBodyResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.ForgotPassword(forgotPasswordRequest);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);

        }
        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="changePasswordRequest"></param>
        /// <returns></returns>
       [ProducesResponseType(typeof(EmptyBodyResponse), 200)]
        [HttpPost("api/authentication/changepassword")]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordRequest changePasswordRequest)
        {
            IResponseModel<EmptyBodyResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.ChangePassword(changePasswordRequest);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);

        }

        /// <summary>
        /// External Login for Web Apps
        /// </summary>
        /// <param name="provider">Facebook/Google/etc</param>
        /// <returns></returns>
        /// 

        [ProducesResponseType(typeof(AuthenticationProperties), 200)]
        [HttpGet("api/authentication/getexternallogin/{provider}")]
        public IActionResult GetExternalLogin(string provider)
        {

            if (string.IsNullOrWhiteSpace(provider))
                return BadRequest();

            return Challenge(new AuthenticationProperties
            {
                RedirectUri = $"{Request.Scheme}://{Request.Host}/api/authentication/externalcallback"
            }, provider.ToLower());
        }

        /// <summary>
        /// Not Relevant for MP
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="externalToken"></param>
        /// <returns></returns>
        [HttpGet("api/authentication/obtaintoken")]
        public async Task<IActionResult> ObtainToken(string provider, string externalToken)
        {
            var result = await _authService.ObtainToken(provider, externalToken);
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// For web external providers
        /// </summary>
        /// <returns></returns>
        [HttpGet("api/authentication/externalcallback")]
        public async Task<IActionResult> ExternalCallback()
        {
            return Ok();
            var result = await _authService.ExternalCallback(HttpContext);
            return CreateHttpResponse(result);
        }

      

        #endregion Public Methods

    }

}

