﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Package.RequestModel;
using MixedPlace.API.Models.Package.RequestModels;
using MixedPlace.API.Models.Package.ResponseModels;
using MixedPlace.API.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Controllers
{
    [Authorize]
    public class StoreController : BaseController
    {
        private readonly ILogger<StoreController> _logger;
        private readonly IStoreService _storeService;

        public StoreController(ILogger<StoreController> logger,
                               IStoreService storeService)
        {
            _logger = logger;
            _storeService = storeService;
        }

        /// <summary>
        /// Api responsible quering all packages that currently available for
        /// client store
        /// </summary>
        /// <returns>GetClientStorePackagesResponse</returns>
        [ProducesResponseType(typeof(IResponseModel<GetClientStorePackagesResponse>), 200)]
        [HttpPost("api/store/getclientstorepackages")]
        public IActionResult GetClientStorePackages([FromBody] GetPackagesRequestModel requestModel)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request {JsonConvert.SerializeObject(requestModel)}");
            var response = _storeService.GetClientStorePackages(requestModel.PackageType,requestModel.RetrieveAllPackages,requestModel.NumberPerPage,requestModel.PageId);
            return CreateHttpResponse(response);
        }

        /// <summary>
        /// Api responsible for execution flow when user try
        /// to buy selected package
        /// </summary>
        /// <param name="request"></param>
        /// <returns>EmptyBodyResponse</returns>
        [ProducesResponseType(typeof(IResponseModel<EmptyBodyResponse>), 200)]
        [HttpPost("api/store/buypackage")]
        public IActionResult BuyPackage([FromBody]BuyPackageRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            var userId = GetUserIdFromCliams();
            if (request != null && !string.IsNullOrWhiteSpace(userId))
            {
                response = _storeService.BuyPackages(GetUserIdFromCliams(), request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
            
        }

        /// <summary>
        /// Api responsible for adding new package for user shopping cart for futher payment
        /// </summary>
        /// <param name="request"></param>
        /// <returns>EmptyBodyResponse</returns>
        [ProducesResponseType(typeof(IResponseModel<GetPackageResponse>), 200)]
        [HttpPost("api/store/addpackagetousershopingcart")]
        public IActionResult AddPackageToUserShoppingCart([FromBody]AddPackageToUserShoppingCartRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request {JsonConvert.SerializeObject(request)}");

            IResponseModel<GetPackageResponse> response = new ResponseModel<GetPackageResponse>();
            var userId = GetUserIdFromCliams();
            if (request != null && !string.IsNullOrWhiteSpace(userId))
            {
                response = _storeService.AddPackageToShoppingCart(GetUserIdFromCliams(), request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        /// <summary>
        ///  Api responsible for removing package from user's shopping cart for futher payment
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("api/store/removepackagefromusershopingcart")]
        public IActionResult RemovePackageFromUserShoppingCart([FromBody] PackageRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            var userId = GetUserIdFromCliams();
            if (request != null && !string.IsNullOrWhiteSpace(userId))
            {
                response = _storeService.RemovePackageFromUserShoppingCart(GetUserIdFromCliams(), request);
                return CreateHttpResponse(response);
            }

            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        /// <summary>
        /// Api returns  user's shopping cart packages in submitted status for futher payment
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(typeof(IResponseModel<GetPackagesResponse>), 200)]
        [HttpGet("api/store/getpackagefromshopingcart")]
        public IActionResult GetSubmittedPackagesFromShopingcart()
        {
            IResponseModel<GetPackagesResponse> response = new ResponseModel<GetPackagesResponse>();
            var userId = GetUserIdFromCliams();
            if (!string.IsNullOrWhiteSpace(userId))
            {
                response = _storeService.GetSubmittedPackagesFromShopingcart(GetUserIdFromCliams());
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }
    }
}
