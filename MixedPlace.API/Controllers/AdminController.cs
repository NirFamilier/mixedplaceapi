﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Coins.RequestModel;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.Map.RequestModel;
using MixedPlace.API.Models.Map.ResponseModel;
using MixedPlace.API.Models.MPChallenge.RequestModels;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.MPModel.RequestModels;
using MixedPlace.API.Models.MPModel.ResponseModels;
using MixedPlace.API.Models.Notifications.Requests;
using MixedPlace.API.Models.Package.RequestModels;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.User.ResponseModels;
using MixedPlace.API.Services;
using MixedPlace.NotificationService.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize(Policy = "Admin")]
    public class AdminController : BaseController
    {
        private readonly ILogger<AdminController> _logger;
        private readonly IChallengeService _challengeService;
        private readonly IUserService _userService;
        private readonly IMapService _mapService;
        private readonly IAdminService _adminService;
        private readonly IStoreService _storeService;
        private readonly ITransactionService _transactionService;
        private readonly INotificationsService _notificationsSerivce;

        public AdminController(ILogger<AdminController> logger,
                               IChallengeService challengeService,
                               IUserService userService,
                               IMapService mapService,
                               IAdminService adminService,
                               IStoreService storeService,
                               ITransactionService transactionService,
                               INotificationsService notificationsSerivce)
        {
            _logger = logger;
            _challengeService = challengeService;
            _userService = userService;
            _mapService = mapService;
            _adminService = adminService;
            _storeService = storeService;
            _transactionService = transactionService;
            _notificationsSerivce = notificationsSerivce;
        }

        #region Challenge APIs

        [HttpGet("api/admin/getchallengemetadata")]
        public IActionResult GetChallengeMetadata()
        {
            var response = _challengeService.GetChallengeMetadata();
            return CreateHttpResponse(response);
        }

        [HttpGet("api/admin/getchallenges")]
        public IActionResult GetChallenges()
        {
            var response = _challengeService.GetChallenges();
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/getchallenges")]
        public IActionResult GetChallenges([FromBody]FilterRequest filterRequest)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(filterRequest)}");

            var response = _challengeService.GetChallenges(filterRequest);
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/createnewchallenge")]
        public IActionResult CreateNewChallenge([FromBody]CreateChallengeReqeust request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (request != null && ModelState.IsValid)
            {
                response = _challengeService.CreateChallenge(request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/updatechallenge")]
        public IActionResult UpdateChallenge([FromBody]UpdateChallengeRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (request != null && ModelState.IsValid)
            {
                response = _challengeService.UpdateChallenge(request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        [HttpGet("api/admin/explorecompletedusers/{id}")]
        public IActionResult ExploreCompletedUsers(int id)
        {
            IResponseModel<ExploreCompletedUsersResponse> response = new ResponseModel<ExploreCompletedUsersResponse>();
            if (id > 0)
            {
                response = _challengeService.ExploreCompletedUsers(id);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        [HttpGet("api/admin/explorechallengecriteria/{id}")]
        public IActionResult ExploreChallengeCriteria(int id)
        {
            IResponseModel<ExploreChallengeCriteriaResponse> response = new ResponseModel<ExploreChallengeCriteriaResponse>();
            if (id > 0)
            {
                response = _challengeService.ExploreChallengeCriteria(id);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);

        }

        #endregion Challenge APIs

        #region User APIs

        [HttpGet("api/admin/getusermetadata")]
        public IActionResult GetUserMetadata()
        {
            var response = _userService.GetUserMetadata();
            return CreateHttpResponse(response);
        }

        [HttpGet("api/admin/getusers")]
        public IActionResult GetUsers()
        {
            var response = _userService.GetUsers();
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/getusers")]
        public IActionResult GetUsers([FromBody]FilterRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            var response = _userService.GetUsers(request);
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/createuser")]
        public async Task<IActionResult> CreateUser([FromBody]CreateUserRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> result;
            if (ModelState.IsValid)
            {
                var response = await _userService.CreateUser(request);
                return CreateHttpResponse(response);
            }
            result = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        [HttpPost("api/admin/updateuserdetails")]
        public async Task<IActionResult> UpdateUserDetails([FromBody]UpdateUserDetailsRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> result;
            if (ModelState.IsValid && request != null)
            {
                var response = await _userService.UpdateUserDetails(request);
                return CreateHttpResponse(response);
            }
            result = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        [HttpPost("api/admin/updateuserstatus")]
        public IActionResult UpdateUserStatus([FromBody]UpdateUserStatusRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> result;
            if (ModelState.IsValid && request != null)
            {
                var response = _userService.UpdateUserStatus(request);
                return CreateHttpResponse(response);
            }
            result = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        #endregion User APIs

        #region Base APIs

        [HttpGet("api/admin/getglobalmetadata")]
        public IActionResult GetGloablMetadata()
        {
            var response = _adminService.GetGlobalMetadata();
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/sendnotification")]
        public IActionResult SendNotification([FromBody]NotificationRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> response;
            if (ModelState.IsValid && request != null)
            {
                response = _userService.NotifyUser(request);
                return CreateHttpResponse(response);
            }
            response = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(response);
        }

        [HttpGet("api/admin/supersearch/{searchterm}")]
        public IActionResult SuperSearch(string searchterm)
        {
            return Ok();
        }

        #endregion Base APIs

        #region Model APIs
        [HttpGet("api/admin/getmodels")]
        public IActionResult GetModelItems()
        {
            var result = _mapService.GetAllModelItems();
            return CreateHttpResponse(result);
        }

        [HttpPost("api/admin/getmodels")]
        public IActionResult GetModels([FromBody]FilterRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            var result = _mapService.GetModels(request);
            return CreateHttpResponse(result);
        }

        [HttpPost("api/admin/ismodeluniqueidexists")]
        public IActionResult IsModelUniqueidExists([FromBody] IsUniqueIdExistsRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<bool> response = new ResponseModel<bool>();
            if (request != null && ModelState.IsValid)
            {
                response = _mapService.IsModelUniqueidExists(request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        [HttpPut("api/admin/suspendmodel/{id}/{status}")]
        public IActionResult SuspendModel(int id, int status)
        {
            var result = _mapService.UpdateModelStatus(id, status);
            return CreateHttpResponse(result);
        }    

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("api/admin/uploadbundleimage")]
        public IActionResult UploadBundleImage([FromForm] UploadImageRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            var result = _adminService.UploadImage(request);
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("api/admin/addbundle")]
        public IActionResult AddBundle([FromBody]BundleRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            var result = _adminService.AddBundle(request);
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("api/admin/bundle/{id}")]
        public IActionResult GetBundleById(int id)
        {
            var result = _adminService.GetBundleById(id);
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("api/admin/bundles")]
        public IActionResult GetBundles()
        {
            var result = _adminService.GetBundles();
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Create new Model
        /// </summary>
        /// <param name="request"></param>
        /// <returns>bool</returns>
        [HttpPost("api/admin/addmodel")]
        public IActionResult AddModel([FromBody] AddModelRequest newModel)
        {
            IResponseModel<bool> response = new ResponseModel<bool>();
            if (newModel != null && ModelState.IsValid)
            {
                response = _adminService.AddModel(newModel);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
            
        }

        /// <summary>
        /// Edit Model
        /// </summary>
        /// <param name="request"></param>
        /// <returns>bool</returns>
        [HttpPost("api/admin/editmodel")]
        public IActionResult UpdateModel([FromBody] UpdateModelRequest editedModel)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(editedModel)}");
            IResponseModel<bool> response = new ResponseModel<bool>();
            if (editedModel != null && ModelState.IsValid)
            {
                response = _adminService.UpdateModel(editedModel);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs; 
            return CreateHttpResponse(response);
        }

        /// <summary>
        /// Change model status to suspend
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("api/admin/suspendmodel")]
        public IActionResult SuspendModel([FromBody] SuspendModelRequest suspendedModel)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(suspendedModel)}");
            var result = _adminService.SuspendModel(suspendedModel);
            return CreateHttpResponse(result);
        }

        [HttpPost("api/admin/enablemodel")]
        public IActionResult EnableModel([FromBody]EnableModelRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (request != null)
            {
                response = _mapService.EnableModel(request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/addnewmodel")]
        public IActionResult AddNewModel([FromBody] AddNewModelRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            var result = _mapService.AddNewModel(request);
            return CreateHttpResponse(result);
        }

        [HttpGet("api/admin/model/getmodelcategories")]
        public IActionResult GetModelCategories()
        {
            var result = _mapService.GetModelCategories();
            return CreateHttpResponse(result);
        }


        [HttpPost("api/admin/model/getmodelsubcategories")]
        public IActionResult GetModelSubCategories([FromBody]GetModelSubCategoryRequest modelCategories)
        {
            var result = _mapService.GetModelSubCategories(modelCategories);
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Add new model category
        /// </summary>
        /// <param name="modelcategory"></param>
        /// <returns>bool</returns>
        [HttpPost("api/admin/model/addmodelcategory")]
        [ProducesResponseType(typeof(bool), 200)]
        public IActionResult AddModelCategory([FromBody] ModelCategory modelcategory)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(modelcategory)}");

            // Admin Service
            var result = _adminService.AddModelCategory(modelcategory);
            return CreateHttpResponse(result);

            //var result = _mapService.AddModelCategory(modelcategory);
            //return CreateHttpResponse(result);
        }

        /// <summary>
        /// Add new model sub category
        /// </summary>
        /// <param name="modelSubcategory"></param>
        /// <returns>bool</returns>
        [HttpPost("api/admin/model/addmodelsubcategory")]
        [ProducesResponseType(typeof(bool), 200)]
        public IActionResult AddModelSubcategory([FromBody] ModelSubCategory modelSubcategory)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(modelSubcategory)}");

            var result = _adminService.AddModelSubCategory(modelSubcategory);
            return CreateHttpResponse(result);
        }

        [Authorize]
        [HttpGet("api/admin/getmodelmetadata")]
        public IActionResult GetModelMetaData()
        {
            var response = _mapService.GetModelMetadata();
            return CreateHttpResponse(response);
        }

        [Authorize]
        [HttpPost("api/admin/getmodelwithfiltering")]
        public IActionResult GetModelWithFiltering([FromBody]FilterRequest filterRequest)
        {
            var response = _mapService.GetModels(filterRequest);
            return CreateHttpResponse(response);
        }

        [HttpGet("api/admin/getmodelsselectitems")]
        public IActionResult GetModelsSelectItems()
        {
            var response = _mapService.GetModelsSelectItems();
            return CreateHttpResponse(response);
        }

        [HttpGet("api/admin/getmodel/{id}")]
        public IActionResult GetModel(int id)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(id)}");
            IResponseModel<GetModelResponse> response = new ResponseModel<GetModelResponse>();
            if (id > 0)
            {
                response = _mapService.GetModel(id);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        #endregion Model APIs

        #region Mixed Place APIs

        [HttpGet("api/admin/getmpmetadata")]
        public IActionResult GetMPMetadata()
        {
            var response = _mapService.GetMPMetadata();
            return CreateHttpResponse(response);
        }

        [HttpGet("api/admin/getmixedplaces")]
        public IActionResult GetMixedPlaces()
        {
            var response = _mapService.GetMixedPlaces();
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/getmixedplaces")]
        public IActionResult GetMixedPlaces([FromBody]FilterRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            var response = _mapService.GetMixedPlaces(request);
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/updatemixedplace")]
        public IActionResult UpdateMixedPlace([FromBody]UpdateMixedPlaceRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (request != null && ModelState.IsValid)
            {
                response = _mapService.UpdateMixedPlace(request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/updatemixedplaceowner")]
        public IActionResult UpdateMixedPlaceOwner([FromBody]UpdateMixedPlaceOwnerRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (request != null)
            {
                response = _mapService.UpdateMixedPlaceOnwer(request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }
        [HttpPost("api/admin/updatemixedplacestatus")]
        public IActionResult UpdateMixedPlaceStatus([FromBody]UpdateMPStatusRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (request != null && ModelState.IsValid)
            {
                response = _mapService.UpdateMPStatus(request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        /// <summary>
        /// Return the optional categories of Mixed Place
        /// </summary>
        /// <returns>GetAllMPCategoriesResponse</returns>
        [Authorize]
        [HttpGet]
        [Route("api/admin/mixedPlace/categories")]
        [ProducesResponseType(typeof(GetAllMPCategoriesResponse), 200)]
        public IActionResult GetAllMPCategories()
        {
            var result = _mapService.GetAllMPCategories();
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Return the optional sub categories of Mixed Place
        /// </summary>
        /// <returns>GetMPSubCategoryResponse</returns>
        [Authorize]
        [HttpGet("api/admin/mixedPlace/getmpsubcategories")]
        [ProducesResponseType(typeof(GetMPSubCategoryResponse), 200)]
        public IActionResult GetMPSubCategories()
        {
            var result = _mapService.GetMPSubCategories();
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Add new sub category of Mixed Place
        /// </summary>
        /// <param name="mpSubcategory"></param>
        /// <returns>bool</returns>
        [Authorize]
        [HttpPost("api/admin/mixedPlace/addmpsubcategory")]
        [ProducesResponseType(typeof(bool), 200)]
        public IActionResult AddMPSubcategory([FromBody] MPSubCategory mpSubcategory)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(mpSubcategory)}");

            var result = _adminService.AddMPSubCategory(mpSubcategory);
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Add new category of Mixed Place
        /// </summary>
        /// <param name="mpCategory"></param>
        /// <returns>bool</returns>
        [Authorize]
        [HttpPost("api/admin/mixedPlace/addmpcategory")]
        [ProducesResponseType(typeof(bool), 200)]
        public IActionResult AddMPCategory([FromBody] MPCategory mpCategory)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(mpCategory)}");

            var result = _adminService.AddMPCategory(mpCategory);
            return CreateHttpResponse(result);
        }

        #endregion Mixed Place APIs

        #region Store APIs

        [HttpGet("api/admin/getstoremetadata")]
        public IActionResult GetStoreMetadata()
        {
            var response = _storeService.GetStoreMetadata();
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/getpackages")]
        public IActionResult GetPackages([FromBody] FilterRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            var response = _storeService.GetPackages(request);
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/updatepackage")]
        public IActionResult UpdatePackage([FromBody] UpdatePackageRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (request != null && ModelState.IsValid)
            {
                response = _storeService.UpdatePackage(request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/createpackage")]
        public IActionResult CreatePackage([FromBody] CreatePackageRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (request != null && ModelState.IsValid)
            {
                response = _storeService.CreatePackage(request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/updatepackagestatus")]
        public IActionResult UpdatePackageStatus([FromBody]UpdatePackageStatusRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (request != null && ModelState.IsValid)
            {
                response = _storeService.UpdatePackageStatus(request);
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

        #endregion Store APIs

        #region Transactions APIs

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterRequest"></param>
        /// <returns></returns>
        [HttpPost("api/admin/gettransactions")]
        public IActionResult GetTransactions([FromBody]FilterRequest filterRequest)
        {
            var response = _transactionService.GetTransactions(filterRequest);
            return CreateHttpResponse(response);
        }
        #endregion Transactions APIs

        #region Coins
        /// <summary>
        /// Reward to or fine the users by coins
        /// </summary>
        /// <param name="rewardRequest"></param>
        /// <returns>bool</returns>
        [HttpPost("api/admin/coins")]
        public IActionResult AddRewardPerBulkOfUsers([FromBody] RewardRequest rewardRequest)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(rewardRequest)}");
            var result = _adminService.AddRewardPerBulkOfUsers(rewardRequest);
            return CreateHttpResponse(result);
        }

        [HttpGet("api/admin/getcoinrulesmetadata")]
        public IActionResult GetCoinRulesMetadata()
        {
            var response = _adminService.GetCoinRulesMetadata();
            return CreateHttpResponse(response);
        }

        [HttpGet("api/admin/getcoinrules")]
        public IActionResult GetCoinRule()
        {
            var response = _adminService.GetCoinRules();
            return CreateHttpResponse(response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterRequest"></param>
        /// <returns></returns>
        [HttpPost("api/admin/getcoinruleswithfiltering")]
        public IActionResult GetCoinRulesWithFiltering([FromBody]FilterRequest filterRequest)
        {
            var response = _adminService.GetCoinRules(filterRequest);
            return CreateHttpResponse(response);
        }

        #endregion

        #region Analytics

        [HttpGet("api/analytics/getusercoinsbalance/{userid}")]
        public IActionResult GetUserCoinsBalance(string userid)
        {
            var response = _transactionService.GetUserCoinsBalance(userid);
            return CreateHttpResponse(response);
        }

        [HttpGet("api/analytics/getuserchallenges")]
        public IActionResult GetClientChallengesPerMonth([FromBody] string userId)
        {
            var response = _adminService.GetClientChallengesPerMonth(userId);
            return CreateHttpResponse(response);
        }

        #endregion Analytics

        #region Notifications

        [HttpPost("api/admin/notifyallusers")]
        public IActionResult NotifyAllUsers([FromBody]AdminNotifyAllRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = _notificationsSerivce.NotifyAllUsers(request);
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/notifyallchallenges")]
        public IActionResult NotifyAllChallenges([FromBody]AdminNotifyAllRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = _notificationsSerivce.NotifyAllChallenges(request);
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/notifyallmixedplaceowners")]
        public IActionResult NotifyAllMixedPlaceOwners([FromBody]AdminNotifyAllRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = _notificationsSerivce.NotifyAllMixedPlaceOwners(request);
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/notifyuser")]
        public IActionResult NotifyUser([FromBody]AdminNotifyUserRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = _notificationsSerivce.NotifyUser(request);
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/notifymixedplaceowner")]
        public IActionResult NotifyMixedPlaceOwner([FromBody]AdminNotifyMixedPlaceRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = _notificationsSerivce.NotifyMixedPlaceOwner(request);
            return CreateHttpResponse(response);
        }

        [HttpPost("api/admin/notifychallenge")]
        public IActionResult NotifyChallenge([FromBody]AdminNotifyChallengeRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");
            IResponseModel<EmptyBodyResponse> response = _notificationsSerivce.NotifyChallenge(request);
            return CreateHttpResponse(response);
        }


        #endregion

    }
}
