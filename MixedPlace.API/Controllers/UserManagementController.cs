﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map.RequestModel;
using MixedPlace.API.Models.MPDesign.ResponseModel;
using MixedPlace.API.Models.UserManagement.RequestModels;
using MixedPlace.API.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Controllers
{
    [Route("api/usermanagement")]
    public class UserManagementController:BaseController
    {
        private readonly IUserManagementService _managementService;
        private readonly ILogger<UserManagementController> _logger;

        public UserManagementController(IUserManagementService managementService, 
                                        ILogger<UserManagementController> logger)
        {
            _managementService = managementService;
            _logger = logger;
        }

        /// <summary>
        /// Add Bad Content Report
        /// </summary>
        /// <param name="report"></param>
        /// <returns>boolean (Succeeded/Not Succeed) </returns>
        [Authorize]
        [HttpPost("report")]
        public IActionResult SetMPReport([FromBody] ReportRequestModel report)
        {
            var result = _managementService.SetMPReport(report, GetUserIdFromCliams());
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Add Comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns>bool (Succeeded/Not Succeded) </returns>
        [Authorize]
        [HttpPost("comments")]
        public IActionResult SetMPComment([FromBody] CommentRequestModel comment)
        {
            var result = _managementService.SetMPComment(comment, GetUserIdFromCliams());
            return CreateHttpResponse(result);
        }


        /// <summary>
        /// Return all the comments of the Mixed Place
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="pageId"></param>
        /// <param name="numPerPage"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(CommentResponseModel), 200)]
        [Authorize]
        [HttpPost]
        [Route("getcomments")]
        public IActionResult GetComments([FromBody] GetCommentsRequestModel requestModel)
        {
            var result = _managementService.GetComments(requestModel.Coordinates.X, requestModel.Coordinates.Y, requestModel.Coordinates.Z, requestModel.PageId, requestModel.NumberPerPage);
            return CreateHttpResponse(result);
        }

        [ProducesResponseType(typeof(EmptyBodyResponse), 200)]
        [Authorize]
        [HttpPost("editcomment")]
        public IActionResult EditComment([FromBody]CommentRequestModel request)
        {
            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (request != null && request.Id > 0)
            {
                response = _managementService.EditComment(request,GetUserIdFromCliams());
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);

        }

        [ProducesResponseType(typeof(EmptyBodyResponse), 200)]
        [Authorize]
        [HttpPost("removecomment")]
        public IActionResult RemoveComment([FromBody]CommentRequestModel request)
        {
            IResponseModel<EmptyBodyResponse> response = new ResponseModel<EmptyBodyResponse>();
            if (request != null && request.Id > 0)
            {
                response = _managementService.RemoveComment(request, GetUserIdFromCliams());
                return CreateHttpResponse(response);
            }
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);

        }

        /// <summary>
        /// Add Likes
        /// </summary>
        /// <param name="umrModel"></param>
        /// <returns>bool (Succeeded/Not Succeded) </returns>
        [Authorize]
        [HttpPost]
        [Route("likes")]
        public IActionResult SetMPLikes([FromBody] MixedPlaceRequestModel umrModel)
        {
            var result = _managementService.SetMPLike(umrModel, GetUserIdFromCliams());
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Send List of visits of the user in various MPs
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("setmpvisits")]
        public async Task<IActionResult> SetMPVisits([FromBody]SetMPVisitRequest request)
        {
            if (ModelState.IsValid && request != null)
            {
                return CreateHttpResponse(
                    await _managementService.SetMPVisits(request, GetUserIdFromCliams()));
            }
            var response = new ResponseModel<EmptyBodyResponse>();
            response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
            return CreateHttpResponse(response);
        }

       

        /// <summary>
        /// Add shares - Need to be changed to recieve a list of shares
        /// </summary>
        /// <param name="umrModel"></param>
        /// <returns>bool (Succeeded/Not Succeded) </returns>
        [Authorize]
        [HttpPost]
        [Route("shares")]
        public IActionResult SetMPShares([FromBody] MixedPlaceShareRequestModel umrModel)
        {
            var result = _managementService.SetMPShare(umrModel.MPCoordinates,umrModel.Source, GetUserIdFromCliams());
            return CreateHttpResponse(result);
        }

        [HttpGet("approvereport")]
        public IActionResult ApproveReport([FromQuery]string token)
        {
            if (!string.IsNullOrWhiteSpace(token))
            {
                _managementService.ApproveReport(token);
            }
            _logger.LogError("Empty token in approvereportapi");
            return Ok();
        }

        [HttpGet("refusereport")]
        public IActionResult RefuseReport([FromQuery]string token)
        {
            if(!string.IsNullOrWhiteSpace(token))
            {
                _managementService.RefuseReport(token);
            }
            _logger.LogError("Empty token in refusereport");
            return Ok();
        }


    }
}
