﻿using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Controllers
{
    public class SchedulerController : BaseController
    {
        private readonly ILogger<SchedulerController> _logger;
        private readonly INotificationsService _notificationsService;

        public SchedulerController(ILogger<SchedulerController> logger,
                                   INotificationsService notificationsService)
        {
            _logger = logger;
            _notificationsService = notificationsService;
        }


        [HttpGet("api/scheduler/runnotificationsprocess")]
        public IActionResult RunNotificationsProcess()
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}");
            var response = _notificationsService.SendQueuedNotifications();
            return CreateHttpResponse(response);
        }
    }
}
