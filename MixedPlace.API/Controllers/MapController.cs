﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MixedPlace.API.Services;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Data;
using MixedPlace.API.Models.Map.RequestModel;
using MixedPlace.API.DataTransferObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using MixedPlace.API.Models.MPModel.RequestModels;
using MixedPlace.API.Models.MPDesign.RequestModel;
using MixedPlace.API.Models.MPDesign.ResponseModel;
using System.Net.Http;
using MixedPlace.API.Models.User.ResponseModels;
using MixedPlace.API.Models.Map.ResponseModel;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.MPModel.ResponseModels;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http.Extensions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MixedPlace.API.Controllers
{
    [Route("api/map")]
    public class MapController : BaseController
    {

        private readonly IMapService _mapService;
        private readonly ILogger<MapController> _logger;

        public MapController(IMapService mapService,
                             ILogger<MapController> logger)
        {
            _mapService = mapService;
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// Returns List of MPs for the user
        /// </summary>
        /// <param name="request"></param>
        /// <returns>UserDataResponseModel</returns>
        [Authorize]
        [HttpPost]
        [Route("mpuserdata")]
        [ProducesResponseType(typeof(UserDataResponseModel), 200)]
        public async Task<IActionResult> GetMPUserData([FromBody]GetMPUserDataRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<UserDataResponseModel> result;
            if (request != null && request.MPCoordinates != null)
            {
                return CreateHttpResponse(await _mapService.GetMPUserData(request));
            }
            result = new ResponseModel<UserDataResponseModel> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Return all models that exists in db
        /// </summary>
        /// <returns>MPModelResponseModel</returns>
        [ProducesResponseType(typeof(MPModelResponseModel), 200)]
        [Authorize]
        [HttpGet]
        [Route("modelsinfo")]
        public IActionResult ModelsInfo()
        {
            var result = _mapService.GetAllModels();
            return CreateHttpResponse(result);
            // return Ok(result);
        }

        /// <summary>
        /// Return models that owned by the user
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(typeof(MPModelResponseModel), 200)]
        [Authorize]
        [HttpGet("models/userownedmodels")]
        public IActionResult UserOwnedModels()
        {
            var result = _mapService.GetAllModelsPerUser(GetUserIdFromCliams());
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Return the optional categories of MP
        /// </summary>
        /// <returns>GetAllMPCategoriesResponse</returns>
        /// 
        [Authorize]
        [HttpGet]
        [Route("mixedPlace/categories")]
        [ProducesResponseType(typeof(GetAllMPCategoriesResponse), 200)]
        public IActionResult GetAllMPCategories()
        {
            var result = _mapService.GetAllMPCategories();
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Return the optional categories of Mixed Place Model
        /// </summary>
        /// <returns>ModelCategoryResponse</returns>
        [Authorize]
        [HttpGet("model/getmodelcategories")]
        [ProducesResponseType(typeof(ModelCategoryResponse), 200)]
        public IActionResult GetModelCategories()
        {
            var result = _mapService.GetModelCategories();
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Return the optional sub categories of Mixed Place
        /// </summary>
        /// <returns>GetMPSubCategoryResponse</returns>
        [Authorize]
        [HttpGet("mixedPlace/getmpsubcategories")]
        [ProducesResponseType(typeof(GetMPSubCategoryResponse), 200)]
        public IActionResult GetMPSubCategories()
        {
            var result = _mapService.GetMPSubCategories();
            return CreateHttpResponse(result);
        }


        /// <summary>
        /// Return the optional sub categories of Mixed Place Model
        /// </summary>
        /// <returns>ModelSubCategoryResponse</returns>
        [Authorize]
        [HttpPost("model/getmodelsubcategories")]
        [ProducesResponseType(typeof(ModelSubCategoryResponse), 200)]
        public IActionResult GetModelSubCategories([FromBody] GetModelSubCategoryRequest modelCategoryIds)
        {
            var result = _mapService.GetModelSubCategories(modelCategoryIds);
            return CreateHttpResponse(result);
        }


        /// <summary>
        /// Obsolote - Not relevant for MP
        /// </summary>
        /// <param name="mixedPlaceList"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("mixedPlace/create")]
        public IActionResult CreateMixedPlacesWithPrice([FromBody] List<MixedPlaceRequestModel> mixedPlaceList)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(mixedPlaceList)}");

            var result = _mapService.CreateMixedPlacesWithPrice(mixedPlaceList);
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Attach Model Per User
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("models/userrecievenewmodel")]
        public IActionResult UserRecieveNewModel([FromBody]List<UserRecieveNewModelItem> request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> result;
            if (ModelState.IsValid && request != null)
            {
                result = _mapService.UserRecieveNewModel(GetUserIdFromCliams(), request);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Obsolete
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("models/createnewmodel")]
        public IActionResult CreateNewModel([FromForm]CreateNewModelRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> result;
            if (ModelState.IsValid && request != null)
            {
                result = _mapService.CreateNewModel(request);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);

        }

        /// <summary>
        /// Purchase the MP - become owner of the MP
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("mixedPlace/buymp")]
        [ProducesResponseType(typeof(EmptyBodyResponse), 200)]
        public async Task<IActionResult> BuyMP([FromBody]MPDto request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            if (ModelState.IsValid && request != null)
            {
                return CreateHttpResponse(await _mapService.BuyMp(GetUserIdFromCliams(), request));
            }
            var result = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Saving all the recieved designs in the user's Mixed Placce
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("mixedplace/setmpitems")]
        [ProducesResponseType(typeof(EmptyBodyResponse), 200)]
        public IActionResult SetMPItems([FromBody]SetMPItemsRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> result;
            if (ModelState.IsValid && request != null)
            {
                result = _mapService.SetMPItems(GetUserIdFromCliams(), request);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Returns all the Desing Items in the Mixed Place
        /// </summary>
        /// <param name="request"></param>
        /// <returns>GetMPItemsResponse</returns>
        [Authorize]
        [HttpPost("mixedplace/getmpitmes")]
        [ProducesResponseType(typeof(GetMPItemsResponse), 200)]
        public async Task<IActionResult> GetMPItems([FromBody]GetMPItemRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            if (ModelState.IsValid && request != null && request.GetMPItemsCollection != null)
            {
                return CreateHttpResponse(await _mapService.GetMPItems(request));
            }
            var result = new ResponseModel<GetMPItemsResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Details of the Mixed Place : Price, Status, Owned, Socials, Categories
        /// </summary>
        /// <param name="mpCoordinats"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("getmixedplaceinfo")]
        [ProducesResponseType(typeof(GetMixedPlaceInfoResponse), 200)]
        public async Task<IActionResult> GetMixedPlaceInfo([FromBody] GetMixedPlaceInfoRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            var result = await _mapService.GetMixedPlaceInfo(request);
            return CreateHttpResponse(result);
        }


        /// <summary>
        /// Update user's Mixed Place Data 
        /// </summary>
        /// <param name="mpUserdataDto"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("mixedplace/updatempuserData")]
        public IActionResult UpdateMPUserData([FromBody] MPUserDataDto mpUserdataDto)
        {
            try
            {
                var result = _mapService.UpdateMPUserData(GetUserIdFromCliams(),mpUserdataDto);
                return CreateHttpResponse(result);
            }
            catch (Exception ex)
            {
                return CreateHttpResponse(new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs });
            }
        }
    }
}
