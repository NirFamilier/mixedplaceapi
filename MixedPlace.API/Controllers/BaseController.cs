﻿using Microsoft.AspNetCore.Mvc;
using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Controllers
{
    public class BaseController: Controller  
    {
        protected string GetUserIdFromCliams()
        {
            return User.Claims?
                       .FirstOrDefault(x=> x.Type == JwtRegisteredClaimNames.Sub)?
                       .Value;

        }
        protected IActionResult CreateHttpResponse<T>(T response)
        {
            var responseModel = new ResponseModel<T>();
            responseModel.Data = response;
            return Ok(responseModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="responseModel"></param>
        /// <returns></returns>
        protected IActionResult CreateHttpResponse<T>(IResponseModel<T> responseModel)
        {
            if (responseModel.StatusCode == 200 || responseModel.StatusCode == 0)
            {
                return Ok(responseModel);
            }

            var errorMap = ErrorMapper.GetErrorModelByErrorCode(responseModel.StatusCode);
            var errorResponseModel = new ErrorMapModel();

            if (errorMap == null)
            {
                errorResponseModel.HttpCode = 500;
                errorResponseModel.InnerCode = 9998;
                errorResponseModel.Message = string.Format("Failed to map error code {0}", responseModel.StatusCode);
            }
            else
            {
                errorResponseModel.HttpCode = errorMap.HttpCode;
                errorResponseModel.InnerCode = errorMap.InnerCode;
                errorResponseModel.Message = errorMap.Message;
              //  return StatusCode(400, errorResponseModel);
            }
            return StatusCode(errorResponseModel.HttpCode, errorResponseModel);
        }
    }
}
