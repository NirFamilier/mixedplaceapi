﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.User.ResponseModels;
using MixedPlace.API.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly ITransactionService _transactionService;
        private readonly ILogger<UserController> _logger;

        public UserController(IUserService userService,
                             ITransactionService transactionService,
                             ILogger<UserController> logger)
        {
            _userService = userService;
            _transactionService = transactionService;
            _logger = logger;
        }

        /// <summary>
        /// Return User Detials
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("api/user/getuserdetails")]
        [ProducesResponseType(typeof(UserBaseDto), 200)]
        public async Task<IActionResult> GetUserDetails()
        {
            var result = await _userService.GetUserDetails(GetUserIdFromCliams());
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// Update Details of user
        /// </summary>
        /// <param name="updateUserRequest"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("api/user/updateuserdetails")]
        [ProducesResponseType(typeof(EmptyBodyResponse), 200)]
        public async Task<IActionResult> UpdateUserDetails([FromBody]UserBaseDto updateUserRequest)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(updateUserRequest)}");

            IResponseModel<EmptyBodyResponse> result;
            if (ModelState.IsValid)
            {
                result = await _userService.UpdateUser(GetUserIdFromCliams(), updateUserRequest);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<EmptyBodyResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        /// <summary>
        /// API called when user loged in for update device details in db.
        /// This details will be used for Push Notifcaiton Service
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("api/user/updatedevicedetails")]
        [ProducesResponseType(typeof(EmptyBodyResponse), 200)]
        public IActionResult UpdateDeviceDetails([FromBody]UpdateDeviceDetailsRequest request)
        {
            _logger.LogDebug($"{Request.GetDisplayUrl()}. Request : {JsonConvert.SerializeObject(request)}");

            IResponseModel<EmptyBodyResponse> response;
            var userId = GetUserIdFromCliams();
            if (request != null && ModelState.IsValid && !string.IsNullOrWhiteSpace(userId))
            {
                response = _userService.UpdateDeviceDetails(userId,request);
                return CreateHttpResponse(response);
            }
            response = new ResponseModel<EmptyBodyResponse> { InnerCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(response);
        }

        /// <summary>
        /// Return all the user transactions
        /// </summary>
        /// <returns>GetUsersTransactionsResponse</returns>
        [Authorize]
        [HttpGet("api/user/gettransactions")]
        [ProducesResponseType(typeof(GetUsersTransactionsResponse), 200)]
        public IActionResult GetTransactions()
        {
            IResponseModel<GetUsersTransactionsResponse> response = new ResponseModel<GetUsersTransactionsResponse>();
            //response.Data = new 
            var userId = GetUserIdFromCliams();

            response.Data = new GetUsersTransactionsResponse()
            {
                Transactions = _transactionService.GetTransactions(userId)
            };
            response.StatusCode = (int)InnerErrorCode.Ok;
            return CreateHttpResponse(response);
        }

    }
}
