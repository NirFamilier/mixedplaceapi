﻿using MixedPlace.API.Models.Notifications;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User
{
    public class NotificationModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Recipient { get; set; }
        public string Message { get; set; }
        public string NotifyResult { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime? DateSent { get; set; }


        #region  Navigation Properties
        
        [ForeignKey("NotificationOrigination")]
        public int? NotificationOriginationId { get; set; }
        public NotificationOriginationModel NotificationOrigination { get; set; }

        [ForeignKey("NotificationStatus")]
        public int? NotificationStatusId { get; set; }
        public NotificationStatusModel NotificationStatus { get; set; }

        [ForeignKey("NotificationTemplate")]
        public int? NotificationTemplateId { get; set; }
        public NotificationTemplateModel NotificationTemplate { get; set; }

        [ForeignKey("NotificationMethod")]
        public int? NotificationMethodId { get; set; }
        public NotificationMethodModel NotificationMethod { get; set; }

        #endregion
        [ForeignKey("User")]
        public string UserId { get; set; }
        public User User { get; set; }
    }
}
