﻿using MixedPlace.API.Models.Notifications;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User
{
    public class NotificationTemplateModel
    {
        public int Id { get; set; }
        public string UniqueName { get; set; }
        public string Message { get; set; }

        #region Navigation Properties

        [ForeignKey("NotificationTemplateType")]
        public int? NotificationTemplateTypeId { get; set; }
        public NotificationTemplateTypeModel NotificationTemplateType { get; set; }
        #endregion
    }
}
