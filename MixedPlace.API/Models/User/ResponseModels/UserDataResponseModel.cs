﻿using MixedPlace.API.Models.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.ResponseModels
{
    public class UserDataResponseModel
    {
        public List<MPUserData> List { get; set; } = new List<MPUserData>();
    }
}
