﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.ResponseModels
{
    public class AuthResponse
    {
        public long ExpirationDate { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public DateTime LastLoginDate { get; set; }
    }
}
