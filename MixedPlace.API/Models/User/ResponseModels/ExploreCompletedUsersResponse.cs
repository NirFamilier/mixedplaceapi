﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.ResponseModels
{
    public class ExploreCompletedUsersResponse
    {
        public int ChallengeId { get; set; }
        public IEnumerable<ChallengeComletedUserDto> Users { get; set; }
    }
}
