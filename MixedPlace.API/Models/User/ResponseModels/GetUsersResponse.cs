﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.ResponseModels
{
    public class GetUsersResponse : FilterBaseResponse
    {
        public IEnumerable<UserDetailsDto> Users = new List<UserDetailsDto>();
    }
}
