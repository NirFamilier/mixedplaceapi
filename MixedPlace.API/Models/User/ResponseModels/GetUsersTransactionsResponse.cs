﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.ResponseModels
{
    public class GetUsersTransactionsResponse : FilterBaseResponse
    {
        public IEnumerable<TransactionDto> Transactions = new List<TransactionDto>();
    }
}
