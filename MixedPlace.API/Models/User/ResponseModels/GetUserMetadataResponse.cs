﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Base.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.ResponseModels
{
    public class GetUserMetadataResponse : MetadataResponseBase
    {
        public IEnumerable<UserStatusDto> UserStatuses = new List<UserStatusDto>();
    }
}
