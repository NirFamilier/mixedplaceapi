﻿using Microsoft.AspNetCore.Identity;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.MPChallenge;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.Package;
using MixedPlace.API.Models.UserManagement;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User
{
    public class User: IdentityUser
    {
        public bool IsTemporaryPassword { get; set; }
        public DateTime PasswordExpirationDate { get; set; }
        public string DeviceID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public int StreetNumber { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public bool Gender { get; set; } 
        public int Age { get; set; }
        public string UserDescription { get; set; }
        public string Image { get; set; } 
        public string MPDescription { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? NotificationSendDate { get; set; }
        public decimal CoinBalance { get; set; }

        public int? UserLevel { get; set; }
        public int? ChallengeLevel { get; set; }
        public int? SocialLevel { get; set; }
        public int? MPLevel { get; set; }
        public int? DesingLevel { get; set; }
        
        

        [ForeignKey("DeviceOS")]
        public int? DeviceOSId { get; set; }
        public DeviceOSModel DeviceOS { get; set; }

        [ForeignKey("UserStatus")]
        public int? UserStatusId { get; set; }
        public UserStatusModel UserStatus { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public ICollection<MPReport> MPReports { get; set; }
        public ICollection<PackagePurchaseModel> PurchasedPackages { get; set; }
        public ICollection<NotificationModel> Notifications { get; set; } = new List<NotificationModel>();
        public ICollection<MP> MixedPlaces { get; set; } = new List<MP>();
        public ICollection<MPModelPerUser> ModelsPerUser { get; set; } = new List<MPModelPerUser>();
        public ICollection<AchievedChallengeModel> AchievedChallenges { get; set; } = new List<AchievedChallengeModel>();
    }
}
