﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User
{
    public class UserBaseDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        
        public string Street { get; set; }

        public int StreetNumber { get; set; }
        [Required]
        public string City { get; set; }
        
        public string Country { get; set; }
        [Required]
        public bool Gender { get; set; } 

        [Range(0,120)]
        public int Age { get; set; }

        public string UserDescription { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }

        public string ImageUrl { get; set; } 
        public UserStatusDto UserStatus { get; set; }
        public decimal CoinBalance { get; set; }
        public string DeviceID { get; set; }
    }
}
