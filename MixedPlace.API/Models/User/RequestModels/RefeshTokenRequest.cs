﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.RequestModels
{
    /// <summary>
    /// RefeshTokenRequest
    /// </summary>
    public class RefeshTokenRequest
    {
        /// <summary>
        /// The expired Token
        /// </summary>
        [Required]
        public string Token { get; set; }

        /// <summary>
        /// The refresh token that was provided in the Login
        /// </summary>
        [Required]
        public string RefreshToken { get; set; }
    }
}
