﻿using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.RequestModels
{
    public class NotificationRequest
    {
        public NotificationMethod NotificationType { get; set; }

        [Required]
        public string UserId { get; set; }
        [Required]
        public string Message { get; set; }
    }
}
