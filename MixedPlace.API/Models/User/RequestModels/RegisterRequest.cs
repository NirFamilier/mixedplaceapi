﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.RequestModels
{
    public class RegisterRequest : UserBaseDto
    {
        [Required]
        public string Username { get; set; }

        /// <summary>
        /// Id of the mobile device
        /// </summary>
        [Required]
        public string DeviceID { get; set; }
       
    }
}
