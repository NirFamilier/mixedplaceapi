﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.RequestModels
{
    public class ExternalLoginRequest
    {
        /// <summary>
        /// Facebook, Google,etc
        /// </summary>
        [Required]
        public string ProviderName { get; set; }
        /// <summary>
        /// AccessToken Recieved by the Provider
        /// </summary>
        [Required]
        public string AccessToken { get; set; }
    }
}
