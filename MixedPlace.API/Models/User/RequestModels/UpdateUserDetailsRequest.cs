﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.RequestModels
{
    public class UpdateUserDetailsRequest 
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string PhoneNumber { get; set; }
        public bool Gender { get; set; }
        public string City { get; set; }
        public UserStatusDto UserStatus { get; set; }
        public decimal CoinBalance { get; set; }
        public int UserLevel { get; set; }
        public int ChallengeLevel { get; set; }
        public int SocialLevel { get; set; }    
        public int ReportsCount { get; set; }
        public bool IsReported { get; set; }
        public string ImageUrl { get; set; }
        public string ImageBase64 { get; set; }
        public string ImageMimeType { get; set; }
    }
}
