﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.RequestModels
{
    public class UpdateUserStatusRequest
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public UserStatusDto UserStatus { get; set; }
    }
}
