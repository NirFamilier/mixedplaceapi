﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Notifications
{
    public class NotificationStatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
