﻿using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Notifications.Requests
{
    public class AdminNotifyAllRequest
    {
        public string Title { get; set; }
        public FilterRequest Filter { get; set; }
        public string Message { get; set; }
        public bool IsAchieved { get; set; }
    }
}
