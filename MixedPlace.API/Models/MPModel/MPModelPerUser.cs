﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel
{
    public class MPModelPerUser
    {
        public int Id { get; set; }

        public int TotalModelsCount { get; set; }
        public int RemainingModelsCount { get; set; }

        public string UserId { get; set; }
        public User.User User { get; set; }

        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public int MPModelId { get; set; }
        public MPModel MPModel { get; set; }
    }
}
