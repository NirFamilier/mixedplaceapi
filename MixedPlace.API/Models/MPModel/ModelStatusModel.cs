﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel
{
    public class ModelStatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
