﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using MixedPlace.API.DataTransferObjects;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class UploadImageRequest
    {
        [Required]
        public IFormFile FormFile { get; set; }
        
    }
}
