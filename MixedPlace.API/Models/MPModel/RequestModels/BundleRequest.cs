﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using MixedPlace.API.DataTransferObjects;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class BundleRequest
    {
       
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public List<ModelCatSubCatDto> BundelCategories { get; set; }
        
        public DateTime CreatedDate { get; set; }
        public DateTime UpdateTime { get; set; }
        public long Size { get; set; }
        public int NumberOfItems { get; set; }
        public string URL { get; set; }
    }
}
