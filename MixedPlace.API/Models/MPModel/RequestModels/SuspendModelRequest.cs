﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class SuspendModelRequest
    {
        public int Id { get; set; }
    }
}
