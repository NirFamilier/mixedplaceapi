﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class CreateNewModelRequest
    {
        [Required]
        public IFormFile FormFile { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int PriceType { get; set; }
        public float Size { get; set; }

        [Range(1,int.MaxValue)]
        public int ModelCategory { get; set; }
        public int ModelSubCategory { get; set; }

    }
}
