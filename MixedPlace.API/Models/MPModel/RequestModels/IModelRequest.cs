﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class ModelRequestBase
    {
        public List<ModelCatSubCatDto> ModelCategories { get; set; }
    }
}
