﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class GetModelDataRequest
    {
        public string ModelUrl { get; set; }
    }
}
