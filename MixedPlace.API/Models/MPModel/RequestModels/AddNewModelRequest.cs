﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class AddNewModelRequest
    {
        [Required]
        public string Name { get; set; }
        //Location
        public string Location { get; set; }//From Bundle Url
        public string Description { get; set; }
        public long Size { get; set; } //From Bundle File Size
        public int PriceType { get; set; }
        public decimal PricePerUnit { get; set; }
    }
}
