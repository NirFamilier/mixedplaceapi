﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class UpdateModelRequest : ModelRequestBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public decimal Price { get; set; }
        [Required]
        public PriceTypeDto PriceType { get; set; }
        public string UniqueId { get; set; }
    }
}
