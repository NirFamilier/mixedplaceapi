﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class UserRecieveNewModelItem
    {
        /// <summary>
        /// Model Primary Key
        /// </summary>
        [Range(0,int.MaxValue)]
        public int ModelId { get; set; }
        public int RecievedModelsCount { get; set; }
    }
}
