﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class EnableModelRequest
    {
        public int Id { get; set; }
    }
}
