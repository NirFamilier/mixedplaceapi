﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class IsUniqueIdExistsRequest
    {
        [Required]
        public string UniqueId { get; set; }
    }
}
