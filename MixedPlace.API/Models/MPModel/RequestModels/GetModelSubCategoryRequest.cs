﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class GetModelSubCategoryRequest
    {
        public List<int> ModelCategoryIds { get; set; }
    }
}
