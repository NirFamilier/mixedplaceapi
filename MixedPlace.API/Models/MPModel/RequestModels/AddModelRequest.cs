﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.RequestModels
{
    public class AddModelRequest : ModelRequestBase
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public PriceTypeDto PriceType { get; set; }
        public decimal Price { get; set; }
        [Required]
        public DropdownDto Bundle { get; set; }
        public int MyProperty { get; set; }
        public string UniqueId { get; set; }
    }
}
