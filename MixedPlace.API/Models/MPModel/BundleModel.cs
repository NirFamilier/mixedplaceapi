﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel
{
    public class BundleModel
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int NumberOfModels { get; set; }
        public long Size { get; set; }

        public ICollection<BundleModelCatSubCat> BundleModelCatSubCat { get; set; }

    }
}
