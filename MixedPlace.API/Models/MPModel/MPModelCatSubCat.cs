﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel
{
    public class MPModelCatSubCat
    {
        public int Id { get; set; }
        [ForeignKey("ModelId")]
        public int? ModelId { get; set; }
        public MPModel Model { get; set; }
        [ForeignKey("ModelCategoryId")]
        public int? ModelCategoryId { get; set; }
        public ModelCategory ModelCategory { get; set; }
        [ForeignKey("ModelSubCategoryId")]
        public int? ModelSubCategoryId { get; set; }
        public ModelSubCategory ModelSubCategory { get; set; }
    }
}
