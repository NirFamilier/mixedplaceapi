﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.ResponseModels
{
    public class GetModelResponse
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public PriceTypeDto PriceType { get; set; }
        public decimal Price { get; set; }
        public string Url { get; set; }
        public DropdownDto Bundle { get; set; }
        public string UniqueId { get; set; }
        public IEnumerable<ModelCatSubCatDto> ModelCategories { get; set; }
    }
}
