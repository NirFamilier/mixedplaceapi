﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.ResponseModels
{
    public class ModelCategoryResponse
    {
        public List<ModelCategoryDto> list { get; set; }
    }
}
