﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MixedPlace.API.DataTransferObjects;

namespace MixedPlace.API.Models.MPModel.ResponseModels
{
    public class BundleResponseModel
    {
        public List<BundleModelDto> List { get; set; }
    }
}
