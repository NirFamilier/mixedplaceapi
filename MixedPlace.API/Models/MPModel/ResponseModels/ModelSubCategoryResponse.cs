﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.ResponseModels
{
    public class ModelSubCategoryResponse
    {
        public List<ModelCatSubCatDto> list { get; set; }
    }
}
