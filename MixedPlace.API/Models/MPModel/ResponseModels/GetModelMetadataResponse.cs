﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Base.ResponseModel;

namespace MixedPlace.API.Models.MPModel.ResponseModels
{
    public class GetModelMetadataResponse: MetadataResponseBase
    {
        public IEnumerable<ModelStatusDto> ModelStatuses { get; set; }
        public IEnumerable<ModelCategoryDto> ModelCategories { get; set; }
        public IEnumerable<ModelSubCategoryDto> ModelSubCategories { get; set; }
        public IEnumerable<PriceTypeDto> PriceTypes { get; set; }
        public IEnumerable<DropdownDto> Bundles { get; set; }
    }
}
