﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.ResponseModels
{
    public class MPModelResponseModel: FilterBaseResponse
    {
        public List<MPModelDto> List { get; set; } = new List<MPModelDto>();
    }
}
