﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel.ResponseModels
{
    public class MPUserModelsResponse
    {
        public IEnumerable<UserModelDto> List { get; set; }
    }
}
