﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel
{
    public class BundleModelCatSubCat
    {
        public int Id { get; set; }
        [ForeignKey("Bundle")]
        public int? BundleId { get; set; }
        public BundleModel Bundle { get; set; }
        [ForeignKey("ModelCategory")]
        public int? ModelCategoryId { get; set; }
        public ModelCategory ModelCategory { get; set; }
        [ForeignKey("ModelSubCategory")]
        public int? ModelSubCategoryId { get; set; }
        public ModelSubCategory ModelSubCategory { get; set; }
    }
}
