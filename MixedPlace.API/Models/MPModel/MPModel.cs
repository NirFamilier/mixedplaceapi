﻿using MixedPlace.API.Models.Map;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MixedPlace.API.Models.Package;

namespace MixedPlace.API.Models.MPModel
{
    public class MPModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        
        [ForeignKey("PriceType")]
        public int? PriceTypeId { get; set; }
        public PriceTypeModel PriceType { get; set; }
        [ForeignKey("ModelStatus")]
        public int? ModelStatusId { get; set; }
        public ModelStatusModel ModelStatus { get; set; }

        public string BundleImageName { get; set; }
        [ForeignKey("Bundle")]
        public int? BundleId { get; set; }
        public BundleModel Bundle { get; set; }

        [Required]
        public string UniqueId { get; set; }

        public ICollection<PackageItemModel> PackageItems { get; set; }
        public ICollection<MPModelPerUser> ModelPerUsers { get; set; } = new List<MPModelPerUser>();
        public ICollection<MPModelCatSubCat> MPModelCatSubCats { get; set; }
    }
}
