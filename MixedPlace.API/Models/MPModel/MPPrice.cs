﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPModel
{
    public class MPPrice
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
    }
}
