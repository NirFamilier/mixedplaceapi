﻿using MixedPlace.API.Models.Base;
using MixedPlace.API.Models.MPModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package
{
    public class PackageModel
    {        
        public int Id { get; set; }        
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime? AvailableFrom { get; set; }
        public DateTime? ExpiresOn { get; set; }

       
        public decimal Price { get; set; }

        public float Size { get; set; }
        public int PurchasedUnits { get; set; }

        [ForeignKey("PackageStatus")]
        public int? PackageStatusId { get; set; }
        public PackageStatusModel PackageStatus { get; set; }

        [ForeignKey("PackageCategory")]
        public int? PackageCategoryId { get; set; }
        public PackageCategoryModel PackageCategory { get; set; }

        [ForeignKey("PriceType")]
        public int PriceTypeId { get; set; }
        public PriceTypeModel PriceType { get; set; }

        public ICollection<PackageItemModel> PackageItems { get; set; } = new List<PackageItemModel>();

        public ICollection<PackagePurchaseModel> PurchasedPackages { get; set; }

    }
}
