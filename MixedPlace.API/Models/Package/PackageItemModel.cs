﻿using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package
{
    public class PackageItemModel
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public int? Sum { get; set; }
        public int? NumOfUnits { get; set; }
        public float TotalSize { get; set; }

        [ForeignKey("Model")]
        public int? ModelId { get; set; }
        public MPModel.MPModel Model { get; set; }

        [ForeignKey("Package")]
        public int? PackageId{ get; set; }
        public PackageModel Package { get; set; }

        [ForeignKey("PackageItemType")]
        public int? PackageItemTypeId { get; set; }
        public PackageItemTypeModel PackageItemType { get; set; }
    }
}
