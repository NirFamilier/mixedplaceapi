﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package
{
    public class PurchaseStatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
