﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package.ResponseModels
{
    public class GetPackageResponse
    {
        public int PackageId { get; set; }
    }
}
