﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package.ResponseModels
{
    /// <summary>
    /// Response object that contain all packages  that are 
    /// relevant for client store
    /// </summary>
    public class GetClientStorePackagesResponse
    {
        /// <summary>
        /// Collection of packages
        /// </summary>
        public IEnumerable<ClientStorePackageDto> Packages { get; set; } 
    }
}
