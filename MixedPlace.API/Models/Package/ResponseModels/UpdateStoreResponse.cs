﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package.ResponseModels
{
    public class UpdateStoreResponse : PackageDto
    {
        public string ImageBase64 { get; set; }
        public string Descriptioin { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
