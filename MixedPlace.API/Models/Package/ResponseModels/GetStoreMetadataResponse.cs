﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Base.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package.ResponseModels
{
    public class GetStoreMetadataResponse : MetadataResponseBase
    {
        public IEnumerable<PackageStatusDto> PackageStatuses { get; set; }
        public IEnumerable<PackageCategoryDto> PackageCategories { get; set; }
        public IEnumerable<PackageItemTypeDto> PackageItemsTypes { get; set; }
        public IEnumerable<PriceTypeDto> PriceTypes { get; set; }


    }
}
