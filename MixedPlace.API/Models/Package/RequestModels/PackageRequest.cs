﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package.RequestModels
{
    public class PackageRequest
    {
        public int PackageId { get; set; }
    }
}
