﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package.RequestModels
{
    public class BuyPackageRequest
    {
        public string InvoiceBase64 { get; set; }
        public string InvoiceMimeType { get; set; }
    }
}
