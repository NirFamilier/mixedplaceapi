﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package.RequestModels
{
    public class AddPackageToUserShoppingCartRequest
    {
        public int PackageId { get; set; }
    }
}
