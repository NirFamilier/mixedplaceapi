﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package.RequestModels
{
    public class UpdatePackageRequest: PackageDto
    {
        public string ImageBase64 { get; set; }
        public string ImageMimeType { get; set; }
    }
}
