﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package.RequestModels
{
    public class CreatePackageRequest : PackageDto
    {
        [Required]
        public string ImageBase64 { get; set; }
        public string ImageMimeType { get; set; }
    }
}
