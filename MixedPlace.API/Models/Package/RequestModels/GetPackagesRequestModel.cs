﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package.RequestModel
{
    public class GetPackagesRequestModel
    {
        public PackageCategoryType PackageType { get; set; }
        public bool RetrieveAllPackages { get; set; }
        public int PageId { get; set; }
        public int NumberPerPage { get; set; }
    }
}
