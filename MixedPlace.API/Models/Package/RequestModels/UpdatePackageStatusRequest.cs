﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package.RequestModels
{
    public class UpdatePackageStatusRequest
    {
        public int Id { get; set; }
        [Required]
        public PackageStatusDto PackageStatus { get; set; }
    }
}
