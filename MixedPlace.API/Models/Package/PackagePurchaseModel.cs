﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Package
{
    public class PackagePurchaseModel
    {
        public int Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string InvoiceUrl { get; set; }
        public decimal AmountPaid { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public User.User User { get; set; }

        [ForeignKey("Package")]
        public int? PackageId { get; set; }
        public PackageModel Package { get; set; }

        [ForeignKey("PurchaseStatus")]
        public int? PurchaseStatusId { get; set; }
        public PurchaseStatusModel PurchaseStatus { get; set; }
    }
}
