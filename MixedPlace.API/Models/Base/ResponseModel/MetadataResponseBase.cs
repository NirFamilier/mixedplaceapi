﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Base.ResponseModel
{
    public class MetadataResponseBase
    {
        public Dictionary<string, List<int>> Filters { get; set; } = new Dictionary<string, List<int>>();
    }
}
