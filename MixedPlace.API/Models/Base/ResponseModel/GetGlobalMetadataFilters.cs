﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Base.ResponseModel
{
    public class GetGlobalMetadataFilters
    {
        public IEnumerable<ConditionDto> Conditions { get; set; }
    }
}
