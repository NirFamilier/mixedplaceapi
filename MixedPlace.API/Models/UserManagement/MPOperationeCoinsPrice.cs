﻿using MixedPlace.API.Models.MPChallenge;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.UserManagement
{
    public class MPOperationeCoinsPrice
    {
        public int Id { get; set; }
        [ForeignKey("ChallengeActionTypeModel")]
        public int CATMId { get; set; }
        public ChallengeActionTypeModel ChallengeActionTypeModel { get; set; }
        public double PriceInCoins { get; set; }
    }
}
