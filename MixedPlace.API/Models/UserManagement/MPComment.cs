﻿using MixedPlace.API.Models.Map;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.UserManagement
{
    public class MPComment
    {
        public int Id { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public User.User User { get; set; }

        [ForeignKey("MixedPlace")]
        public int MPId { get; set; }
        public MP MixedPlace { get; set; }

        public DateTime CreatedDate {get;set;}
        public string CommentText { get; set; }
    }
}
