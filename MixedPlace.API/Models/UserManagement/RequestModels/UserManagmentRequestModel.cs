﻿using MixedPlace.API.Models.Map.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.UserManagement.RequestModels
{
    public class UserManagmentRequestModel
    {
        public MixedPlaceRequestModel mpCoordinats { get; set; }
        public string Userid { get; set; }
    }
}
