﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Map.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.UserManagement.RequestModels
{
    public class CommentRequestModel
    {
        public MPCoordinateDto MPCoordinate { get; set; }
        public int Id { get; set; }
        public string Comment { get; set; }

    }
}
