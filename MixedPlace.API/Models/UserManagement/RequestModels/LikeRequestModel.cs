﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.UserManagement.RequestModels
{
    public class LikeRequestModel
    {
        public string UserId { get; set; }
        public int MPId { get; set; }
    }
}
