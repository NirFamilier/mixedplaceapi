﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.UserManagement.RequestModels
{
    public class SetMPVisitRequestItem
    {
        [Required]
        public MPCoordinateDto MPCoordinate { get; set; }
        public DateTime Date { get; set; }
    }
}
