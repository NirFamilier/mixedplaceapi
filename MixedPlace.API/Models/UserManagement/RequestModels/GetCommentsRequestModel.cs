﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.RequestModel
{
    public class GetCommentsRequestModel
    {
        public MPCoordinateDto Coordinates { get; set; }
        public int PageId { get; set; }
        public int NumberPerPage { get; set; }
    }
}
