﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Map.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.UserManagement.RequestModels
{
    public class ReportRequestModel
    {
        public MPCoordinateDto mpCoordinats { get; set; }
        public string ReportText { get; set; }
    }
}
