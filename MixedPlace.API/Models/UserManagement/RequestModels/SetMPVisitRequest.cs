﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.UserManagement.RequestModels
{
    public class SetMPVisitRequest
    {
        [Required]
        public IEnumerable<SetMPVisitRequestItem> MPVisitsCollection { get; set; }
    }
}
