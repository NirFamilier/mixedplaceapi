﻿using MixedPlace.API.Models.Map;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.UserManagement
{
    public class MPReport
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string ReportText { get; set; }


        #region Navigation Properties
        [ForeignKey("User")]
        public string UserId { get; set; }
        public User.User User { get; set; }

        [ForeignKey("ReportStatus")]
        public int? ReportStatusId { get; set; }
        public MPReportStatusModel ReportStatus { get; set; }

        [ForeignKey("MixedPlace")]
        public int MPId { get; set; }
        public MP MixedPlace { get; set; }
        #endregion Navigation Properties
    }
}
