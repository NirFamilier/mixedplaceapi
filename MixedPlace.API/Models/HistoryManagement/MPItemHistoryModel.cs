﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.HistoryManagement
{
    public class MPItemHistoryModel
    {
        public int Id { get; set; }

        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int ItemType { get; set; }
        public string Json { get; set; }

        [ForeignKey("MPHistory")]
        public int MPHistoryId { get; set; }
        public MPHistoryModel MPHistory { get; set; }
    }
}
