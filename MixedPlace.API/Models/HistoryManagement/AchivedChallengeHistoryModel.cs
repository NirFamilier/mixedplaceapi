﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.HistoryManagement
{
    public class AchivedChallengeHistoryModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public User.User User { get; set; }

        [ForeignKey("Challenge")]
        public int ChallengeId { get; set; }

        [ForeignKey("MPHistory")]
        public int MPHistoryId { get; set; }
        public MPHistoryModel MPHistory{ get; set; }
    }
}
