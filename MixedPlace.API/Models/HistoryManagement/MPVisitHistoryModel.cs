﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.HistoryManagement
{
    public class MPVisitHistoryModel
    {

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int PublicCounter { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public User.User User { get; set; }

        [ForeignKey("MPHistory")]
        public int MPHisotoryId { get; set; }
        public MPHistoryModel MPHistory { get; set; }

    }
}
