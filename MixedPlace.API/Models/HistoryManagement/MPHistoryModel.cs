﻿using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.MPChallenge;
using MixedPlace.API.Models.MPDesign;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.HistoryManagement
{
    public class MPHistoryModel
    {
        public int Id { get; set; }
        public long X { get; set; }
        public long Y { get; set; }
        public long Z { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public int HiddenCounter { get; set; }

        #region Navigation Properties

        [ForeignKey("Price")]
        public int? PriceId { get; set; }
        public MPPrice Price { get; set; }

        [ForeignKey("MixedPlace")]
        public int? MPId { get; set; }
        public MP MixedPlace { get; set; }

        [ForeignKey("DesignStatus")]
        public int? DesignStatusId { get; set; }
        public DesignStatusModel DesignStatus { get; set; }

        [ForeignKey("MPStatus")]
        public int? MPStatusId { get; set; }
        public MPStatusModel MPStatus { get; set; }

        [ForeignKey("User")]
        public string OwnerId { get; set; }
        public User.User User { get; set; }

        public ICollection<MPVisitHistoryModel> MPVisitsHistory { get; set; }
        public ICollection<MPCommentHistoryModel> MPCommentsHistory { get; set; }
        public ICollection<MPLikeHistoryModel> MPLikesHistory { get; set; }
        public ICollection<MPShareHistoryModel> MPSharesHistory { get; set; }
        public ICollection<MPItemHistoryModel> MPItemsHistory { get; set; }
        public ICollection<AchivedChallengeHistoryModel> AchievedChallengesHistory { get; set; }

        #endregion Navigation Properties

    }
}
