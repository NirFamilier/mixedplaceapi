﻿using MixedPlace.API.Models.HistoryManagement;
using MixedPlace.API.Models.Map;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.HistoryManagement
{
    public class MPHistoryModelCatSubCat
    {
        public int Id { get; set; }
        [ForeignKey("MPId")]
        public int? MPId { get; set; }
        public MPHistoryModel MP { get; set; }
        [ForeignKey("MPCategoryId")]
        public int? MPCategoryId { get; set; }
        public MPCategory MPCategory { get; set; }
        [ForeignKey("MPSubCategoryId")]
        public int? MPSubCategoryId { get; set; }
        public MPSubCategory MPSubCategory { get; set; }
    }
}
