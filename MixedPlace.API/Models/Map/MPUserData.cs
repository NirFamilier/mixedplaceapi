﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map
{
    public class MPUserData
    {
        public string MPName { get; set; }
        public MPCoordinateDto MPCoordinates { get; set; }
        public UserBaseDto Owner { get; set; }
    }
}
