﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Base.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.ResponseModel
{
    public class GetMPMetadataResponse : MetadataResponseBase
    {
        public IEnumerable<MPStatusDto> MPStatuses { get; set; }
        public IEnumerable<DesignStatusDto> DesignStatuses {get; set;}
        public IEnumerable<OwnerDto> Users { get; set; }
    }
}
