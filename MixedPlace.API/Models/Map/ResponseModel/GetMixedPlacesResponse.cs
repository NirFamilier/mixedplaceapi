﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.ResponseModel
{
    public class GetMixedPlacesResponse : FilterBaseResponse
    {
        public IEnumerable<MPDetailsDto> MixedPlaces { get; set; }
    }
}
