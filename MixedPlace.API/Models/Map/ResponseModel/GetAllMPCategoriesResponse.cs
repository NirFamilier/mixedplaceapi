﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.ResponseModel
{

    public class GetAllMPCategoriesResponse
    {
        public List<MPCategory> List { get; set; }
    }
}
