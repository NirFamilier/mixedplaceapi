﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.ResponseModel
{
    public class GetMixedPlaceInfoResponse
    {
        public List<GetMixedPlaceInfoItem> List { get; set; }
    }
}
