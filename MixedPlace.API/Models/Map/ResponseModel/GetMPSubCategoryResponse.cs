﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.ResponseModel
{
    public class GetMPSubCategoryResponse
    {
        public List<MPSubCategory> list { get; set; }
    }
}
