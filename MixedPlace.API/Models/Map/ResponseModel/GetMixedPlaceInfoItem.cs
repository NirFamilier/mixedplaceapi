﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.ResponseModel
{
    public class GetMixedPlaceInfoItem
    {
        public MPCoordinateDto MPCoordinate { get; set; } = new MPCoordinateDto();
        public bool IsFree { get; set; }
        public Guid OwnerID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int LikesCount{ get; set; }
        public int CommentsCount{ get; set; }
        public int ShareCount { get; set; }
        public int VisitsCount { get; set; }
        public decimal Price { get; set; }
        public DateTime UpdateDate { get; set; }
        public List<MPCatSubCatDto> Categories { get; set; }
    }
}
