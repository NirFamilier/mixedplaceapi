﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map
{
    public class MPCatSubCat
    {
        public int Id { get; set; }
        [ForeignKey("MP")]
        public int? MPId { get; set; }
        public MP MP { get; set; }
        [ForeignKey("MPCategory")]
        public int? MPCategoryId { get; set; }
        public MPCategory MPCategory { get; set; }
        [ForeignKey("MPSubCategory")]
        public int? MPSubCategoryId { get; set; }
        public MPSubCategory MPSubCategory { get; set; }
    }
}
