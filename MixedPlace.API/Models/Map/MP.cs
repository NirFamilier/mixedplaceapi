﻿using MixedPlace.API.Models.MPChallenge;
using MixedPlace.API.Models.MPDesign;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.UserManagement;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map
{
    public class MP
    {
        public int Id { get; set; }
        public long X { get; set; }
        public long Y { get; set; }
        public long Z { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public int HiddenCounter { get; set; }

        //TODO: Verify about reports management: Need to be sotred on the tabel or
        //calculated for response object based on reports table
        //public bool IsReported { get; set; }
        //public int ReportsCount { get; set; }

        public int? PriceId { get; set; }
        public MPPrice Price { get; set; }

        [ForeignKey("DesignStatus")]
        public int? DesignStatusId { get; set; }
        public DesignStatusModel DesignStatus { get; set; }

        [ForeignKey("MPStatus")]
        public int? MPStatusId { get; set; }
        public MPStatusModel MPStatus { get; set; }

        [ForeignKey("User")]
        public string OwnerId { get; set; }
        public User.User User { get; set; }

        public string City { get; set; }
        public string Country { get; set; }

        public ICollection<MPReport> MPReports { get; set; }
        public ICollection<MPVisit> MPVisits { get; set; }
        public ICollection<MPComment> MPComments { get; set; }
        public ICollection<MPLike> MPLike { get; set; }
        public ICollection<MPShare> MPShares { get; set; }
        public ICollection<MPItemModel> MPDesigns { get; set; }
        public ICollection<AchievedChallengeModel> AchievedChallenges { get; set; }
        public ICollection<MPCatSubCat> MPCategories { get; set; }
    }
}
