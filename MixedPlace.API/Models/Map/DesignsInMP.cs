﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map
{
    public class DesignsInMP
    {
        public int Id { get; set; }
        //Mixed Place ID
        public int MPId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int DesignId { get; set; }
    }
}
