﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.RequestModel
{
    public class UpdateMixedPlaceRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Required]
        public MPStatusDto MPStatus { get; set; }
        [Required]
        public DesignStatusDto DesignStatus { get; set; }
        public decimal Price { get; set; }
        public List<MPCatSubCatDto> Categories { get; set; }
        // public IEnumerable<int> CategoryIds { get; set; }
        public string OwnerId { get; set; }

    }
}
