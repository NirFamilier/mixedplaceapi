﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.RequestModel
{
    public class GetMPUserDataRequest
    {
        public IEnumerable<MPCoordinateDto> MPCoordinates { get; set; }
    }
}
