﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.RequestModel
{
    public class UpdateMixedPlaceOwnerRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public MPStatusDto  MPStatus { get; set; }
        public string OwnerId { get; set; }
        public string OwnerName { get; set; }
    }
}
