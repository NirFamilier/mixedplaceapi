﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.RequestModel
{
    public class MixedPlaceShareRequestModel
    {
        public string Source { get; set; }
        public MPCoordinateDto MPCoordinates { get; set; }
    }
}
