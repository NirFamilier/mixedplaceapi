﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Map.RequestModel
{
    public class UpdateMPStatusRequest
    {
        public int MPId { get; set; }
        [Required]
        public MPStatusDto  MPStatus { get; set; }
    }
}
