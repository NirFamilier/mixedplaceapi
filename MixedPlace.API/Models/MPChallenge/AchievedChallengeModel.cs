﻿using MixedPlace.API.Models.Map;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge
{
    public class AchievedChallengeModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public User.User User { get; set; }

        [ForeignKey("Challenge")]
        public int? ChallengeId { get; set; }
        public Challenge Challenge { get; set; }

        [ForeignKey("MixedPlace")]
        public int? MPId { get; set; }
        public MP MixedPlace { get; set; }
    }
}
