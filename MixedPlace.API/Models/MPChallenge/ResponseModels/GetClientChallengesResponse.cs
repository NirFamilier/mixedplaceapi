﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge.ResponseModels
{
    public class GetClientChallengesResponse
    {
        public List<ClientChallengeDto> ClientChallenges { get; set; }
    }
}
