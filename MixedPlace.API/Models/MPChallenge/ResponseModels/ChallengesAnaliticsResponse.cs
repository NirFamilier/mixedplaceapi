﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge.ResponseModels
{
    public class ChallengesAnaliticsResponse
    {
        public Dictionary<DateTime,int> UserChallengesCounts { get; set;}
    }
}
