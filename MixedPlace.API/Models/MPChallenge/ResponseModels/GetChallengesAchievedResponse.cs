﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge.ResponseModels
{
    public class GetChallengesAchievedResponse
    {
        public IEnumerable<ChallengeDto> Challenges { get; set; }
    }
}
