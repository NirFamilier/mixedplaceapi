﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Base.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge.ResponseModels
{
    public class GetChallengeMetadataResponse : MetadataResponseBase
    {
        public IEnumerable<ChallengeActionTypeDto> ActionTypes { get; set; }
        public IEnumerable<ChallengeTypeDto> ChallengeTypes { get; set; }
        public IEnumerable<ChallengeStatusDto> ChallengeStatuses { get; set; }
        public IEnumerable<ChallengeCategoryDto> ChallengeCategories { get; set; }
    }
}
