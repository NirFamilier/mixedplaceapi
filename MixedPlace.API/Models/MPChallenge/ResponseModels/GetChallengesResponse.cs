﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge.ResponseModels
{
    public class GetChallengesResponse : FilterBaseResponse
    {
        public IEnumerable<ChallengeDto> Challenges{ get; set; }
    }
}
