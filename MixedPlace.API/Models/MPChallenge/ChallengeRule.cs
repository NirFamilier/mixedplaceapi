﻿using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge
{
    public class ChallengeRule
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public int Amount { get; set; }

        [ForeignKey("Condition")]
        public int? ConditionId { get; set; }
        public ConditionModel Condition { get; set; }

        [ForeignKey("ActionType")]
        public int ActionTypeId { get; set; }
        public ChallengeActionTypeModel ActionType { get; set; }

        [ForeignKey("Challenge")]
        public int ChallengeId { get; set; }
        public Challenge Challenge { get; set; }
    }
}
