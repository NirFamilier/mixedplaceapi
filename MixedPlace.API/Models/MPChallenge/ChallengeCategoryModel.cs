﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge
{
    public class ChallengeCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
