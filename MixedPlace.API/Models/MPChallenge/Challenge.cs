﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge
{
    public class Challenge
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal Reward { get; set; }
        public bool UnlimitedDuration { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }

        public int? LogicalOperator { get; set; }

        [ForeignKey("ChallengeType")]
        public int? ChallengeTypeId { get; set; }
        public ChallengeTypeModel ChallengeType { get; set; }

        [ForeignKey("ChallenegeStatus")]
        public int? ChallengeStatusId { get; set; }
        public ChallengeStatusModel ChallenegeStatus { get; set; }

        [ForeignKey("ChallengeCategory")]
        public int? ChallengeCategoryId { get; set; }
        public ChallengeCategoryModel ChallengeCategory { get; set; }

        public ICollection<ChallengeRule> Rules { get; set; } = new List<ChallengeRule>();

        public ICollection<AchievedChallengeModel> AchievedChallenges { get; set; }
    }
}
