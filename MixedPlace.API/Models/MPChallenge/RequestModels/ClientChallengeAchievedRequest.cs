﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge.RequestModels
{
    public class ClientChallengeAchievedRequest
    {
        [Range(1,int.MaxValue)]
        public int ChallengeId { get; set; }
    }
}
