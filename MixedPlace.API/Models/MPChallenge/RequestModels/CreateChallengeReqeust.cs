﻿using Microsoft.AspNetCore.Http;
using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge.RequestModels
{
    public class CreateChallengeReqeust : ChallengeDto
    {
        [Required]
        public string ImageBase64 { get; set; }
        //[Required]
        //public string ImageMimeType { get; set; }
    }
}
