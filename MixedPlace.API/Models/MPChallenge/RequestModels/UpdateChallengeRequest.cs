﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge.RequestModels
{
    public class UpdateChallengeRequest : ChallengeDto
    {

        public string ImageBase64 { get; set; }

        public string ImageMimeType { get; set; }
    }
}
