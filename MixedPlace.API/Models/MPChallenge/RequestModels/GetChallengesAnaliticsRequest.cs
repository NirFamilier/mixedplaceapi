﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge.RequestModels
{
    public class GetChallengesAnaliticsRequest
    {
        string UserId { get; set; }
        public int? Year {get;set;}
        public int Month { get; set;}
    }
}
