﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Transactions
{
    public class MoneyTotal
    {
        public decimal Money { get; set; }
        public string CurrencyCode { get; set; }
    }
}
