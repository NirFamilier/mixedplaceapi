﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPChallenge.ResponseModels
{
    public class GetTransactionsResponse : FilterBaseResponse
    {
        public IEnumerable<TransactionDto> Transactions{ get; set; }
    }
}
