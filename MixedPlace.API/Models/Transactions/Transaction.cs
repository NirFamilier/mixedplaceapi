﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Transactions
{
    public class Transaction
    {
        public int ID { get; set; }
        public decimal Coins { get; set; }
        public decimal Money { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        [ForeignKey("User")]
        public string OwnerId { get; set; }
        public User.User User { get; set; }
        [ForeignKey("TransactionCategory")]
        public int? TransactionCategoryId { get; set; }
        public TransactionCategory TransactionCategory { get; set; }


    }
}
