﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public class FilterItemRequest
    {
        public object Value { get; set; }
        public string ColumnId { get; set; }
        public ConditionsType Condition { get; set; }
    }
}
