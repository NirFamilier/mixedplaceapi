﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public class ErrorMapModel
    {
        [JsonProperty("httpCode")]
        public int HttpCode { get; set; }

        [JsonProperty("innerCode")]
        public int InnerCode { get; set; }
        
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
