﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public class ConditionValue<T>
    {
        public T Value { get; set; }

        public static implicit operator T(ConditionValue<T> value)
        {
            return value.Value;
        }

        public static implicit operator ConditionValue<T>(T value)
        {
            return new ConditionValue<T> { Value = value };
        }
    }
}
