﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public class FilterRequest
    {
        public int Page { get; set; }
        public int AmountPerPage { get; set; }
        public string Search { get; set; }
        public IEnumerable<FilterItemRequest> Filters { get; set; }
        public int TotalAmount { get; set; }
    }
}
