﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public static class UniqueTemplatesName
    {
        public const string SC_Email_ReportToAdminHtml = "Email_ReportToAdminAboutBadContentHTML";
        public const string SC_Email_ReportToAdminText = "Email_ReportToAdminAboutBadContentText";
        public const string SC_Email_UserDisabled = "Email_UserDisabled";
        public const string SC_Email_UserSuspended = "Email_UserSuspended";
        public const string SC_Email_UserActivated = "Email_UserActivated";
    }
}
