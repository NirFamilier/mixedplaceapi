﻿using Microsoft.EntityFrameworkCore;
using MixedPlace.API.Models.Package;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common.MixedPlaceExtensions
{
    public static class DbContextExtensionMethods
    {
        public static IEnumerable<PackageModel> LoadWithAllRelationships(this DbSet<PackageModel> list, Func<PackageModel, bool> predicate)
        {
            return list.Include(x => x.PackageStatus)
                       .Include(x => x.PackageCategory)
                       .Include(x=> x.PriceType)
                       .Include(x => x.PackageItems).ThenInclude(x => x.PackageItemType)
                       .Include(x => x.PackageItems).ThenInclude(x => x.Model).ThenInclude(x=> x.Bundle)
                       .Where(predicate)
                       .ToList();
        }

        public static IEnumerable<PackageModel> LoadWithAllRelationships(this DbSet<PackageModel> list)
        {
            return list.Include(x => x.PackageStatus)
                       .Include(x => x.PackageCategory)
                       .Include(x => x.PriceType)
                       .Include(x => x.PackageItems).ThenInclude(x => x.PackageItemType)
                       .Include(x => x.PackageItems).ThenInclude(x => x.Model).ThenInclude(x => x.Bundle)
                       .ToList();
        }

        public static IEnumerable<MPModel.MPModel> LoadWithAllRelationships(this DbSet<MPModel.MPModel> list)
        {
            return list.Include(x => x.ModelStatus)
                       .Include(x => x.Bundle)
                       .Include(x => x.MPModelCatSubCats).ThenInclude(y=> y.ModelCategory)
                       .Include(x => x.MPModelCatSubCats).ThenInclude(y=> y.ModelSubCategory)
                       .ToList();
        }

        public static IEnumerable<MPModel.MPModel> LoadWithAllRelationships(this DbSet<MPModel.MPModel> list, Func<MPModel.MPModel, bool> predicate)
        {
            return list.Include(x => x.ModelStatus)
                       .Include(x => x.Bundle)
                       .Include(x => x.MPModelCatSubCats).ThenInclude(y => y.ModelCategory)
                       .Include(x => x.MPModelCatSubCats).ThenInclude(y => y.ModelSubCategory)
                       .Where(predicate)
                       .ToList();
        }
    }

    public static class StringExtensionMethods
    {
        public static string ToCamelCase(this string value)
        {
            if (value.Length > 2)
                return value.Substring(0, 2).ToLower() + value.Substring(2);
            else
                return value.ToLower();
        }

        public static (string mimeType, string data) ParseFromBase64(this string value)
        {
            var _regex = new Regex(@"data:(?<mime>[\w/\-\.]+);(?<encoding>\w+),(?<data>.*)", RegexOptions.Compiled);
            var match = _regex.Match(value);

            return (mimeType: match.Groups["mime"].Value, data: match.Groups["data"].Value);
        }
    }
}
