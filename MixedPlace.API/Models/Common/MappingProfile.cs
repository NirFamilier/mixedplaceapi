﻿using AutoMapper;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.DataTransferObjects.StoreDtos;
using MixedPlace.API.Models;
using MixedPlace.API.Models.Coins;
using MixedPlace.API.Models.HistoryManagement;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.MPChallenge;
using MixedPlace.API.Models.MPChallenge.RequestModels;
using MixedPlace.API.Models.MPDesign;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.MPModel.ResponseModels;
using MixedPlace.API.Models.Package;
using MixedPlace.API.Models.Package.RequestModels;
using MixedPlace.API.Models.Transactions;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region  User Object Mapping

            CreateMap<UserBaseDto, User.User>()
                .ForMember(x => x.Image, opt => opt.Ignore());

            CreateMap<User.User, UserBaseDto>();

            CreateMap<RegisterRequest, User.User>()
                .ForMember(x => x.UserName,
                           opt => opt.MapFrom(s => s.Username))
                .ForMember(x => x.Email,
                           opt => opt.MapFrom(s => s.Username));

            CreateMap<UserDetailsDto, User.User>().
                     ForMember(dest => dest.Image,
                               opt => opt.Ignore()).
                     ForMember(dest => dest.UserStatusId,
                          opt => opt.MapFrom(x => x.UserStatus.Id));

            CreateMap<User.User, UserDetailsDto>().
                ForMember(dest => dest.UserId,
                          opt => opt.MapFrom(x => x.Id)).
                ForMember(dest => dest.ImageUrl,
                          opt => opt.MapFrom(x => x.Image)).
                ForMember(dest => dest.UserStatus,
                          opt => opt.ResolveUsing(src => src.UserStatus == null ?
                                                         new UserStatusDto() :
                                                         Mapper.Map<UserStatusModel, UserStatusDto>(src.UserStatus))).
                ForMember(dest => dest.MPCount,
                          opt => opt.ResolveUsing(src => src.MixedPlaces == null ?
                                                         0 :
                                                         src.MixedPlaces.Count())).
                ForMember(dest => dest.ReportsCount,
                          opt => opt.ResolveUsing(src => src.MixedPlaces == null ?
                                                        0 :
                                                        src.MixedPlaces.Sum(x => x?.MPReports?.Sum(y => y.Id) ?? 0)));

            CreateMap<CreateUserRequest, User.User>()
                .ForMember(dest => dest.Email,
                           opt => opt.MapFrom(x => x.UserName))
                .ForMember(dest => dest.UserStatus,
                           opt => opt.Ignore())
                     .ForMember(dest => dest.UserStatusId,
                          opt => opt.MapFrom(x => x.UserStatus.Id));

            CreateMap<UpdateUserDetailsRequest, User.User>()
                 .ForMember(dest => dest.Id,
                            opt => opt.MapFrom(src => src.UserId))
                 .ForMember(dest => dest.UserStatus,
                           opt => opt.Ignore())
                .ForMember(dest => dest.UserStatusId,
                             opt => opt.MapFrom(x => x.UserStatus.Id))
                .ForMember(x => x.Image,
                                opt => opt.MapFrom(u => u.ImageUrl));

            CreateMap<Transaction, TransactionDto>()
                .ForMember(dest => dest.TransactionType,
                           opt => opt.ResolveUsing(source => source.TransactionCategory == null ? "" : source.TransactionCategory.Name));

            #endregion User Objects Mappings

            #region Mixed Place Items Object Mapping

            CreateMap<MPItemDto, MPItemModel>();
            CreateMap<MPItemModel, MPItemDto>()
                .ForMember(dest => dest.MPCoordinate,
                             opt => opt.ResolveUsing(src => src.MixedPlace == null ?
                                                         new MPCoordinateDto() :
                                                         new MPCoordinateDto
                                                         {
                                                             X = src.MixedPlace.X,
                                                             Y = src.MixedPlace.Y,
                                                             Z = src.MixedPlace.Z
                                                         }));

            #endregion Mixed Place Items Objects Mappings

            #region Mixed Place Objects Mapping

            CreateMap<MPModel.MPModel, MPModelDto>().
                ForMember(dest => dest.Url,
                           opt => opt.ResolveUsing(src => src.Bundle == null ? String.Empty : src.Bundle.Url)).
                ForMember(dest => dest.ModelStatusId,
                         opt => opt.ResolveUsing(src => src.ModelStatus == null ? null : src.ModelStatusId)).
                ForMember(dest => dest.PriceTypeId,
                         opt => opt.ResolveUsing(src => src.PriceType == null ? null : src.PriceTypeId));

            CreateMap<MPModel.MPModel, GetModelResponse>().
                ForMember(dest => dest.Url,
                          opt => opt.ResolveUsing(src => src.Bundle == null ? String.Empty : src.Bundle.Url)).
                ForMember(dest => dest.Bundle,
                           opt => opt.ResolveUsing(src => src.Bundle == null ?
                                                              new DropdownDto() :
                                                              new DropdownDto { Id = src.Bundle.Id, Name = src.Bundle.Name })).
                ForMember(dest => dest.PriceType,
                         opt => opt.ResolveUsing(src => src.PriceType == null ?
                                                        new PriceTypeDto() :
                                                        new PriceTypeDto { Id = src.PriceType.Id, Name = src.PriceType.Name })).
                 ForMember(dest => dest.ModelCategories,
                            opt => opt.ResolveUsing(src => Mapper.Map<IEnumerable<MPModelCatSubCat>, IEnumerable<ModelCatSubCatDto>>
                                                          (src.MPModelCatSubCats)));

            CreateMap<MPModelDto, MPModel.MPModel>();
            CreateMap<ModelStatusModel, ModelStatusDto>();

            CreateMap<BundleModel, BundleModelDto>();

            CreateMap<MP, MPDetailsDto>().
                 ForMember(dest => dest.Demand,
                           opt => opt.MapFrom(x => x.HiddenCounter)).
                 ForMember(dest => dest.OwnerName,
                           opt => opt.ResolveUsing(src => src.User == null ?
                                                         "" :
                                                         src.User.UserName)).
                 ForMember(dest => dest.Price,
                           opt => opt.ResolveUsing(src => src.Price == null ?
                                                   0 :
                                                   src.Price.Price)).
                 ForMember(dest => dest.MPStatus,
                           opt => opt.ResolveUsing(src => src.MPStatus == null ?
                                                         new MPStatusDto() :
                                                         Mapper.Map<MPStatusModel, MPStatusDto>(src.MPStatus))).
                 ForMember(dest => dest.DesignStatus,
                          opt => opt.ResolveUsing(src => src.DesignStatus == null ?
                                 new DesignStatusDto() :
                                 Mapper.Map<DesignStatusModel, DesignStatusDto>(src.DesignStatus))).
                 ForMember(dest => dest.SharesCount,
                           opt => opt.ResolveUsing(src => src.MPShares == null ?
                                                         0 :
                                                         src.MPShares.Count())).
                 ForMember(dest => dest.LikesCount,
                           opt => opt.ResolveUsing(src => src.MPLike == null ?
                                                         0 :
                                                         src.MPLike.Count())).
                 ForMember(dest => dest.CommentsCount,
                           opt => opt.ResolveUsing(src => src.MPComments == null ?
                                                         0 :
                                                         src.MPComments.Count())).
                 ForMember(dest => dest.VisitsCount,
                           opt => opt.ResolveUsing(src => src.MPVisits == null ?
                                                         0 :
                                                         src.MPVisits.Sum(x => x.PublicCounter)));

           CreateMap<BundleModelCatSubCat, ModelCatSubCatDto>()
                 .ForMember(dest => dest.Category,
                            opt => opt.ResolveUsing(src => src.ModelCategory == null ?
                                                           new ModelCategoryDto() :
                                                           Mapper.Map<ModelCategory,ModelCategoryDto>(src.ModelCategory)))
                 .ForMember(dest => dest.SubCategory,
                            opt => opt.ResolveUsing(src => src.ModelSubCategory == null ?
                                                           new ModelSubCategoryDto() :
                                                           Mapper.Map<ModelSubCategory,ModelSubCategoryDto>(src.ModelSubCategory)));

            CreateMap<MPModelCatSubCat, ModelCatSubCatDto>()
               .ForMember(dest => dest.Category,
                          opt => opt.ResolveUsing(src => src.ModelCategory == null ?
                                                         new ModelCategoryDto() :
                                                         Mapper.Map<ModelCategory, ModelCategoryDto>(src.ModelCategory)))
               .ForMember(dest => dest.SubCategory,
                          opt => opt.ResolveUsing(src => src.ModelSubCategory == null ?
                                                         new ModelSubCategoryDto() :
                                                         Mapper.Map<ModelSubCategory, ModelSubCategoryDto>(src.ModelSubCategory)));

            CreateMap<MPDetailsDto, MP>();


            CreateMap<MPComment, CommentDto>()
                .ForMember(x => x.Username,
                            opt => opt.MapFrom(s => s.User.UserName))
                .ForMember(x => x.Comment,
                            opt => opt.MapFrom(s => s.CommentText));

            CreateMap<ModelCategory, ModelCategoryDto>().
               ForMember(dest => dest.Id,
                         opt => opt.MapFrom(s => s.Id)).
              ForMember(dest => dest.Name,
                         opt => opt.MapFrom(s => s.Name));

            CreateMap<ModelCategoryDto, ModelCategory>().
            ForMember(dest => dest.Id,
                      opt => opt.MapFrom(s => s.Id)).
           ForMember(dest => dest.Name,
                      opt => opt.MapFrom(s => s.Name));

            CreateMap<ModelSubCategoryDto, ModelSubCategory>().
          ForMember(dest => dest.Id,
                    opt => opt.MapFrom(s => s.Id)).
         ForMember(dest => dest.Name,
                    opt => opt.MapFrom(s => s.Name));

            CreateMap<BundleModelCatSubCat, MPModelCatSubCat>().
             ForMember(dest => dest.ModelCategory,
                       opt => opt.MapFrom(s => s.ModelCategory)).
            ForMember(dest => dest.ModelCategoryId,
                       opt => opt.MapFrom(s => s.ModelCategoryId)).
             ForMember(dest => dest.ModelSubCategory,
                       opt => opt.MapFrom(s => s.ModelSubCategory)).
            ForMember(dest => dest.ModelSubCategoryId,
                       opt => opt.MapFrom(s => s.ModelSubCategoryId));
            #endregion Mixed Place Objects Mapping

            #region Challenges Objects Mapping

            CreateMap<ChallengeActionTypeModel, ChallengeActionTypeDto>();
            CreateMap<ChallengeActionTypeDto, ChallengeActionTypeModel>();
            CreateMap<ChallengeCategoryModel, ChallengeCategoryDto>();

            CreateMap<Challenge, ChallengeDto>()
                     .ForMember(dest => dest.ChallengeStatus,
                                opt => opt.ResolveUsing(src => src.ChallenegeStatus == null ?
                                                               new ChallengeStatusDto() :
                                                               Mapper.Map<ChallengeStatusModel, ChallengeStatusDto>(src.ChallenegeStatus)))
                     .ForMember(dest => dest.ChallengeType,
                                opt => opt.ResolveUsing(src => src.ChallengeType == null ?
                                                               new ChallengeTypeDto() :
                                                               Mapper.Map<ChallengeTypeModel, ChallengeTypeDto>(src.ChallengeType)))
                    .ForMember(dest => dest.ChallengeCategory,
                                opt => opt.ResolveUsing(src => src.ChallengeCategory == null ?
                                                               new ChallengeCategoryDto() :
                                                               Mapper.Map<ChallengeCategoryModel, ChallengeCategoryDto>(src.ChallengeCategory)));

            CreateMap<ChallengeDto, Challenge>();


            CreateMap<ChallengeRule, ChallengeRuleDto>()
                 .ForMember(dest => dest.ActionType,
                          opt => opt.ResolveUsing(src => src.ActionType == null ?
                                                         new ChallengeActionTypeDto() :
                                                         Mapper.Map<ChallengeActionTypeModel, ChallengeActionTypeDto>(src.ActionType)));

            CreateMap<CreateChallengeReqeust, Challenge>()
                .ForMember(dest => dest.Id,
                           opt => opt.Ignore())
                .ForMember(dest => dest.ChallengeCategory,
                           opt => opt.Ignore())
                .ForMember(dest => dest.ChallenegeStatus,
                           opt => opt.Ignore())
                .ForMember(dest => dest.ChallengeType,
                           opt => opt.Ignore())
                .ForMember(dest => dest.Rules,
                           opt => opt.Ignore())
                .ForMember(dest => dest.ChallengeTypeId,
                           opt => opt.MapFrom(x => x.ChallengeType.Id))
                .ForMember(dest => dest.ChallengeStatusId,
                           opt => opt.MapFrom(x => x.ChallengeStatus.Id));

            CreateMap<UpdateChallengeRequest, Challenge>()
                .ForMember(dest => dest.ChallenegeStatus,
                           opt => opt.Ignore())
                .ForMember(dest => dest.ChallengeCategory,
                           opt => opt.Ignore())
                .ForMember(dest => dest.ChallengeType,
                           opt => opt.Ignore())
                .ForMember(dest => dest.Rules,
                           opt => opt.Ignore())
                .ForMember(dest => dest.ChallengeTypeId,
                           opt => opt.MapFrom(x => x.ChallengeType.Id))
                .ForMember(dest => dest.ChallengeStatusId,
                           opt => opt.MapFrom(x => x.ChallengeStatus.Id));



            CreateMap<ChallengeRuleDto, ChallengeRule>().
                ForMember(dest => dest.Id,
                          opt => opt.Ignore()).
                ForMember(dest => dest.ActionType,
                          opt => opt.Ignore()).
                ForMember(dest => dest.Condition,
                          opt => opt.Ignore()).
                ForMember(dest => dest.ActionTypeId,
                          opt => opt.MapFrom(x => x.ActionType.Id)).
                ForMember(dest => dest.ConditionId,
                          opt => opt.MapFrom(x => x.Condition.Id));

            CreateMap<ChallengeRule, ChallengeRuleDto>().
               ForMember(dest => dest.Condition,
                         opt => opt.ResolveUsing(src => src.Condition == null ?
                                new ConditionDto() :
                                Mapper.Map<ConditionModel, ConditionDto>(src.Condition)));

            CreateMap<ChallengeTypeModel, ChallengeTypeDto>();
            CreateMap<ChallengeTypeDto, ChallengeTypeModel>();

            CreateMap<ChallengeStatusModel, ChallengeStatusDto>();
            CreateMap<ChallengeStatusDto, ChallengeStatusModel>();

            #endregion Challenges Objects Mapping

            #region Store Objects Mapping

            CreateMap<PriceTypeDto, PriceTypeModel>();
            CreateMap<PriceTypeModel, PriceTypeDto>();

            CreateMap<PackageStatusDto, PackageStatusModel>();
            CreateMap<PackageStatusModel, PackageStatusDto>();

            CreateMap<PackageCategoryDto, PackageCategoryModel>();
            CreateMap<PackageCategoryModel, PackageCategoryDto>();

            CreateMap<PackageItemTypeModel, PackageItemTypeDto>();
            CreateMap<PackageItemTypeDto, PackageItemTypeModel>();

            CreateMap<PackageItemModel, PackageItemDto>().
                ForMember(dest => dest.Model,
                          opt  => opt.Ignore()).
                ForMember(dest => dest.PackageItemType,
                          opt  => opt.ResolveUsing(src => Mapper.Map<PackageItemTypeModel,PackageItemTypeDto>(src.PackageItemType))).
                ForMember(dest => dest.Model,
                          opt  => opt.ResolveUsing(src => src.Model?.Bundle == null ? 
                                                          new PackageModelItemDto() :
                                                          new PackageModelItemDto
                                                          {
                                                               Id = src.Model.Id,
                                                               Name = src.Model.Name,
                                                               ImageUrl = src.Model.Bundle.Url
                                                          })).
                ForMember(dest => dest.NumberOfUnits,
                          opt  => opt.MapFrom(src => src.NumOfUnits));

            CreateMap<PackageItemDto, PackageItemModel>().
                ForMember(dest => dest.Id,
                          opt => opt.Ignore()).
                ForMember(dest => dest.PackageItemType,
                          opt => opt.Ignore()).
                ForMember(dest => dest.PackageItemTypeId,
                          opt => opt.MapFrom(src => src.PackageItemType.Id)).
                ForMember(dest => dest.ModelId,
                          opt => opt.ResolveUsing(src=> src.Model == null ? 0: 1));


            CreateMap<PackageDto, PackageModel>();
            CreateMap<PackageModel, PackageDto>().
                ForMember(dest => dest.PriceType,
                          opt => opt.ResolveUsing(src => src.PriceType == null ?
                                                        new PriceTypeDto() :
                                                        Mapper.Map<PriceTypeModel, PriceTypeDto>(src.PriceType))).
                ForMember(dest => dest.PackageStatus,
                          opt => opt.ResolveUsing(src => src.PackageStatus == null ?
                                              new PackageStatusDto() :
                                              Mapper.Map<PackageStatusModel, PackageStatusDto>(src.PackageStatus))).
                ForMember(dest => dest.PackageCategory,
                          opt => opt.ResolveUsing(src => src.PackageCategory == null ?
                                              new PackageCategoryDto() :
                                              Mapper.Map<PackageCategoryModel, PackageCategoryDto>(src.PackageCategory))).
                ForMember(dest => dest.PackageItems,
                          opt => opt.ResolveUsing(src => src.PackageItems == null ?
                                                        null :
                                                        Mapper.Map<IEnumerable<PackageItemModel>, IEnumerable<PackageItemDto>>(src.PackageItems)));


            CreateMap<CreatePackageRequest, PackageModel>().
                ForMember(dest => dest.Id,
                   opt => opt.Ignore()).
                ForMember(dest => dest.PackageCategory,
                          opt => opt.Ignore()).
                ForMember(dest => dest.PackageStatus,
                          opt => opt.Ignore()).
                ForMember(dest => dest.PackageItems,
                          opt => opt.Ignore()).
                ForMember(dest => dest.PriceType,
                          opt => opt.Ignore()).
                ForMember(dest => dest.PackageCategoryId,
                   opt => opt.MapFrom(x => x.PackageCategory.Id)).
                ForMember(dest => dest.PackageStatusId,
                   opt => opt.MapFrom(x => x.PackageStatus.Id)).
                ForMember(dest => dest.PriceTypeId,
                          opt => opt.MapFrom(src => src.PriceType.Id));
               

            CreateMap<UpdatePackageRequest, PackageModel>().
                ForMember(dest => dest.Id,
                          opt => opt.Ignore()).
                ForMember(dest => dest.PackageCategory,
                          opt => opt.Ignore()).
                ForMember(dest => dest.PackageStatus,
                          opt => opt.Ignore()).
                ForMember(dest => dest.PriceType,
                          opt => opt.Ignore()).
                ForMember(dest => dest.PackageCategoryId,
                          opt => opt.MapFrom(x => x.PackageCategory.Id)).
                ForMember(dest => dest.PackageStatusId,
                          opt => opt.MapFrom(x => x.PackageStatus.Id)).
                ForMember(dest => dest.PriceTypeId,
                          opt => opt.MapFrom(src => src.PriceType.Id)).
                ForMember(dest => dest.PackageItems,
                          opt => opt.Ignore());


            CreateMap<PackageModel, ClientStorePackageDto>().
                  ForMember(dest => dest.PackageItems,
                            opt => opt.ResolveUsing(src => src.PackageItems == null ?
                                                          new List<PackageItemDto>() :
                                                          Mapper.Map<IEnumerable<PackageItemModel>, IEnumerable<PackageItemDto>>(src.PackageItems))).
                  ForMember(dest => dest.PriceType,
                           opt => opt.MapFrom(src => (PriceType)src.PriceTypeId)).
                  ForMember(dest => dest.PackageType,
                            opt => opt.MapFrom(src => (PackageCategoryType)src.PackageCategoryId));

            CreateMap<PackagePurchaseModel, PackageDto>();
            #endregion Store Objects Mapping

            #region Coins
            CreateMap<CoinsRuleConditionDto, CoinsRuleConditionModel>().
                ForMember(dest => dest.FieldName, opt => opt.MapFrom(x => x.FieldName.Id)).
                ForMember(dest => dest.Value, opt => opt.MapFrom(x => x.Value.ToString())).
                 ForMember(dest => dest.ConditionId, opt => opt.MapFrom(x => x.Operator.Id));

            CreateMap<CoinsRuleModel, CoinRuleDto>().
                ForMember(dest => dest.Condition,
                          opt => opt.Ignore()).
                ForMember(dest=> dest.NumberOfUsersAffected,
                          opt=> opt.MapFrom(src=> src.NumberOfAffectedUsers)).
                ForMember(dest => dest.CoinRuleType,
                          opt => opt.ResolveUsing(src => src.CoinsRuleType == null ?
                                                        new CoinRuleTypeDto() :
                                                        new CoinRuleTypeDto { Id = src.CoinsRuleType.Id, Name = src.CoinsRuleType.Name }));

            #endregion

            #region History Management

            CreateMap<MP, MPHistoryModel>()
                .ForMember(dest => dest.Id,
                           opt => opt.Ignore())
                .ForMember(dest => dest.MPId,
                           opt => opt.MapFrom(x => x.Id))
                .ForMember(dest => dest.MPItemsHistory,
                           opt => opt.MapFrom(x => x.MPDesigns))
                .ForMember(dest => dest.MPCommentsHistory,
                           opt => opt.MapFrom(x => x.MPComments))
                .ForMember(dest => dest.MPLikesHistory,
                           opt => opt.MapFrom(x => x.MPLike))
                .ForMember(dest => dest.MPSharesHistory,
                           opt => opt.MapFrom(x => x.MPShares))
                .ForMember(dest => dest.MPVisitsHistory,
                           opt => opt.MapFrom(x => x.MPVisits))
                .ForMember(dest => dest.AchievedChallengesHistory,
                           opt => opt.MapFrom(x => x.AchievedChallenges));

            CreateMap<MPCatSubCat, MPHistoryModelCatSubCat>()
               .ForMember(dest => dest.MPCategory,
                         opt => opt.MapFrom(x => x.MPCategory))
                .ForMember(dest => dest.MPCategoryId,
                         opt => opt.MapFrom(x => x.MPCategoryId))
                .ForMember(dest => dest.MPSubCategoryId,
                         opt => opt.MapFrom(x => x.MPSubCategoryId))
               .ForMember(dest => dest.MPSubCategory,
                         opt => opt.MapFrom(x => x.MPSubCategory));

            CreateMap<MPHistoryModelCatSubCat, MPCatSubCat>()
             .ForMember(dest => dest.MPCategory,
                       opt => opt.MapFrom(x => x.MPCategory))
              .ForMember(dest => dest.MPCategoryId,
                       opt => opt.MapFrom(x => x.MPCategoryId))
              .ForMember(dest => dest.MPSubCategoryId,
                       opt => opt.MapFrom(x => x.MPSubCategoryId))
             .ForMember(dest => dest.MPSubCategory,
                       opt => opt.MapFrom(x => x.MPSubCategory));

            CreateMap<MPItemModel, MPItemHistoryModel>()
                .ForMember(dest => dest.Id,
                           opt => opt.Ignore());

            CreateMap<AchievedChallengeModel, AchivedChallengeHistoryModel>()
                .ForMember(dest => dest.Id,
                           opt => opt.Ignore());

            CreateMap<MPComment, MPCommentHistoryModel>()
                .ForMember(dest => dest.Id,
                           opt => opt.Ignore());

            CreateMap<MPLike, MPLikeHistoryModel>()
                .ForMember(dest => dest.Id,
                           opt => opt.Ignore());

            CreateMap<MPShare, MPShareHistoryModel>()
                .ForMember(dest => dest.Id,
                           opt => opt.Ignore());

            CreateMap<MPVisit, MPVisitHistoryModel>()
                .ForMember(dest => dest.Id,
                           opt => opt.Ignore());

            #endregion History Management

            #region Common Object Mapping
            CreateMap<ConditionDto, ConditionModel>();
            CreateMap<ConditionModel, ConditionDto>();
            #endregion Common Object Mapping

        }
    }
}
