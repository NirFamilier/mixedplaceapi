﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public class ErrorMapper
    {
        private static Dictionary<int, ErrorMapModel> _instance;
        private static readonly string errorsMapping = "[{\"innerCode\":2000,\"httpCode\":200,\"message\":\"SUCCESS\"}," +
            "{\"innerCode\":2001,\"httpCode\":200,\"message\":\"Passowrd exipired\"}," +
            "{\"innerCode\":2002,\"httpCode\":200,\"message\":\"Change password required\"}," +
            "{\"innerCode\":2003,\"httpCode\":200,\"message\":\"Invalid credentials\"}," +
            "{\"innerCode\":2004,\"httpCode\":200,\"message\":\"Username already in use\"}," +
            "{\"innerCode\":2005,\"httpCode\":200,\"message\":\"Mixed place already owned\"}," +
            "{\"innerCode\":2006,\"httpCode\":200,\"message\":\"Mixed place not owned\"}," +
            "{\"innerCode\":2007,\"httpCode\":200,\"message\":\"Mixed place already owned by other user\"}," +
            "{\"innerCode\":2008,\"httpCode\":200,\"message\":\"Mixed place already was visited by you\"}," +
            "{\"innerCode\":2009,\"httpCode\":200,\"message\":\"Mixed place not exists or is not owned\"}," +
            "{\"innerCode\":2010,\"httpCode\":200,\"message\":\"Mixed place Category alredy exists\"}," +
            "{\"innerCode\":2011,\"httpCode\":200,\"message\":\"Model Category alredy exists\"}," +
            "{\"innerCode\":2012,\"httpCode\":200,\"message\":\"Model Category not exists\"}," +
            "{\"innerCode\":2013,\"httpCode\":200,\"message\":\"Mixed Place Category not exists\"}," +
            "{\"innerCode\":2014,\"httpCode\":200,\"message\":\"Model SubCategory alredy exists\"}," +
            "{\"innerCode\":2018,\"httpCode\":200,\"message\":\"Bundle not exists\"}," +
            "{\"innerCode\":2015,\"httpCode\":200,\"message\":\"Mixed Place SubCategory alredy exists\"}," +
            "{\"innerCode\":2019,\"httpCode\":200,\"message\":\"Model alredy exists\"}," +
            "{\"innerCode\":2020,\"httpCode\":200,\"message\":\"Model exists in active not single package\"}," +
            "{\"innerCode\":2021,\"httpCode\":200,\"message\":\"User Not Found\"}," +
             "{\"innerCode\":2022,\"httpCode\":200,\"message\":\"Package in status Submitted for user not exists\"}," +
             "{\"innerCode\":20022,\"httpCode\":200,\"message\":\"No package were added to shopping cart\"}," +
            "{\"innerCode\":20023,\"httpCode\":200,\"message\":\"More than one like for mixed place prohibited \"}," +
            "{\"innerCode\":20016,\"httpCode\":200,\"message\":\"Your account is suspended.Please contact support.\"}," +
            "{\"innerCode\":20017,\"httpCode\":200,\"message\":\"Your account is disabled.Please contact support\"}," +
            "{\"innerCode\":20018,\"httpCode\":200,\"message\":\"User has no enought coins for purchase.\"}," +
            "{\"innerCode\":4001,\"httpCode\":400,\"message\":\"Bad request arguments\"}," +
            "{\"innerCode\":4003,\"httpCode\":400,\"message\":\"User not has the model\"}," +
            "{\"innerCode\":4011,\"httpCode\":401,\"message\":\"Unauthorized\"}," +
            "{\"innerCode\":9999,\"httpCode\":500,\"message\":\"UNKNOWN_ERROR\"}," +
            "{\"innerCode\":9998,\"httpCode\":500,\"message\":\"MISSING_MAPPING\"}," +
            "{\"innerCode\":9997,\"httpCode\":500,\"message\":\"OBJECT_NOT_VALID\"}," +
            "{\"innerCode\":8000,\"httpCode\":404,\"message\":\"NOT_VALID\"}]";

        public static void Initialize()
        {
            if (_instance == null)
            {
                var errorResponseModel = JsonConvert.DeserializeObject<List<ErrorMapModel>>(errorsMapping);
                _instance = errorResponseModel.ToDictionary(e => e.InnerCode, e => e);
            }
        }

        public static ErrorMapModel GetErrorModelByErrorCode(int errorCode)
        {
            if (_instance == null)
                Initialize();
            return _instance.ContainsKey(errorCode) ? _instance.FirstOrDefault(e => e.Key == errorCode).Value : null;
        }
    }
}
