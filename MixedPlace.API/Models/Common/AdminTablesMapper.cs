﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common.MixedPlaceExtensions;
using MixedPlace.API.Models.Package;
using MixedPlace.API.Models.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public class AdminTablesMapper
    {
        //Column Mappings
        public Dictionary<string, string> UsersTableMap { get; private set; }
        public Dictionary<string, string> MixedPlaceTableMap { get; set; }
        public Dictionary<string, string> ChallengesTableMap { get; private set; }
        public Dictionary<string, string> ModelsTableMap { get; private set; }
        public Dictionary<string, string> AnalyticsTableMap { get; set; }
        public Dictionary<string, string> PackagesTableMap { get; set; }
        public Dictionary<string, string> CoinsTableMap { get; set; }
        public Dictionary<string,string> StoreTableMap { get; set; }
        public Dictionary<string, string> TransactionsTableMap { get; set; }

        //Table Condions 
        public Dictionary<string, List<int>> UsersTableFiltesConditionsMap { get; set; }
        public Dictionary<string, List<int>> PackagesTableFiltersConditionsMap { get; set; }
        public Dictionary<string, List<int>> MixedPlacesTableFiltersConditionsMap { get; set; }
        public Dictionary<string, List<int>> ChallengesTableFiltersConditionsMap { get; set; }
        public Dictionary<string, List<int>> ModelsTableFiltersConditionsMap { get; set; }
        public Dictionary<string, List<int>> CoinsRulesTableFiltersConditionsMap { get; set; }
        public Dictionary<string, List<int>> AnalyticsTableFiltersConditionsMap { get; set; }

        public AdminTablesMapper()
        {

            UsersTableMap = InitializeTableColumnsMap<UserDetailsDto>();
            PackagesTableMap = InitializeTableColumnsMap<PackageDto>();
            MixedPlaceTableMap = InitializeTableColumnsMap<MPDetailsDto>();
            ChallengesTableMap = InitializeTableColumnsMap<ChallengeDto>();
            ModelsTableMap = InitializeTableColumnsMap<MPModelDto>();
            CoinsTableMap= InitializeTableColumnsMap<CoinRuleDto>();

            UsersTableFiltesConditionsMap = InitializeTabelConditionsMap<UserDetailsDto>();
            PackagesTableFiltersConditionsMap = InitializeTabelConditionsMap<PackageDto>();
            MixedPlacesTableFiltersConditionsMap = InitializeTabelConditionsMap<MPDetailsDto>();
            ChallengesTableFiltersConditionsMap = InitializeTabelConditionsMap<ChallengeDto>();
            ModelsTableFiltersConditionsMap = InitializeTabelConditionsMap<MPModelDto>();
            CoinsRulesTableFiltersConditionsMap = InitializeTabelConditionsMap<CoinRuleDto>();

            InitializeTransactionsTableMap();
        }


        private Dictionary<string, string> InitializeTableColumnsMap<T>()
        {
            var dictionary = new Dictionary<string, string>();
            var propertyInfos = typeof(T).GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                if (propertyInfo.PropertyType.BaseType == typeof(DropdownDto))
                    dictionary.Add(propertyInfo.Name.ToCamelCase(), $"{propertyInfo.Name}.Id");
                else
                    dictionary.Add(propertyInfo.Name.ToCamelCase(), propertyInfo.Name);

            }
            return dictionary;
        }

        private Dictionary<string,List<int>> InitializeTabelConditionsMap<T>()
        {
            var filtersMap = new Dictionary<string, List<int>>();
            var propertyInfos = typeof(T).GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
              filtersMap.Add(propertyInfo.Name.ToCamelCase(),CreateConditions(propertyInfo.PropertyType));
            }
            return filtersMap;
        }

  

        #region Common Methods

        private List<int> CreateConditions(Type propertyType)
        {
            if (IsNumeric(propertyType))
                return NumericConditions();
            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                return  DatetimeConditions();
            if (propertyType == typeof(bool) || propertyType == typeof(bool?))
                return  BooleanConditions();
            if (propertyType == typeof(string))
                return  StringConditions();
            if (propertyType.BaseType == typeof(DropdownDto))
                return  DropdownConditions();
            else
                return null;
        }

        private List<int> NumericConditions()
        {
            return new List<int>
                {
                     (int)ConditionsType.EqualTo,
                     (int)ConditionsType.GreaterThan,
                     (int)ConditionsType.GreaterThanOrEqualTo,
                     (int)ConditionsType.LessThan,
                     (int)ConditionsType.LessThanOrEqualTo,
                     (int)ConditionsType.NotEqualTo
                };

        }

        private List<int> BooleanConditions()
        {
            return new List<int>
                {
                     (int)ConditionsType.EqualTo,
                     (int)ConditionsType.NotEqualTo
                };
        }

    private void InitializeTransactionsTableMap()
    {
        var u = new Transaction();
        TransactionsTableMap = new Dictionary<string, string>();
        TransactionsTableMap.Add($"{nameof(u.OwnerId) }", $"{nameof(u.OwnerId) }");
        TransactionsTableMap.Add($"{nameof(u.CurrencyCode) }", $"{nameof(u.CurrencyCode) }");
        TransactionsTableMap.Add($"{nameof(u.CreationDate) }", $"{nameof(u.CreationDate) }");
        TransactionsTableMap.Add($"{nameof(u.TransactionCategoryId) }", $"{nameof(u.TransactionCategoryId) }");

    }

        private List<int> DropdownConditions()
        {
            return new List<int>
                {
                     (int)ConditionsType.EqualTo,
                     (int)ConditionsType.GreaterThan,
                     (int)ConditionsType.GreaterThanOrEqualTo,
                     (int)ConditionsType.LessThan,
                     (int)ConditionsType.LessThanOrEqualTo,
                     (int)ConditionsType.NotEqualTo
                };
        }

        private List<int> DatetimeConditions()
        {
            return new List<int>
                {
                     (int)ConditionsType.EqualTo,
                     (int)ConditionsType.GreaterThan,
                     (int)ConditionsType.GreaterThanOrEqualTo,
                     (int)ConditionsType.LessThan,
                     (int)ConditionsType.LessThanOrEqualTo,
                     (int)ConditionsType.NotEqualTo
                };
        }

        private List<int> StringConditions()
        {
            return new List<int>
                {
                     (int)ConditionsType.Contains,
                     (int)ConditionsType.EndsWith,
                     (int)ConditionsType.StartsWith,
                };
        }

        private static bool IsNumeric(Type propertyType)
        {
            return (propertyType == typeof(Int16) ||
                    propertyType == typeof(Int32) ||
                    propertyType == typeof(Int64) ||
                    propertyType == typeof(decimal) ||
                    propertyType == typeof(double) ||
                    propertyType == typeof(float)||
                    propertyType == typeof(Int16?) ||
                    propertyType == typeof(Int32?) ||
                    propertyType == typeof(Int64?) ||
                    propertyType == typeof(decimal?) ||
                    propertyType == typeof(double?) ||
                    propertyType == typeof(float?));
        }

        #endregion Common Methods
    }
}
