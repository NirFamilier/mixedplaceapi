﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public enum InnerErrorCode
    {
        Ok = 0,
        PasswordExipired = 2001,
        ChangePassowrdRequired = 2002,
        InvalidCredentials = 2003,
        UsernameInUse = 2004,
        MixedPlaceAlreadyOwned = 2005,
        MixedPlaceNotOwned = 2006,
        MixedPlaceOwnedByOtherUser = 2007,
        MixedPlaceAlreadyVisited = 2008,
        MixedPlaceNotExistsOrNotQwned = 2009,
        MPCategoryExists = 2010,
        ModelCategoryExists = 2011,
        ModelCategoryNotExists = 2012,
        MixedPlaceCategoryNotExists = 2013,
        ModelSubCategoryExists = 2014,
        MixedPlaceSubCategoryExists = 2015,
        BundleNotExist=2018,
        UserAccountSuspended = 20016,
        UserAccountDisabled = 20017,
        ModelExists=2019,
        ModelExistsInNotSinglePackage=2020,
        UserNotFound=2021,
        PackageInSubmittedStatusNotExist=2022,
        MPAddressMotFound=2023,
        NoPackagesInSoppingCart = 20022,
        OnlyOneLikeADayApproved = 20023,
        InsufficientBalance = 20018,
        BadRequestArgs = 4001,
        UserNotHasTheModel = 4003,
        UnauthorizedRequest = 4011,
        NotValid = 8000,
        ObjectNotValid = 9997,
        MissingMapping = 9998,
        UnknownError = 9999
    }

    public enum MixedPlaceItemType
    {
        All = 1,
        Paint = 2,
        Object = 3,
        TextBanner = 4,
        TextureBanner = 5
    }

    public enum ActionType
    {
        Visit = 1,
        Like = 2,
        Comment = 3,
        Share = 4
    }

    public enum ChallengeType
    {
        Client = 1,
        ServerUser = 2,
        ServerMixedPlace = 3
    }

    public enum ChallengeStatus
    {
        Scheduled = 1,
        Ongoing = 2,
        Complete = 3
    }

    public enum ConditionsType
    {
        EqualTo = 1,
        Contains = 2,
        StartsWith = 3,
        EndsWith = 4,
        NotEqualTo = 5,
        GreaterThan = 6,
        GreaterThanOrEqualTo = 7,
        LessThan = 8,
        LessThanOrEqualTo = 9
    }

    public enum LogicalOperatorType
    {
        OR = 1,
        AND = 2
    }

    public enum UserStatus
    {
        Registered = 1,
        Active = 2,
        Suspended = 3,
        Disabled = 4
    }

    public enum MixedPlaceStatus
    {
        Available = 1,
        Purchased = 2,
        NotAvailable = 3

    }

    public enum DesignStatus
    {
        Empty = 1,
        Available = 2,
        Suspended = 3,
        Deleted = 4
    }

    public enum ModelStatus
    {
        Default = 1,
        Active = 2,
        Suspended = 3,
    }

    public enum PackageStatus
    {
        Active = 1,
        Suspended = 2,
        Disabled = 3
    }

    public enum PackageItemType
    {
        Coin = 1,
        Model = 2
    }

    public enum PackageCategoryType
    {
        Default = 1,
        Coin = 2,
        Model = 3,
        Combo = 4,
        SingleModel = 5
    }

    public enum PriceType
    {
        Coins = 1,
        Currency = 2
    }

    public enum NotificationMethod
    {
        SMS = 1,
        Email = 2,
        PushNotification = 3,
        PriorityPushNotification = 4
    }

    public enum NotificationOriginator
    {
        UserAdminManagement = 1,
        MixedPlaceAdminManagement = 2,
        ChallengesAdminManagement = 3,
        ContentReport = 4,
        UserStatusChange = 5
    }

    public enum NotificationType
    {
        PreconfiguredText = 1,
        SemiPreconfiguredText = 2,
        FreeText = 3

    }

    public enum NotificationStatus
    {
        Send = 1,
        NotSend = 2,
        Failed = 3,
        Decliended = 4,
        Queued = 5
    }

    public enum ReportStatus
    {
        SubmitedForApprove = 1,
        ApprovedByAdmin = 2,
        RefusedByAdmin = 3
    }

    public enum TransactionCategoryEnum
    {
        ChallengeReward = 1,//
        BuyPackage = 2,
        BuyPackageReward = 3,
        BuyMixedPlace = 4,//
        AdminReward = 5,
        AdminFine = 6,
        MPOwnerPriceReceived = 7,
        DefaultPackageReward = 8
    }

    public enum PurchaseStatus
    {
        Submitted = 1,
        PaidUp = 2,
        Decliend = 3,
        Canceled = 4
    }
    public enum CoinRuleType
    {
        Reward=1,
        Fine=2
    }

    public enum ChallengeCategory
    {
        Challenge = 1,
        Quest = 2
    }
}
