﻿using MixedPlace.API.Models.Map;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPDesign
{
    public class MPItemModel
    {
        public int Id { get; set; }

        public DateTime? CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int ItemType { get; set; }
        public string Json { get; set; }

        [ForeignKey("MixedPlace")]
        public int MPId { get; set; }
        public MP MixedPlace{ get; set; }

    }
}
