﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPDesign.ResponseModel
{
    public class GetMPItemsResponse
    {
        public List<MPItemDto> MPItems { get; set; } = new List<MPItemDto>();
    }
}
