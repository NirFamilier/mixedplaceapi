﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MixedPlace.API.DataTransferObjects;
using Newtonsoft.Json;

namespace MixedPlace.API.Models.MPDesign.ResponseModel
{
    public class CommentResponseModel
    {
        [JsonProperty("List")]
        public List<CommentDto> List { get; set; } = new List<CommentDto>();
    }
}
