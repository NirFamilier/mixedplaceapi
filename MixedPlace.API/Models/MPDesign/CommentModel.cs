﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPDesign
{
    public class CommentModel
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public string NameOfUser { get; set; }
    }
}
