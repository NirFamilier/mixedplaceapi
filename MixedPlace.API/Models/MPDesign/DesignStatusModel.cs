﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPDesign
{
    public class DesignStatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
