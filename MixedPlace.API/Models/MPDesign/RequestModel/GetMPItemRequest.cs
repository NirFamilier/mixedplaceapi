﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPDesign.RequestModel
{
    public class GetMPItemRequest
    {
        [MinLength(1)]
        public List<GetMPItem> GetMPItemsCollection { get; set; }
    }
}
