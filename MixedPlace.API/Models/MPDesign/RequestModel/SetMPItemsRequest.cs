﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPDesign.RequestModel
{
    public class SetMPItemsRequest
    {
        /// <summary>
        /// Contain three properties : X,Y,Z
        /// </summary>
        [Required]
        public MPCoordinateDto MPCoordinate { get; set; }

        /// <summary>
        /// Contain ModelId, Applied Model Counter - counter this is delta 
        /// </summary>
        [Required]
        public IEnumerable<AppliedModelCounterDto> AppliedModelsCollection { get; set; }

        /// <summary>
        /// Contain Model ItemType and MPDesignJson
        /// </summary>
        [Required]
        public IEnumerable<MPDesignItemDto> Items { get; set; }
    }


}
