﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.MPDesign.RequestModel
{
    /// <summary>
    /// Item Details
    /// </summary>
    public class GetMPItem
    {
        [Required]
        public MPCoordinateDto MPCoordinate { get; set; }

        /// <summary>
        /// 1 - All, 2- Paint, 3- Object 4- Text Banner 5-Texture Banner
        /// </summary>
        [Range(1,5)]
        public int ItemsType { get; set; }
    }
}
