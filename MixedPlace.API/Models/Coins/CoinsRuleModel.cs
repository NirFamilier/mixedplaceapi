﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Coins
{
    public class CoinsRuleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<CoinsRuleConditionModel> CoinRuleConditions { get; set; }

        [ForeignKey("CoinsRuleType")]
        public int TypeId { get; set; }
        public CoinsRuleType CoinsRuleType { get; set; }

        [ForeignKey("LogicalOperator")]
        public int LogicalOperatorId { get; set; }
        public LogicalOperatorModel LogicalOperator { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Amount { get; set; }
        public int NumberOfAffectedUsers { get; set; }

    }
}
