﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Coins.ResponseModel
{
    public class GetCoinRulesResponse: FilterBaseResponse
    {
        public IEnumerable<CoinRuleDto> CoinRulesList { get; set; }
    }
}
