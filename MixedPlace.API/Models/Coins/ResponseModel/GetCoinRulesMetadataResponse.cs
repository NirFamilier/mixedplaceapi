﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Base.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Coins.ResponseModel
{
    public class GetCoinRulesMetadataResponse : MetadataResponseBase
    {
        public IEnumerable<CoinRuleTypeDto> CoinRuleTypes { get; set; }
        // AND OR
        public IEnumerable<LogicalOperatorDto> LogicalOperators { get; set; }
        //Operator
        public Dictionary<string,List<int>> UserFilter { get; set; }
    }
    
}
