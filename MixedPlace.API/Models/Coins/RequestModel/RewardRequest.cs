﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Transactions;
using MixedPlace.API.Models.User;

namespace MixedPlace.API.Models.Coins.RequestModel
{
    public class RewardRequest
    {
        public string Name { get; set; } 
        public string Description { get; set; }
        public CoinRuleTypeDto CoinRuleType { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Amount { get; set; }
        public List<CoinsRuleConditionDto> RuleConditions { get; set; }
        public LogicalOperatorDto LogicalOperator { get; set; }
    }
}
