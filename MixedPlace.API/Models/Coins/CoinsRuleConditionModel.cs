﻿using MixedPlace.API.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Coins
{
    public class CoinsRuleConditionModel
    {
        
        public int Id { get; set; }

        [ForeignKey("CoinsRule")]
        public int CoinsRuleId { get; set; }
        public CoinsRuleModel CoinsRule { get; set; }

        [ForeignKey("Condition")]
        public int? ConditionId { get; set; }
        public ConditionModel Condition { get; set; }

        public string FieldName { get; set; }
        public string Value {get;set;}
    }
}
