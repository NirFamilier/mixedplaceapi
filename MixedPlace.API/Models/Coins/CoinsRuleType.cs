﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Coins
{
    public class CoinsRuleType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
