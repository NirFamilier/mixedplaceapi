﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class ChallengeCategory_ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Challenges_ChallengeStatuses_ChallengeCategoryId",
                table: "Challenges");

            migrationBuilder.AddForeignKey(
                name: "FK_Challenges_ChallengeCategories_ChallengeCategoryId",
                table: "Challenges",
                column: "ChallengeCategoryId",
                principalTable: "ChallengeCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Challenges_ChallengeCategories_ChallengeCategoryId",
                table: "Challenges");

            migrationBuilder.AddForeignKey(
                name: "FK_Challenges_ChallengeStatuses_ChallengeCategoryId",
                table: "Challenges",
                column: "ChallengeCategoryId",
                principalTable: "ChallengeStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
