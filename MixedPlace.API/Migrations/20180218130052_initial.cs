﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChallengeActionTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChallengeActionTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChallengeStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChallengeStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChallengeTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChallengeTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Conditions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Conditions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Currencies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Country = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Symbol = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currencies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DesignStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DesignStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DeviceOperationSystems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceOperationSystems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ModelCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModelCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ModelStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModelStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MPCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CategoryName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MPStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NotificationTemplates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Message = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationTemplates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PackageCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PackageItemTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageItemTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PackageStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Prices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Price = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PriceTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransactionCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Challenges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChallengeStatusId = table.Column<int>(nullable: true),
                    ChallengeTypeId = table.Column<int>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    LogicalOperator = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Reward = table.Column<decimal>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: true),
                    UnlimitedDuration = table.Column<bool>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Challenges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Challenges_ChallengeStatuses_ChallengeStatusId",
                        column: x => x.ChallengeStatusId,
                        principalTable: "ChallengeStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Challenges_ChallengeTypes_ChallengeTypeId",
                        column: x => x.ChallengeTypeId,
                        principalTable: "ChallengeTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ModelSubCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    ModelCategoryId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModelSubCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ModelSubCategories_ModelCategories_ModelCategoryId",
                        column: x => x.ModelCategoryId,
                        principalTable: "ModelCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MPSubCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CategoryId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPSubCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPSubCategories_MPCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "MPCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Packages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AvailableFrom = table.Column<DateTime>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ExpiresOn = table.Column<DateTime>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PackageCategoryId = table.Column<int>(nullable: true),
                    PackageStatusId = table.Column<int>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    PriceTypeId = table.Column<int>(nullable: false),
                    PurchasedUnits = table.Column<int>(nullable: false),
                    Size = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Packages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Packages_PackageCategories_PackageCategoryId",
                        column: x => x.PackageCategoryId,
                        principalTable: "PackageCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Packages_PackageStatuses_PackageStatusId",
                        column: x => x.PackageStatusId,
                        principalTable: "PackageStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Packages_PriceTypes_PriceTypeId",
                        column: x => x.PriceTypeId,
                        principalTable: "PriceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Age = table.Column<int>(nullable: false),
                    ChallengeLevel = table.Column<int>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    CoinBalance = table.Column<decimal>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    DesingLevel = table.Column<int>(nullable: true),
                    DeviceID = table.Column<string>(nullable: true),
                    DeviceOSId = table.Column<int>(nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    Gender = table.Column<bool>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    IsTemporaryPassword = table.Column<bool>(nullable: false),
                    LastName = table.Column<string>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    MPDescription = table.Column<string>(nullable: true),
                    MPLevel = table.Column<int>(nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordExpirationDate = table.Column<DateTime>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    RegistrationDate = table.Column<DateTime>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    SocialLevel = table.Column<int>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    StreetNumber = table.Column<int>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    UserDescription = table.Column<string>(nullable: true),
                    UserLevel = table.Column<int>(nullable: true),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    UserStatusId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_DeviceOperationSystems_DeviceOSId",
                        column: x => x.DeviceOSId,
                        principalTable: "DeviceOperationSystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_UserStatuses_UserStatusId",
                        column: x => x.UserStatusId,
                        principalTable: "UserStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ChallengeRules",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActionTypeId = table.Column<int>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    ChallengeId = table.Column<int>(nullable: false),
                    ConditionId = table.Column<int>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChallengeRules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChallengeRules_ChallengeActionTypes_ActionTypeId",
                        column: x => x.ActionTypeId,
                        principalTable: "ChallengeActionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChallengeRules_Challenges_ChallengeId",
                        column: x => x.ChallengeId,
                        principalTable: "Challenges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChallengeRules_Conditions_ConditionId",
                        column: x => x.ConditionId,
                        principalTable: "Conditions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BundleModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ModelCategoryId = table.Column<int>(nullable: true),
                    ModelSubCategoryId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NumberOfModels = table.Column<int>(nullable: false),
                    Size = table.Column<long>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BundleModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BundleModel_ModelCategories_ModelCategoryId",
                        column: x => x.ModelCategoryId,
                        principalTable: "ModelCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BundleModel_ModelSubCategories_ModelSubCategoryId",
                        column: x => x.ModelSubCategoryId,
                        principalTable: "ModelSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MixedPlaces",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Category = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DesignStatusId = table.Column<int>(nullable: true),
                    HiddenCounter = table.Column<int>(nullable: false),
                    MPStatusId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    OwnerId = table.Column<string>(nullable: true),
                    PriceId = table.Column<int>(nullable: true),
                    PurchaseDate = table.Column<DateTime>(nullable: true),
                    SubCategory = table.Column<int>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    X = table.Column<long>(nullable: false),
                    Y = table.Column<long>(nullable: false),
                    Z = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MixedPlaces", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MixedPlaces_DesignStatuses_DesignStatusId",
                        column: x => x.DesignStatusId,
                        principalTable: "DesignStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MixedPlaces_MPStatuses_MPStatusId",
                        column: x => x.MPStatusId,
                        principalTable: "MPStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MixedPlaces_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MixedPlaces_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    NotifyResult = table.Column<string>(nullable: true),
                    Recipient = table.Column<string>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notifications_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PurchasedPackages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AmountPaid = table.Column<decimal>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    InvoiceUrl = table.Column<string>(nullable: true),
                    PackageId = table.Column<int>(nullable: true),
                    PaymentDate = table.Column<DateTime>(nullable: true),
                    PurchaseStatusId = table.Column<int>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchasedPackages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PurchasedPackages_Packages_PackageId",
                        column: x => x.PackageId,
                        principalTable: "Packages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchasedPackages_PurchaseStatuses_PurchaseStatusId",
                        column: x => x.PurchaseStatusId,
                        principalTable: "PurchaseStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchasedPackages_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Coins = table.Column<decimal>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    CurrencyCode = table.Column<string>(nullable: true),
                    Money = table.Column<decimal>(nullable: false),
                    OwnerId = table.Column<string>(nullable: true),
                    TransactionCategoryId = table.Column<int>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Transactions_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transactions_TransactionCategories_TransactionCategoryId",
                        column: x => x.TransactionCategoryId,
                        principalTable: "TransactionCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Models",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BundleId = table.Column<int>(nullable: true),
                    BundleImageName = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ModelCategoryId = table.Column<int>(nullable: true),
                    ModelStatusId = table.Column<int>(nullable: true),
                    ModelSubCategoryId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    PriceTypeId = table.Column<int>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Models", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Models_BundleModel_BundleId",
                        column: x => x.BundleId,
                        principalTable: "BundleModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Models_ModelCategories_ModelCategoryId",
                        column: x => x.ModelCategoryId,
                        principalTable: "ModelCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Models_ModelStatuses_ModelStatusId",
                        column: x => x.ModelStatusId,
                        principalTable: "ModelStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Models_ModelSubCategories_ModelSubCategoryId",
                        column: x => x.ModelSubCategoryId,
                        principalTable: "ModelSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Models_PriceTypes_PriceTypeId",
                        column: x => x.PriceTypeId,
                        principalTable: "PriceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AchievedChallenges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChallengeId = table.Column<int>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    MPId = table.Column<int>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AchievedChallenges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AchievedChallenges_Challenges_ChallengeId",
                        column: x => x.ChallengeId,
                        principalTable: "Challenges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AchievedChallenges_MixedPlaces_MPId",
                        column: x => x.MPId,
                        principalTable: "MixedPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AchievedChallenges_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPComments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CommentText = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    MPId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPComments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPComments_MixedPlaces_MPId",
                        column: x => x.MPId,
                        principalTable: "MixedPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MPComments_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Category = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DesignStatusId = table.Column<int>(nullable: true),
                    HiddenCounter = table.Column<int>(nullable: false),
                    MPId = table.Column<int>(nullable: true),
                    MPStatusId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    OwnerId = table.Column<string>(nullable: true),
                    PriceId = table.Column<int>(nullable: true),
                    PurchaseDate = table.Column<DateTime>(nullable: true),
                    SubCategory = table.Column<int>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    X = table.Column<long>(nullable: false),
                    Y = table.Column<long>(nullable: false),
                    Z = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPHistory_DesignStatuses_DesignStatusId",
                        column: x => x.DesignStatusId,
                        principalTable: "DesignStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MPHistory_MixedPlaces_MPId",
                        column: x => x.MPId,
                        principalTable: "MixedPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MPHistory_MPStatuses_MPStatusId",
                        column: x => x.MPStatusId,
                        principalTable: "MPStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MPHistory_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MPHistory_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ItemType = table.Column<int>(nullable: false),
                    Json = table.Column<string>(nullable: true),
                    MPId = table.Column<int>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPItems_MixedPlaces_MPId",
                        column: x => x.MPId,
                        principalTable: "MixedPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MPLikes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    MPId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPLikes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPLikes_MixedPlaces_MPId",
                        column: x => x.MPId,
                        principalTable: "MixedPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MPLikes_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPReports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    MPId = table.Column<int>(nullable: false),
                    ReportText = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPReports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPReports_MixedPlaces_MPId",
                        column: x => x.MPId,
                        principalTable: "MixedPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MPReports_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPShares",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    MPId = table.Column<int>(nullable: false),
                    Source = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPShares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPShares_MixedPlaces_MPId",
                        column: x => x.MPId,
                        principalTable: "MixedPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MPShares_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPVisits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    MPId = table.Column<int>(nullable: false),
                    PublicCounter = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPVisits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPVisits_MixedPlaces_MPId",
                        column: x => x.MPId,
                        principalTable: "MixedPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MPVisits_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPModelsPerUsers",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    MPModelId = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    Id = table.Column<int>(nullable: false),
                    RemainingModelsCount = table.Column<int>(nullable: false),
                    TotalModelsCount = table.Column<int>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPModelsPerUsers", x => new { x.UserId, x.MPModelId });
                    table.ForeignKey(
                        name: "FK_MPModelsPerUsers_Models_MPModelId",
                        column: x => x.MPModelId,
                        principalTable: "Models",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MPModelsPerUsers_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PackageItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModelId = table.Column<int>(nullable: true),
                    NumOfUnits = table.Column<int>(nullable: true),
                    PackageId = table.Column<int>(nullable: true),
                    PackageItemTypeId = table.Column<int>(nullable: true),
                    Sum = table.Column<int>(nullable: true),
                    TotalSize = table.Column<float>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PackageItems_Models_ModelId",
                        column: x => x.ModelId,
                        principalTable: "Models",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PackageItems_Packages_PackageId",
                        column: x => x.PackageId,
                        principalTable: "Packages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PackageItems_PackageItemTypes_PackageItemTypeId",
                        column: x => x.PackageItemTypeId,
                        principalTable: "PackageItemTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AchivedChallengesHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChallengeId = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    MPHistoryId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AchivedChallengesHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AchivedChallengesHistory_MPHistory_MPHistoryId",
                        column: x => x.MPHistoryId,
                        principalTable: "MPHistory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AchivedChallengesHistory_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPCommentsHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CommentText = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    MPHistoryId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPCommentsHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPCommentsHistory_MPHistory_MPHistoryId",
                        column: x => x.MPHistoryId,
                        principalTable: "MPHistory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MPCommentsHistory_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPItemsHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    ItemType = table.Column<int>(nullable: false),
                    Json = table.Column<string>(nullable: true),
                    MPHistoryId = table.Column<int>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPItemsHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPItemsHistory_MPHistory_MPHistoryId",
                        column: x => x.MPHistoryId,
                        principalTable: "MPHistory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MPLikesHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    MPHistoryId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPLikesHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPLikesHistory_MPHistory_MPHistoryId",
                        column: x => x.MPHistoryId,
                        principalTable: "MPHistory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MPLikesHistory_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPSharesHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    MPHistoryId = table.Column<int>(nullable: false),
                    Source = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPSharesHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPSharesHistory_MPHistory_MPHistoryId",
                        column: x => x.MPHistoryId,
                        principalTable: "MPHistory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MPSharesHistory_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPVisitsHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    MPHisotoryId = table.Column<int>(nullable: false),
                    PublicCounter = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPVisitsHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPVisitsHistory_MPHistory_MPHisotoryId",
                        column: x => x.MPHisotoryId,
                        principalTable: "MPHistory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MPVisitsHistory_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AchievedChallenges_ChallengeId",
                table: "AchievedChallenges",
                column: "ChallengeId");

            migrationBuilder.CreateIndex(
                name: "IX_AchievedChallenges_MPId",
                table: "AchievedChallenges",
                column: "MPId");

            migrationBuilder.CreateIndex(
                name: "IX_AchievedChallenges_UserId",
                table: "AchievedChallenges",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AchivedChallengesHistory_MPHistoryId",
                table: "AchivedChallengesHistory",
                column: "MPHistoryId");

            migrationBuilder.CreateIndex(
                name: "IX_AchivedChallengesHistory_UserId",
                table: "AchivedChallengesHistory",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_DeviceOSId",
                table: "AspNetUsers",
                column: "DeviceOSId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_UserStatusId",
                table: "AspNetUsers",
                column: "UserStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BundleModel_ModelCategoryId",
                table: "BundleModel",
                column: "ModelCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_BundleModel_ModelSubCategoryId",
                table: "BundleModel",
                column: "ModelSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ChallengeRules_ActionTypeId",
                table: "ChallengeRules",
                column: "ActionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ChallengeRules_ChallengeId",
                table: "ChallengeRules",
                column: "ChallengeId");

            migrationBuilder.CreateIndex(
                name: "IX_ChallengeRules_ConditionId",
                table: "ChallengeRules",
                column: "ConditionId");

            migrationBuilder.CreateIndex(
                name: "IX_Challenges_ChallengeStatusId",
                table: "Challenges",
                column: "ChallengeStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Challenges_ChallengeTypeId",
                table: "Challenges",
                column: "ChallengeTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MixedPlaces_DesignStatusId",
                table: "MixedPlaces",
                column: "DesignStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_MixedPlaces_MPStatusId",
                table: "MixedPlaces",
                column: "MPStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_MixedPlaces_OwnerId",
                table: "MixedPlaces",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_MixedPlaces_PriceId",
                table: "MixedPlaces",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_MixedPlaces_X_Y_Z",
                table: "MixedPlaces",
                columns: new[] { "X", "Y", "Z" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Models_ModelCategoryId",
                table: "Models",
                column: "ModelCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Models_ModelStatusId",
                table: "Models",
                column: "ModelStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Models_ModelSubCategoryId",
                table: "Models",
                column: "ModelSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Models_PriceTypeId",
                table: "Models",
                column: "PriceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Models_BundleId_BundleImageName",
                table: "Models",
                columns: new[] { "BundleId", "BundleImageName" },
                unique: true,
                filter: "[BundleId] IS NOT NULL AND [BundleImageName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ModelSubCategories_ModelCategoryId",
                table: "ModelSubCategories",
                column: "ModelCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPComments_MPId",
                table: "MPComments",
                column: "MPId");

            migrationBuilder.CreateIndex(
                name: "IX_MPComments_UserId",
                table: "MPComments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MPCommentsHistory_MPHistoryId",
                table: "MPCommentsHistory",
                column: "MPHistoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPCommentsHistory_UserId",
                table: "MPCommentsHistory",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MPHistory_DesignStatusId",
                table: "MPHistory",
                column: "DesignStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_MPHistory_MPId",
                table: "MPHistory",
                column: "MPId");

            migrationBuilder.CreateIndex(
                name: "IX_MPHistory_MPStatusId",
                table: "MPHistory",
                column: "MPStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_MPHistory_OwnerId",
                table: "MPHistory",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_MPHistory_PriceId",
                table: "MPHistory",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_MPItems_MPId",
                table: "MPItems",
                column: "MPId");

            migrationBuilder.CreateIndex(
                name: "IX_MPItemsHistory_MPHistoryId",
                table: "MPItemsHistory",
                column: "MPHistoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPLikes_MPId",
                table: "MPLikes",
                column: "MPId");

            migrationBuilder.CreateIndex(
                name: "IX_MPLikes_UserId",
                table: "MPLikes",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MPLikesHistory_MPHistoryId",
                table: "MPLikesHistory",
                column: "MPHistoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPLikesHistory_UserId",
                table: "MPLikesHistory",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MPModelsPerUsers_MPModelId",
                table: "MPModelsPerUsers",
                column: "MPModelId");

            migrationBuilder.CreateIndex(
                name: "IX_MPReports_MPId",
                table: "MPReports",
                column: "MPId");

            migrationBuilder.CreateIndex(
                name: "IX_MPReports_UserId",
                table: "MPReports",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MPShares_MPId",
                table: "MPShares",
                column: "MPId");

            migrationBuilder.CreateIndex(
                name: "IX_MPShares_UserId",
                table: "MPShares",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MPSharesHistory_MPHistoryId",
                table: "MPSharesHistory",
                column: "MPHistoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPSharesHistory_UserId",
                table: "MPSharesHistory",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MPSubCategories_CategoryId",
                table: "MPSubCategories",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPVisits_MPId",
                table: "MPVisits",
                column: "MPId");

            migrationBuilder.CreateIndex(
                name: "IX_MPVisits_UserId",
                table: "MPVisits",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MPVisitsHistory_MPHisotoryId",
                table: "MPVisitsHistory",
                column: "MPHisotoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPVisitsHistory_UserId",
                table: "MPVisitsHistory",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_UserId",
                table: "Notifications",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageItems_ModelId",
                table: "PackageItems",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageItems_PackageId",
                table: "PackageItems",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageItems_PackageItemTypeId",
                table: "PackageItems",
                column: "PackageItemTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Packages_PackageCategoryId",
                table: "Packages",
                column: "PackageCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Packages_PackageStatusId",
                table: "Packages",
                column: "PackageStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Packages_PriceTypeId",
                table: "Packages",
                column: "PriceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchasedPackages_PackageId",
                table: "PurchasedPackages",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchasedPackages_PurchaseStatusId",
                table: "PurchasedPackages",
                column: "PurchaseStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchasedPackages_UserId",
                table: "PurchasedPackages",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_OwnerId",
                table: "Transactions",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_TransactionCategoryId",
                table: "Transactions",
                column: "TransactionCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AchievedChallenges");

            migrationBuilder.DropTable(
                name: "AchivedChallengesHistory");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ChallengeRules");

            migrationBuilder.DropTable(
                name: "Currencies");

            migrationBuilder.DropTable(
                name: "MPComments");

            migrationBuilder.DropTable(
                name: "MPCommentsHistory");

            migrationBuilder.DropTable(
                name: "MPItems");

            migrationBuilder.DropTable(
                name: "MPItemsHistory");

            migrationBuilder.DropTable(
                name: "MPLikes");

            migrationBuilder.DropTable(
                name: "MPLikesHistory");

            migrationBuilder.DropTable(
                name: "MPModelsPerUsers");

            migrationBuilder.DropTable(
                name: "MPReports");

            migrationBuilder.DropTable(
                name: "MPShares");

            migrationBuilder.DropTable(
                name: "MPSharesHistory");

            migrationBuilder.DropTable(
                name: "MPSubCategories");

            migrationBuilder.DropTable(
                name: "MPVisits");

            migrationBuilder.DropTable(
                name: "MPVisitsHistory");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "NotificationTemplates");

            migrationBuilder.DropTable(
                name: "PackageItems");

            migrationBuilder.DropTable(
                name: "PurchasedPackages");

            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "ChallengeActionTypes");

            migrationBuilder.DropTable(
                name: "Challenges");

            migrationBuilder.DropTable(
                name: "Conditions");

            migrationBuilder.DropTable(
                name: "MPCategories");

            migrationBuilder.DropTable(
                name: "MPHistory");

            migrationBuilder.DropTable(
                name: "Models");

            migrationBuilder.DropTable(
                name: "PackageItemTypes");

            migrationBuilder.DropTable(
                name: "Packages");

            migrationBuilder.DropTable(
                name: "PurchaseStatuses");

            migrationBuilder.DropTable(
                name: "TransactionCategories");

            migrationBuilder.DropTable(
                name: "ChallengeStatuses");

            migrationBuilder.DropTable(
                name: "ChallengeTypes");

            migrationBuilder.DropTable(
                name: "MixedPlaces");

            migrationBuilder.DropTable(
                name: "BundleModel");

            migrationBuilder.DropTable(
                name: "ModelStatuses");

            migrationBuilder.DropTable(
                name: "PackageCategories");

            migrationBuilder.DropTable(
                name: "PackageStatuses");

            migrationBuilder.DropTable(
                name: "PriceTypes");

            migrationBuilder.DropTable(
                name: "DesignStatuses");

            migrationBuilder.DropTable(
                name: "MPStatuses");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Prices");

            migrationBuilder.DropTable(
                name: "ModelSubCategories");

            migrationBuilder.DropTable(
                name: "DeviceOperationSystems");

            migrationBuilder.DropTable(
                name: "UserStatuses");

            migrationBuilder.DropTable(
                name: "ModelCategories");
        }
    }
}
