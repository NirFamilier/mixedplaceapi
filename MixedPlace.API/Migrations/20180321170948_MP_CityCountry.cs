﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class MP_CityCountry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "Models");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Models");

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "MixedPlaces",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "MixedPlaces",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "MixedPlaces");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "MixedPlaces");

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Models",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Models",
                nullable: true);
        }
    }
}
