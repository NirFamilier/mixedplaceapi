﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class MPReportStatuses_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MPReports_MPReportStatusModel_ReportStatusId",
                table: "MPReports");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MPReportStatusModel",
                table: "MPReportStatusModel");

            migrationBuilder.RenameTable(
                name: "MPReportStatusModel",
                newName: "MPReportStatuses");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MPReportStatuses",
                table: "MPReportStatuses",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MPReports_MPReportStatuses_ReportStatusId",
                table: "MPReports",
                column: "ReportStatusId",
                principalTable: "MPReportStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MPReports_MPReportStatuses_ReportStatusId",
                table: "MPReports");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MPReportStatuses",
                table: "MPReportStatuses");

            migrationBuilder.RenameTable(
                name: "MPReportStatuses",
                newName: "MPReportStatusModel");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MPReportStatusModel",
                table: "MPReportStatusModel",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MPReports_MPReportStatusModel_ReportStatusId",
                table: "MPReports",
                column: "ReportStatusId",
                principalTable: "MPReportStatusModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
