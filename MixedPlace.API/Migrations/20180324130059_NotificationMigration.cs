﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class NotificationMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AddColumn<int>(
            //    name: "NotificationMethodId",
            //    table: "Notifications",
            //    nullable: true);

            //migrationBuilder.AddColumn<DateTime>(
            //    name: "NotificationSendDate",
            //    table: "AspNetUsers",
            //    nullable: true);

            //migrationBuilder.CreateTable(
            //    name: "NotificationMethodModel",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Name = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_NotificationMethodModel", x => x.Id);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Notifications_NotificationMethodId",
            //    table: "Notifications",
            //    column: "NotificationMethodId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Notifications_NotificationMethodModel_NotificationMethodId",
            //    table: "Notifications",
            //    column: "NotificationMethodId",
            //    principalTable: "NotificationMethodModel",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notifications_NotificationMethodModel_NotificationMethodId",
                table: "Notifications");

            migrationBuilder.DropTable(
                name: "NotificationMethodModel");

            migrationBuilder.DropIndex(
                name: "IX_Notifications_NotificationMethodId",
                table: "Notifications");

            migrationBuilder.DropColumn(
                name: "NotificationMethodId",
                table: "Notifications");

            migrationBuilder.DropColumn(
                name: "NotificationSendDate",
                table: "AspNetUsers");
        }
    }
}
