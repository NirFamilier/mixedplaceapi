﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class Alter_MPReports_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ReportStatusId",
                table: "MPReports",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "MPReports",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MPReportStatusModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPReportStatusModel", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MPReports_ReportStatusId",
                table: "MPReports",
                column: "ReportStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_MPReports_MPReportStatusModel_ReportStatusId",
                table: "MPReports",
                column: "ReportStatusId",
                principalTable: "MPReportStatusModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MPReports_MPReportStatusModel_ReportStatusId",
                table: "MPReports");

            migrationBuilder.DropTable(
                name: "MPReportStatusModel");

            migrationBuilder.DropIndex(
                name: "IX_MPReports_ReportStatusId",
                table: "MPReports");

            migrationBuilder.DropColumn(
                name: "ReportStatusId",
                table: "MPReports");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "MPReports");
        }
    }
}
