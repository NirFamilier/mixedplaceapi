﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class CoinRules_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CoinsRuleTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoinsRuleTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LogicalOperators",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogicalOperators", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CoinsRuleModels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    LogicalOperatorId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NumberOfAffectedUsers = table.Column<int>(nullable: false),
                    TypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoinsRuleModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CoinsRuleModels_LogicalOperators_LogicalOperatorId",
                        column: x => x.LogicalOperatorId,
                        principalTable: "LogicalOperators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoinsRuleModels_CoinsRuleTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "CoinsRuleTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CoinsRuleConditionModels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CoinsRuleId = table.Column<int>(nullable: false),
                    FieldName = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoinsRuleConditionModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CoinsRuleConditionModels_CoinsRuleModels_CoinsRuleId",
                        column: x => x.CoinsRuleId,
                        principalTable: "CoinsRuleModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CoinsRuleConditionModels_CoinsRuleId",
                table: "CoinsRuleConditionModels",
                column: "CoinsRuleId");

            migrationBuilder.CreateIndex(
                name: "IX_CoinsRuleModels_LogicalOperatorId",
                table: "CoinsRuleModels",
                column: "LogicalOperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_CoinsRuleModels_TypeId",
                table: "CoinsRuleModels",
                column: "TypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoinsRuleConditionModels");

            migrationBuilder.DropTable(
                name: "CoinsRuleModels");

            migrationBuilder.DropTable(
                name: "LogicalOperators");

            migrationBuilder.DropTable(
                name: "CoinsRuleTypes");
        }
    }
}
