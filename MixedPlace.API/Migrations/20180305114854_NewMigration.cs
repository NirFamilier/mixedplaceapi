﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class NewMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BundleModel_ModelCategories_ModelCategoryId",
                table: "BundleModel");

            migrationBuilder.DropForeignKey(
                name: "FK_BundleModel_ModelSubCategories_ModelSubCategoryId",
                table: "BundleModel");

            migrationBuilder.DropForeignKey(
                name: "FK_Models_ModelSubCategories_ModelSubCategoryId",
                table: "Models");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_ModelSubCategories_ModelCategoryDto_ModelCategoryId",
            //    table: "ModelSubCategories");

            //migrationBuilder.DropTable(
            //    name: "ModelCategoryDto");

            migrationBuilder.DropIndex(
                name: "IX_Models_ModelSubCategoryId",
                table: "Models");

            migrationBuilder.DropIndex(
                name: "IX_BundleModel_ModelCategoryId",
                table: "BundleModel");

            migrationBuilder.DropIndex(
                name: "IX_BundleModel_ModelSubCategoryId",
                table: "BundleModel");

            migrationBuilder.DropColumn(
                name: "Category",
                table: "MPHistory");

            migrationBuilder.DropColumn(
                name: "SubCategory",
                table: "MPHistory");

            migrationBuilder.DropColumn(
                name: "ModelSubCategoryId",
                table: "Models");

            migrationBuilder.DropColumn(
                name: "Category",
                table: "MixedPlaces");

            migrationBuilder.DropColumn(
                name: "SubCategory",
                table: "MixedPlaces");

            migrationBuilder.DropColumn(
                name: "ModelCategoryId",
                table: "BundleModel");

            migrationBuilder.DropColumn(
                name: "ModelSubCategoryId",
                table: "BundleModel");

            migrationBuilder.CreateTable(
                name: "BundleModelCatSubCat",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BundleId = table.Column<int>(nullable: true),
                    ModelCategoryId = table.Column<int>(nullable: true),
                    ModelSubCategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BundleModelCatSubCat", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BundleModelCatSubCat_BundleModel_BundleId",
                        column: x => x.BundleId,
                        principalTable: "BundleModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BundleModelCatSubCat_ModelCategories_ModelCategoryId",
                        column: x => x.ModelCategoryId,
                        principalTable: "ModelCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BundleModelCatSubCat_ModelSubCategories_ModelSubCategoryId",
                        column: x => x.ModelSubCategoryId,
                        principalTable: "ModelSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPCatSubCat",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MPCategoryId = table.Column<int>(nullable: true),
                    MPId = table.Column<int>(nullable: true),
                    MPSubCategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPCatSubCat", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPCatSubCat_MPCategories_MPCategoryId",
                        column: x => x.MPCategoryId,
                        principalTable: "MPCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MPCatSubCat_MixedPlaces_MPId",
                        column: x => x.MPId,
                        principalTable: "MixedPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MPCatSubCat_MPSubCategories_MPSubCategoryId",
                        column: x => x.MPSubCategoryId,
                        principalTable: "MPSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPHistoryCatSubCat",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MPCategoryId = table.Column<int>(nullable: true),
                    MPId = table.Column<int>(nullable: true),
                    MPSubCategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPHistoryCatSubCat", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPHistoryCatSubCat_MPCategories_MPCategoryId",
                        column: x => x.MPCategoryId,
                        principalTable: "MPCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MPHistoryCatSubCat_MPHistory_MPId",
                        column: x => x.MPId,
                        principalTable: "MPHistory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MPHistoryCatSubCat_MPSubCategories_MPSubCategoryId",
                        column: x => x.MPSubCategoryId,
                        principalTable: "MPSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MPModelCatSubCat",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModelCategoryId = table.Column<int>(nullable: true),
                    ModelId = table.Column<int>(nullable: true),
                    ModelSubCategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPModelCatSubCat", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPModelCatSubCat_ModelCategories_ModelCategoryId",
                        column: x => x.ModelCategoryId,
                        principalTable: "ModelCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MPModelCatSubCat_Models_ModelId",
                        column: x => x.ModelId,
                        principalTable: "Models",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MPModelCatSubCat_ModelSubCategories_ModelSubCategoryId",
                        column: x => x.ModelSubCategoryId,
                        principalTable: "ModelSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BundleModelCatSubCat_BundleId",
                table: "BundleModelCatSubCat",
                column: "BundleId");

            migrationBuilder.CreateIndex(
                name: "IX_BundleModelCatSubCat_ModelCategoryId",
                table: "BundleModelCatSubCat",
                column: "ModelCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_BundleModelCatSubCat_ModelSubCategoryId",
                table: "BundleModelCatSubCat",
                column: "ModelSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPCatSubCat_MPCategoryId",
                table: "MPCatSubCat",
                column: "MPCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPCatSubCat_MPId",
                table: "MPCatSubCat",
                column: "MPId");

            migrationBuilder.CreateIndex(
                name: "IX_MPCatSubCat_MPSubCategoryId",
                table: "MPCatSubCat",
                column: "MPSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPHistoryCatSubCat_MPCategoryId",
                table: "MPHistoryCatSubCat",
                column: "MPCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPHistoryCatSubCat_MPId",
                table: "MPHistoryCatSubCat",
                column: "MPId");

            migrationBuilder.CreateIndex(
                name: "IX_MPHistoryCatSubCat_MPSubCategoryId",
                table: "MPHistoryCatSubCat",
                column: "MPSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPModelCatSubCat_ModelCategoryId",
                table: "MPModelCatSubCat",
                column: "ModelCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MPModelCatSubCat_ModelId",
                table: "MPModelCatSubCat",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_MPModelCatSubCat_ModelSubCategoryId",
                table: "MPModelCatSubCat",
                column: "ModelSubCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelSubCategories_ModelCategories_ModelCategoryId",
                table: "ModelSubCategories",
                column: "ModelCategoryId",
                principalTable: "ModelCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelSubCategories_ModelCategories_ModelCategoryId",
                table: "ModelSubCategories");

            migrationBuilder.DropTable(
                name: "BundleModelCatSubCat");

            migrationBuilder.DropTable(
                name: "MPCatSubCat");

            migrationBuilder.DropTable(
                name: "MPHistoryCatSubCat");

            migrationBuilder.DropTable(
                name: "MPModelCatSubCat");

            migrationBuilder.AddColumn<int>(
                name: "Category",
                table: "MPHistory",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubCategory",
                table: "MPHistory",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ModelSubCategoryId",
                table: "Models",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Category",
                table: "MixedPlaces",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubCategory",
                table: "MixedPlaces",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ModelCategoryId",
                table: "BundleModel",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ModelSubCategoryId",
                table: "BundleModel",
                nullable: true);

            //migrationBuilder.CreateTable(
            //    name: "ModelCategoryDto",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Name = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ModelCategoryDto", x => x.Id);
            //    });

            migrationBuilder.CreateIndex(
                name: "IX_Models_ModelSubCategoryId",
                table: "Models",
                column: "ModelSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_BundleModel_ModelCategoryId",
                table: "BundleModel",
                column: "ModelCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_BundleModel_ModelSubCategoryId",
                table: "BundleModel",
                column: "ModelSubCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_BundleModel_ModelCategories_ModelCategoryId",
                table: "BundleModel",
                column: "ModelCategoryId",
                principalTable: "ModelCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BundleModel_ModelSubCategories_ModelSubCategoryId",
                table: "BundleModel",
                column: "ModelSubCategoryId",
                principalTable: "ModelSubCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Models_ModelSubCategories_ModelSubCategoryId",
                table: "Models",
                column: "ModelSubCategoryId",
                principalTable: "ModelSubCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ModelSubCategories_ModelCategoryDto_ModelCategoryId",
                table: "ModelSubCategories",
                column: "ModelCategoryId",
                principalTable: "ModelCategoryDto",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
