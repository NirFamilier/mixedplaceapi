﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class MPOperationeCoinsPrice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelSubCategories_ModelCategories_ModelCategoryId",
                table: "ModelSubCategories");

            //migrationBuilder.CreateTable(
            //    name: "ModelCategoryDto",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Name = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ModelCategoryDto", x => x.Id);
            //    });

            migrationBuilder.CreateTable(
                name: "MPOperationeCoinsPrice",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CATMId = table.Column<int>(nullable: false),
                    PriceInCoins = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MPOperationeCoinsPrice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MPOperationeCoinsPrice_ChallengeActionTypes_CATMId",
                        column: x => x.CATMId,
                        principalTable: "ChallengeActionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MPOperationeCoinsPrice_CATMId",
                table: "MPOperationeCoinsPrice",
                column: "CATMId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ModelSubCategories_ModelCategoryDto_ModelCategoryId",
            //    table: "ModelSubCategories",
            //    column: "ModelCategoryId",
            //    principalTable: "ModelCategoryDto",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelSubCategories_ModelCategoryDto_ModelCategoryId",
                table: "ModelSubCategories");

            migrationBuilder.DropTable(
                name: "ModelCategoryDto");

            migrationBuilder.DropTable(
                name: "MPOperationeCoinsPrice");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelSubCategories_ModelCategories_ModelCategoryId",
                table: "ModelSubCategories",
                column: "ModelCategoryId",
                principalTable: "ModelCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
