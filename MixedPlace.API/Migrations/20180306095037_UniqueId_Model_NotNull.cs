﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class UniqueId_Model_NotNull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Models_BundleId_BundleImageName_UniqueId",
                table: "Models");

            migrationBuilder.AlterColumn<string>(
                name: "UniqueId",
                table: "Models",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Models_UniqueId",
                table: "Models",
                column: "UniqueId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Models_BundleId_BundleImageName",
                table: "Models",
                columns: new[] { "BundleId", "BundleImageName" },
                unique: true,
                filter: "[BundleId] IS NOT NULL AND [BundleImageName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Models_UniqueId",
                table: "Models");

            migrationBuilder.DropIndex(
                name: "IX_Models_BundleId_BundleImageName",
                table: "Models");

            migrationBuilder.AlterColumn<string>(
                name: "UniqueId",
                table: "Models",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.CreateIndex(
                name: "IX_Models_BundleId_BundleImageName_UniqueId",
                table: "Models",
                columns: new[] { "BundleId", "BundleImageName", "UniqueId" },
                unique: true,
                filter: "[BundleId] IS NOT NULL AND [BundleImageName] IS NOT NULL AND [UniqueId] IS NOT NULL");
        }
    }
}
