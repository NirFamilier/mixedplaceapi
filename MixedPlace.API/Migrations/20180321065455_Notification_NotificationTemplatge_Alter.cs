﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class Notification_NotificationTemplatge_Alter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropIndex(
            //    name: "IX_Models_BundleId_BundleImageName",
            //    table: "Models");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "NotificationTemplates",
                newName: "UniqueName");

            migrationBuilder.AddColumn<int>(
                name: "NotificationTemplateTypeId",
                table: "NotificationTemplates",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateSent",
                table: "Notifications",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NotificationOriginationId",
                table: "Notifications",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NotificationStatusId",
                table: "Notifications",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NotificationTemplateId",
                table: "Notifications",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BundleImageName",
                table: "Models",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "NotificationOriginationModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationOriginationModel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NotificationStatusModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationStatusModel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NotificationTemplateTypeModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationTemplateTypeModel", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NotificationTemplates_NotificationTemplateTypeId",
                table: "NotificationTemplates",
                column: "NotificationTemplateTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_NotificationOriginationId",
                table: "Notifications",
                column: "NotificationOriginationId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_NotificationStatusId",
                table: "Notifications",
                column: "NotificationStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_NotificationTemplateId",
                table: "Notifications",
                column: "NotificationTemplateId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Models_BundleId",
            //    table: "Models",
            //    column: "BundleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Notifications_NotificationOriginationModel_NotificationOriginationId",
                table: "Notifications",
                column: "NotificationOriginationId",
                principalTable: "NotificationOriginationModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notifications_NotificationStatusModel_NotificationStatusId",
                table: "Notifications",
                column: "NotificationStatusId",
                principalTable: "NotificationStatusModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notifications_NotificationTemplates_NotificationTemplateId",
                table: "Notifications",
                column: "NotificationTemplateId",
                principalTable: "NotificationTemplates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NotificationTemplates_NotificationTemplateTypeModel_NotificationTemplateTypeId",
                table: "NotificationTemplates",
                column: "NotificationTemplateTypeId",
                principalTable: "NotificationTemplateTypeModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notifications_NotificationOriginationModel_NotificationOriginationId",
                table: "Notifications");

            migrationBuilder.DropForeignKey(
                name: "FK_Notifications_NotificationStatusModel_NotificationStatusId",
                table: "Notifications");

            migrationBuilder.DropForeignKey(
                name: "FK_Notifications_NotificationTemplates_NotificationTemplateId",
                table: "Notifications");

            migrationBuilder.DropForeignKey(
                name: "FK_NotificationTemplates_NotificationTemplateTypeModel_NotificationTemplateTypeId",
                table: "NotificationTemplates");

            migrationBuilder.DropTable(
                name: "NotificationOriginationModel");

            migrationBuilder.DropTable(
                name: "NotificationStatusModel");

            migrationBuilder.DropTable(
                name: "NotificationTemplateTypeModel");

            migrationBuilder.DropIndex(
                name: "IX_NotificationTemplates_NotificationTemplateTypeId",
                table: "NotificationTemplates");

            migrationBuilder.DropIndex(
                name: "IX_Notifications_NotificationOriginationId",
                table: "Notifications");

            migrationBuilder.DropIndex(
                name: "IX_Notifications_NotificationStatusId",
                table: "Notifications");

            migrationBuilder.DropIndex(
                name: "IX_Notifications_NotificationTemplateId",
                table: "Notifications");

            migrationBuilder.DropIndex(
                name: "IX_Models_BundleId",
                table: "Models");

            migrationBuilder.DropColumn(
                name: "NotificationTemplateTypeId",
                table: "NotificationTemplates");

            migrationBuilder.DropColumn(
                name: "DateSent",
                table: "Notifications");

            migrationBuilder.DropColumn(
                name: "NotificationOriginationId",
                table: "Notifications");

            migrationBuilder.DropColumn(
                name: "NotificationStatusId",
                table: "Notifications");

            migrationBuilder.DropColumn(
                name: "NotificationTemplateId",
                table: "Notifications");

            migrationBuilder.RenameColumn(
                name: "UniqueName",
                table: "NotificationTemplates",
                newName: "Name");

            migrationBuilder.AlterColumn<string>(
                name: "BundleImageName",
                table: "Models",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Models_BundleId_BundleImageName",
                table: "Models",
                columns: new[] { "BundleId", "BundleImageName" },
                unique: true,
                filter: "[BundleId] IS NOT NULL AND [BundleImageName] IS NOT NULL");
        }
    }
}
