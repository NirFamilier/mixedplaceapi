﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class CoinsRuleConditionModel_ConditionId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ConditionId",
                table: "CoinsRuleConditionModels",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CoinsRuleConditionModels_ConditionId",
                table: "CoinsRuleConditionModels",
                column: "ConditionId");

            migrationBuilder.AddForeignKey(
                name: "FK_CoinsRuleConditionModels_Conditions_ConditionId",
                table: "CoinsRuleConditionModels",
                column: "ConditionId",
                principalTable: "Conditions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CoinsRuleConditionModels_Conditions_ConditionId",
                table: "CoinsRuleConditionModels");

            migrationBuilder.DropIndex(
                name: "IX_CoinsRuleConditionModels_ConditionId",
                table: "CoinsRuleConditionModels");

            migrationBuilder.DropColumn(
                name: "ConditionId",
                table: "CoinsRuleConditionModels");
        }
    }
}
