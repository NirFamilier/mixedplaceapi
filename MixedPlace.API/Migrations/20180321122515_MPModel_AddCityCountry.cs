﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class MPModel_AddCityCountry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropIndex(
            //    name: "IX_Models_BundleId_BundleImageName",
            //    table: "Models");

            migrationBuilder.AlterColumn<string>(
                name: "BundleImageName",
                table: "Models",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Models",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Models",
                nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Models_BundleId",
            //    table: "Models",
            //    column: "BundleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Models_BundleId",
                table: "Models");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Models");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Models");

            migrationBuilder.AlterColumn<string>(
                name: "BundleImageName",
                table: "Models",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Models_BundleId_BundleImageName",
                table: "Models",
                columns: new[] { "BundleId", "BundleImageName" },
                unique: true,
                filter: "[BundleId] IS NOT NULL AND [BundleImageName] IS NOT NULL");
        }
    }
}
