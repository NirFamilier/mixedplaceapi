﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MixedPlace.API.Migrations
{
    public partial class ChallengeCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ChallengeCategoryId",
                table: "Challenges",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ChallengeCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChallengeCategories", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Challenges_ChallengeCategoryId",
                table: "Challenges",
                column: "ChallengeCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Challenges_ChallengeStatuses_ChallengeCategoryId",
                table: "Challenges",
                column: "ChallengeCategoryId",
                principalTable: "ChallengeStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Challenges_ChallengeStatuses_ChallengeCategoryId",
                table: "Challenges");

            migrationBuilder.DropTable(
                name: "ChallengeCategories");

            migrationBuilder.DropIndex(
                name: "IX_Challenges_ChallengeCategoryId",
                table: "Challenges");

            migrationBuilder.DropColumn(
                name: "ChallengeCategoryId",
                table: "Challenges");
        }
    }
}
