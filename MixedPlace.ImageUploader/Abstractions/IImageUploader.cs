﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.ImageUploader.Abstractions
{
    public interface IImageUploader
    {
        string UploadImage(byte[] fileContent, string fileName, string contentType, string subDirctory = null);
    }
}
