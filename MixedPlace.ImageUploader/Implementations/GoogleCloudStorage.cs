﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Storage.v1;
using Google.Cloud.Storage.V1;
using MixedPlace.ImageUploader.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MixedPlace.ImageUploader.Implementations
{
    public class GoogleCloudStorage : IImageUploader
    {
        private ServiceConfig _serviceConfig;

        public GoogleCloudStorage(ServiceConfig serviceConfig)
        {
            _serviceConfig = serviceConfig;
        }

        public string UploadImage(byte[] fileContent, string fileName, string contentType,string subDirectory = null)
        {
            try
            {
                using (var memoryStream = new MemoryStream(fileContent, false))
                {
                    GoogleCredential credential = GoogleCredential.FromJson(JsonConvert.SerializeObject(_serviceConfig));// FromFile("imageuploaderconfig.json");
                    var options = new UploadObjectOptions { PredefinedAcl = PredefinedObjectAcl.PublicRead };
                    var stClient = StorageClient.Create(credential);
                    var storageFileName = subDirectory == null ? $"{DateTime.Now.Ticks}{fileName}" : $"{subDirectory}/{DateTime.Now.Ticks}{fileName}";
                    var obj = stClient.UploadObject(_serviceConfig.BucketName, storageFileName, contentType, memoryStream, options);
                    return obj.MediaLink;
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

        }
    }
}
