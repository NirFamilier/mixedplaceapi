﻿using MixedPlace.ImageUploader.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.ImageUploader.Implementations
{
    public class FileSystemStorageUploader : IImageUploader
    {
        private ServiceConfig _serviceConfig;

        public FileSystemStorageUploader(ServiceConfig serviceConfig)
        {
            _serviceConfig = serviceConfig;
        }

        public string UploadImage(byte[] fileContent, string fileName, string contentType,string subDirectory = null)
        {
            try
            {
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), subDirectory == null ? fileName : $"{subDirectory}/{fileName}");
                File.WriteAllBytes(filePath, fileContent);
                return filePath;
            }
            catch (Exception exp)
            {
                return string.Empty;
            }
            
        }
    }
}
