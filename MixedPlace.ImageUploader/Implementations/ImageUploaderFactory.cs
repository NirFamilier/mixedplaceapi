﻿using MixedPlace.ImageUploader.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.ImageUploader.Implementations
{
    public class ImageUploaderFactory 
    {
      
        public static IImageUploader CreateImageUploader(string storageProviderName = "GoogleCloudStorage")
        {
            try
            {
                switch (storageProviderName)
                {
                    case "GoogleCloudStorage":
                        return new GoogleCloudStorage(InitialzeConfigurations());
                    case "FileSystemStorage":
                        return new FileSystemStorageUploader(InitialzeConfigurations());
                    default:
                        return null;
                }
            }
            catch (Exception exp)
            {
                return null;
            }
           
        }

        private static ServiceConfig InitialzeConfigurations()
        {
            using (StreamReader r = new StreamReader("imageuploaderconfig.json"))
            {
                string json = r.ReadToEnd();
                ServiceConfig serviceConfig = JsonConvert.DeserializeObject<ServiceConfig>(json);
                return serviceConfig;
            }
        }
    }
}
