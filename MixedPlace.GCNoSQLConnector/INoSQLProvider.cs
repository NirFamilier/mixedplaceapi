﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MixedPlace.NoSQLConnector
{
    interface INoSQLProvider
    {

        bool Save(string data, string tablename);
        string Get(string table);

    }
}
