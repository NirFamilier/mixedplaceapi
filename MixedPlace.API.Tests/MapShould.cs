﻿using AutoMapper.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.Map.RequestModel;
using MixedPlace.API.Models.Map.ResponseModel;
using MixedPlace.API.Models.MPDesign.RequestModel;
using MixedPlace.API.Models.MPDesign.ResponseModel;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.MPModel.RequestModels;
using MixedPlace.API.Models.MPModel.ResponseModels;
using MixedPlace.API.Models.User.ResponseModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MixedPlace.API.Tests
{
    public class MapShould : BaseShould
    {

        // Builds the data to send to the server
        private StringContent GetAPIData(object request)
        {
            return new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
        }





        [Fact]
        public async void GetUserData()
        {
            bool succeeded = true;

            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var loginResult = await LoginAndReturnToken(succeeded, guid);

            var random = new Random();
            var mpDto1 = new MPDto
            {
                X = random.Next(),
                Y = random.Next(),
                Z = random.Next(),
            };
            var mpDto2 = new MPDto
            {
                X = random.Next(),
                Y = random.Next(),
                Z = random.Next(),
            };
            var mpRMList = new List<MixedPlaceRequestModel>
            {
                 new MixedPlaceRequestModel
                 {
                      X = mpDto1.X,
                      Y = mpDto1.Y,
                      Z = mpDto1.Z,
                 },
                  new MixedPlaceRequestModel
                 {
                      X = mpDto2.X,
                      Y = mpDto2.Y,
                      Z = mpDto2.Z,
                 }
            };
            //Craete New MixedPlaces (List with two mixed places)
            //var result = await CreateMixedPlaceWithPrice(mpRMList, loginResult.Token);
            //Assert.True(result.IsSuccessStatusCode);

            //Buy this one of MixedPlaces to user that loged in
            var result = await BuyMixedPlace(mpDto1, loginResult.Token);
            Assert.True(result.IsSuccessStatusCode);

            //Get MixedPlace from MixedPlaces Table according to mpDto coordinats
            //in other words get MixedPlace that was created in the begginer of the test
            var listOfCoordinates = new List<MPDto> { mpDto1, mpDto2 };
            result = await GetMixedplaces(listOfCoordinates);
            Assert.True(result.IsSuccessStatusCode);

            var gmContent = await result.Content.ReadAsStringAsync();
            var gmResponse = JsonConvert.DeserializeObject<ResponseModel<GetMixedPlaceInfoResponse>>(gmContent);//convert from Json to ResponseModel
            Assert.True(gmResponse.Data != null);

            var mpCoordinates = new List<MPCoordinateDto> { mpDto1, mpDto2 };


            var result2 = await _client.PostAsync("api/map/mpUserData", GetAPIData(mpCoordinates));
            Assert.Equal(HttpStatusCode.OK, result2.StatusCode);

            var response = await CreateResponse<ResponseModel<UserDataResponseModel>>(result2);
            var expr = response.Data.List[0].Owner != null;
            Assert.True(expr);
            expr = response.Data.List[1].Owner == null;
            Assert.True(expr);
        }

        [Fact]
        public async void GetAllModel()
        {
            bool succeeded = true;
            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var result = await LoginAndReturnToken(succeeded, guid);
            var adminAuthResponse = await AdminLogin();

            if (succeeded)
            {
                 await AddNewModelModify(adminAuthResponse.Token);


                // _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.GetAsync("api/map/modelsinfo");
                
                Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);
                if (succeeded)
                {
                    var response = await CreateResponse<ResponseModel<MPModelResponseModel>>(result2);
                    var list = response.Data.List.Where(x => x.Description != "Test Model");
                    succeeded = list.Count() == 3 ? true : false;
                    Assert.True(succeeded);

                    succeeded = response.Data.List.First(x=> x.Id == 2).Price == 1000 ? true : false;
                    Assert.True(succeeded);
                }
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void BuyMPBadRequest()
        {
            bool succeeded = true;
            MPDto mpDtoBadRequest = null;

            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var result = await LoginAndReturnToken(succeeded, guid);

            if (succeeded)
            {
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.PostAsync("api/map/mixedPlace/buymp", GetAPIData(mpDtoBadRequest));
                succeeded = result2.StatusCode == System.Net.HttpStatusCode.BadRequest ? true : false;
                Assert.True(succeeded);
            }

            Assert.True(succeeded);
        }

        [Fact]
        public async void BuyMPGoodRequestAndNewMP()
        {
            bool succeeded = true;
            var random = new Random();
            var mpDto = new MPDto
            {
                X = random.Next(),
                Y = random.Next(),
                Z = random.Next(),
            };


            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var result = await LoginAndReturnToken(succeeded, guid);

            if (succeeded)
            {
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.PostAsync("api/map/mixedPlace/buymp", GetAPIData(mpDto));
                succeeded = result2.StatusCode == System.Net.HttpStatusCode.OK ? true : false;
                Assert.True(succeeded);

            }
            Assert.True(succeeded);
        }

        //TODO: Flow will be implemented when ValidateUserBalance in MapService will be implemented.

        /* [Fact]
        public async void BuyMPNoBalance()
        {
            bool succeeded = true;
            MPDto mpDto = new MPDto { X = 1, Y = 1, Z = 1 };

            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var result = await LoginAndReturnToken(succeeded, guid);

            if (succeeded)
            {
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.PostAsync("api/map/mixedPlace/buymp", GetAPIData(mpDto));
                succeeded = result2.StatusCode == System.Net.HttpStatusCode.BadRequest ? true : false;
                Assert.True(succeeded);

            }
            Assert.True(succeeded);
        }*/

        [Fact]
        public async void AddModelToUserModelNotExist()
        {
            bool succeeded = true;
            var modelToUser = new List<UserRecieveNewModelItem>
                { new UserRecieveNewModelItem { ModelId = 1345543345},
               };
            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var result = await LoginAndReturnToken(succeeded, guid);

            if (succeeded)
            {
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.PostAsync("api/map/models/userrecievenewmodel", GetAPIData(modelToUser));
                succeeded = result2.StatusCode == System.Net.HttpStatusCode.BadRequest;
                Assert.True(succeeded);
            }
            Assert.True(succeeded);
        }


        [Fact]
        public async void SetMPDataV2()
        {
            bool succeeded = true;
            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var loginResult = await LoginAndReturnToken(succeeded, guid);
            var adminAuthResponse = await AdminLogin();
            var random = new Random();
            var mpDto = new MPDto
            {
                X = random.Next(),
                Y = random.Next(),
                Z = random.Next(),
            };
            var mpRMList = new List<MixedPlaceRequestModel>
            {
                 new MixedPlaceRequestModel
                 {
                      X = mpDto.X,
                      Y = mpDto.Y,
                      Z = mpDto.Z,
                 }
            };
            
            //THIS TEST IS IRELEVANT BECAUSE DEPRICATION
            //Create New MixedPlace(List with one mixed place)
            //var result = await CreateMixedPlaceWithPrice(mpRMList, loginResult.Token);
            //Assert.True(result.IsSuccessStatusCode);

            //Buy this New MixedPlace to user that loged in
            var result = await BuyMixedPlace(mpDto, loginResult.Token);
            Assert.True(result.IsSuccessStatusCode);

            //Add new model to Models Table
            await AddNewModelModify(adminAuthResponse.Token);


            //Get all Models from Models Table and then get first from the result list
            result = await GetAllModels(loginResult.Token);
            Assert.True(result.IsSuccessStatusCode);
            var gamContent = await result.Content.ReadAsStringAsync();
            var gamResponse = JsonConvert.DeserializeObject<ResponseModel<MPModelResponseModel>>(gamContent);
            Assert.True(gamResponse.Data != null);

            var amtuRequest = new List<UserRecieveNewModelItem>
            {
                new UserRecieveNewModelItem { ModelId = gamResponse.Data.List.First().Id, RecievedModelsCount = 5 }
            };

            //Bind the model to User (add row to MPModelsPerUsers table)
            result = await AddModelToUser(amtuRequest);
            Assert.True(result.IsSuccessStatusCode);

            //Get MixedPlace from MixedPlaces Table according to mpDto coordinats
            //in other words get MixedPlace that was created in the begginer of the test
            var listOfCoordinates = new List<MPDto> { mpDto };
            result = await GetMixedplaces(listOfCoordinates);
            Assert.True(result.IsSuccessStatusCode);

            var gmContent = await result.Content.ReadAsStringAsync();
            var gmResponse = JsonConvert.DeserializeObject<ResponseModel<GetMixedPlaceInfoResponse>>(gmContent);
            Assert.True(gmResponse.Data != null);

            var smpdRequest = new SetMPItemsRequest
            {
                AppliedModelsCollection = new List<AppliedModelCounterDto>
                {
                     new AppliedModelCounterDto
                     {
                          Amount = 2,
                          ModelId = amtuRequest.First().ModelId
                     }
                },
                MPCoordinate = mpDto,
                Items = new List<MPDesignItemDto>
                 {
                     new MPDesignItemDto
                      {
                          ItemsType = (int)MixedPlaceItemType.Object,
                          MPItemsJson = $"{DateTime.Now}:JSON"
                      },
                     new MPDesignItemDto
                      {
                          ItemsType = (int)MixedPlaceItemType.Paint,
                          MPItemsJson = $"{DateTime.Now}:JSON"
                      },
                 }
            };
            result = await SetMPData(smpdRequest);
            Assert.True(result.IsSuccessStatusCode);

            //GetMPDesigns
            var mpDesignsRequest = new GetMPItemRequest();
            mpDesignsRequest.GetMPItemsCollection = new List<GetMPItem>();
            mpDesignsRequest.GetMPItemsCollection.Add(new GetMPItem
            {
                MPCoordinate = mpDto,
                ItemsType = 1
            });
            result = await GetMpDesigns(mpDesignsRequest);
            Assert.True(result.IsSuccessStatusCode);
        }

        [Fact]
        public async void UpdateMixedPlace()
        {
            bool succeeded = true;
            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var loginResult = await LoginAndReturnToken(succeeded, guid);
            _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + loginResult.Token);
            var mpDto1 = new MPDto
            {
                X = 100,
                Y = 200,
                Z = 300,
            };

            var result = await BuyMixedPlace(mpDto1, null);
            Assert.True(result.IsSuccessStatusCode);

            var data = new MPUserDataDto();

            data.Coordinates = new MPCoordinateDto
            {
                X = 100,
                Y = 200,
                Z = 300,
            };
            data.Name = "MP Name1";
            data.MPDescription = "MP Description 1";
            data.Categories = new List<ModelCatSubCatDto>
            {
                new ModelCatSubCatDto()
                {
                    Category = new ModelCategoryDto(){Id = 1},
                    SubCategory = new ModelSubCategoryDto(){Id = 1,ModelCategoryId = 1},
                }
            };
            var result2 = await _client.PostAsync("api/map/mixedplace/updatempuserData", GetAPIData(data));
            Assert.True(result2.IsSuccessStatusCode);
        }




        [Obsolete]
        //CreateMixedPlaceWithPrice
        private async Task<HttpResponseMessage> CreateMixedPlaceWithPrice(List<MixedPlaceRequestModel> list, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/map/mixedPlace/create", GetAPIData(list));
            return result;
        }

        //BuyMP
        private async Task<HttpResponseMessage> BuyMixedPlace(MPDto mpDto, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/map/mixedPlace/buymp", GetAPIData(mpDto));
            return result;
        }

        //AddModel
        public async Task AddNewModelModify(string token = null)
        {
            AddModelRequest request = new AddModelRequest();
            

            byte[] byteArray = File.ReadAllBytes(@"../../../Images/img.JPG");
            Stream stream = new MemoryStream(byteArray);

            var byteArraycontent = new ByteArrayContent(byteArray);
            byteArraycontent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");
            MultipartFormDataContent multiplePartFormDataContent = new MultipartFormDataContent
              {
                  {byteArraycontent, "\"FormFile\"", "\"img.JPG\""}
              };

            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HandleAuthorizationHeader(token);
            var addImageHttpResponse = await _client.PostAsync("api/admin/uploadbundleimage", multiplePartFormDataContent);
            var url = await CreateResponse<ResponseModel<string>>(addImageHttpResponse);
            BundleRequest brequest = new BundleRequest()
            {
                CreatedDate = DateTime.Today,
                URL = url.Data,
                Description = "test 11",
                Name = "test 11",
                NumberOfItems = 3,
                Size = 100,
                BundelCategories = new List<ModelCatSubCatDto>()
                {
                    new ModelCatSubCatDto()
                    {
                        Category = new ModelCategoryDto(){ Id=1},
                        SubCategory = new ModelSubCategoryDto(){ Id=1}
                    },
                    new ModelCatSubCatDto()
                    {
                        Category = new ModelCategoryDto(){ Id=1},
                        SubCategory = new ModelSubCategoryDto(){ Id=2}
                    },
                     new ModelCatSubCatDto()
                    {
                        Category = new ModelCategoryDto(){ Id=2},
                        SubCategory = new ModelSubCategoryDto(){ Id=1}
                    },
                }
            };
            var addBundelHttpResponse = await _client.PostAsync("api/admin/addbundle", GetAPIData(brequest) );
            // var status = TestServerWrapper._adminService.AddBundle(brequest);
            
            var bundles = TestServerWrapper._adminService.GetBundles();
           // TestServerWrapper.Context.Entry(bundles).Reload();
            var a = TestServerWrapper.Context.Models.ToList();
            request.Bundle  = new DropdownDto { Id = bundles.Data.List[1].Id };
            request.Description = "ffff";
            request.PriceType = new PriceTypeDto { Id = (int)PriceType.Coins,};
            request.Price = 100;
            TestServerWrapper._adminService.AddModel(request);

            request.Bundle = new DropdownDto { Id = bundles.Data.List[0].Id };
            request.Description = "ffff";
            request.PriceType = new PriceTypeDto { Id = (int)PriceType.Coins, }; ;
            request.Price = 80;
            TestServerWrapper._adminService.AddModel(request);


            TestServerWrapper._adminService.SuspendModel(new SuspendModelRequest
            {
                Id = 1
            });

            TestServerWrapper._adminService.UpdateModel(new UpdateModelRequest
            {
                Id = 2,
                Price = 1000
            });


        }

        //GetAllModels
        private async Task<HttpResponseMessage> GetAllModels(string token = null)
        {
            HandleAuthorizationHeader(token);
            var result2 = await _client.GetAsync("api/map/modelsinfo");
            return result2;
        }

        //AddModelTouser
        private async Task<HttpResponseMessage> AddModelToUser(List<UserRecieveNewModelItem> request, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result2 = await _client.PostAsync("api/map/models/userrecievenewmodel", GetAPIData(request));
            return result2;
        }

        //GetModelsPerUser
        private async Task<HttpResponseMessage> GetModelsPerUser(List<MPModel> mpModelsPerUser, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result2 = await _client.GetAsync("api/map/models/userownedmodels");
            return result2;
        }

        //GetMP
        private async Task<HttpResponseMessage> GetMixedplaces(List<MPDto> mpCoordinats, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/map/getmixedplaceinfo", GetAPIData(mpCoordinats));
            return result;
        }

        //SetMPData
        private async Task<HttpResponseMessage> SetMPData(SetMPItemsRequest mpdata, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/map/mixedplace/setmpitems", GetAPIData(mpdata));
            return result;
        }

        //GetMpDesigns
        private async Task<HttpResponseMessage> GetMpDesigns(GetMPItemRequest mpDesignsRequest, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/map/mixedplace/getmpitmes", GetAPIData(mpDesignsRequest));
            return result;
        }






    }
}
