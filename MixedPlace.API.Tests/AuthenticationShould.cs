﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using MixedPlace.API.Models;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Transactions;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.User.ResponseModels;
using Newtonsoft.Json;
using Xunit;

namespace MixedPlace.API.Tests
{
    public class AuthenticationShould : BaseShould
    {
      
       
        private RegisterRequest _regUser;
        private LoginRequest _registeredUser;
        private LoginRequest _notregisteredUser;


        public AuthenticationShould()
        {
            _notregisteredUser = new LoginRequest()
            {
                Username = "ancjdncdcd",
                Password = "1Qa!12"
            };
        }
 
      

        private async System.Threading.Tasks.Task<bool> LoginWithTempPassword(bool succeeded, string guid)
        {
            LoginRequest logreq = new LoginRequest()
            {
                Username = guid + "@msbitsoftware.com",
                Password = "1!QqAs"
            };



            if (succeeded)
            {
                try
                {
                    var result = await _client.PostAsync("api/authentication/login", GetAPIData(logreq));
                    succeeded = result.StatusCode == System.Net.HttpStatusCode.OK ? true : false;
                    if (succeeded)
                    {
                        var response = await CreateResponse(result);

                        if (response.InnerCode != (int)InnerErrorCode.ChangePassowrdRequired)
                        {
                            succeeded = false;
                        }

                    }

                }
                catch (Exception ex)
                {
                    succeeded = false;
                }
            }

            return succeeded;
        }

        private async System.Threading.Tasks.Task<bool> Login(bool succeeded, string guid)
        {
            LoginRequest logreq = new LoginRequest()
            {
                Username = guid + "@msbitsoftware.com",
                Password = "1qazxsw2#"
            };
      
            if (succeeded)
            {
                try
                {
                    var result = await _client.PostAsync("api/authentication/login", GetAPIData(logreq));
                    succeeded = result.StatusCode == System.Net.HttpStatusCode.OK ? true : false;
                    if (succeeded)
                    {
                        var response = await CreateResponse(result);

                        if (String.IsNullOrEmpty(response.Data.Token))
                        {
                            succeeded = false;
                        }
                        else
                        {

                        }
                       
                    }

                }
                catch (Exception ex)
                {
                    succeeded = false;
                }
            }

            return succeeded;
        }

      


        private async System.Threading.Tasks.Task<bool> RefreshToken(bool succeeded, RefeshTokenRequest rtr)
        {
            if (succeeded)
            {
                var result = await _client.PostAsync("api/authentication/refreshtoken", GetAPIData(rtr));
                succeeded = result.StatusCode == System.Net.HttpStatusCode.OK ? true : false;
                if (succeeded)
                {
                    var response = await CreateResponse(result);

                    if (String.IsNullOrEmpty(response.Data.Token) || response.Data.Token == rtr.Token)
                    {
                        succeeded = false;
                    }

                }
            }

            return succeeded;
        }
    

        private async System.Threading.Tasks.Task<bool> ForgotPassword(bool succeeded, string guid)
        {
            ForgotPasswordRequest cpreq = new ForgotPasswordRequest()
            {
                Email = guid + "@msbitsoftware.com"
            };
            // Forgot password
            if (succeeded)
            {
                try
                {
                    var result = await _client.PostAsync("api/authentication/forgotpassword", GetAPIData(cpreq));
                    succeeded = result.StatusCode == System.Net.HttpStatusCode.OK ? true : false;
                }
                catch (Exception ex)
                {
                    succeeded = false;
                }
            }

            return succeeded;
        }
        


        [Fact]
        public async void RegistrationAndLoginWithTempPasswordShouldReturnChangePassowrdRequired()
        {
            bool succeeded = true;

            var guid = Guid.NewGuid().ToString();

            succeeded = await Register(succeeded, guid);

            succeeded = await LoginWithTempPassword(succeeded, guid);

            Assert.True(succeeded);
        }


        [Fact]
        public async void RegistrationChangePasswordAndLoginShouldPass()
        {
            bool succeeded = true;

            var guid = Guid.NewGuid().ToString();

            succeeded = await Register(succeeded, guid);

            succeeded = await ChangePassword(succeeded, guid);

            succeeded = await Login(succeeded, guid);

            Assert.True(succeeded);


        }


        [Fact]
        public async void RegistrationForgotPasswordAndLoginWithTempPasswordShouldReturnChangePassowrdRequired()
        {
            bool succeeded = true;
            var guid = Guid.NewGuid().ToString();
            
            succeeded = await Register(succeeded, guid);

            succeeded = await ForgotPassword(succeeded, guid);

            succeeded = await LoginWithTempPassword(succeeded, guid);

            Assert.True(succeeded);
        }

   

        [Fact]
        public async void RegistrationForgotPasswordChangePasswordAndLoginShouldPass()
        {
            bool succeeded = true;
            var guid = Guid.NewGuid().ToString();
            

            succeeded = await Register(succeeded, guid);

            succeeded = await ForgotPassword(succeeded, guid);

            succeeded = await ChangePassword(succeeded, guid);

            succeeded = await Login(succeeded, guid);

            Assert.True(succeeded);
        }


        [Fact]
        public async void RegisterLoginAndRefreshToken()
        {
            bool succeeded = true;

            var guid = Guid.NewGuid().ToString();

            succeeded = await Register(succeeded, guid);

            succeeded = await ChangePassword(succeeded, guid);
            
            var rtr = await LoginAndReturnToken(succeeded, guid);

            succeeded = await RefreshToken(succeeded, rtr);

            var user = TestServerWrapper.Context.Users.Where(x => x.UserName == guid + "@msbitsoftware.com").ToList();
            var package = TestServerWrapper.Context.PurchasedPackages.Where(x => x.User.UserName== guid + "@msbitsoftware.com").ToList();
            var modelperuser = TestServerWrapper.Context.MPModelsPerUsers.Where(z => z.User.UserName == guid + "@msbitsoftware.com").ToList();
            var trans = TestServerWrapper._tranService.GetTransactions().Where(y => y.OwnerId == user[0].UserName).ToList();
            succeeded = trans != null && package != null && modelperuser != null? true : false;

        Assert.True(succeeded);

        }

    }
}
