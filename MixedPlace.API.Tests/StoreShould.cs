﻿using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Package;
using MixedPlace.API.Models.Package.RequestModel;
using MixedPlace.API.Models.Package.RequestModels;
using MixedPlace.API.Models.Package.ResponseModels;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.User.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MixedPlace.API.Tests
{
    public class StoreShould : BaseShould
    {

        #region  Public Methods

        #region Ok Tests

        [Fact]
        public async void GetClientStoreOkTest()
        {
            //Registration and Login
            string userPrifix = Guid.NewGuid().ToString();
            Assert.True(await Register(true, userPrifix));
            Assert.True(await ChangePassword(true, userPrifix));
            var authResponse = await LoginAndReturnToken(true, userPrifix);
            Assert.True(authResponse != null ? true : false);

            //Admin Login
            var adminAuthResponse = await AdminLogin();
            Assert.True(adminAuthResponse != null ? true : false);

            var userId = RetrieveUserIdFromJwtToken(authResponse.Token);

            //Get All Users
            var guHttpResponse = await AdminGetUsers(token: adminAuthResponse.Token);
            Assert.True(guHttpResponse.IsSuccessStatusCode);
            var guResponse = await CreateResponse<ResponseModel<GetUsersResponse>>(guHttpResponse);
            var user = guResponse?.Data?.Users?.FirstOrDefault(x => x.UserId == userId);
            Assert.True(user != null);

            //Update User Balance
            var auuHttpResponse = await AdminUpdateUser(new UpdateUserDetailsRequest
            {
                Age = user.Age,
                UserId = user.UserId,
                ChallengeLevel = user.ChallengeLevel,
                City = user.City,
                FirstName = user.FirstName,
                CoinBalance = 1000,
                Gender = user.Gender,
                ImageUrl = user.ImageUrl,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                UserStatus = user.UserStatus
            });
            Assert.True(auuHttpResponse.IsSuccessStatusCode);

            //Get Client Store
            var gcsHttpMessage = await GetClientStore(authResponse.Token);
            Assert.True(gcsHttpMessage.IsSuccessStatusCode);
            var gcsResponse = await CreateResponse<ResponseModel<GetClientStorePackagesResponse>>(gcsHttpMessage);
            Assert.True(gcsResponse?.Data?.Packages?.Count() == 1);

        }

        [Fact]
        public async void AddPackageToShopCartOkTest()
        {
            //Registration and Login
            string userPrifix = Guid.NewGuid().ToString();
            Assert.True(await Register(true, userPrifix));
            Assert.True(await ChangePassword(true, userPrifix));
            var authResponse = await LoginAndReturnToken(true, userPrifix);
            Assert.True(authResponse != null ? true : false);

            //Admin Login
            var adminAuthResponse = await AdminLogin();
            Assert.True(adminAuthResponse != null ? true : false);

            var userId = RetrieveUserIdFromJwtToken(authResponse.Token);

            //Get All Users
            var guHttpResponse = await AdminGetUsers(token: adminAuthResponse.Token);
            Assert.True(guHttpResponse.IsSuccessStatusCode);
            var guResponse = await CreateResponse<ResponseModel<GetUsersResponse>>(guHttpResponse);
            var user = guResponse?.Data?.Users?.FirstOrDefault(x => x.UserId == userId);
            Assert.True(user != null);

            //Update User Balance
            var auuHttpResponse = await AdminUpdateUser(new UpdateUserDetailsRequest
            {
                Age = user.Age,
                UserId = user.UserId,
                ChallengeLevel = user.ChallengeLevel,
                City = user.City,
                FirstName = user.FirstName,
                CoinBalance = 1000,
                Gender = user.Gender,
                ImageUrl = user.ImageUrl,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                UserStatus = user.UserStatus
            });
            Assert.True(auuHttpResponse.IsSuccessStatusCode);

            //Get Client Store
            var gcsHttpMessage = await GetClientStore(authResponse.Token);
            Assert.True(gcsHttpMessage.IsSuccessStatusCode);
            var gcsResponse = await CreateResponse<ResponseModel<GetClientStorePackagesResponse>>(gcsHttpMessage);
            Assert.True(gcsResponse?.Data?.Packages?.Count() == 1);

            //Add Package To Store
            var package = gcsResponse.Data.Packages.First();
            var aptscHttpResponse = await AddPackageToShopCart(new AddPackageToUserShoppingCartRequest { PackageId = package.Id });
            Assert.True(aptscHttpResponse.IsSuccessStatusCode);
            var aptscResponse = await CreateResponse<ResponseModel<GetPackageResponse>>(aptscHttpResponse);
            Assert.True(aptscResponse.InnerCode == (int)InnerErrorCode.Ok);
        }

        [Fact]
        public async void GetSubmittedPackagesFromShopingcart()
        {
            //Registration and Login
            string userPrifix = Guid.NewGuid().ToString();
            Assert.True(await Register(true, userPrifix));
            Assert.True(await ChangePassword(true, userPrifix));
            var authResponse = await LoginAndReturnToken(true, userPrifix);
            Assert.True(authResponse != null ? true : false);

            //Get Client Store
            var gcsHttpMessage = await GetClientStore(authResponse.Token);
            Assert.True(gcsHttpMessage.IsSuccessStatusCode);
            var gcsResponse = await CreateResponse<ResponseModel<GetClientStorePackagesResponse>>(gcsHttpMessage);
            Assert.True(gcsResponse?.Data?.Packages?.Count() == 1);

            //Add Package To ShopCard
            var package = gcsResponse.Data.Packages.First();
            var aptscHttpResponse = await AddPackageToShopCart(new AddPackageToUserShoppingCartRequest { PackageId = package.Id });
            Assert.True(aptscHttpResponse.IsSuccessStatusCode);
            var aptscResponse = await CreateResponse<ResponseModel<GetPackageResponse>>(aptscHttpResponse);
            Assert.True(aptscResponse.InnerCode == (int)InnerErrorCode.Ok);

            //Get User's submitted packages
            var packages = await GetUserSubmittedPackages(authResponse.Token);
            var pckgResponse = await CreateResponse<ResponseModel<GetPackagesResponse>>(packages);
            Assert.True(pckgResponse?.Data?.Packages?.Count()>0);
        }


        [Fact]
        public async void BuyPackageOkTest()
        {
            //Registration and Login
            string userPrifix = Guid.NewGuid().ToString();
            Assert.True(await Register(true, userPrifix));
            Assert.True(await ChangePassword(true, userPrifix));
            var authResponse = await LoginAndReturnToken(true, userPrifix);
            Assert.True(authResponse != null ? true : false);

            //Admin Login
            var adminAuthResponse = await AdminLogin();
            Assert.True(adminAuthResponse != null ? true : false);

            var userId = RetrieveUserIdFromJwtToken(authResponse.Token);

            //Get All Users
            var guHttpResponse = await AdminGetUsers(token: adminAuthResponse.Token);
            Assert.True(guHttpResponse.IsSuccessStatusCode);
            var guResponse = await CreateResponse<ResponseModel<GetUsersResponse>>(guHttpResponse);
            var user = guResponse?.Data?.Users?.FirstOrDefault(x => x.UserId == userId);
            Assert.True(user != null);

            //Update User Balance
            var auuHttpResponse = await AdminUpdateUser(new UpdateUserDetailsRequest
            {
                Age = user.Age,
                UserId = user.UserId,
                ChallengeLevel = user.ChallengeLevel,
                City = user.City,
                FirstName = user.FirstName,
                CoinBalance = 1000,
                Gender = user.Gender,
                ImageUrl = user.ImageUrl,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                UserStatus = user.UserStatus
            });
            Assert.True(auuHttpResponse.IsSuccessStatusCode);

            //Get Client Store
            var gcsHttpMessage = await GetClientStore(authResponse.Token);
            Assert.True(gcsHttpMessage.IsSuccessStatusCode);
            var gcsResponse = await CreateResponse<ResponseModel<GetClientStorePackagesResponse>>(gcsHttpMessage);
            Assert.True(gcsResponse?.Data?.Packages?.Count() == 1);

            //Add Package To Store
            var package = gcsResponse.Data.Packages.First();
            var aptscHttpResponse = await AddPackageToShopCart(new AddPackageToUserShoppingCartRequest { PackageId = package.Id});
            Assert.True(aptscHttpResponse.IsSuccessStatusCode);
            var aptscResponse = await CreateResponse<ResponseModel<GetPackageResponse>>(aptscHttpResponse);
            Assert.True(aptscResponse.InnerCode == (int)InnerErrorCode.Ok);

            var bpHttpResponse = await BuyPackage(new BuyPackageRequest {});
            Assert.True(bpHttpResponse.IsSuccessStatusCode);
            var bpResponse = await CreateResponse<ResponseModel<EmptyBodyResponse>>(bpHttpResponse);
            Assert.True(bpResponse.InnerCode == (int)InnerErrorCode.Ok);

        }

        #endregion Ok Tests

        #region Errors Tests

        [Fact]
        public async void AddPackageToShoppingCartIncufficientBalanceTest()
        {
            //Registration and Login
            string userPrifix = Guid.NewGuid().ToString();
            Assert.True(await Register(true, userPrifix));
            Assert.True(await ChangePassword(true, userPrifix));
            var authResponse = await LoginAndReturnToken(true, userPrifix);
            Assert.True(authResponse != null ? true : false);

            //Admin Login
            var adminAuthResponse = await AdminLogin();
            Assert.True(adminAuthResponse != null ? true : false);

            var userId = RetrieveUserIdFromJwtToken(authResponse.Token);

            //Get All Users
            var guHttpResponse = await AdminGetUsers(token: adminAuthResponse.Token);
            Assert.True(guHttpResponse.IsSuccessStatusCode);
            var guResponse = await CreateResponse<ResponseModel<GetUsersResponse>>(guHttpResponse);
            var user = guResponse?.Data?.Users?.FirstOrDefault(x => x.UserId == userId);
            Assert.True(user != null);

            //Update User Balance
            var auuHttpResponse = await AdminUpdateUser(new UpdateUserDetailsRequest
            {
                Age = user.Age,
                UserId = user.UserId,
                ChallengeLevel = user.ChallengeLevel,
                City = user.City,
                FirstName = user.FirstName,
                CoinBalance = 0,
                Gender = user.Gender,
                ImageUrl = user.ImageUrl,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                UserStatus = user.UserStatus
            });
            Assert.True(auuHttpResponse.IsSuccessStatusCode);

            //Get Client Store
            var gcsHttpMessage = await GetClientStore(authResponse.Token);
            Assert.True(gcsHttpMessage.IsSuccessStatusCode);
            var gcsResponse = await CreateResponse<ResponseModel<GetClientStorePackagesResponse>>(gcsHttpMessage);
            Assert.True(gcsResponse?.Data?.Packages?.Count() == 1);

            //Add Package To Store
            var package = gcsResponse.Data.Packages.First(x=> x.PriceType == PriceType.Coins);
            var aptscHttpResponse = await AddPackageToShopCart(new AddPackageToUserShoppingCartRequest { PackageId = package.Id });
            Assert.True(aptscHttpResponse.IsSuccessStatusCode);
            var aptscResponse = await CreateResponse<ResponseModel<EmptyBodyResponse>>(aptscHttpResponse);
            Assert.True(aptscResponse.InnerCode == (int)InnerErrorCode.InsufficientBalance);
        }

        public async void RemovePackageFromUserShoppingCart()
        {
            //Registration and Login
            string userPrifix = Guid.NewGuid().ToString();
            Assert.True(await Register(true, userPrifix));
            Assert.True(await ChangePassword(true, userPrifix));
            var authResponse = await LoginAndReturnToken(true, userPrifix);
            Assert.True(authResponse != null ? true : false);

        }

       [Fact]
        public async void BuyPackageIncufficientBalanceTest()
        {
            //Registration and Login
            string userPrifix = Guid.NewGuid().ToString();
            Assert.True(await Register(true, userPrifix));
            Assert.True(await ChangePassword(true, userPrifix));
            var authResponse = await LoginAndReturnToken(true, userPrifix);
            Assert.True(authResponse != null ? true : false);

            //Admin Login
            var adminAuthResponse = await AdminLogin();
            Assert.True(adminAuthResponse != null ? true : false);

            var userId = RetrieveUserIdFromJwtToken(authResponse.Token);

            //Get All Users
            var guHttpResponse = await AdminGetUsers(token: adminAuthResponse.Token);
            Assert.True(guHttpResponse.IsSuccessStatusCode);
            var guResponse = await CreateResponse<ResponseModel<GetUsersResponse>>(guHttpResponse);
            var user = guResponse?.Data?.Users?.FirstOrDefault(x => x.UserId == userId);
            Assert.True(user != null);

            //Update User Balance
            var auuHttpResponse = await AdminUpdateUser(new UpdateUserDetailsRequest
            {
                Age = user.Age,
                UserId = user.UserId,
                ChallengeLevel = user.ChallengeLevel,
                City = user.City,
                FirstName = user.FirstName,
                CoinBalance = 1000,
                Gender = user.Gender,
                ImageUrl = user.ImageUrl,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                UserStatus = user.UserStatus
            });
            Assert.True(auuHttpResponse.IsSuccessStatusCode);

            //Get Client Store
            var gcsHttpMessage = await GetClientStore(authResponse.Token);
            Assert.True(gcsHttpMessage.IsSuccessStatusCode);
            var gcsResponse = await CreateResponse<ResponseModel<GetClientStorePackagesResponse>>(gcsHttpMessage);
            Assert.True(gcsResponse?.Data?.Packages?.Count() == 1);

            //Add Package To Store
            var package = gcsResponse.Data.Packages.First(x=> x.PriceType == PriceType.Coins);
            var aptscHttpResponse = await AddPackageToShopCart(new AddPackageToUserShoppingCartRequest { PackageId = package.Id });
            Assert.True(aptscHttpResponse.IsSuccessStatusCode);
            var aptscResponse = await CreateResponse<ResponseModel<EmptyBodyResponse>>(aptscHttpResponse);
            Assert.True(aptscResponse.InnerCode == (int)InnerErrorCode.Ok);

            auuHttpResponse = await AdminUpdateUser(new UpdateUserDetailsRequest
            {
                Age = user.Age,
                UserId = user.UserId,
                ChallengeLevel = user.ChallengeLevel,
                City = user.City,
                FirstName = user.FirstName,
                CoinBalance = 0,
                Gender = user.Gender,
                ImageUrl = user.ImageUrl,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                UserStatus = user.UserStatus
            },adminAuthResponse.Token);

            Assert.True(auuHttpResponse.IsSuccessStatusCode);

            //Buy Package
            var bpHttpResponse = await BuyPackage(new BuyPackageRequest {  },authResponse.Token);
            Assert.True(bpHttpResponse.IsSuccessStatusCode);
            var bpResponse = await CreateResponse<ResponseModel<EmptyBodyResponse>>(bpHttpResponse);
            Assert.True(bpResponse.InnerCode == (int)InnerErrorCode.InsufficientBalance);
        }

        #endregion Errors Tests

        #endregion Public Methods

        #region Private Methods

        private async Task<HttpResponseMessage> GetClientStore(string token = null)
        {
            GetPackagesRequestModel model = new GetPackagesRequestModel()
            {
                PackageType = PackageCategoryType.Coin,
                RetrieveAllPackages = true,
                NumberPerPage = 10,
                PageId = 1
                
            };
            HandleAuthorizationHeader(token);
            return await _client.PostAsync("api/store/getclientstorepackages",GetAPIData(model));
        }

        private async Task<HttpResponseMessage> AddPackageToShopCart(AddPackageToUserShoppingCartRequest request, string token = null)
        {
            HandleAuthorizationHeader(token);
            return await _client.PostAsync("api/store/addpackagetousershopingcart", GetAPIData(request));
        }

        private async Task<HttpResponseMessage> GetUserSubmittedPackages(string token = null)
        {
            HandleAuthorizationHeader(token);
            return await _client.GetAsync("api/store/getpackagefromshopingcart");
        }

        private async Task<HttpResponseMessage> BuyPackage(BuyPackageRequest request, string token = null)
        {
            HandleAuthorizationHeader(token);
            return await _client.PostAsync("api/store/buypackage", GetAPIData(request));
        }

        #endregion Private Methods
    }
}
