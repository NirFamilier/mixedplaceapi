﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.User.ResponseModels;
using Newtonsoft.Json;
using Xunit;

namespace MixedPlace.API.Tests
{
    public class UserShoud : BaseShould
    {

        public UserShoud()
        {
        }

        protected static async System.Threading.Tasks.Task<ResponseModel<UserBaseDto>> CreateUserResponse(HttpResponseMessage result)
        {
            var resultContent = await result.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<ResponseModel<UserBaseDto>>(resultContent);
            return response;
        }

        protected static async System.Threading.Tasks.Task<ResponseModel<GetUsersTransactionsResponse>> CreateUserTransactionsResponse(HttpResponseMessage result)
        {
            var resultContent = await result.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<ResponseModel<GetUsersTransactionsResponse>>(resultContent);
            return response;
        }
        [Fact]
        public async void UserShoudGetDetails()
        {
            bool succeeded = true;

            ResponseModel<UserBaseDto> response = null;

            var guid = Guid.NewGuid().ToString();

            succeeded = await Register(succeeded, guid);

            succeeded = await ChangePassword(succeeded, guid);

            var result = await LoginAndReturnToken(succeeded, guid);

            if (succeeded)
            {
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.GetAsync("api/user/getuserdetails");
                 response = await CreateUserResponse(result2);

            }

            succeeded = response.Data.Age == 18 ? true : false;

            Assert.True(succeeded);
        }

        [Fact]
        public async void UserShoudGetListOfTransactions()
        {
            bool succeeded = true;

            ResponseModel<GetUsersTransactionsResponse> response = null;

            var guid = Guid.NewGuid().ToString();

            succeeded = await Register(succeeded, guid);

            succeeded = await ChangePassword(succeeded, guid);

            var result = await LoginAndReturnToken(succeeded, guid);

            if (succeeded)
            {
                var jwtToken = new JwtSecurityToken(result.Token);

                TestServerWrapper._tranService.AddTransaction(TransactionCategoryEnum.ChallengeReward, 100, 0, "", jwtToken.Subject);
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.GetAsync("api/user/gettransactions");
                response = await CreateUserTransactionsResponse(result2);

            }

            succeeded = ((List<TransactionDto>)response.Data.Transactions).Count > 0 ;

            Assert.True(succeeded);
        }
    }
}
