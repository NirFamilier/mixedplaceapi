﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using MixedPlace.API.Data;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.MPChallenge;
using MixedPlace.API.Models.MPDesign;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.Package;
using MixedPlace.API.Models.Transactions;
using MixedPlace.API.Models.User;
using MixedPlace.API.Services;

namespace MixedPlace.API.Tests
{
    public class TestServerWrapper
    {
        private static TestServer _server;
        public static ApplicationDbContext Context;
        public static ITransactionService _tranService;

        public static IAdminService _adminService;
        private static Object thisLock = new Object();
        private static PasswordHasher<User> _hasher;

        public static TestServer GetInstance()
        {
         
            lock (thisLock)
            {
                if (_server == null)
                {
                    IConfiguration config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                    _server = new TestServer(new WebHostBuilder().ConfigureAppConfiguration(configBuilder =>
                    {
                        configBuilder.AddJsonFile("appsettings.json");
                    })
                    .UseEnvironment("Testing")
                    .UseStartup<Startup>());

                    Context = _server.Host.Services.GetService(typeof(ApplicationDbContext)) as ApplicationDbContext;

                    _tranService = _server.Host.Services.GetService(typeof(ITransactionService)) as TransactionService;

                    _adminService = _server.Host.Services.GetService(typeof(IAdminService)) as AdminService;
                    TestServerWrapper.Context.ChallengeActionTypes.Add(
                    new ChallengeActionTypeModel()
                    {
                        Id = 1,
                        Name = "Visit"
                    }
                   );
                    TestServerWrapper.Context.ChallengeActionTypes.Add(
                    new ChallengeActionTypeModel()
                    {
                        Id = 2,
                        Name = "Like"
                    }
                   );
                    TestServerWrapper.Context.ChallengeActionTypes.Add(
                       new ChallengeActionTypeModel()
                       {
                           Id = 3,
                           Name = "Comment"
                       }
                      );
                    TestServerWrapper.Context.ChallengeActionTypes.Add(
                      new ChallengeActionTypeModel()
                      {
                          Id = 4,
                          Name = "Share"
                      }
                     );
                    TestServerWrapper.Context.ChallengeTypes.Add(
                       new ChallengeTypeModel()
                       {
                           Name = "Client"
                       }
                      );
                    TestServerWrapper.Context.ChallengeTypes.Add(
                       new ChallengeTypeModel()
                       {
                           Name = "ActiveChallenge"
                       }
                      );
                    TestServerWrapper.Context.ChallengeStatuses.Add(
                       new ChallengeStatusModel()
                       {
                           Name = "Active"
                       }
                      );
                    TestServerWrapper.Context.UserStatuses.Add(
                 new UserStatusModel()
                 {
                     Name = "Active"
                 }
                );
                    TestServerWrapper.Context.UserStatuses.Add(
                       new UserStatusModel()
                       {
                           Name = "Disable"
                       }
                      );
                    TestServerWrapper.Context.Users.AddRange(
                   new User()
                   {
                       Age = 18,
                       UserName = Guid.NewGuid() + "@msbitsoftware.com",
                       PasswordHash = "Aa123456!",
                       City = "Hod HaSharon",
                       StreetNumber = 8,
                       Country = "Israel",
                       DeviceID = "a1314v",
                       Email = "kama@yossi.com",
                       FirstName = "Yossi",
                       Gender = true,
                       Image = "image",
                       LastName = "Kama",
                       Street = "Berry",
                       UserDescription = "singer",
                       CoinBalance = 1000,
                       UserStatusId = (int)UserStatus.Active
                   },
                    new User()
                    {
                        Age = 18,
                        UserName = "v.sergey.st@msbitsoftware.com",
                        PasswordHash = "Aa123456!",
                        City = "Hod HaSharon",
                        StreetNumber = 8,
                        Country = "Israel",
                        DeviceID = "a1314v",
                        Email = "kama@yossi.com",
                        FirstName = "Yossi",
                        Gender = true,
                        Image = "image",
                        LastName = "Kama",
                        Street = "Berry",
                        UserDescription = "singer",
                        CoinBalance = 1000,
                        UserStatusId = (int)UserStatus.Active
                    });
                    TestServerWrapper.Context.MPStatuses.Add(
                  new MPStatusModel()
                  {
                      Name = "Active"
                  }
                 );
                    TestServerWrapper.Context.MPStatuses.Add(
                       new MPStatusModel()
                       {
                           Name = "Disable"
                       }
                      );
                    TestServerWrapper.Context.DesignStatuses.Add(
                       new DesignStatusModel()
                       {
                           Name = "Disable"
                       }
                      );
                    TestServerWrapper.Context.DesignStatuses.Add(
                       new DesignStatusModel()
                       {
                           Name = "Active"
                       }
                      );
                    TestServerWrapper.Context.MixedPlaces.Add(new MP()
                    {
                        X = 100,
                        Y = 200,
                        Z = 300,
                        MPCategories = new List<MPCatSubCat>
                        {
                            new MPCatSubCat()
                            {
                                MPCategoryId = 1,
                                MPSubCategoryId = 1
                            }
                        }
                    });
                    TestServerWrapper.Context.MixedPlaces.Add(new MP()
                    {
                        X = 100,
                        Y = 200,
                        Z = 300,
                        HiddenCounter = 0,
                        MPCategories = new List<MPCatSubCat>
                        {
                            new MPCatSubCat()
                            {
                                MPCategoryId = 1,
                                MPSubCategoryId = 1
                            }
                        }

                    });
                    TestServerWrapper.Context.MixedPlaces.Add(new MP()
                    {
                        X = 101,
                        Y = 201,
                        Z = 301,
                        HiddenCounter = 2,
                        MPCategories = new List<MPCatSubCat>
                        {
                            new MPCatSubCat()
                            {
                                MPCategoryId = 1,
                                MPSubCategoryId = 1
                            }
                        }

                    });
                    TestServerWrapper.Context.Conditions.Add(
                   new ConditionModel()
                   {
                       Id = 1,
                       Name = "Equal"
                   });
                    var rule = new ChallengeRule()
                    {
                        ActionTypeId = 1,
                        Amount = 5,
                        Condition = new ConditionModel()
                        {
                            Id = 1
                        }
                    };

                    TestServerWrapper.Context.Challenges.Add(
                       new Challenge()
                       {
                           ChallengeStatusId = 1,
                           ChallengeTypeId = 1,
                           CreationDate = DateTime.Now,
                           Description = "fdsfkljsd",
                           EndDate = DateTime.Now.AddDays(3),
                           ImageUrl = "image",
                           LogicalOperator = 1,
                           Name = "Challenge 1",
                           Reward = 100,
                           StartDate = DateTime.Now,
                           Rules = new List<ChallengeRule> { rule }
                       }
                      );
                    TestServerWrapper.Context.Prices.Add(new MPPrice()
                    {
                        Price = 100
                    });

                    
                    #region Models Mock Data
                    var mpModel1 = new MPModel
                    {
                        CreationDate = DateTime.Now,
                        Description = "Description",
                        Price = 100,
                        Name = "Cheap Model",
                        PriceTypeId = (int)PriceType.Coins,
                        ModelStatusId = (int)ModelStatus.Active

                    };
                    var mpModel2 = new MPModel
                    {
                        CreationDate = DateTime.Now,
                        Description = "Description",
                        Price = 100,
                        Name = "Cheap Model",
                        PriceTypeId = (int)PriceType.Coins,
                        ModelStatusId = (int)ModelStatus.Active

                    };
                    TestServerWrapper.Context.Models.AddRange(
                        mpModel1,
                        mpModel2);

                    //TestServerWrapper.Context.MPModelCatSubCat.AddRange(
                    //    new MPModelCatSubCat
                    //    {
                    //        Model = mpModel1,
                    //        ModelCategoryId = 1,
                    //        ModelSubCategoryId = 1
                    //    },
                    //    new MPModelCatSubCat
                    //    {
                    //        Model = mpModel2,
                    //        ModelCategoryId = 2,
                    //        ModelSubCategoryId = 1
                    //    }
                     //   );
               

                    #endregion Models Mock Data

                    #region Packages Mock Data

                    TestServerWrapper.Context.PackageStatuses.AddRange(
                      new PackageStatusModel { Id = (int)PackageStatus.Active, Name = PackageStatus.Active.ToString() },
                      new PackageStatusModel { Id = (int)PackageStatus.Disabled, Name = PackageStatus.Disabled.ToString() },
                      new PackageStatusModel { Id = (int)PackageStatus.Suspended, Name = PackageStatus.Suspended.ToString() });

                    TestServerWrapper.Context.PackageItemTypes.AddRange(
                      new PackageItemTypeModel { Id = (int)PackageItemType.Coin, Name = PackageItemType.Coin.ToString() },
                      new PackageItemTypeModel { Id = (int)PackageItemType.Model, Name = PackageItemType.Model.ToString() });

                    TestServerWrapper.Context.PackageCategories.AddRange(
                      new PackageCategoryModel { Id = (int)PackageCategoryType.Default, Name = PackageCategoryType.Default.ToString() },
                      new PackageCategoryModel { Id = (int)PackageCategoryType.Coin, Name = PackageCategoryType.Coin.ToString() },
                      new PackageCategoryModel { Id = (int)PackageCategoryType.Model, Name = PackageCategoryType.Model.ToString() },
                      new PackageCategoryModel { Id = (int)PackageCategoryType.Combo, Name = PackageCategoryType.Combo.ToString() },
                      new PackageCategoryModel { Id = (int)PackageCategoryType.SingleModel, Name = PackageCategoryType.SingleModel.ToString() });

                    TestServerWrapper.Context.PriceTypes.AddRange(
                      new PriceTypeModel { Id = (int)PriceType.Coins, Name = PriceType.Coins.ToString() },
                      new PriceTypeModel { Id = (int)PriceType.Currency, Name = PriceType.Currency.ToString() });

                    TestServerWrapper.Context.PurchaseStatuses.AddRange(
                      new PurchaseStatusModel { Id = (int)PurchaseStatus.Submitted, Name = PurchaseStatus.Submitted.ToString() },
                      new PurchaseStatusModel { Id = (int)PurchaseStatus.PaidUp, Name = PurchaseStatus.PaidUp.ToString() },
                      new PurchaseStatusModel { Id = (int)PurchaseStatus.Decliend, Name = PurchaseStatus.Decliend.ToString() },
                      new PurchaseStatusModel { Id = (int)PurchaseStatus.Canceled, Name = PurchaseStatus.Canceled.ToString() });

                    var model1 = new MPModel()
                    {
                        Bundle = new BundleModel()
                        {
                            CreationDate = DateTime.Today,
                            Description = "Test Bundle",
                            Name = "Test Bundle",
                            NumberOfModels = 3,
                            Size = 100,
                            Url = "TestUrl"
                        },
                        ModelStatusId = (int)ModelStatus.Active,
                        BundleImageName = "TestUrl1",
                        Description = "Test Model",
                        CreationDate = DateTime.Today,
                        Name = "Test Model",
                        Price = 100,
                        PriceTypeId = (int)PriceType.Coins
                    };

                    TestServerWrapper.Context.Packages.AddRange(
                      new PackageModel
                        {

                            AvailableFrom = DateTime.Now.AddDays(-1),
                            ExpiresOn = DateTime.Now.AddDays(10),
                            CreateDate = DateTime.Now,
                            Description = "Available Combo Package",
                            ImageUrl = "http://imgaeurl.com",
                            Name = "Available Combo Package",
                            PackageCategoryId = (int)PackageCategoryType.Combo,
                            PackageStatusId = (int)PackageStatus.Active,
                            PriceTypeId = (int)PriceType.Coins,
                            PurchasedUnits = 0,
                            Price = 100,
                            PackageItems = new List<PackageItemModel>
                            {
                                 new PackageItemModel
                                 {    

                                      NumOfUnits = 1000,
                                      PackageItemTypeId = (int)PackageItemType.Model,
                                      TotalSize = 123123
                                 },
                                 new PackageItemModel
                                 {
                                      NumOfUnits = 1000,
                                      PackageItemTypeId = (int)PackageItemType.Coin,
                                      TotalSize = 123123
                                 },
                            }
                        },
                      new PackageModel
                        {

                            AvailableFrom = DateTime.Now.AddDays(1),
                            ExpiresOn = DateTime.Now.AddDays(10),
                            CreateDate = DateTime.Now,
                            Description = "Still Not Available Combo Package",
                            ImageUrl = "http://imgaeurl.com",
                            Name = "Still Not Available Combo Package",
                            PackageCategoryId = (int)PackageCategoryType.Combo,
                            PackageStatusId = (int)PackageStatus.Active,
                            PriceTypeId = (int)PriceType.Coins,
                            PurchasedUnits = 0,
                            Price = 100,
                        },
                      new PackageModel
                        {
                            AvailableFrom = DateTime.Now.AddDays(-10),
                            ExpiresOn = DateTime.Now.AddDays(-1),
                            CreateDate = DateTime.Now,
                            Description = "Expired Combo Package",
                            ImageUrl = "http://imgaeurl.com",
                            Name = "Expired Combo Package",
                            PackageCategoryId = (int)PackageCategoryType.Combo,
                            PackageStatusId = (int)PackageStatus.Active,
                            PriceTypeId = (int)PriceType.Coins,
                            PurchasedUnits = 0,
                            Price = 100,
                        },
                      new PackageModel
                        {
                            AvailableFrom = DateTime.Now.AddDays(1),
                            ExpiresOn = DateTime.Now.AddDays(10),
                            CreateDate = DateTime.Now,
                            Description = "Disabled Combo Package",
                            ImageUrl = "http://imgaeurl.com",
                            Name = "Disabled Combo Package",
                            PackageCategoryId = (int)PackageCategoryType.Combo,
                            PackageStatusId = (int)PackageStatus.Disabled,
                            PriceTypeId = (int)PriceType.Coins,
                            PurchasedUnits = 0,
                            Price = 100,
                        },
                      new PackageModel
                      {

                          AvailableFrom = DateTime.Now.AddDays(-1),
                          ExpiresOn = DateTime.Now.AddDays(10),
                          CreateDate = DateTime.Now,
                          Description = "Default Combo Package",
                          ImageUrl = "http://imgaeurl.com",
                          Name = "Available Combo Package",
                          PackageCategoryId = (int)PackageCategoryType.Default,
                          PackageStatusId = (int)PackageStatus.Active,
                          PriceTypeId = (int)PriceType.Coins,
                          PurchasedUnits = 0,
                          Price = 100,
                          PackageItems = new List<PackageItemModel>
                            {
                                 new PackageItemModel
                                 {

                                      NumOfUnits = 1000,
                                      PackageItemTypeId = (int)PackageItemType.Model,
                                      TotalSize = 123123,
                                      Model = model1
                                 },
                                 new PackageItemModel
                                 {
                                      NumOfUnits = 1000,
                                      PackageItemTypeId = (int)PackageItemType.Coin,
                                      TotalSize = 123123
                                 },
                            }
                      });
                    Context.MPModelCatSubCat.Add(
                        new MPModelCatSubCat()
                        {
                            Model = model1,
                            ModelCategoryId = 1,
                            ModelSubCategoryId = 2
                        });

                    #endregion Packages Mock Data


                    Context.MPCategories.Add(new MPCategory()
                    {
                        CategoryName = "MPNature"
                    });

                    Context.MPCategories.Add(new MPCategory()
                    {
                        CategoryName = "MPNature2"
                    });

                    Context.MPSubCategories.Add(new MPSubCategory()
                    {
                       CategoryId = 1,
                       Name = "MPSubNature1"
                    });
                    Context.MPSubCategories.Add(new MPSubCategory()
                    {
                        CategoryId = 1,
                        Name = "MPSubNature2"
                    });
                    Context.MPSubCategories.Add(new MPSubCategory()
                    {
                        CategoryId = 2,
                        Name = "MPSubNature11"
                    });


                    Context.ModelCategories.Add(new ModelCategory()
                    {
                        CreationDate = DateTime.Now,
                        Name = "Nature"
                    }
                   );

                    Context.ModelCategories.Add(new ModelCategory()
                    {
                        CreationDate = DateTime.Now,
                        Name = "Soccer"
                    });

                    Context.ModelSubCategories.Add(new ModelSubCategory()
                    {
                        ModelCategoryId = 1,
                        Name = "Trees",
                        Description = "Tress"
                    });
                    Context.ModelSubCategories.Add(new ModelSubCategory()
                    {
                        ModelCategoryId = 1,
                        Name = "Flower",
                        Description = "Tress"
                    });
                    Context.ModelSubCategories.Add(new ModelSubCategory()
                    {
                        ModelCategoryId = 2,
                        Name = "Barcelona",
                        Description = "String"
                    });
                    Context.ModelStatuses.Add(new ModelStatusModel()
                    {
                        Name = "Active"
                    });
                    Context.ModelStatuses.Add(new ModelStatusModel()
                    {
                        Name = "Suspended"
                    });
                    Context.ModelStatuses.Add(new ModelStatusModel()
                    {
                        Name = "Disable"
                    });

                    Context.MPOperationeCoinsPrice.Add(new Models.UserManagement.MPOperationeCoinsPrice()
                    {
                        CATMId = 1,
                        PriceInCoins = 1
                    });

                    Context.MPOperationeCoinsPrice.Add(new Models.UserManagement.MPOperationeCoinsPrice()
                    {
                        CATMId = 2,
                        PriceInCoins = 1.5
                    });

                    TestServerWrapper.Context.SaveChanges();
                }
                return _server;
            }
        }
    }
}
