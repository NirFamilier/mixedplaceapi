﻿using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map.RequestModel;
using MixedPlace.API.Models.Map.ResponseModel;
using MixedPlace.API.Models.MPDesign.ResponseModel;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.UserManagement;
using MixedPlace.API.Models.UserManagement.RequestModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MixedPlace.API.Tests
{
    public class UserManagementShould : BaseShould
    {

        // Builds the data to send to the server
        private StringContent GetAPIData(object request)
        {
            return new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
        }

        private static async System.Threading.Tasks.Task<T> CreateCommentResponse<T>(HttpResponseMessage result)
        {

            var resultContent = await result.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<T>(resultContent);
            return response;
        }

        [Fact]
        public async void SetReport()
        {
            bool succeeded = true;

            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var loginResult = await LoginAndReturnToken(succeeded, guid);

            var random = new Random();
            var mpDto = new MPDto
            {
                X = random.Next(),
                Y = random.Next(),
                Z = random.Next(),
            };
            var mpRMList = new List<MixedPlaceRequestModel>
            {
                 new MixedPlaceRequestModel
                 {
                      X = mpDto.X,
                      Y = mpDto.Y,
                      Z = mpDto.Z,
                 }
            };

            if (succeeded)
            {
               
                //Create New MixedPlace(List with one mixed place)
                var result = await CreateMixedPlaceWithPrice(mpRMList, loginResult.Token);
                Assert.True(result.IsSuccessStatusCode);

                //Buy this New MixedPlace to user that loged in
                result = await BuyMixedPlace(mpDto);
                Assert.True(result.IsSuccessStatusCode);

                ReportRequestModel report = new ReportRequestModel
                {
                    mpCoordinats = mpRMList[0],
                    ReportText = "test set report"
                };
                //SetComment
                result = await SetMpReport(report);
                Assert.True(result.IsSuccessStatusCode);

            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void SetAndGetComment()
        {
            bool succeeded = true;

            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var loginResult = await LoginAndReturnToken(succeeded, guid);

            var random = new Random();
            var mpDto = new MPDto
            {
                X = random.Next(),
                Y = random.Next(),
                Z = random.Next(),
            };
            var mpRMList = new List<MixedPlaceRequestModel>
            {
                 new MixedPlaceRequestModel
                 {
                      X = mpDto.X,
                      Y = mpDto.Y,
                      Z = mpDto.Z,
                 }
            };

            if (succeeded)
            {
              
                //Create New MixedPlace(List with one mixed place)
                var result = await CreateMixedPlaceWithPrice(mpRMList, loginResult.Token);
                Assert.True(result.IsSuccessStatusCode);

                //Buy this New MixedPlace to user that loged in
                result = await BuyMixedPlace(mpDto);
                Assert.True(result.IsSuccessStatusCode);

                CommentRequestModel comment = new CommentRequestModel
                {
                    MPCoordinate = mpRMList[0],
                    Comment = "test set comment"
                };
                //SetComment
                result = await SetMpComment(comment);
                Assert.True(result.IsSuccessStatusCode);

                result = await GetMpComments((int)mpRMList[0].X, (int)mpRMList[0].Y, (int)mpRMList[0].Z);
                var response = await CreateCommentResponse<ResponseModel<CommentResponseModel>>(result);
                succeeded = response.Data.List.Count > 0 && !String.IsNullOrWhiteSpace(response.Data.List[0].Comment)  ? true : false;
                Assert.True(succeeded);
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void SetLike()
        {
            bool succeeded = true;
            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var loginResult = await LoginAndReturnToken(succeeded, guid);

            var random = new Random();
            var mpDto = new MPDto
            {
                X = random.Next(),
                Y = random.Next(),
                Z = random.Next(),
            };
            var mpRMList = new List<MixedPlaceRequestModel>
            {
                 new MixedPlaceRequestModel
                 {
                      X = mpDto.X,
                      Y = mpDto.Y,
                      Z = mpDto.Z,
                 }
            };

            if (succeeded)
            {
              
                //Create New MixedPlace(List with one mixed place)
                var result = await CreateMixedPlaceWithPrice(mpRMList, loginResult.Token);
                Assert.True(result.IsSuccessStatusCode);

                //Buy this New MixedPlace to user that loged in
                result = await BuyMixedPlace(mpDto);
                Assert.True(result.IsSuccessStatusCode);

                MixedPlaceRequestModel like = new MixedPlaceRequestModel
                {
                    X= mpDto.X,
                    Y=mpDto.Y,
                    Z=mpDto.Z
                };
                //SetMPLike
                result = await SetMpLikes(like);
                var jwtToken = new JwtSecurityToken(loginResult.Token);
                var ownerid = jwtToken.Subject;

                var usertrans = TestServerWrapper.Context.Transactions.Where(x => x.OwnerId == ownerid && x.TransactionCategoryId == (int)TransactionCategoryEnum.MPOwnerPriceReceived).LastOrDefault();
                var coins = TestServerWrapper.Context.MPOperationeCoinsPrice.Where(x => x.CATMId == (int)ActionType.Like).FirstOrDefault();
                Assert.True(usertrans !=  null && usertrans.Coins == Convert.ToDecimal(coins.PriceInCoins));
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void SetShare()
        {
            bool succeeded = true;
            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var loginResult = await LoginAndReturnToken(succeeded, guid);

            var random = new Random();
            var mpDto = new MPDto
            {
                X = random.Next(),
                Y = random.Next(),
                Z = random.Next(),
            };
            var mpRMList = new List<MixedPlaceRequestModel>
            {
                 new MixedPlaceRequestModel
                 {
                      X = mpDto.X,
                      Y = mpDto.Y,
                      Z = mpDto.Z
                 }
            };

            if (succeeded)
            {
              
                //Create New MixedPlace(List with one mixed place)
                var result = await CreateMixedPlaceWithPrice(mpRMList, loginResult.Token);
                Assert.True(result.IsSuccessStatusCode);

                //Buy this New MixedPlace to user that loged in
                result = await BuyMixedPlace(mpDto);
                Assert.True(result.IsSuccessStatusCode);

                MixedPlaceShareRequestModel share = new MixedPlaceShareRequestModel
                {
                    MPCoordinates =  new MPCoordinateDto() {
                        X = mpDto.X,
                        Y = mpDto.Y,
                        Z = mpDto.Z
                    } ,
                    Source = "Facebook"
                };
                //SetMPShare
                result = await SetMpShares(share);
                Assert.True(true);
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void SetVisits()
        {
            bool succeeded = true;
            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var loginResult = await LoginAndReturnToken(succeeded, guid);

            var random = new Random();
            var mpDto = new MPDto
            {
                X = random.Next(),
                Y = random.Next(),
                Z = random.Next(),
            };
            var mpRMList = new List<MixedPlaceRequestModel>
            {
                 new MixedPlaceRequestModel
                 {
                      X = mpDto.X,
                      Y = mpDto.Y,
                      Z = mpDto.Z,
                 }
            };

            if (succeeded)
            {
              
                //Create New MixedPlace(List with one mixed place)
                var result = await CreateMixedPlaceWithPrice(mpRMList, loginResult.Token);
                Assert.True(result.IsSuccessStatusCode);

                MixedPlaceRequestModel visit = new MixedPlaceRequestModel
                {
                    X = mpDto.X,
                    Y = mpDto.Y,
                    Z = mpDto.Z
                };

                //SetVisit for MP that not ownered
                result = await SetMpVisits(visit);
                Assert.True(true);

                //Buy this New MixedPlace to user that loged in
                result = await BuyMixedPlace(mpDto);
                Assert.True(result.IsSuccessStatusCode);

                
                //SetVisit for MP that has owner
                result = await SetMpVisits(visit);
                Assert.True(true);
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void SetHiddenAndPublicVisits()
        {
            bool succeeded = true;
            var guid = Guid.NewGuid().ToString();
            succeeded = await Register(succeeded, guid);
            succeeded = await ChangePassword(succeeded, guid);
            var loginResult = await LoginAndReturnToken(succeeded, guid);

            var random = new Random();
            var mpDto = new MPDto
            {
                X = random.Next(),
                Y = random.Next(),
                Z = random.Next(),
            };
            var mpRMList = new List<MixedPlaceRequestModel>
            {
                 new MixedPlaceRequestModel
                 {
                      X = mpDto.X,
                      Y = mpDto.Y,
                      Z = mpDto.Z,
                 }
            };

            if (succeeded)
            {
               
                //Create New MixedPlace(List with one mixed place)
                var result = await CreateMixedPlaceWithPrice(mpRMList, loginResult.Token);
                Assert.True(result.IsSuccessStatusCode);


                SetMPVisitRequest visitsRequest = new SetMPVisitRequest
                {
                    MPVisitsCollection = new List<SetMPVisitRequestItem>()
                    {
                     new SetMPVisitRequestItem
                        {
                             Date = DateTime.Now.AddHours(-3),
                               MPCoordinate = mpDto
                        },
                        new SetMPVisitRequestItem
                        {
                             Date = DateTime.Now.AddHours(-2),
                               MPCoordinate = mpDto
                        },
                        new SetMPVisitRequestItem
                        {
                             Date = DateTime.Now.AddHours(-1),
                               MPCoordinate = mpDto
                        }
                    }
                };

                
                //SetVisit for MP that not ownered
                result = await SetMpVisits(visitsRequest);
                Assert.True(true);

                //Buy this New MixedPlace to user that loged in
                result = await BuyMixedPlace(mpDto);
                Assert.True(result.IsSuccessStatusCode);


                //SetVisit for MP that has owner
                result = await SetMpVisits(visitsRequest);
                Assert.True(true);
            }
            Assert.True(succeeded);
        }


        private async Task<HttpResponseMessage> GetMpComments(int x, int y, int z, string token = null)
        {

            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var reqmodel = new GetCommentsRequestModel()
            {
                Coordinates = new MPCoordinateDto()
                {
                    X = x,
                    Y = y,
                    Z = z
                },
                NumberPerPage = 10,
                PageId = 1
            };
           var result = await _client.PostAsync("api/userManagement/getcomments/",GetAPIData(reqmodel));
            return result;
        }

        private async Task<HttpResponseMessage> SetMpComment(CommentRequestModel comment, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/userManagement/comments", GetAPIData(comment));
            return result;
        }

        private async Task<HttpResponseMessage> SetMpReport(ReportRequestModel comment, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/userManagement/report", GetAPIData(comment));
            return result;
        }

        private async Task<HttpResponseMessage> SetMpLikes(MixedPlaceRequestModel like, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/userManagement/likes", GetAPIData(like));
            return result;
        }

        private async Task<HttpResponseMessage> SetMpShares(MixedPlaceShareRequestModel share, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/userManagement/shares", GetAPIData(share));
            return result;
        }

        private async Task<HttpResponseMessage> SetMpVisits(MixedPlaceRequestModel like, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/userManagement/visits", GetAPIData(like));
            return result;
        }

        private async Task<HttpResponseMessage> SetMpVisits(SetMPVisitRequest request, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/usermanagement/setmpvisits", GetAPIData(request));
            return result;
        }
    }
}
