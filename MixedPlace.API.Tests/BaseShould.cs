﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.User.ResponseModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using MixedPlace.API.Models.Map.RequestModel;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.User;
using Microsoft.AspNetCore.Identity;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using MixedPlace.API.Models.Transactions;

namespace MixedPlace.API.Tests
{
    public class BaseShould
    {
        protected readonly HttpClient _client;
        private readonly IPasswordHasher<User> _hasher;

        public BaseShould()
        {
            _client = TestServerWrapper.GetInstance().CreateClient();
            _hasher = new PasswordHasher<User>();

        }

        protected StringContent GetAPIData(object request)
        {
            return new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json");
        }

        protected static async Task<ResponseModel<AuthResponse>> CreateResponse(HttpResponseMessage result)
        {
            var resultContent = await result.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<ResponseModel<AuthResponse>>(resultContent);
            return response;
        }

        protected async Task<bool> Register(bool succeeded, string guid)
        {
            //var user = new User()
            //{
            //    Age = 18,
            //    UserName = guid + "@msbitsoftware.com",
            //    City = "Hod HaSharon",
            //    StreetNumber = 8,
            //    Country = "Israel",
            //    DeviceID = "a1314v",
            //    Email = "kama@yossi.com",
            //    FirstName = "Yossi",
            //    Gender = true,
            //    Image = "image",
            //    LastName = "Kama",
            //    Street = "Berry",
            //    UserDescription = "singer",
            //    IsTemporaryPassword = false
            //};

            //var passwordHash = _hasher.HashPassword(user, "12qwaszx!@");
            //user.PasswordHash = passwordHash;
            //TestServerWrapper.Context.Users.Add(user);
            //TestServerWrapper.Context.SaveChanges();
            RegisterRequest regreq = new RegisterRequest()
            {
                Age = 18,
                Username = guid + "@msbitsoftware.com",
                City = "Hod HaSharon",
                StreetNumber = 8,
                Country = "Israel",
                DeviceID = "a1314v",
                Email = "kama@yossi.com",
                FirstName = "Yossi",
                Gender = true,
                ImageUrl = "image",
                LastName = "Kama",
                Street = "Berry",
                UserDescription = "singer",
                PhoneNumber = "111111111"
            };

            try
            {
                var result = await _client.PostAsync("api/authentication/register", GetAPIData(regreq));
               
                succeeded = result.StatusCode == System.Net.HttpStatusCode.OK ? true : false;

            }
            catch (Exception exp)
            {
                succeeded = false;
            }

            return succeeded;
        }

        protected async Task<bool> ChangePassword(bool succeeded, string guid)
        {
            ChangePasswordRequest cpreq = new ChangePasswordRequest()
            {
                Email = guid + "@msbitsoftware.com",
                OldPassword = "1!QqAs",
                NewPassword = "1qazxsw2#",
                NewPasswordConfirm = "1qazxsw2#"
            };

            if (succeeded)
            {
                var result = await _client.PostAsync("api/authentication/changepassword", GetAPIData(cpreq));
                succeeded = result.StatusCode == System.Net.HttpStatusCode.OK ? true : false;
            }

            return succeeded;
        }

        protected async Task<RefeshTokenRequest> LoginAndReturnToken(bool succeeded, string guid)
        {
            LoginRequest logreq = new LoginRequest()
            {
                Username = guid + "@msbitsoftware.com",
                Password = "1qazxsw2#"
            };
            RefeshTokenRequest rtr = null;

            if (succeeded)
            {
                try
                {
                    var result = await _client.PostAsync("api/authentication/login", GetAPIData(logreq));
                    succeeded = result.StatusCode == System.Net.HttpStatusCode.OK ? true : false;
                    if (succeeded)
                    {
                        var response = await CreateResponse(result);

                        if (String.IsNullOrEmpty(response.Data.Token))
                        {
                            // succeeded = false;
                        }
                        else
                        {
                            rtr = new RefeshTokenRequest()
                            {
                                RefreshToken = response.Data.RefreshToken,
                                Token = response.Data.Token
                            };

                        }
                    }

                }
                catch (Exception ex)
                {
                    // succeeded = false;
                }
            }

            return rtr;
        }

        protected async Task<AuthResponse> Login(string userName, string password)
        {
            var loginReqeust = new LoginRequest
            {
                Username = userName,
                Password = password
            };


            var result = await _client.PostAsync("api/authentication/login", GetAPIData(loginReqeust));
            var succeeded = result.StatusCode == System.Net.HttpStatusCode.OK ? true : false;
            if (succeeded)
            {
                var response = await CreateResponse(result);
                return response.Data;
            }
            return null;
        }

        protected async Task<AuthResponse> AdminLogin()
        {
            var loginReqeust = new LoginRequest
            {
                Username = "Admin",
                Password = "Admin"
            };


            var result = await _client.PostAsync("api/authentication/login", GetAPIData(loginReqeust));
            var succeeded = result.StatusCode == System.Net.HttpStatusCode.OK ? true : false;
            if (succeeded)
            {
                var response = await CreateResponse(result);
                return response.Data;
            }
            return null;
        }

        //CreateMixedPlaceWithPrice
        public async Task<HttpResponseMessage> CreateMixedPlaceWithPrice(List<MixedPlaceRequestModel> list, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/map/mixedPlace/create", GetAPIData(list));
            return result;
        }

        //BuyMP
        public async Task<HttpResponseMessage> BuyMixedPlace(MPDto mpDto, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/map/mixedPlace/buymp", GetAPIData(mpDto));
            return result;
        }

        //GetMP
        public async Task<HttpResponseMessage> GetMixedplaces(List<MPDto> mpCoordinats, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/map/mixedPlace", GetAPIData(mpCoordinats));
            return result;
        }

        // Test the response from the server
        protected static async Task<T> CreateResponse<T>(HttpResponseMessage result)
        {

            var resultContent = await result.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<T>(resultContent);
            return response;
        }

        #region Admin Calls

        protected async Task<HttpResponseMessage> AdminGetUsers(FilterRequest request = null, string token = null)
        {
            HandleAuthorizationHeader(token);
            return await _client.PostAsync("api/admin/getusers", GetAPIData(request));
        }

        protected async Task<HttpResponseMessage> AdminUpdateUser(UpdateUserDetailsRequest request, string token = null)
        {
            HandleAuthorizationHeader(token);
            return await _client.PostAsync("api/admin/updateuserdetails", GetAPIData(request));
        }

        protected async Task<HttpResponseMessage> AdminUpdateUserStatus(UpdateUserStatusRequest request, string token = null)
        {
            HandleAuthorizationHeader(token);
            return await _client.PostAsync("api/admin/updateuserstatus", GetAPIData(request));
        }

        #endregion Admin Calls

        #region Common Methods
        protected string RetrieveUserIdFromJwtToken(string token)
        {
            var jwtToken = new JwtSecurityToken(token);
            return jwtToken.Subject;
        }

        protected void HandleAuthorizationHeader(string token = null)
        {
            if (token != null)
            {
                if (_client.DefaultRequestHeaders.Any(x => x.Key == "Authorization"))
                    _client.DefaultRequestHeaders.Remove("Authorization");

                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            }
        }

        #endregion Common Methods
    }
}