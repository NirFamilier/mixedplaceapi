﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using MixedPlace.API.Models.User.RequestModels;

namespace MixedPlace.API.Tests
{
    public class TestFlowParameters
    {
        private static TestFlowParameters _server;
        private static LoginRequest _loginDetails;
        private static Object thisLock = new Object();
        public static TestFlowParameters GetInstance()
        {
            lock (thisLock)
            {
                if (_server == null)
                {
                    _server = new TestFlowParameters();
                }
                return _server;
            }
        }

        public static void UpdateUserDetails(LoginRequest lreq)
        {
            _loginDetails = lreq;
        }


        public static LoginRequest GetUserDetails()
        {
            return _loginDetails;
        }
    }
}
