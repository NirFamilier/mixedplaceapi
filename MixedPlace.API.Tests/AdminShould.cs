﻿using MixedPlace.API.Data;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Models.Coins.RequestModel;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.Map;
using MixedPlace.API.Models.Map.RequestModel;
using MixedPlace.API.Models.Map.ResponseModel;
using MixedPlace.API.Models.MPChallenge;
using MixedPlace.API.Models.MPChallenge.ResponseModels;
using MixedPlace.API.Models.MPDesign;
using MixedPlace.API.Models.MPModel;
using MixedPlace.API.Models.MPModel.ResponseModels;
using MixedPlace.API.Models.Transactions;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.User.ResponseModels;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MixedPlace.API.Tests
{
    public class AdminShould : BaseShould
    {
        #region Categories and Sub Categories
        [Fact]
        public async void GetMPSubCategories()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;

            if (succeeded)
            {
                //Add new MPCategory ->Status OK
                var random = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 5);
                var mpCategory = new MPCategory()
                {
                    CategoryName = "animals" + random
                };

                var result1 = await AddMPCategory(mpCategory, result.Token);
                Assert.True(result1.IsSuccessStatusCode);

                //Add same Category ->Status MPCategoryExists
                result1 = await AddMPCategory(mpCategory);
                var response = await CreateResponse<ResponseModel<bool>>(result1);
                var exp = response.InnerCode == (int)InnerErrorCode.MPCategoryExists ? true : false;
                Assert.True(exp);

                //Get MPCategories to get CategoryId
                result1 = await GetMixedPlacePCategories();
                Assert.True(result1.IsSuccessStatusCode);
                var response2 = await CreateResponse<ResponseModel<GetAllMPCategoriesResponse>>(result1);
                var mpSubCategory = new MPSubCategory()
                {
                    Name = "birds"
                };
                foreach (var item in response2.Data.List)
                {
                    if (item.CategoryName == mpCategory.CategoryName)
                    {
                        mpSubCategory.CategoryId = item.Id;
                        return;
                    }
                }
                //Add MPSubCategory that contain MPCategoryId that exists -> Status Ok
                result1 = await AddMPSubCategory(mpSubCategory);
                Assert.True(result1.IsSuccessStatusCode);

                //Add MPSubCategory that contain MPCategoryId that not exists -> Status MixedPlaceCategoryNotExists
                mpSubCategory.CategoryId = 1000;
                result1 = await AddMPSubCategory(mpSubCategory);
                var response3 = await CreateResponse<ResponseModel<bool>>(result1);
                exp = response3.InnerCode == (int)InnerErrorCode.MixedPlaceCategoryNotExists ? true : false;
                Assert.True(exp);

                //Get MPSubCategories
                result1 = await GetMPSubCategory();
                var response4 = await CreateResponse<ResponseModel<GetMPSubCategoryResponse>>(result1);
                exp = response4.Data.list.Contains(mpSubCategory);
                Assert.True(exp);
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void GetModelSubCategories()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;

            if (succeeded)
            {
                //Add new ModelCategory ->Status OK
                var random = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 5);
                var modelCategory = new ModelCategory()
                {
                    Name = "animals" + random
                };

                var result1 = await AddModelCategory(modelCategory, result.Token);
                Assert.True(result1.IsSuccessStatusCode);

                //Add same Category ->Status ModelCategoryExists
                result1 = await AddModelCategory(modelCategory);
                var response = await CreateResponse<ResponseModel<bool>>(result1);
                var exp = response.InnerCode == (int)InnerErrorCode.ModelCategoryExists ? true : false;
                Assert.True(exp);

                //Get ModelCategories to get CategoryId
                var modelCategoriesIds = new List<int>();
                result1 = await GetAllModelCategories();
                Assert.True(result1.IsSuccessStatusCode);
                var response2 = await CreateResponse<ResponseModel<ModelCategoryResponse>>(result1);
                var modelSubCategory = new ModelSubCategory()
                {
                    Name = "birds" + random
                };
                foreach (var item in response2.Data.list)
                {
                    modelCategoriesIds.Add(item.Id);
                    if (item.Name == modelCategory.Name)
                    {
                        modelSubCategory.ModelCategoryId = item.Id;
                        break;
                    }
                }
                //Add ModelSubCategory that contain ModelCategoryId that exists -> Status Ok
                result1 = await AddModelSubCategory(modelSubCategory);
                Assert.True(result1.IsSuccessStatusCode);

                //Get ModelSubCategories
                result1 = await GetAllModelSubCategories(modelCategoriesIds);
                var response4 = await CreateResponse<ResponseModel<ModelSubCategoryResponse>>(result1);
                exp = response4.Data.list.Count > 0;
                Assert.True(exp);

                //Add ModelSubCategory that contain ModelCategoryId that not exists -> Status ModelCategoryNotExists
                modelSubCategory.ModelCategoryId = 1000;
                result1 = await AddModelSubCategory(modelSubCategory);
                var response3 = await CreateResponse<ResponseModel<bool>>(result1);
                exp = response3.InnerCode == (int)InnerErrorCode.ModelCategoryNotExists ? true : false;
                Assert.True(exp);


            }
            Assert.True(succeeded);

        }

        #endregion Categories and Sub Categories

        #region User APIs

        [Fact]
        public async void GetUserMetadata()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            if (succeeded)
            {
                if (result.Token != null)
                    _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.GetAsync("api/admin/getusermetadata");
                var response = await CreateResponse<ResponseModel<GetUserMetadataResponse>>(result2);
                var list = (List<UserStatusDto>)response.Data.UserStatuses;
                succeeded = list.Count == 2 ? true : false;
                Assert.True(succeeded);
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void GetUsers()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            if (succeeded)
            {
               

               var result2 = await GetUsersList(result.Token);
                Assert.True(result2.IsSuccessStatusCode);
                //Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);

                var response = await CreateResponse<ResponseModel<GetUsersResponse>>(result2);
                var list = (List<UserDetailsDto>)response.Data.Users;
                 succeeded = list.Count >0 ? true : false;
                 Assert.True(succeeded);
                
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void GetUsersWithFilters()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            if (succeeded)
            {
                var filter = new FilterRequest
                {
                    AmountPerPage = 200,
                    Filters = new List<FilterItemRequest>()
                    {
                        new FilterItemRequest()
                        {
                            ColumnId = "userName",
                            Condition = ConditionsType.EqualTo,
                            Value = "msbitsoftware"
                        }
                    }
                };
                if (result.Token != null)
                    _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.PostAsync("api/admin/getusers", GetAPIData(filter));
                Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);

                var response = await CreateResponse<ResponseModel<GetUsersResponse>>(result2);
                var list = (List<UserDetailsDto>)response.Data.Users;
                succeeded = list.Count > 0 ? true : false;
                Assert.True(succeeded);

            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void UpdateUserDetails()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            
            var result1 = await GetUsersList(result.Token);
            var response1 = await CreateResponse<ResponseModel<GetUsersResponse>>(result1);
            Assert.True(result1.IsSuccessStatusCode);
            var list = (List<UserDetailsDto>)response1.Data.Users;
            
            if (succeeded)
            {
                var req = new UpdateUserDetailsRequest()
                {
                    UserId = list[0].UserId,
                    Age = 1,
                    ChallengeLevel = 0,
                    CoinBalance = 100,
                    FirstName = "Nir",
                    ImageBase64 = null,
                    ImageMimeType = "8",
                    ImageUrl = "",
                    IsReported = true,
                    LastName= "ggg",
                    PhoneNumber ="11111",
                    ReportsCount = 4,
                    SocialLevel = 1,
                    UserLevel = 1,
                    UserStatus = new UserStatusDto()
                    {
                        Id = 1,
                        Name = ""
                    }
                    
                };
                var result2 = await _client.PostAsync("api/admin/updateuserdetails", GetAPIData(req));
                Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void UpdateUserStatus()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            
            var result1 = await GetUsersList(result.Token);
            var response1 = await CreateResponse<ResponseModel<GetUsersResponse>>(result1);
            Assert.True(result1.IsSuccessStatusCode);
            var list = (List<UserDetailsDto>)response1.Data.Users;

            if (succeeded)
            {
                var req = new UpdateUserStatusRequest()
                {
                    UserId = list[0].UserId,
                    UserStatus = new UserStatusDto()
                    {
                        Id = 1,
                        Name = "dd"
                    }
                };
                var result2 = await _client.PostAsync("api/admin/updateuserstatus", GetAPIData(req));
                Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);
            }
            Assert.True(succeeded);
        }

        #endregion User APIs

        #region Analytics

        [Fact]
        public async void GetUserCoinsBalance()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;

            var result1 = await GetUsersList(result.Token);
            var response1 = await CreateResponse<ResponseModel<GetUsersResponse>>(result1);
            Assert.True(result1.IsSuccessStatusCode);
            var list = (List<UserDetailsDto>)response1.Data.Users;

            if (succeeded)
            {
                TestServerWrapper._tranService.AddTransaction(TransactionCategoryEnum.ChallengeReward, 300, 0, "", list[0].UserId);
                TestServerWrapper._tranService.AddTransaction(TransactionCategoryEnum.ChallengeReward, 100, 0, "", list[0].UserId);
                TestServerWrapper._tranService.AddTransaction(TransactionCategoryEnum.AdminFine, 20, 0, "", list[0].UserId);
                TestServerWrapper._tranService.AddTransaction(TransactionCategoryEnum.BuyMixedPlace, 100, 0, "", list[0].UserId);
                TestServerWrapper._tranService.AddTransaction(TransactionCategoryEnum.BuyPackage, 300, 50, "USD", list[0].UserId);
                TestServerWrapper._tranService.AddTransaction(TransactionCategoryEnum.BuyPackage, 200, 0, "USD", list[0].UserId);

                var spent = TestServerWrapper._tranService.GetMoneySpent(list[0].UserId);
                var result2 = await _client.GetAsync("api/analytics/getusercoinsbalance/" + list[0].UserId);
                Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);
            }
            Assert.True(succeeded);
        }

        #endregion Analytics

        #region Mixed Place APIs
        [Fact]
        public async void GetModelsWithFilters()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            if (succeeded)
            {
               var request = new FilterRequest();
                request.Filters = new List<FilterItemRequest>
                {
                    new FilterItemRequest{
                        ColumnId = "modelStatus",
                        Condition = ConditionsType.EqualTo,
                        Value =ModelStatus.Active
                    }
                };
                request.AmountPerPage = 10;
                request.Page = 1;

                if (result.Token != null)
                    _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.GetAsync("api/admin/getmodelmetadata");
                Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);

                var response = await CreateResponse<ResponseModel<GetModelMetadataResponse>>(result2);
                var list = (List<ModelStatusDto>)response.Data.ModelStatuses;
                succeeded = list.Count > 0 ? true : false;
                Assert.True(succeeded);

                result2 = await _client.PostAsync("api/admin/getmodelwithfiltering", GetAPIData(request));
                Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);
                
            }
        }


        [Fact]
        public async void GetMPMetadata()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            if (succeeded)
            {
              
                if (result.Token != null)
                    _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.GetAsync("api/admin/getmpmetadata");
                var response = await CreateResponse<ResponseModel<GetMPMetadataResponse>>(result2);
                var list = (List<DesignStatusDto>)response.Data.DesignStatuses;
                succeeded = list.Count == 2 ? true : false;
                Assert.True(succeeded);
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void GetMixedPlaces()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            if (succeeded)
            {
              


                   var result2 = await GetMixedPlaceList(result.Token);
                Assert.True(result2.IsSuccessStatusCode);

                var response = await CreateResponse<ResponseModel<GetMixedPlacesResponse>>(result2);
                var list = (List<MPDetailsDto>)response.Data.MixedPlaces;
                succeeded = list.Count > 0 ? true : false;
                Assert.True(succeeded);
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void GetMixedPlacesWithFilters()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            if (succeeded)
            {
              
                var filter = new FilterRequest
                {
                    AmountPerPage = 200,
                    Filters = new List<FilterItemRequest>()
                    {
                        new FilterItemRequest()
                        {
                            ColumnId = "demand",
                            Condition = ConditionsType.GreaterThan,
                            Value = 0
                        }
                    }
                };
                if (result.Token != null)
                    _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.PostAsync("api/admin/getmixedplaces", GetAPIData(filter));
                Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);

                var response = await CreateResponse<ResponseModel<GetMixedPlacesResponse>>(result2);
                var list = (List<MPDetailsDto>)response.Data.MixedPlaces;
                succeeded = list.Count > 0 ? true : false;
                Assert.True(succeeded);

            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void UpdateMixedPlace()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
          
            var result1 = await GetMixedPlaceList(result.Token);
            var response1 = await CreateResponse<ResponseModel<GetMixedPlacesResponse>>(result1);
            Assert.True(result1.IsSuccessStatusCode);
            var list = (List<MPDetailsDto>)response1.Data.MixedPlaces;

            if (succeeded)
            {
               
                var result2 = await GetUsersList();
                Assert.True(result2.IsSuccessStatusCode);
                //Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);

                var response = await CreateResponse<ResponseModel<GetUsersResponse>>(result2);
                var userslist = (List<UserDetailsDto>)response.Data.Users;
                var req = new UpdateMixedPlaceRequest()
                {
                    Id = list[0].Id,
                    Categories = new List<MPCatSubCatDto>
                    {
                        new MPCatSubCatDto()
                        {
                            Category = new MPCategoryDto()
                            {
                                 Id = 1
                            },
                            SubCategory = new MPSubCategoryDto
                            {
                                 Id = 1
                            }
                        }
                    },
                    DesignStatus = new DesignStatusDto()
                    {
                        Id = 1,
                        Name = ""
                    },
                    MPStatus = new MPStatusDto()
                    {
                        Id = 1,
                        Name = ""
                    },
                    Name = "hhhh",
                    OwnerId = userslist[0].UserId,
                    Price = 20
                };
                var result3 = await _client.PostAsync("api/admin/updatemixedplace", GetAPIData(req));
                Assert.True(result3.StatusCode == System.Net.HttpStatusCode.OK);
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void UpdateMixedPlaceStatus()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            
            var result1 = await GetMixedPlaceList(result.Token);
            var response1 = await CreateResponse<ResponseModel<GetMixedPlacesResponse>>(result1);
            Assert.True(result1.IsSuccessStatusCode);
            var list = (List<MPDetailsDto>)response1.Data.MixedPlaces;

            if (succeeded)
            {
                var result2 = await GetUsersList();
                Assert.True(result2.IsSuccessStatusCode);
                //Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);

                var response = await CreateResponse<ResponseModel<GetUsersResponse>>(result2);
                var userslist = (List<UserDetailsDto>)response.Data.Users;
                var req = new UpdateMPStatusRequest()
                {
                    MPId = list[0].Id,
                    MPStatus = new MPStatusDto()
                    {
                        Id = 1,
                        Name = ""
                    }
                };
                var result3 = await _client.PostAsync("api/admin/updatemixedplacestatus", GetAPIData(req));
                Assert.True(result3.StatusCode == System.Net.HttpStatusCode.OK);
            }
            Assert.True(succeeded);
        }

        #endregion Mixed Place APIs

        #region Challenges

        [Fact]
        public async void GetChallengesMetadata()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            if (succeeded)
            {
               
                if (result.Token != null)
                    _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + result.Token);
                var result2 = await _client.GetAsync("api/admin/getchallengemetadata");
                var response = await CreateResponse<ResponseModel<GetChallengeMetadataResponse>>(result2);
                
                var list1 = (List<ChallengeActionTypeDto>)response.Data.ActionTypes;
                var list2 = (List<ChallengeTypeDto>)response.Data.ChallengeTypes;
                var list3 = (List<ChallengeStatusDto>)response.Data.ChallengeStatuses;
                succeeded = list1.Count == TestServerWrapper.Context.ChallengeActionTypes.ToList().Count
                    && list2.Count == TestServerWrapper.Context.ChallengeTypes.ToList().Count
                    && list3.Count == TestServerWrapper.Context.ChallengeStatuses.ToList().Count ? true : false;
            }
            Assert.True(succeeded);
        }

        [Fact]
        public async void GetChallenges()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            if (succeeded)
            {
               
                var result2 = await GetChallengesList(result.Token);
                Assert.True(result2.IsSuccessStatusCode);
                //Assert.True(result2.StatusCode == System.Net.HttpStatusCode.OK);

                var response = await CreateResponse<ResponseModel<GetChallengesResponse>>(result2);
                var list = (List<ChallengeDto>)response.Data.Challenges;
                succeeded = list.Count > 0 ? true : false;
                Assert.True(succeeded);

            }
            Assert.True(succeeded);
        }

        #endregion Challenges

        #region Coins
        [Fact]
        public async void RewardsAndFines()
        {
            bool succeeded = true;
            var result = await AdminLogin();
            succeeded = result != null ? true : false;
            if (succeeded)
            {
                //register user
                var guid = Guid.NewGuid().ToString();
                succeeded = await Register(succeeded, guid);
                Assert.True(succeeded);

                var curuser = TestServerWrapper.Context.Users.Where(x => x.UserName == guid + "@msbitsoftware.com").FirstOrDefault();
                string userId = curuser.Id;

                var rewardRequest = new RewardRequest
                {
                    Name = "Add coins rule",
                    Description = "Add coins rule description",
                    CoinRuleType = new CoinRuleTypeDto
                    {
                        Id = 1,
                        Name = "reward",

                    },
                    Amount = 50,
                    CreatedDate = DateTime.Now,
                    LogicalOperator = new LogicalOperatorDto { Id = 1, Name = "and" },
                    RuleConditions = new List<CoinsRuleConditionDto>
                    {
                        new CoinsRuleConditionDto
                        {
                            FieldName=new CoinRuleFieldNameDto{ Id="city",Name="City"},
                            Operator=new DropdownDto{ Id=2,Name="Contains"},
                            Value="Hod HaSharon"
                        },
                         new CoinsRuleConditionDto
                        {
                            FieldName=new CoinRuleFieldNameDto{ Id="age",Name="Age"},
                            Operator=new DropdownDto{ Id=6,Name="GreaterThan"},
                            Value=18
                        }
                    },
                };

                //AddRewards and check that request status is Ok with coin balance 0
                var result2 = await AddRewardPerBulkOfUsers(rewardRequest, result.Token);
                var response = await CreateResponse<ResponseModel<bool>>(result2);
                var curentUser = TestServerWrapper.Context.Users.Where(x => x.Id == userId).FirstOrDefault();
                TestServerWrapper.Context.Entry(curentUser).Reload();

                //var userafterFine = TestServerWrapper.Context.Users.Where(x => x.Id == userId).FirstOrDefault();
                Assert.True(curentUser.CoinBalance == 0);

                //fine coins and check that request status is BadRequestArgs
                var result3 = await AddRewardPerBulkOfUsers(rewardRequest);
                var response2 = await CreateResponse<ResponseModel<bool>>(result3);
                // Assert.True((int)InnerErrorCode.BadRequestArgs==4001);
                Assert.True(response2.InnerCode == 4001);

                //AddRewards and check that request status is Ok
                rewardRequest.Amount = 100;
                result2 = await AddRewardPerBulkOfUsers(rewardRequest);
                response = await CreateResponse<ResponseModel<bool>>(result2);
                TestServerWrapper.Context.Entry(curuser).Reload();
                var userafterReward = TestServerWrapper.Context.Users.Where(x => x.Id == userId).FirstOrDefault();
                Assert.True(curuser.CoinBalance == 100);
            }
            Assert.True(succeeded);
        }

            #endregion
            //AddMPCategories
            private async Task<HttpResponseMessage> AddMPCategory(MPCategory mpCategory, string token = null)
        {

            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/admin/mixedPlace/addmpcategory", GetAPIData(mpCategory));
            return result;
        }

        //AddModelCategories
        private async Task<HttpResponseMessage> AddModelCategory(ModelCategory modelCategory, string token = null)
        {

            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/admin/model/addmodelcategory", GetAPIData(modelCategory));
            return result;
        }

        //AddMPsubCategory
        private async Task<HttpResponseMessage> AddMPSubCategory(MPSubCategory subCategory, string token = null)
        {

            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/map/mixedPlace/addmpsubcategory", GetAPIData(subCategory));
            return result;
        }

        //AddModelSubCategory
        private async Task<HttpResponseMessage> AddModelSubCategory(ModelSubCategory subCategory, string token = null)
        {

            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/admin/model/addmodelsubcategory", GetAPIData(subCategory));
            return result;
        }

        private async Task<HttpResponseMessage> GetMPSubCategory(string token = null)
        {

            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.GetAsync("api/map/mixedPlace/getmpsubcategories");
            return result;
        }

        //AddModelSubCategory
        private async Task<HttpResponseMessage> GetAllModelSubCategories(List<int> modelCategoriesIds,string token = null)
        {

            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/map/model/getmodelsubcategories",GetAPIData(modelCategoriesIds));
            return result;
        }

        //GetMPCategories
        private async Task<HttpResponseMessage> GetMixedPlacePCategories(string token = null)
        {

            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.GetAsync("api/map/mixedPlace/categories");
            return result;
        }

        //GetModelCategories
        private async Task<HttpResponseMessage> GetAllModelCategories(string token = null)
        {

            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.GetAsync("api/map/model/getmodelcategories");
            return result;
        }

        private async Task<HttpResponseMessage> GetUsersList(string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.GetAsync("api/admin/getusers");
            return result;
        }

        private async Task<HttpResponseMessage> GetMixedPlaceList(string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.GetAsync("api/admin/getmixedplaces");
            return result;
        }

        private async Task<HttpResponseMessage> GetChallengesList(string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.GetAsync("api/admin/getchallenges");
            return result;
        }

        private async Task<HttpResponseMessage> AddRewardPerBulkOfUsers(RewardRequest users, string token = null)
        {
            if (token != null)
                _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var result = await _client.PostAsync("api/admin/coins", GetAPIData(users));
            return result;
        }
    }
}
