﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.MessageSender
{
    public class ServiceConfig
    {
        public string Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string OutgingEmail { get; set; }
        public string ApiKey { get; set; }
        public string SmtpServer { get; set; }
        public string TwilioSid { get; set; }
        public string TwilioAuthToken { get; set; }
        public string PhoneNumber { get; set; }
    }
}
