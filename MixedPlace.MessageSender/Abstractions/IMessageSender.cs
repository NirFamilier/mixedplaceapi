﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.MessageSender.Abstractions
{
    public interface IMessageSender
    {
        bool SendSingleMessageToRecipient(string from , string to, string message, string htmlContent,string subject = "Notification");
        bool SendSingleMessageToMultipleRecipients(string from, List<string> recipients, string message, string subject = "Notification");
    }
}
