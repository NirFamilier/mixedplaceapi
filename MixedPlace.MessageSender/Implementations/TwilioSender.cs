﻿using MixedPlace.MessageSender.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace MixedPlace.MessageSender.Implementations
{
    public class TwilioSender : IMessageSender
    {
        private ServiceConfig _serviceConfig;

        public TwilioSender(ServiceConfig serviceConfig)
        {
            _serviceConfig = serviceConfig;
        }
        public bool SendSingleMessageToMultipleRecipients(string from, List<string> recipients, string message, string subject = "Notification")
        {
            try
            {
                TwilioClient.Init(_serviceConfig.TwilioSid, _serviceConfig.TwilioAuthToken);
                foreach (var to in recipients)
                {
                    var messageResource = MessageResource.Create(new PhoneNumber(to),
                                                                 from: new PhoneNumber(from ?? _serviceConfig.PhoneNumber),
                                                                 body: message);
                    if (messageResource.ErrorCode != null)
                        return false;
                }
                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
        }

        public bool SendSingleMessageToRecipient(string from, string to, string message, string htmlContent, string subject = "Notification")
        {
            try
            {
                TwilioClient.Init(_serviceConfig.TwilioSid, _serviceConfig.TwilioAuthToken);
                var messageResource = MessageResource.Create(
                    new PhoneNumber(to),
                    from: new PhoneNumber(from ?? _serviceConfig.PhoneNumber),
                    body: message);
              
                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
        }
    }
}
