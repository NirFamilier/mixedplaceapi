﻿using MixedPlace.MessageSender.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.MessageSender.Implementations
{
    public class MessageSenderFactory
    {
        public static IMessageSender CreateMessageSender(string providerName)
        {
            switch (providerName)
            {
                case "GmailSender":
                    return new GmailSender(InitialzeConfigurations());
                case "SendGridSender":
                    return new SendGridSender(InitialzeConfigurations());
                case "Twilio":
                    return new TwilioSender(InitialzeConfigurations());
                default:
                    return null;
            }
        }

        private static ServiceConfig InitialzeConfigurations()
        {
            using (StreamReader r = new StreamReader("messagesenderconfig.json"))
            {
                string json = r.ReadToEnd();
                ServiceConfig serviceConfig = JsonConvert.DeserializeObject<ServiceConfig>(json);
                return  serviceConfig;
            }
        }
    }
}
