﻿using MixedPlace.MessageSender.Abstractions;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.MessageSender.Implementations
{
    public class SendGridSender : IMessageSender
    {
        private readonly ServiceConfig _serviceConfig;
        public SendGridSender(ServiceConfig serviceConfig)
        {
            _serviceConfig = serviceConfig;
        }
        public bool SendSingleMessageToRecipient(string from, string to, string message,string htmlContent, string subject = "Notification")
        {
            try
            {
                var msg = new SendGridMessage()
                {
                    From = new EmailAddress(from),
                    Subject = subject
                };

                if (!string.IsNullOrWhiteSpace(message))
                    msg.PlainTextContent = message;
                if (!string.IsNullOrWhiteSpace(htmlContent))
                    msg.HtmlContent = htmlContent;

                msg.AddTo(new EmailAddress(to));

                var apiKey = _serviceConfig.ApiKey;
                var client = new SendGridClient(apiKey);
                var response = client.SendEmailAsync(msg).Result;
                if (response.StatusCode == HttpStatusCode.Accepted)
                    return true;
                else
                    return false;
            }
            catch (Exception exp)
            {
                return false;
            }
        }

        public bool SendSingleMessageToMultipleRecipients(string from, List<string> recipients, string message, string subject = "Notification")
        {
            try
            {
                var fromEmail = new EmailAddress(from);
                var recipientsEmails = recipients.Select(x => new EmailAddress(x)).ToList();
                var messages = MailHelper.CreateSingleEmailToMultipleRecipients(fromEmail, recipientsEmails, subject, message, null);

                var apiKey = _serviceConfig.ApiKey;
                var client = new SendGridClient(apiKey);
                var response = client.SendEmailAsync(messages).Result;
                if (response.StatusCode == HttpStatusCode.Accepted)
                    return true;
                else
                    return false;
            }
            catch (Exception exp)
            {
                return false;
            }
        }
    }
}
