﻿using MailKit.Net.Smtp;
using MimeKit;
using MixedPlace.MessageSender.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.MessageSender.Implementations
{
    public class GmailSender : IMessageSender
    {
        private readonly ServiceConfig _serviceConfig;
        public GmailSender(ServiceConfig serviceConfig)
        {
            _serviceConfig = serviceConfig;
        }
        public bool SendSingleMessageToRecipient(string from, string to, string message,string htmlContent,string subject = "Notification")
        {
            try
            {
                string smtpServer = _serviceConfig.SmtpServer;
                int smtpPortNumber = Convert.ToInt32(_serviceConfig.Port);

                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(from));
                mimeMessage.To.Add(new MailboxAddress(to));
                mimeMessage.Subject = subject;
                mimeMessage.Body = new TextPart("plain") { Text = message };

                using (var client = new SmtpClient())
                {
                    client.Connect(smtpServer, smtpPortNumber, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(_serviceConfig.Username, _serviceConfig.Password);
                    client.Send(mimeMessage);
                    client.Disconnect(true);
                }

                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
        }

        public bool SendSingleMessageToMultipleRecipients(string from, List<string> recipients, string message, string subject = "")
        {
            try
            {
                string smtpServer = _serviceConfig.SmtpServer;
                int smtpPortNumber = Convert.ToInt32(_serviceConfig.Port
                    );

                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(from));

                foreach (string recipient in recipients)
                {
                    if (!string.IsNullOrEmpty(recipient))
                    { mimeMessage.To.Add(new MailboxAddress(recipient)); ; }
                }

                mimeMessage.Importance = MessageImportance.High;
                mimeMessage.Body = new TextPart("plain") { Text = message };

                using (var client = new SmtpClient())
                {
                    client.Connect(smtpServer, smtpPortNumber, false);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(_serviceConfig.Username, _serviceConfig.Password);
                    client.Send(mimeMessage);
                    client.Disconnect(true);
                }
                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
        }
    }
}
