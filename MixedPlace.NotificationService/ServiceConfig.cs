﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.NotificationService
{
    public class ServiceConfig
    {
        public string ServiceEndpoint { get; set; }
        public string ServerKey { get; set; } 
        public string SenderId { get; set; } 
    }
}
