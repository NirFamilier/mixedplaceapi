﻿using MixedPlace.NotificationService.Abstractions;
using MixedPlace.NotificationService.Implementations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.NotificationService.Implementations
{
    public class NotificationServiceFactory
    {
        public static INotificationService CreateNotificationService(string providerName)
        {
            switch (providerName)
            {
                case "GoogleCloudMessaging":
                    return new GoogleCloudMessagingService(InitialzeConfigurations());
                default:
                    return null;
            }
        }

        private static ServiceConfig InitialzeConfigurations()
        {
            using (StreamReader r = new StreamReader("notificationserviceconfig.json"))
            {
                string json = r.ReadToEnd();
                ServiceConfig serviceConfig = JsonConvert.DeserializeObject<ServiceConfig>(json);
                return serviceConfig;
            }
        }
    }
}
