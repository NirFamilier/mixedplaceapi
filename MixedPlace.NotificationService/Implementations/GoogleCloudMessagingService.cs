﻿using MixedPlace.NotificationService.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.NotificationService.Implementations
{
    public class GoogleCloudMessagingService : INotificationService
    {
        private readonly ServiceConfig _serviceConfig;

        public GoogleCloudMessagingService(ServiceConfig serviceConfig)
        {
            _serviceConfig = serviceConfig;
        }

        public bool Notify(string deviceId, string message,string title = "", int deviceOS = 0)
        {
            try
            {
                var result = "-1";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_serviceConfig.ServiceEndpoint);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add($"Authorization:key={_serviceConfig.ServerKey}");
                httpWebRequest.Headers.Add($"Sender: id={_serviceConfig.SenderId}");
                httpWebRequest.Method = "POST";

                var postRequest = new ServicePostRequest
                {
                    To = deviceId,
                    Notification = new ServicePostData
                    {
                        Title = title,
                        Body = message
                     }
                };

               // return true;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(postRequest);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                return true;
                // return result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }

    class ServicePostRequest
    {
        [JsonProperty(PropertyName = "to")]
        public string To { get; set; }

        [JsonProperty(PropertyName = "notification")]
        public ServicePostData Notification { get; set; } = new ServicePostData();
    }

    class ServicePostData
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }
        [JsonProperty(PropertyName = "body")]
        public string Body { get; set; }
    }
}
