﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.NotificationService.Abstractions
{
    public interface INotificationService
    {
        bool Notify(string recipient, string message, string title = "", int deviceOS = 0);
    }
}
